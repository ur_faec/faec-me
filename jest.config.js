module.exports = {
	"modulePaths": [
		"<rootDir>/src/",
	],
	transform: {'^.+\\.ts?$': 'ts-jest'},
	testEnvironment: 'node',
	//testRegex: '/tests/.*\\.(test|spec)?\\.(ts|tsx)$',
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};
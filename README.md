# faec-me

Math visualization code for http://faec.me

To build:

```
git clone https://github.com/faec/faec-me.git
cd faec-me
npm i
npm run build         ("buildrelease" to strip debug metadata)
```

Then open `examples/phase.html` in a browser to see an example of the
running code.

To run in node:

```
node
> const faec = require('./build/faec-bundle.node');
> var z = new faec.Complex(1, 1);
> z.abs();
1.4142135623730951
...
```

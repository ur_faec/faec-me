// ANSI color derived from
// https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
const blue = '\033[1;34m';
const darkblue = '\033[0;34m';
const yellow = '\033[1;33m';
const purple = '\033[1;35m';
const brown = '\033[0;33m';
const gray = '\033[1;30m';
const nc='\033[0m';
console.log(
	`${blue}~~~~~~~~~~~~~~~~🐙~~${nc} ☀️  ` +
	`${yellow}Building${nc} ${purple}faec-me${nc} 🌗 ` +
	`${blue}~~🐙~~~~~~~~~~~~~~~~${nc}`);
console.log(
	`${darkblue}      ▵       ‣                       ▻                ▫︎${nc}`);
console.log(
	`${darkblue} ▪︎           🐟          ▴     ✨                 .      ‣${nc}`);
console.log(
	`${darkblue}      ‣         .                       🦑        ⚬  ${nc}`);
console.log(
	`${brown}__🦀_____${gray}⎚${brown}______________________________________________________${nc}`
)
var path = require("path");
var webpack = require("webpack");

let root_dir = path.dirname("");
var dir_src = path.resolve(root_dir, "src");
var dir_build = path.resolve(root_dir, "build");

var webTarget = {
	target: "web",
	entry: {
		"faec": path.resolve(dir_src, "entries/web_entry.ts"),
	},
	plugins: [
		new webpack.LoaderOptionsPlugin({
			debug: true,
		}),
	],
	devtool: "source-map",
	output: {
		path: dir_build,
		filename: "[name]-web-bundle.js",
		library: "[name]",
		libraryTarget: "umd",
		globalObject: "this",
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: {
					loader: 'babel-loader',
					options: {
						"presets": [
							["@babel/preset-env", {
								"targets": ["> 0.25%, not dead"],
								"useBuiltIns": "usage",
								"corejs": 3,
							}],
							"@babel/preset-typescript"
						],
						"plugins": ["transform-class-properties"]
					}
				},
				exclude: /node_modules/,
			},
			{
				test: /\.(frag|vert)$/,
				use: [
					'core/shader_loader'
				]
			},
			{
				test: /\.css$/,
				use: "style!css",
			},
		],
	},
	resolve: {
		modules: [dir_src, "node_modules"],
		extensions: ['.ts', '.js'],
	},
	resolveLoader: {
		modules: [dir_src, "node_modules"],
		extensions: ['.ts', '.js'],
	},
};

var nodeTarget = {
	target: "node",
	entry: {
		"faec": path.resolve(dir_src, "entries/node_entry.ts"),
	},
	plugins: [
		new webpack.LoaderOptionsPlugin({
			debug: true,
		}),
	],
	devtool: "source-map",
	output: {
		path: dir_build,
		filename: "[name]-node-bundle.js",
		library: "[name]",
		libraryTarget: "umd",
		globalObject: "this",
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: {
					loader: "babel-loader",
					options: {
						"presets": [
							["@babel/preset-env", {
								"targets": {"node": "current"},
								"useBuiltIns": "usage",
								"corejs": 3,
							}],
							"@babel/preset-typescript"
						],
						"plugins": [
							"transform-class-properties",
							"@babel/plugin-proposal-nullish-coalescing-operator",
							"@babel/plugin-proposal-optional-chaining",
						]
					}
				},
				exclude: /node_modules/,
			},
		],
	},
	resolve: {
		modules: [dir_src, "node_modules"],
		extensions: ['.js', '.ts'],
	},
};

module.exports = [webTarget, nodeTarget];

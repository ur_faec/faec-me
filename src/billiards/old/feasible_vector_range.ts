

import { Coords2d } from "core/geometry";

import { BilliardsParams } from "./billiards_params";


// Encodes the linear constraint ah + bv + c > 0 on planar coords (h, v).
export class LinearConstraint2d {

	a: number;
	b: number;
	c: number;

	constructor(a: number, b: number, c: number) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	copy(): LinearConstraint2d {
		return new LinearConstraint2d(this.a, this.b, this.c);
	}

	// Given xy coords, returns the dual constraint in the zx plane.
	// sign: 1 for lower boundary, -1 for upper boundary.
	// TODO: Break this out into a generic geometry class, and make functions
	// like these that rely on a particular affine embedding into local helpers
	// (or design a class for converting arbitrary affine embeddings).
	static fromDualCoords(coords: Coords2d, sign: number): LinearConstraint2d {
		return new LinearConstraint2d(-sign, -sign * (coords.x + 1), -sign * coords.y);
	}

	// Returns the LinearConstraint2d for given slope and intercept in the
	// zx plane.
	static fromSlopeAndIntercept(slope: number, intercept: number): LinearConstraint2d {
		return new LinearConstraint2d(-slope, 1, -intercept);
	}

	// Given a pair of coords in the zx plane, returns the constraint going
	// through them.
	static fromSegment(from: Coords2d, to: Coords2d): LinearConstraint2d {
		const dh = to.x - from.x;
		const dv = to.y - from.y;
		const intercept = from.y - dv / dh * from.x;
		return new LinearConstraint2d(-dv, dh, -intercept);
	}

	// Given a constraint in the xy plane, find the corresponding
	// coordinates in the zx plane.
	asDualCoords(): Coords2d {
		const correctedC = this.c - this.a;
		const h = correctedC / this.b;
		const v = this.a / this.b;
		return Coords2d.fromXY(h, v);
	}

	// TODO: This is a gratuitously axis-specific way to do things, rewrite this
	// so it works for any inputs.
	intersectionWith(constraint: LinearConstraint2d): Coords2d {
		const interceptDelta = -this.c / this.b - -constraint.c / constraint.b;
		const slopeDelta = -this.a / this.b - -constraint.a / constraint.b;
		const h = -interceptDelta / slopeDelta;
		const v = -this.a / this.b * h + -this.c / this.b;
		return Coords2d.fromXY(h, v);
	}

	intersectionWithSegment(from: Coords2d, to: Coords2d): Coords2d {
		const baseOffset = this.a * from.x + this.b * from.y + this.c;
		const delta = to.asOffsetFrom(from);
		const deltaOffset = this.a * delta.dx + this.b * delta.dy + this.c;
		const coefficient = -baseOffset / deltaOffset;
		return from.plus(delta.timesReal(coefficient));
	}

	offsetOfCoords(coords: Coords2d): number {
		return this.a * coords.x + this.b * coords.y + this.c;
	}

	lastNonzeroOffset(coordsArray: Array<Coords2d>): number {
		for (let i = coordsArray.length - 1; i >= 0; i--) {
			const offset = this.offsetOfCoords(coordsArray[i]);
			if (offset > 0 || offset < 0) {
				return offset;
			}
		}
		return 0;
	}

	reverse(): LinearConstraint2d {
		return new LinearConstraint2d(-this.a, -this.b, -this.c);
	}

	alternate(sign: number): LinearConstraint2d {
		if (sign >= 0) {
			return this;
		}
		return this.reverse();
	}
}

type EdgeCallback = (edge: LinearConstraint2d, from: Coords2d, to: Coords2d) => void;

export class ConvexPolygon {

	// edges[i] = LinearConstraint2d.fromSegment(vertices[i - 1], vertices[i])
	edges: Array<LinearConstraint2d>;
	vertices: Array<Coords2d>;

	constructor(edges: Array<LinearConstraint2d>, vertices: Array<Coords2d>) {
		this.edges = edges;
		this.vertices = vertices;
	}

	copy(): ConvexPolygon {
		const edges = this.edges.map(edge => edge.copy());
		const vertices = this.vertices.map(v => v.copy());
		return new ConvexPolygon(edges, vertices);
	}

	static fromEdges(edges: Array<LinearConstraint2d>): ConvexPolygon {
		const vertices: Array<Coords2d> = [];
		for (let i = 0; i < edges.length; i++) {
			const next = (i + 1) % edges.length;
			vertices.push(edges[i].intersectionWith(edges[next]));
		}
		return new ConvexPolygon(edges, vertices);
	}

	length(): number {
		return this.edges.length;
	}

	enumerateEdges(callback: EdgeCallback): void {
		const n = this.edges.length;
		let prev = n - 1;
		for (let i = 0; i < n; i++) {
			callback(this.edges[i], this.vertices[prev], this.vertices[i]);
			prev = i;
		}
	}

	// Returns the region with positive offset as
	// pair.valueForSign(1) and negative offset as
	// pair.valueForSign(-1).
	splitByConstraint(constraint: LinearConstraint2d): OrderedPair<ConvexPolygon | null | undefined> {
		const builders = new OrderedPair(new ConvexPolygonBuilder(), new ConvexPolygonBuilder());
		this.enumerateEdges((edge: LinearConstraint2d, from: Coords2d, to: Coords2d) => {
			const fromOffset = constraint.offsetOfCoords(from);
			const toOffset = constraint.offsetOfCoords(to);
			if (fromOffset * toOffset >= 0) {
				const builder = builders.valueForSign(fromOffset + toOffset);
				if (fromOffset == 0) {
					// Just crossed the boundary, add it as an edge.
					const boundary = constraint.alternate(fromOffset + toOffset);
					builder.append(boundary, from);
				}
				builder.append(edge, to);
			} else {
				const fromBuilder = builders.valueForSign(fromOffset);
				const toBuilder = builders.valueForSign(toOffset);
				const intersection = edge.intersectionWith(constraint);
				const boundary = constraint.alternate(toOffset);
				fromBuilder.append(edge, intersection);
				toBuilder.append(boundary, intersection);
				toBuilder.append(edge, to);
			}
		});
		return builders.map(b => b.build());
	}
}

export function PathSpacePolygonForApex(apex: Coords2d): ConvexPolygon {
	const constraints = [LinearConstraint2d.fromDualCoords(Coords2d.origin(), 1), LinearConstraint2d.fromDualCoords(Coords2d.fromXY(1, 0), 1), LinearConstraint2d.fromDualCoords(apex, -1)];
	return ConvexPolygon.fromEdges(constraints);
}

export class ConvexPolygonBuilder {

	edges: Array<LinearConstraint2d>;
	vertices: Array<Coords2d>;

	constructor() {
		this.edges = [];
		this.vertices = [];
	}

	append(edge: LinearConstraint2d, vertex: Coords2d): void {
		this.edges.push(edge);
		this.vertices.push(vertex);
	}

	build(): ConvexPolygon | null | undefined {
		if (this.edges.length < 3) {
			return null;
		}
		return new ConvexPolygon(this.edges, this.vertices);
	}
}

// Maintains a convex hull of the dual of a set of path boundaries (treating
// them as a linear program). A segment tree is feasible iff the
// FeasibleVectorRange it generates is nonempty.
// Input assumptions:
//   1 <= x
export class FeasibleVectorRange {

	constraints: Array<LinearConstraint2d>;

	// constraintIntersections[i] =
	//   constraints[i].intersect(constraints[i+1])
	constraintIntersections: Array<Coords2d>;

	constructor(constraints: Array<LinearConstraint2d>, constraintIntersections: Array<Coords2d>) {
		this.constraints = constraints.map(c => c);
		this.constraintIntersections = constraintIntersections.map(c => c);
	}

	copy(): FeasibleVectorRange {
		return new FeasibleVectorRange(this.constraints, this.constraintIntersections);
	}

	static fromApex(apex: Coords2d): FeasibleVectorRange {
		// Initializes to the finite path region going through the two apex
		// edges of the specified triangle.
		const apexConstraint = LinearConstraint2d.fromDualCoords(apex, -1);
		const constraints = [LinearConstraint2d.fromDualCoords(Coords2d.origin(), 1), LinearConstraint2d.fromDualCoords(Coords2d.fromXY(1, 0), 1), apexConstraint];
		const constraintIntersections = [constraints[0].intersectionWith(constraints[1]), constraints[1].intersectionWith(constraints[2]), constraints[2].intersectionWith(constraints[0])];
		return new FeasibleVectorRange(constraints, constraintIntersections);
	}

	static fromParams(params: BilliardsParams): FeasibleVectorRange {
		return FeasibleVectorRange.fromApex(params.apex);
	}

	// Sign: whether this vertex is a lower (1) or upper (-1) boundary for the
	// vectors.
	addBoundaryVertex(coords: Coords2d, sign: number): void {
		if (this.constraintIntersections.length == 0) {
			return;
		}
		const constraint = LinearConstraint2d.fromDualCoords(coords, sign);
		let firstFeasible: number | null | undefined = null;
		let lastFeasible: number | null | undefined = null;
		const coordsArray = this.constraintIntersections;
		//let prevOffset = constraint.lastNonzeroOffset(coordsArray);
		let prevOffset = constraint.offsetOfCoords(coordsArray[coordsArray.length - 1]);
		for (let i = 0; i < coordsArray.length; i++) {
			const offset = constraint.offsetOfCoords(coordsArray[i]);
			if (prevOffset <= 0 && offset > 0) {
				firstFeasible = i;
			} else if (prevOffset > 0 && offset <= 0) {
				lastFeasible = i;
			}
			prevOffset = offset;
		}
		if (firstFeasible == null) {
			// Either all existing intersections are fully within this constraint
			// or all are outside it.
			if (prevOffset <= 0) {
				// Everything is masked by this constraint.
				this.constraints = [];
				this.constraintIntersections = [];
			}
			return;
		}
		const first: number = firstFeasible;
		if (lastFeasible == null) {
			// This is impossible given firstFeasible != null, but we include the
			// explicit check to satisfy the typechecker.
			return;
		}
		const last: number = lastFeasible;
		const count = (last - first + coordsArray.length) % coordsArray.length + 1;

		const newConstraints: Array<LinearConstraint2d> = [];
		const newConstraintIntersections: Array<Coords2d> = [];
		for (let i = 0; i < count; i++) {
			const index = (first + i) % coordsArray.length;
			newConstraints.push(this.constraints[index]);
			newConstraintIntersections.push(this.constraintIntersections[index]);
		}
		// The last intersection needs to be recomputed because it now intersects
		// with the new constraint.
		newConstraintIntersections[count - 1] = newConstraints[count - 1].intersectionWith(constraint);
		newConstraints.push(constraint);
		newConstraintIntersections.push(constraint.intersectionWith(newConstraints[0]));

		this.constraints = newConstraints;
		this.constraintIntersections = newConstraintIntersections;
	}

	isEmpty(): boolean {
		return this.constraints.length == 0;
	}

	crossesSegment(lower: Coords2d, upper: Coords2d): boolean {
		const region = this.copy();
		region.addBoundaryVertex(lower, 1);
		region.addBoundaryVertex(upper, -1);
		return !region.isEmpty();
	}

	// Checks whether there is a feasible line incident to coords in the xy
	// plane.
	areCoordsReachable(coords: Coords2d): boolean {
		const constraint = LinearConstraint2d.fromDualCoords(coords, 1);

		const points = this.constraintIntersections;
		const offset = constraint.lastNonzeroOffset(points);
		for (let i = 0; i < points.length; i++) {
			if (constraint.offsetOfCoords(points[i]) * offset < 0) {
				return true;
			}
		}
		return false;
	}

	contains(coords: Coords2d): boolean {
		for (let i = 0; i < this.constraints.length; i++) {
			const offset = this.constraints[i].offsetOfCoords(coords);
			if (offset < 0) {
				return false;
			}
		}
		return true;
	}
}


// Conceptually this is a pair that can be indexed in two dual ways, either
// "index" (0 and 1), as in the position in an array, or "sign" (1 and -1),
// as in the sign of the edge vector pointing to a particular value (as
// though we were indexing vertices and edges between them).
//
// More concretely: pairForSign(s) goes from
// valueForSign(-s) to valueForSign(s).
//
// pairForIndex(i) goes from
// valueForIndex(!i) to valueForIndex(i).
export class OrderedPair<T> {

	v0: T;
	v1: T;

	constructor(v0: T, v1: T) {
		this.v0 = v0;
		this.v1 = v1;
	}

	valueForIndex(index: number): T {
		if (index == 0) {
			return this.v1;
		}
		return this.v0;
	}

	pairForIndex(index: number): [T, T] {
		if (index == 0) {
			return [this.v0, this.v1];
		}
		return [this.v1, this.v0];
	}

	valueForSign(sign: number): T {
		if (sign >= 0) {
			return this.v1;
		}
		return this.v0;
	}

	pairForSign(sign: number): [T, T] {
		if (sign >= 0) {
			return [this.v0, this.v1];
		}
		return [this.v1, this.v0];
	}

	map<S>(f: (value: T) => S): OrderedPair<S> {
		return new OrderedPair(f(this.v0), f(this.v1));
	}

	enumerateValues(callback: (index: number, sign: number, value: T) => void): void {
		callback(0, 1, this.v1);
		callback(1, -1, this.v0);
	}
}
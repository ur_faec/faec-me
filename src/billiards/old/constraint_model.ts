// @flow

/*
import $ from 'jquery';
import { CallbackPointsInterface } from 'faec/callback_points_interface';
import { Complex } from 'faec/complex';
//import { ComplexPoly } from 'faec/complex_poly';
import { IsoScaleTransform } from 'faec/coordinate_transform';
import CanvasRender, {
  RenderableElementsRenderer, RenderableElementsRendererParams }
  from 'faec/element_render';
import { Coords2d, TangentCoords2d, OffsetCoords2d } from 'faec/geometry';
import { Rect, Triangle2d } from 'faec/polygon';

import { TriangleForApex, BilliardsParams, DReal, DTCoords2d }
  from './billiards_params';
import { EdgeSequenceForEdgePath } from './edge_path';
import { BoundaryOffsetFunction, PathStudy } from './billiards_study';
import { EdgePath } from './edge_path';

export function TriangleWithBaseAngles(a0: number, a1: number): Triangle2d {
  let v0 = Coords2d.origin();
  let t0 = TangentCoords2d.fromBaseAndOffset(
    v0, OffsetCoords2d.fromDxDy(Math.cos(a0), Math.sin(a0)));
  let v1 = Coords2d.fromXY(1, 0);
  let t1 = TangentCoords2d.fromBaseAndOffset(
    v1, OffsetCoords2d.fromDxDy(-Math.cos(a1), Math.sin(a1)));
  let apex = t0.intersectionWith(t1);
  return TriangleForApex(apex);
}

const EPSILON = 0.000001;
const uShowPhase = true;
const uCartesianScale = 1.0;
const uPolarScale = 0.0;//1.0;
const uLineWidth = 0.003;

function decayFactor(x: number, dx: number): number {
  let radius = 0.6 * uLineWidth;
  if (Math.abs(x) >= radius) {
    return 1.0;
  }
  // If uLineWidth * dx >= 1, the lines are pretty much solid, so dim
  // the maximum intensity to let the background through.
  let density = Math.min(uLineWidth * dx, 1.0);
  let minimum = density * 0.75;
  return minimum + (1.0 - minimum) * Math.abs(0.5 * x / radius);
}

// Offset of x from the integer grid, ignoring integer multiples of n.
function offsetFromGridExcept(n: number, x: number): number {
  x = Math.abs(x);
  let below = Math.floor(x);
  let above = Math.ceil(x);
  if (x % n < 0.5) {
    below -= 1.0;
  }
  if (x % n > n - 0.5) {
    above += 1.0;
  }
  return Math.min(x-below, above-x);
}

function offsetFromGrid(x: number): number {
  return Math.min(x-Math.floor(x), Math.ceil(x)-x);
}


function lightGridFn(x: number): number {
  return offsetFromGrid(x);//ExpGrid(x);
}

function darkGridFn(x: number): number {
  return offsetFromGrid(x);
}


function GridOverlay(tc: DTCoords2d) {
  let magnitude = tc.length();
  let dmagnitude = Math.sqrt(
    magnitude.dv[0] * magnitude.dv[0] + magnitude.dv[1] * magnitude.dv[1]);
  let dxabs =
    Math.sqrt(tc.dx.dv[0] * tc.dx.dv[0] + tc.dx.dv[1] * tc.dx.dv[1]) + EPSILON;
  let dyabs =
    Math.sqrt(tc.dy.dv[0] * tc.dy.dv[0] + tc.dy.dv[1] * tc.dy.dv[1]) + EPSILON;
  //let vabs = tc.z.abs();
  //let dvabs = tc.dz.abs() + EPSILON;
  let phase = DReal.atan2(tc.dy, tc.dx);
  let dphase = Math.sqrt(phase.dv[0] * phase.dv[0] + phase.dv[1] * phase.dv[1]);
  //let dphase = dvabs / (vabs + EPSILON);

  let saturation = 1.0;
  if (uShowPhase) {
    let phaseOffset: number;
    if (uCartesianScale > 0.0) {
      phaseOffset = offsetFromGridExcept(3, 12.0*phase.v / (2.0 * Math.PI));
    } else {
      phaseOffset = offsetFromGrid(12.0 * phase.v / (2.0 * Math.PI));
    }
    saturation *= decayFactor(
      phaseOffset * Math.PI / (6 * dphase), dphase * 6.0);
  }
  if (uPolarScale > 0.0) {
    let absFactor = 1.0;
    if (magnitude.v > 0.5) {
      let absOffset = lightGridFn(magnitude.v * uPolarScale) / uPolarScale;
      absFactor = decayFactor(absOffset / dmagnitude, dmagnitude * uPolarScale);
    }
    saturation *= absFactor;
  }
  let intensity = 1.0;
  if (uCartesianScale > 0.0) {
    let yOffset = //Math.abs(tc.dy.v - 2);
        darkGridFn(Math.abs(tc.dy.v * uCartesianScale) + 1.0) / uCartesianScale;
    let yFactor = decayFactor(yOffset / dyabs, dyabs * uCartesianScale);
    let xOffset = //Math.abs(tc.dx.v - 1);
      darkGridFn(Math.abs(tc.dx.v * uCartesianScale) + 1.0) / uCartesianScale;
    let xFactor = decayFactor(xOffset / dxabs, dxabs * uCartesianScale);
    intensity = yFactor * xFactor;
  }
  return [saturation, intensity];
}

// Scaling weirdness: "hue" is expected to be an angle measured in radians,
// sv are [0, 1].
function HSVtoRGB(hsv: [number, number, number]): [number, number, number] {
  let hue = hueForPhase(hsv[0]);
  return [
    255 * (hsv[1] * hue[0] + (1.0 - hsv[1])) * hsv[2],
    255 * (hsv[1] * hue[1] + (1.0 - hsv[1])) * hsv[2],
    255 * (hsv[1] * hue[2] + (1.0 - hsv[1])) * hsv[2],
  ];
}

function hueForDTCoords2d(tc: DTCoords2d): [number, number, number] {
  let phase = Complex.fromCartesian(tc.dx.v, tc.dy.v).phase();
  let grid = GridOverlay(tc);
  return HSVtoRGB([phase, grid[0], grid[1]]);
}

function hueForPhase(phase: number): [number, number, number] {
  phase += Math.PI * 1.2;
  let offsets = [phase, phase + Math.PI, 2.0 * phase];
  let v = [
    Math.sin(offsets[0]) + 1.0,
    Math.sin(offsets[1]) + 1.0,
    Math.sin(offsets[2]) + 1.0];
  let hue = [0.4 * v[0], 0.25 * v[1], 0.4 * v[2]];
  hue = [hue[0] * hue[0], hue[1] * hue[1], hue[2] * hue[2]];
  let den = Math.sqrt(hue[0] * hue[0] + hue[1] * hue[1] + hue[2] * hue[2]);
  return [
    hue[0] / den,
    hue[1] / den,
    hue[2] / den,
  ];
}

export class BilliardsConstraintModel {
  _container: HTMLElement;
  _viewSize: [number, number];
  _canvasSize: [number, number];
  _canvasBounds: Rect;
  _modelBounds: Rect;
  _coordinateTransform: IsoScaleTransform;
  _renderers: [RenderableElementsRenderer, RenderableElementsRenderer];
  renderParams: RenderableElementsRendererParams;

  //_sineSum: SineSum;
  _boundaryOffsetFunction: BoundaryOffsetFunction;

  extra: Array<CanvasRender.Renderable>;
  _needsRedraw: boolean;
  _controlPointsInterfaces: ?Array<CallbackPointsInterface>;

  vForward: number;
  vRight: number;
  vAngle: number;

  drawDistance: number;
  skipOdd: boolean;
  decayParam: number;
  _showPath: boolean;
  _showUnfolding: boolean;
  _unfoldingViewSize: number;

  sequenceResults: Array<[number, number]>;

  _apex: Coords2d;
  _source: TangentCoords2d;

  highlightCount: number;

  plotFunctions: ?Array<(c: Coords2d) => DTCoords2d>
  edgePaths: Array<string>;
  originalEdgePaths: Array<string>;
  _reflectedEdgeSequence: ?Array<number>;

  constructor(container: HTMLElement, viewSize: [number, number]) {
    window.console.log("SineSumModel");
    // Internal UI state
    this._container = container;
    this._viewSize = viewSize;
    this._canvasSize = [viewSize[0] / 2, viewSize[1]];
    this._canvasBounds = new Rect(
      Coords2d.fromXY(0, this._canvasSize[1]),
      OffsetCoords2d.fromDxDy(this._canvasSize[0], -this._canvasSize[1])
    );
    this._modelBounds = Rect.fromDimensions(2, 2);

    let path = new EdgePath("RRLLRRRRRRRRRRRRRRRLRLLRRLLLLLLLLLLLLLLLRL");
    let pathStudy = new PathStudy(path);
    let boundaryOffsetFunction = pathStudy.constraintFunction(9, 15);
    this._boundaryOffsetFunction = boundaryOffsetFunction;

    this._renderers = [
      new RenderableElementsRenderer(this._canvasSize),
      new RenderableElementsRenderer(this._canvasSize),
    ];
    this.renderParams = new RenderableElementsRendererParams();
    this._needsRedraw = false;

    this._apex = Coords2d.fromXY(0.6, 0.6);
    this._source = TangentCoords2d.fromBaseAndOffset(
      Coords2d.fromXY(0.5, 0.3),
      OffsetCoords2d.fromDxDy(1, 0)
    );

    this.vForward = 0;
    this.vRight = 0;
    this.vAngle = 0;

    this.drawDistance = 30;
    this.skipOdd = false;
    this.decayParam = 100;
    this._showPath = true;
    this._showUnfolding = false;
    this._unfoldingViewSize = 7.14;

    this.edgePaths = [

    ];
    this.originalEdgePaths = this.edgePaths.map((x) => x);
    this.highlightCount = 1;
  }

  // Triangle: the triangle in which we're looking at billiards.
  get triangle (): Triangle2d {
    return Triangle2d.fromCoords(
      Coords2d.origin(),
      Coords2d.fromXY(1, 0),
      this._apex);
  }

  // Coords2d: The apex of the triangle. The base is always from (0,0) to (1,0).
  get apex (): Coords2d {
    return this._apex;
  }

  set apex (apex: Coords2d) {
    this._apex = apex.copy();
    this._needsRedraw = true;
  }

  get boundaryOffsetFunction (): BoundaryOffsetFunction {
    return this._boundaryOffsetFunction;
  }

  set boundaryOffsetFunction (boundaryOffsetFunction: BoundaryOffsetFunction) {
    this._boundaryOffsetFunction = boundaryOffsetFunction;
    this._needsRedraw = true;
  }

  // RootedTCoords(2d): the starting position / vector from which to draw the
  // billiard path.
  get source (): TangentCoords2d {
    return this._source;
  }

  set source (source: TangentCoords2d) {
    this._source = source;
    this._needsRedraw = true;
  }

  get reflectedEdgeSequence (): ?Array<number> {
    return this._reflectedEdgeSequence;
  }

  set reflectedEdgeSequence (edgeSequence: ?Array<number>) {
    this._reflectedEdgeSequence = edgeSequence;
    this._needsRedraw = true;
  }

  get showPath (): boolean {
    return this._showPath;
  }

  set showPath (showPath: boolean) {
    this._showPath = showPath;
    this._needsRedraw = true;
  }

  get showUnfolding (): boolean {
    return this._showUnfolding;
  }

  set showUnfolding (showUnfolding: boolean) {
    this._showUnfolding = showUnfolding;
    this._needsRedraw = true;
  }

  get modelBounds (): Rect {
    return this._modelBounds;
  }
  set modelBounds (modelBounds: Rect) {
    var transform = IsoScaleTransform.forCenteredRect(
      this._canvasBounds, modelBounds);
    this._coordinateTransform = transform;
    var {origin, diagonal} = this._canvasBounds;
    var mappedOrigin = transform.transformCoords(origin);
    var mappedDiagonal = transform.transformOffset(diagonal);
    this._modelBounds = new Rect(mappedOrigin, mappedDiagonal);
    //mappedDiagonal.timesReal(1.0 / 3.0);
    //mappedOrigin.plus(mappedDiagonal);
    this._needsRedraw = true;
  }

  get unfoldingViewSize (): number {
    return this._unfoldingViewSize;
  }
  set unfoldingViewSize (unfoldingViewSize: number) {
    this._unfoldingViewSize = unfoldingViewSize;
    this._needsRedraw = true;
  }

  start () {
    $(this._container).css(
      {'width': `${this._viewSize[0]+4}px`, 'margin': 'auto',
        'position': 'relative'});
    $(this._container).append(this._renderers[0].canvas);
    $(this._container).append(this._renderers[1].canvas);
    $(this._container).append(this._createControls());

    this._createControlInterfaces();
    this._addEventListeners();

    window.console.log("Start");
    requestAnimationFrame(this.generateClockTick());
  }

  KeyDown (event: KeyboardEvent) {
    if (event.metaKey) {
      return;
    }
    var speed = 0.01;
    if (event.shiftKey) {
      speed *= 0.05;
      if (event.ctrlKey) {
        speed *= 0.05;
      }
    }
    switch (event.which) {
    case 38: // up
    case 87: // W
      this.vForward = 1.5*speed;
      break;

    case 40: // down
    case 83: // S
      this.vForward = -1.5*speed;
      break;

    case 37: // left
    case 65: // A
      this.vAngle = 2.0*speed;
      break;

    case 39: // right
    case 68: // D
      this.vAngle = -2.0*speed;
      break;

    case 81: // Q
      this.vRight = speed;
      break;

    case 69: // E
      this.vRight = -speed;
      break;

    default:
      return;
    }
    event.preventDefault();
  }

  KeyUp (event: KeyboardEvent) {
    switch (event.which) {
    case 38: // up
    case 87: // W
      if (this.vForward > 0.0) {
        this.vForward = 0;
      }
      break;

    case 40: // down
    case 83: // S
      if (this.vForward < 0.0) {
        this.vForward = 0;
      }
      break;

    case 37: // left
    case 65: // A
      if (this.vAngle > 0.0) {
        this.vAngle = 0;
      }
      break;

    case 39: // right
    case 68: // D
      if (this.vAngle < 0.0) {
        this.vAngle = 0.0;
      }
      break;

    case 81: // Q
      if (this.vRight > 0.0) {
        this.vRight = 0.0;
      }
      break;

    case 69: // E
      if (this.vRight < 0.0) {
        this.vRight = 0.0;
      }
      break;
    }
  }

  generateClockTick() {
    if (this._needsRedraw) {
      this.redraw();
    }
    return () => {
      requestAnimationFrame(this.generateClockTick());
    };
  }

  redraw () {

    this._needsRedraw = false;

    window.console.log("Hello?");

    if (this._renderers[0].canvas == null) {
      return;
    }
    let canvas = this._renderers[0].canvas;
    this._renderers[0].modelBounds = this.modelBounds;
    let pathStr = "RRLLRRRLRRRLLRRLLLRLLL";//"RRRLRRRLLLRLLL";
    let edgePath = new EdgePath(pathStr);
    //let edgeSequence = EdgeSequenceForEdgePath(pathStr);
    let pathStudy = new PathStudy(edgePath);
    //let cf = pathStudy.constraintFunction(3, 4);
    let spine = pathStudy.spineEdges.map((e) => e.reflectedEdge);
    let constraintPath = pathStudy.reflectedEdgesForSpinePath(
      pathStudy.leftVertices[2], pathStudy.rightVertices[6]);
    //let plotFunction = this.boundaryOffsetFunction;
    let plotFunction0 = (c: Coords2d): DTCoords2d => {
      let params = new BilliardsParams(c);

      let spineOffsets = spine.map((e) => e.valueOnParams(params));
      let spineValue = DTCoords2d.zero();
      for (let i = 0; i < spineOffsets.length; i++) {
        spineValue = spineValue.plus(spineOffsets[i]);
      }
      let constraintOffsets = constraintPath.map(
        (e) => e.valueOnParams(params));
      let constraintValue = DTCoords2d.zero();
      for (let i = 0; i < constraintPath.length; i++) {
        constraintValue = constraintValue.plus(constraintOffsets[i]);
      }

      return spineValue.dividedBy(constraintValue);
    };
    let plotFunction1 = (c: Coords2d): DTCoords2d => {
      let params = new BilliardsParams(c);
      let spineOffsets = spine.map((e) => e.valueOnParams(params));
      let spineValue = DTCoords2d.zero();
      for (let i = 0; i < spineOffsets.length; i++) {
        spineValue = spineValue.plus(spineOffsets[i]);
      }
      let constraintOffsets = constraintPath.map(
        (e) => e.valueOnParams(params));
      let constraintValue = DTCoords2d.zero();
      for (let i = 0; i < constraintPath.length; i++) {
        constraintValue = constraintValue.plus(constraintOffsets[i]);
      }

      if (c.x == 0.5 && c.y == 0.34) {
        window.console.log("plotFunction1");
      }

      return spineValue;

    };
    this.plotFunctions = [plotFunction0, plotFunction1];
    plotFunction1(Coords2d.fromXY(0.5, 0.34));
    let pixelSpaceToModelSpace = this._renderers[0].canvasToModelTransform();
    let imagePosition =
      pixelSpaceToModelSpace.transformCoords(Coords2d.origin());

    let plot0 = new CanvasRender.ImageRendering(
      imagePosition, canvas.width, canvas.height,
      (imageData: ImageData) => {
        let width = canvas.width;
        let height = canvas.height;
        let renderScale = this._renderers[0].canvas.renderScale;
        for (let y = 0; y < height; y++) {
          for (let x = 0; x < width; x++) {
            let offset = y * width + x;
            let modelCoords = pixelSpaceToModelSpace.transformCoords(
              Coords2d.fromXY(x / renderScale, y / renderScale));
            let value = plotFunction0(modelCoords);
            let hue = hueForDTCoords2d(value);
            imageData.data[offset*4 + 0] = hue[0];
            imageData.data[offset*4 + 1] = hue[1];
            imageData.data[offset*4 + 2] = hue[2];
            imageData.data[offset*4 + 3] = 255;
          }
        }
      }
    );
    let params = this.renderParams;
    params.elements = [plot0];
    this._renderers[0].render(params);

    canvas = this._renderers[1].canvas;
    this._renderers[1].modelBounds = this.modelBounds;

    let plot1 = new CanvasRender.ImageRendering(
      imagePosition, canvas.width, canvas.height,
      (imageData: ImageData) => {
        let width = canvas.width;
        let height = canvas.height;
        let renderScale = this._renderers[0].canvas.renderScale;
        for (let y = 0; y < height; y++) {
          for (let x = 0; x < width; x++) {
            let offset = y * width + x;
            let modelCoords = pixelSpaceToModelSpace.transformCoords(
              Coords2d.fromXY(x / renderScale, y / renderScale));
            let value = plotFunction1(modelCoords);
            let hue = hueForDTCoords2d(value);
            imageData.data[offset*4 + 0] = hue[0];
            imageData.data[offset*4 + 1] = hue[1];
            imageData.data[offset*4 + 2] = hue[2];
            imageData.data[offset*4 + 3] = 255;
          }
        }
      }
    );
    params.elements = [plot1];
    this._renderers[1].render(params);
  }

  _createControls() {
    // TODO: I'm really mad at myself for resorting to this. Fix it.
    var div = $("<div></div>")
      .css({width: this._viewSize[0], height: '31px', margin: 'auto',
        position: 'absolute', left: '0px', top: `${this._viewSize[1]}px`});
    div.html(`
      <form style="margin-top: 5px; float: left; width: 50%;"
          class="phase-controls">
        <div style="display: inline-block;float: left;">
          <label>
            <input style="margin:4px;" type="checkbox" checked="checked"
                name="show-path">
            Show path
          </label>
        </div>
        <div style="display: inline-block;float: left; padding-left:20px;">
          <label>
            <input type="radio" name="draw-distance" value="3">
            3
          </label>
        </div>
        <div style="display: inline-block;float: left;">
          <label>
            <input type="radio" name="draw-distance" value="30"
                checked="checked">
            30
          </label>
        </div>
        <div style="display: inline-block;float: left;">
          <label>
            <input type="radio" name="draw-distance" value="200">
            200
          </label>
        </div>
        <div style="display: inline-block;float: left;">
          <label>
            <input style="margin:2px 8px;" type="checkbox"
                name="show-unfolding">
            Show unfolding
          </label>
        </div>
      </form>
      <div style="padding: 4px 6px; float: right;">
        <input name="edgePath" type="text" size="50">
      </div>
      <div style="padding: 4px 6px; float: right;">
        Zoom:
        <input name="zoom" type="range" min="1" max="10" step="0.01"
          value="7">
      </div>
      `);
    return div;
  }

  _triangleEditCallback (canvasCoords: Coords2d) {
    // This code assumes the base of the triangle is from (0,0) to (1,0)
    // in model space.
    var maxDist = this._coordinateTransform.isoScale() * 30;
    var modelCoords =
        this._coordinateTransform.transformCoords(canvasCoords);

    let originalApexCoords = this.apex.copy();
    let originalSource = this.source.copy();
    // Measure the fraction of the apex height.
    let originalSourceVert = originalSource.base.y / originalApexCoords.y;
    // Measure the intersection with the base when projecting down
    // from the apex.
    let originalSourceHorz = originalApexCoords.x +
      (originalSource.base.x - originalApexCoords.x) *
      (1.0 / (1.0 - originalSourceVert));
    if (modelCoords.distanceFrom(originalApexCoords) <= maxDist) {
      return (newCanvasCoords: Coords2d) => {
        var newModelCoords =
            this._coordinateTransform.transformCoords(newCanvasCoords);
        if (newModelCoords.y < 0.01) {
          newModelCoords.y = 0.01;
        }
        if (newModelCoords.x < 0) {
          newModelCoords.x = 0;
        }
        if (newModelCoords.x > 1) {
          newModelCoords.x = 1;
        }
        var modelDelta = newModelCoords.asOffsetFrom(modelCoords);
        let newApexCoords = originalApexCoords.plus(modelDelta);

        let newSourceY = newApexCoords.y * originalSourceVert;
        let newSourceX = newApexCoords.x +
          (originalSourceHorz - newApexCoords.x) *
          (1.0 - originalSourceVert);

        this.apex = newApexCoords;
        this.source.base = Coords2d.fromXY(newSourceX, newSourceY);
        this.source.offset.dy = originalSource.offset.dy *
          (newApexCoords.y / originalApexCoords.y);
        this.source.offset = this.source.offset.normalized();
        this._needsRedraw = true;
      };
    }
  }

  _createControlInterfaces() {
    if (this._renderers[0].canvas != null) {
      this._controlPointsInterfaces = [
        new CallbackPointsInterface(
          (canvasCoords: Coords2d) => {
            let t = this._renderers[0].canvasToModelTransform();
            let modelCoords = t.transformCoords(canvasCoords);
            console.log("Left canvas click");
            console.log(`Position: ${modelCoords.toString()}`);
            if (this.plotFunctions != null) {
              let value = this.plotFunctions[0](modelCoords);
              console.log(`Value: ${value.toString()}`);
            }
            return null;
          }, this._renderers[0].canvas),
        new CallbackPointsInterface(
          (canvasCoords: Coords2d) => {
            let t = this._renderers[1].canvasToModelTransform();
            let modelCoords = t.transformCoords(canvasCoords);
            console.log("Right canvas click");
            console.log(`Position: ${modelCoords.toString()}`);
            if (this.plotFunctions != null) {
              let value = this.plotFunctions[1](modelCoords);
              console.log(`Value: ${value.toString()}`);
            }
            return null;
          }, this._renderers[1].canvas)];
    }
  }

  _addEventListeners() {
    $(this._container).find('input').change(() => {
      window.console.log("Input changed in container:", this._container);
      this.drawDistance = parseInt($(this._container)
        .find('input[name="draw-distance"]:checked')[0].value);
      this.showPath =
        $(this._container).find('input[name="show-path"]').is(':checked');
      this.showUnfolding =
        $(this._container).find('input[name="show-unfolding"]').is(':checked');
      let zoom = $(this._container).find('input[name="zoom"]')[0].value;
      this.unfoldingViewSize = 50 / zoom;
      this._needsRedraw = true;
    });
    $(this._container).find('input[name="zoom"]').on('input', () => {
      let zoom = $(this._container).find('input[name="zoom"]')[0].value;
      this.unfoldingViewSize = 50 / zoom;
    });
    $(this._container).find('.refresh').click((event) => {
      if (event.button == 0) {
        let edgePath = $(this._container)
          .find('input[name="edgePath"]')[0].value;
        if (edgePath.length > 0) {
          let edgeSequence = EdgeSequenceForEdgePath(edgePath);
          this.reflectedEdgeSequence = edgeSequence;
        } else {
          this.reflectedEdgeSequence = null;
        }
      }
    });
    $(document).keydown((event) => {
      this.KeyDown(event);
    });
    $(document).keyup((event) => {
      this.KeyUp(event);
    });
  }
}
*/
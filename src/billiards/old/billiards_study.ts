

import { Coords2d } from "core/geometry";
import { Triangle2d } from "core/polygon";

import { TriangleForApex } from "./billiards_params";
import { EdgePair, ReflectedEdgeInnerProduct } from "./edge_inner_product";
import { EdgePath } from "./edge_path";
import { ReflectedAngle, ReflectedEdge, ReflectedTriangle } from "./reflected_triangle";
import { SineEntry, SineSum } from "./sine_sum";

function Mod3(n: number): number {
	return (n % 3 + 3) % 3;
}

export class InternalPathEdge {

	edge: ReflectedEdge;
	leftBoundaryIndex: number;
	rightBoundaryIndex: number;

	constructor(edge: ReflectedEdge, leftBoundaryIndex: number, rightBoundaryIndex: number) {
		this.edge = edge;
		this.leftBoundaryIndex = leftBoundaryIndex;
		this.rightBoundaryIndex = rightBoundaryIndex;
	}
}

export class BoundaryVertexIndex {

	side: "left" | "right";
	index: number;

	constructor(side: "left" | "right", index: number) {
		this.side = side;
		this.index = index;
	}

	static left(index: number): BoundaryVertexIndex {
		return new BoundaryVertexIndex("left", index);
	}

	static right(index: number): BoundaryVertexIndex {
		return new BoundaryVertexIndex("right", index);
	}

	plus(n: number): BoundaryVertexIndex {
		return new BoundaryVertexIndex(this.side, this.index + n);
	}

	toString(): string {
		return `${this.side}(${this.index})`;
	}

	// Partial order
	equals(vi: BoundaryVertexIndex): boolean {
		return this.side == vi.side && this.index == vi.index;
	}
	precedes(vi: BoundaryVertexIndex): boolean {
		return this.side == vi.side && this.index < vi.index;
	}
}

export class BoundaryVertex {

	index: BoundaryVertexIndex;
	incomingEdges: Array<PathEdge>;
	outgoingEdges: Array<PathEdge>;

	spinePosition: {
		index: number; // The index of the nearest spine index.

		// The edge from spineEdges[index] to this vertex, or null if the vertex
		// is on the spine.
		offset: PathEdge | null;
	} = {index: 0, offset: null};

	constructor(index: BoundaryVertexIndex) {
		this.index = index;
		this.incomingEdges = [];
		this.outgoingEdges = [];
	}
}

export class PathEdge {

	reflectedEdge: ReflectedEdge;
	from: BoundaryVertex;
	to: BoundaryVertex;

	constructor(reflectedEdge: ReflectedEdge, from: BoundaryVertex, to: BoundaryVertex) {
		this.reflectedEdge = reflectedEdge;
		this.from = from;
		this.to = to;
	}

	reverse(): PathEdge {
		return new PathEdge(this.reflectedEdge.reverse(), this.to, this.from);
	}
}

export class PathStudy {

	n: number; // The number of reflections (there are n+1 triangles).
	path: EdgePath;
	leftVertices: Array<BoundaryVertex>;
	rightVertices: Array<BoundaryVertex>;
	leftEdges: Array<PathEdge>;
	rightEdges: Array<PathEdge>;
	leftOffsets: Array<BoundaryOffsetFunction>;
	rightOffsets: Array<BoundaryOffsetFunction>;
	internalEdges: Array<PathEdge>;
	baseEdges: Array<PathEdge>;
	spineEdges: Array<PathEdge>;
	//internalSpine: Array<PathEdge>;

	triangles: Array<ReflectedTriangle>;

	constructor(path: EdgePath) {
		this.path = path;

		let triangle = ReflectedTriangle.zero();
		const triangles = [triangle];
		const leftEdges: Array<PathEdge> = [];
		const rightEdges: Array<PathEdge> = [];
		let leftVertex = new BoundaryVertex(BoundaryVertexIndex.left(0));
		let rightVertex = new BoundaryVertex(BoundaryVertexIndex.right(0));
		const leftVertices: Array<BoundaryVertex> = [leftVertex];
		const rightVertices: Array<BoundaryVertex> = [rightVertex];
		let baseEdgeIndex = 0;
		const baseEdge = new PathEdge(triangle.edge(baseEdgeIndex), leftVertex, rightVertex);
		leftVertex.outgoingEdges.push(baseEdge);
		rightVertex.incomingEdges.push(baseEdge);
		const internalEdges: Array<PathEdge> = [baseEdge];
		const baseEdges: Array<PathEdge> = [baseEdge];
		let sign = 1;
		for (let i = 0; i < path.str.length; i++) {
			const turn = path.str[i];
			let newBaseEdgeIndex: number;
			let newBoundaryEdge: number;
			if (turn == 'L') {
				newBaseEdgeIndex = Mod3(baseEdgeIndex - sign);
				newBoundaryEdge = Mod3(baseEdgeIndex + sign);
				const newRightVertex = new BoundaryVertex(rightVertex.index.plus(1));
				rightVertices.push(newRightVertex);
				const pathEdge = new PathEdge(triangle.edge(newBoundaryEdge), rightVertex, newRightVertex);
				rightEdges.push(pathEdge);
				rightVertex.outgoingEdges.push(pathEdge);
				newRightVertex.incomingEdges.push(pathEdge);
				if (pathEdge.reflectedEdge.index == 0) {
					baseEdges.push(pathEdge);
				}
				rightVertex = newRightVertex;
			} else {
				newBaseEdgeIndex = Mod3(baseEdgeIndex + sign);
				newBoundaryEdge = Mod3(baseEdgeIndex - sign);
				const newLeftVertex = new BoundaryVertex(leftVertex.index.plus(1));
				leftVertices.push(newLeftVertex);
				const pathEdge = new PathEdge(triangle.edge(newBoundaryEdge).reverse(), leftVertex, newLeftVertex);
				leftEdges.push(pathEdge);
				leftVertex.outgoingEdges.push(pathEdge);
				newLeftVertex.incomingEdges.push(pathEdge);
				if (pathEdge.reflectedEdge.index == 0) {
					baseEdges.push(pathEdge);
				}
				leftVertex = newLeftVertex;
			}
			triangle = triangle.reflectThroughEdgeIndex(newBaseEdgeIndex);
			triangles.push(triangle);
			// triangle.edge returns counterclockwise order, so this edge always
			// points left to right.
			let internalEdge = new PathEdge(triangle.edge(newBaseEdgeIndex), leftVertex, rightVertex);
			const prevInternalEdge = internalEdges[internalEdges.length - 1];
			if (internalEdge.to.index.equals(prevInternalEdge.to.index) || internalEdge.to.index.equals(prevInternalEdge.from.index)) {
				internalEdge = internalEdge.reverse();
			}
			internalEdges.push(internalEdge);
			internalEdge.from.outgoingEdges.push(internalEdge);
			internalEdge.to.incomingEdges.push(internalEdge);
			if (internalEdge.reflectedEdge.index == 0) {
				baseEdges.push(internalEdge);
			}
			sign = -sign;
			baseEdgeIndex = newBaseEdgeIndex;
		}
		this.n = triangles.length - 1;
		this.triangles = triangles;
		this.leftVertices = leftVertices;
		this.rightVertices = rightVertices;
		this.leftEdges = leftEdges;
		this.rightEdges = rightEdges;
		this.internalEdges = internalEdges;
		this.baseEdges = baseEdges;

		const spineEdges: Array<PathEdge> = [];
		let prevBaseEdge = baseEdges[0];
		for (let i = 1; i < baseEdges.length; i++) {
			const baseEdge = baseEdges[i];
			if (baseEdge.from.index.equals(prevBaseEdge.to.index)) {
				spineEdges.push(prevBaseEdge);
			}
			prevBaseEdge = baseEdge;
		}
		if (!spineEdges[spineEdges.length - 1].to.index.equals(BoundaryVertexIndex.left(leftEdges.length))) {
			// We want the base spine to always end at the last left boundary
			// vertex, so its total vector equals the path offset.
			spineEdges.push(prevBaseEdge);
			console.log("Base spine seems wrong? Remember to canonicalize your path");
		}
		spineEdges[0].from.spinePosition = { index: 0, offset: null };
		for (let i = 0; i < spineEdges.length; i++) {
			const spineEdge = spineEdges[i];
			for (let j = 0; j < spineEdge.from.outgoingEdges.length; j++) {
				const edge = spineEdge.from.outgoingEdges[j];
				edge.to.spinePosition = { index: i, offset: edge };
			}
			spineEdge.to.spinePosition = { index: i + 1, offset: null };
		}
		// Handle outgoing of the last vertex.
		const spineVertex = spineEdges[spineEdges.length - 1].to;
		for (let j = 0; j < spineVertex.outgoingEdges.length; j++) {
			const edge = spineVertex.outgoingEdges[j];
			edge.to.spinePosition = { index: spineEdges.length, offset: edge };
		}
		for (let i = 0; i <= leftEdges.length; i++) {
			if (leftVertices[i].spinePosition == null) {
				console.log(`Weird? left ${i} / ${leftVertices.length}`);
			}
		}
		for (let i = 0; i <= rightEdges.length; i++) {
			if (rightVertices[i].spinePosition == null) {
				console.log(`Weird? right ${i} / ${rightVertices.length}`);
			}
		}
		this.spineEdges = spineEdges;

		/*let internalSpine: Array<PathEdge> = [];
		let prevInternalEdge = internalEdges[0];
		for (let i = 1; i < internalEdges.length; i++) {
			let internalEdge = internalEdges[i];
			if (internalEdge.from.index.equals(prevInternalEdge.to.index)) {
				internalSpine.push(prevInternalEdge);
			}
			prevInternalEdge = internalEdge;
		}
		this.internalSpine = internalSpine;*/
		const leftOffsets: Array<BoundaryOffsetFunction> = [];
		const rightOffsets: Array<BoundaryOffsetFunction> = [];
		for (let i = 0; i <= leftEdges.length; i++) {
			leftOffsets.push(this.offsetFunctionForBoundaryVertex(BoundaryVertexIndex.left(i)));
		}
		for (let i = 0; i <= rightEdges.length; i++) {
			rightOffsets.push(this.offsetFunctionForBoundaryVertex(BoundaryVertexIndex.right(i)));
		}
		this.leftOffsets = leftOffsets;
		this.rightOffsets = rightOffsets;
	}

	vertex(index: BoundaryVertexIndex): BoundaryVertex {
		if (index.side == "left") {
			return this.leftVertices[index.index];
		}
		return this.rightVertices[index.index];
	}

	oldInnerProductsForBoundaryVertex(boundaryVertex: BoundaryVertexIndex): Array<ReflectedEdgeInnerProduct> {
		const boundaryEdges = boundaryVertex.side == "left" ? this.leftEdges : this.rightEdges;
		const innerProducts: Array<ReflectedEdgeInnerProduct> = [];
		const vertexEdges: Array<ReflectedEdge> = [];
		if (boundaryVertex.side == "right") {
			// The right side is offset by the base edge, so add that to the
			// vertex's edge sum.
			vertexEdges.push(new ReflectedEdge(0, ReflectedAngle.zero()));
		}
		const offsetEdges: Array<ReflectedEdge> = [];
		for (let i = 0; i < boundaryVertex.index; i++) {
			if (boundaryVertex.side == "right") {
				// On the right side we don't get full cancellation of vertex and
				// offset edges because of the base edge offset, so we add the
				// compensating terms here.
				innerProducts.push(ReflectedEdgeInnerProduct.fromEdges(vertexEdges[0], boundaryEdges[i].reflectedEdge));
			}
			vertexEdges.push(boundaryEdges[i].reflectedEdge);
		}
		for (let i = boundaryVertex.index; i < boundaryEdges.length; i++) {
			offsetEdges.push(boundaryEdges[i].reflectedEdge);
		}
		for (let i = 0; i < vertexEdges.length; i++) {
			for (let j = 0; j < offsetEdges.length; j++) {
				innerProducts.push(ReflectedEdgeInnerProduct.fromEdges(vertexEdges[i], offsetEdges[j]));
			}
		}
		return innerProducts;
	}

	innerProductsForBoundaryVertex(vertexIndex: BoundaryVertexIndex): Array<ReflectedEdgeInnerProduct> {
		const spinePosition = this.vertex(vertexIndex).spinePosition;

		const innerProducts: Array<ReflectedEdgeInnerProduct> = [];
		const vertexEdges: Array<ReflectedEdge> = [];
		const offsetEdges: Array<ReflectedEdge> = [];
		for (let i = 0; i < spinePosition.index; i++) {
			vertexEdges.push(this.spineEdges[i].reflectedEdge);
		}
		if (spinePosition.offset != null) {
			const offset: PathEdge = spinePosition.offset;
			vertexEdges.push(offset.reflectedEdge);
			offsetEdges.push(offset.reflectedEdge.reverse());
		}
		for (let i = spinePosition.index; i < this.spineEdges.length; i++) {
			offsetEdges.push(this.spineEdges[i].reflectedEdge);
		}
		for (let i = 0; i < vertexEdges.length; i++) {
			for (let j = 0; j < offsetEdges.length; j++) {
				innerProducts.push(ReflectedEdgeInnerProduct.fromEdges(vertexEdges[i], offsetEdges[j]));
			}
		}
		return innerProducts;
	}

	offsetFunctionForInnerProducts(innerProducts: Array<ReflectedEdgeInnerProduct>): BoundaryOffsetFunction {
		// One array for each of 6 possible edge pairs.
		const sineEntries: Array<Array<SineEntry>> = [[], [], [], [], [], []];
		for (let i = 0; i < innerProducts.length; i++) {
			const innerProduct: ReflectedEdgeInnerProduct = innerProducts[i];
			sineEntries[innerProduct.edgePair.asInt()].push(SineEntry.fromAngle(innerProduct.angle));
		}
		const sineSums = sineEntries.map(SineSum.fromEntries);
		return new BoundaryOffsetFunction(sineSums);
	}

	offsetFunctionForBoundaryVertex(boundaryVertex: BoundaryVertexIndex): BoundaryOffsetFunction {
		const innerProducts = this.innerProductsForBoundaryVertex(boundaryVertex);
		return this.offsetFunctionForInnerProducts(innerProducts);
	}

	pathRadiusBound(triangle: Triangle2d): number {
		const radii: Array<number> = [];
		for (let i = 0; i < this.leftVertices.length; i++) {
			for (let j = 0; j < this.rightVertices.length; j++) {
				const offset = this.constraintFunction(i, j);
				radii.push(offset.radiusOnTriangle(triangle));
			}
		}
		return Math.min.apply(null, radii);
	}

	reflectedEdgesForSpinePath(from: BoundaryVertex, to: BoundaryVertex): Array<ReflectedEdge> {
		let p1 = from.spinePosition;
		let p2 = to.spinePosition;
		let reversed = false;
		if (p2.index < p1.index) {
			const tmp = p1;
			p1 = p2;
			p2 = tmp;
			reversed = true;
		}
		let edges: Array<ReflectedEdge> = [];
		if (p1.offset != null) {
			edges.push(p1.offset.reflectedEdge.reverse());
		}
		for (let i = p1.index; i < p2.index; i++) {
			edges.push(this.spineEdges[i].reflectedEdge);
		}
		if (p2.offset != null) {
			edges.push(p2.offset.reflectedEdge);
		}
		if (reversed) {
			edges.reverse();
			edges = edges.map(e => e.reverse());
		}
		return edges;
	}

	constraintFunction(i: number, j: number): BoundaryOffsetFunction {
		let p1 = this.leftVertices[i].spinePosition;
		let p2 = this.rightVertices[j].spinePosition;
		let reversed = false;
		if (p2.index < p1.index) {
			const tmp = p1;
			p1 = p2;
			p2 = tmp;
			reversed = true;
		}
		const innerProducts: Array<ReflectedEdgeInnerProduct> = [];
		for (let i = 0; i < this.spineEdges.length; i++) {
			let edge: ReflectedEdge = this.spineEdges[i].reflectedEdge;
			if (reversed) {
				edge = edge.reverse();
			}
			if (p1.offset != null) {
				innerProducts.push(ReflectedEdgeInnerProduct.fromEdges(p1.offset.reflectedEdge, edge));
			}
			if (p2.offset != null) {
				innerProducts.push(ReflectedEdgeInnerProduct.fromEdges(p2.offset.reflectedEdge.reverse(), edge));
			}
			for (let j = p1.index; j < p2.index; j++) {
				innerProducts.push(ReflectedEdgeInnerProduct.fromEdges(this.spineEdges[j].reflectedEdge.reverse(), edge));
			}
		}
		return this.offsetFunctionForInnerProducts(innerProducts);
	}
}

// Stores the sine representation to compute the offset of a single boundary
// vertex in terms of an explicit triangle.
export class BoundaryOffsetFunction {

	sineSums: Array<SineSum>; // One sine sum for each possible EdgePair.

	constructor(sineSums: Array<SineSum>) {
		this.sineSums = sineSums;
	}

	valueOnTriangle(t: Triangle2d): number {
		let total = 0;
		for (let i = 0; i < 6; i++) {
			const pair = EdgePair.fromInt(i);
			const sineSum = this.sineSums[i];
			total += pair.lengthProductOnTriangle(t) * sineSum.valueOnTriangle(t);
		}
		return total;
	}

	// Gives a naive bound on the absolute value of the derivative of this
	// function in terms of the two base angles. Result is with respect to
	// dr, i.e. the magnitude of change in the apex.
	derivativeBound(t: Triangle2d): number {
		let total = 0;
		for (let i = 0; i < 6; i++) {
			const pair = EdgePair.fromInt(i);
			const sineSum = this.sineSums[i];
			const lengthCoeff = pair.lengthProductOnTriangle(t);
			for (let j = 0; j < sineSum.entries.length; j++) {
				const entry = sineSum.entries[j];
				// Component 1: fixed lengths times the SineSum derivatives.
				// (Just use +- 1 as the bound on the actual derivative cos.)
				//angleComponents[0] +=
				total += Math.abs(lengthCoeff * entry.coefficient * entry.angleCoeffs[0]);
				total += //angleComponents[1] +=
				Math.abs(lengthCoeff * entry.coefficient * entry.angleCoeffs[1]);
				// Component 2: fixed sines (approximated as 1) times the
				// length derivatives, using the naive bound of side lengths <= 1.
				if (i == 3 || i == 4) {
					// These are multiplied by A = 1, so they only change by 1.
					total += Math.abs(entry.coefficient);
				} else if (i != 0) {
					total += Math.abs(2 * entry.coefficient);
				}
			}
		}
		return total;
	}

	radiusOnTriangle(triangle: Triangle2d): number {
		const value = this.valueOnTriangle(triangle);
		if (value <= 0) {
			return 0;
		}
		return value / this.derivativeBound(triangle);
	}

	toString(): string {
		const lines: Array<string> = ["BoundaryOffsetFunction("];
		for (let i = 0; i < 6; i++) {
			const sineSum = this.sineSums[i];
			if (sineSum.entries.length == 0) {
				continue;
			}
			lines.push(`  ${EdgePair.fromInt(i).toString()} ${sineSum.toString()}`);
		}
		lines.push(")");
		return lines.join("\n");
	}
}

export const equilateralTriangle = TriangleForApex(Coords2d.fromXY(0.5, Math.sqrt(3) / 2));


import { Coords2d, OffsetCoords2d } from "core/geometry";

import { BilliardsParams } from "./billiards_params";
import { ConvexPolygon, FeasibleVectorRange, LinearConstraint2d, PathSpacePolygonForApex, OrderedPair } from "./feasible_vector_range";


export function FeasibleSegmentsForApex(apex: Coords2d, maxLength: number): Array<FeasibleSegmentTree> {
	const params = new BilliardsParams(apex);
	const root = FeasibleSegmentTree.rootForParams(params);
	let curSegments = [root];
	for (let i = 0; i < maxLength; i++) {
		const newSegments = [];
		for (let j = 0; j < curSegments.length; j++) {
			const segment = curSegments[j];
			const children = segment.children();
			for (let k = 0; k < children.length; k++) {
				newSegments.push(children[k]);
			}
		}
		curSegments = newSegments;
	}
	return curSegments;
}

export function FeasibleSegmentTreeForApex(apex: Coords2d, depth: number): FeasibleSegmentTree {
	const params = new BilliardsParams(apex);
	const root = FeasibleSegmentTree.rootForParams(params);
	let curSegments = [root];
	for (let i = 0; i < depth; i++) {
		const newSegments = [];
		for (let j = 0; j < curSegments.length; j++) {
			const segment = curSegments[j];
			const children = segment.children();
			for (let k = 0; k < children.length; k++) {
				newSegments.push(children[k]);
			}
		}
		curSegments = newSegments;
	}
	return root;
}

// TODO: This is kind of a mess. Currently FanPathNode is used for visualization
// and FeasibleSegmentTree is used for exhaustive search, but really they're
// just different iterations of the same code. Bring everything in line with
// the (new, improved) swift design.
export class FanPathNode {

	params: BilliardsParams;

	// The core state of a FanPathNode is its edge: one FanPathNode equals
	// one base edge in path space, along with extra metadata to track the path
	// that produced it and the possible children / extensions to this edge.
	// Note that FanPathEdge's state includes an orientation, which is how
	// the tree distinguishes between "children" and "flipped children"
	// below.
	edge: FanPathEdge;

	parent: FanPathNode | null | undefined;
	// The length of the fan at this path node, measured in integer rotations by
	// 2 * baseAngle(angleIndex). fanLength = null indicates that this is a root
	// node.
	fanLength: number | null | undefined;

	// The (dual) path region going through this edge and all its ancestors,
	// split along its destination singularity:
	// pathRegions.valueForSign(1) is the region that crosses the singularity
	// on the opposite side as its parent, and thus does not flip.
	// pathRegions.valueForSign(-1) is the region that crosses the singularity
	// on the same side as its parent, and thus flips.
	// These would not be bounded in general, but because we always start from
	// a flip, we always have a bounded initial path space.
	// null indicates no feasible range. at least one element of the pair
	// must be non-null.
	pathRegions: OrderedPair<ConvexPolygon | null | undefined>;

	// The depth of this node from its root.
	depth: number;

	_children: Array<FanPathNode> | null | undefined;

	constructor(params: BilliardsParams, edge: FanPathEdge, parent: FanPathNode | null | undefined, fanLength: number | null | undefined, pathRegions: OrderedPair<ConvexPolygon | null | undefined>) {
		this.params = params;
		this.edge = edge;
		this.parent = parent;
		this.fanLength = fanLength;
		this.pathRegions = pathRegions;
		if (parent == null) {
			this.depth = 0;
		} else {
			this.depth = parent.depth + 1;
		}
	}

	static rootForParams(params: BilliardsParams): FanPathNode {
		const region = PathSpacePolygonForApex(params.apex);
		const regionPair = new OrderedPair(null, region);
		return new FanPathNode(params, FanPathEdge.fromParams(params), null, null, regionPair);
	}

	static rootForApex(apex: Coords2d): FanPathNode {
		const params = new BilliardsParams(apex);
		return FanPathNode.rootForParams(params);
	}

	children(): Array<FanPathNode> {
		if (this._children == null) {
			this._children = this._computeChildren();
		}
		return this._children;
	}

	path(): Array<number> {
		const path = this.parent != null ? this.parent.path() : [];
		if (this.fanLength != null) {
			path.push(this.fanLength);
		}
		return path;
	}

	_computeChildren(): Array<FanPathNode> {
		const noFlipRegion = this.pathRegions.valueForSign(1);
		if (noFlipRegion == null) {
			return [];
		}
		// For now don't do any flips.
		let pathRegion: ConvexPolygon = noFlipRegion;
		const children: Array<FanPathNode> = [];
		const angleIndex = this.edge.destIndex();
		// We alternate angles each step, starting by convention at angle 1.
		const maxFanLength = this.params.maxFanLength(angleIndex);

		// The boundary sign is the sign of the constraint for the
		// singularity we're rotating around.
		// E.g. in the standard starting position, the edge moves from
		// v0 to v1 (vectorSign 1) with edge.rotationSign = 1, and the
		// singularity is on the lower boundary, so boundarySign = 1.
		const boundarySign = this.edge.rotationSign * this.edge.vectorSign;

		for (let fanLength = 1; fanLength <= maxFanLength; fanLength++) {
			const edge = this.edge.copy();
			edge.advance(fanLength);

			const fanVertex = edge.apex();
			const fanConstraint = LinearConstraint2d.fromDualCoords(fanVertex, boundarySign);
			const regions = pathRegion.splitByConstraint(fanConstraint).pairForSign(1);
			const childRegion = regions[1];
			const remainder = regions[0];
			if (childRegion != null) {
				// There is a child with this fan length.
				const nextSingularity = edge.destCoords();
				// For there to be no flip, the next singularity has to lie on the
				// opposite boundary sign as this one.
				const singularityConstraint = LinearConstraint2d.fromDualCoords(nextSingularity, -boundarySign);
				const childRegions = childRegion.splitByConstraint(singularityConstraint);
				const child = new FanPathNode(this.params, edge, this, fanLength, childRegions);
				children.push(child);
			}
			if (remainder == null) {
				// There is nothing left to process.
				break;
			}
			pathRegion = remainder;
		}
		return children;
	}
}

// Tracks the space of feasible path segments for a given apex.
export class FeasibleSegmentTree {


	params: BilliardsParams;

	//fanLengths: Array<number>;
	fanLength: number | null | undefined;
	angleIndex: number;
	enteringEdge: FanPathEdge;
	vectorRange: FeasibleVectorRange;
	parent: FeasibleSegmentTree | null | undefined;
	depth: number;

	_children: Array<FeasibleSegmentTree> | null | undefined;

	constructor(params: BilliardsParams, parent: FeasibleSegmentTree | null | undefined, fanLength: number | null | undefined, angleIndex: number, enteringEdge: FanPathEdge, vectorRange: FeasibleVectorRange) {
		this.params = params;
		this.parent = parent;
		this.fanLength = fanLength;
		this.angleIndex = angleIndex;
		this.enteringEdge = enteringEdge;
		this.vectorRange = vectorRange;
		if (parent == null) {
			this.depth = 0;
		} else {
			this.depth = parent.depth + 1;
		}
	}

	static rootForParams(params: BilliardsParams): FeasibleSegmentTree {
		return new FeasibleSegmentTree(params, null, null, 0, FanPathEdge.fromParams(params), FeasibleVectorRange.fromParams(params));
	}

	static rootForApex(apex: Coords2d): FeasibleSegmentTree {
		const params = new BilliardsParams(apex);
		return FeasibleSegmentTree.rootForParams(params);
	}

	children(): Array<FeasibleSegmentTree> {
		if (this._children == null) {
			this._children = this._computeChildren();
		}
		return this._children;
	}

	path(): Array<number> {
		const path = this.parent != null ? this.parent.path() : [];
		if (this.fanLength != null) {
			path.push(this.fanLength);
		}
		return path;
	}

	_computeChildForFanLength(fanLength: number): FeasibleSegmentTree | null | undefined {
		const vectorRange = this.vectorRange.copy();

		const childEnteringEdge = this.enteringEdge.copy();
		childEnteringEdge.advance(fanLength);

		// The boundary sign of the vertex closer to the singularity.
		// A feasible monotonic segment always has angles on consistent sides, and
		// for this search we use the convention that angle 0 is on the
		// top and angle 1 is on the bottom.
		const nearSign = this.enteringEdge.vectorSign;

		// "near" means on the same boundary side as the singularity we're
		// currently crossing.
		const nearVertex = childEnteringEdge.apex();
		const farVertex = childEnteringEdge.destCoords();

		vectorRange.addBoundaryVertex(nearVertex, nearSign);
		vectorRange.addBoundaryVertex(farVertex, -nearSign);

		if (vectorRange.isEmpty()) {
			return null;
		}
		return new FeasibleSegmentTree(this.params, this, fanLength, 1 - this.angleIndex, childEnteringEdge, vectorRange);
	}

	_computeChildren(): Array<FeasibleSegmentTree> {
		const children: Array<FeasibleSegmentTree> = [];
		// We alternate angles each step, starting by convention at angle 1.
		const childAngleIndex = 1 - this.angleIndex;
		const maxFanLength = this.params.maxFanLength(childAngleIndex);
		for (let fanLength = 1; fanLength <= maxFanLength; fanLength++) {
			const vectorRange = this.vectorRange.copy();

			const childEnteringEdge = this.enteringEdge.copy();
			childEnteringEdge.advance(fanLength);

			// The boundary sign of the vertex closer to the singularity.
			// A feasible monotonic segment always has angles on consistent sides, and
			// for this search we use the convention that angle 0 is on the
			// top and angle 1 is on the bottom.
			const nearSign = this.enteringEdge.vectorSign;

			// "near" means on the same boundary side as the singularity we're
			// currently crossing.
			const nearVertex = childEnteringEdge.apex();
			const farVertex = childEnteringEdge.destCoords();

			vectorRange.addBoundaryVertex(nearVertex, nearSign);
			vectorRange.addBoundaryVertex(farVertex, -nearSign);

			if (!vectorRange.isEmpty()) {
				const child = new FeasibleSegmentTree(this.params, this, fanLength, childAngleIndex, childEnteringEdge, vectorRange);
				children.push(child);
			}
		}
		return children;
	}
}

export class FanPathEdge {
	// the sign of edge motion (v0 to v1 is 1, v1 to v0 is -1).
	vectorSign: number;
	// The coords of (the singularity corresponding to) vertex 0.
	v0: Coords2d;
	// The coords of (the singularity corresponding to) vertex 1.
	v1: Coords2d;

	// Whether current motion is up (1) or down (-1) relative to the triangle's
	// initial orientation.
	rotationSign: number;
	rotationCounts: Array<number>;

	_params: BilliardsParams;

	constructor(params: BilliardsParams, v0: Coords2d, v1: Coords2d, vectorSign: number, rotationSign: number, rotationCounts: Array<number>) {
		this._params = params;
		this.v0 = v0.copy();
		this.v1 = v1.copy();
		this.vectorSign = vectorSign;
		this.rotationSign = rotationSign;
		this.rotationCounts = [rotationCounts[0], rotationCounts[1]];
	}

	static fromParams(params: BilliardsParams): FanPathEdge {
		return new FanPathEdge(params, Coords2d.origin(), Coords2d.fromXY(1, 0), 1, 1, [0, 0]);
	}

	copy(): FanPathEdge {
		return new FanPathEdge(
			this._params, this.v0, this.v1,
			this.vectorSign, this.rotationSign, this.rotationCounts);
	}

	flip(): void {
		this.rotationSign = -this.rotationSign;
	}

	sourceCoords(): Coords2d {
		if (this.vectorSign >= 0) {
			return this.v0.copy();
		}
		return this.v1.copy();
	}

	destCoords(): Coords2d {
		if (this.vectorSign >= 0) {
			return this.v1.copy();
		}
		return this.v0.copy();
	}

	// The vertex index of the singularity this edge is moving from.
	sourceIndex(): number {
		if (this.vectorSign >= 0) {
			return 0;
		}
		return 1;
	}

	// The vertex index of the singularity this edge is moving to.
	destIndex(): number {
		if (this.vectorSign >= 0) {
			return 1;
		}
		return 0;
	}

	advance(count: number): void {
		const rotationIndex = this.destIndex();
		let rotationCoeff = this._params.vectorForRotation(rotationIndex, count);
		if (this.rotationSign < 0) {
			rotationCoeff = rotationCoeff.conjugate();
		}
		if (this.vectorSign >= 0) {
			const offset = this.v0.asOffsetFrom(this.v1);
			this.v0 = this.v1.plus(offset.times(rotationCoeff));
		} else {
			const offset = this.v1.asOffsetFrom(this.v0);
			this.v1 = this.v0.plus(offset.times(rotationCoeff));
		}
		this.rotationCounts[rotationIndex] += count * this.rotationSign;
		this.vectorSign = -this.vectorSign;
	}

	apex(): Coords2d {
		return this.apexForSign(1);
	}

	conjugateApex(): Coords2d {
		return this.apexForSign(-1);
	}

	apexForSign(sign: number): Coords2d {
		let apexCoords = this._params.apex.asOffsetFromOrigin();
		const offset = this.v1.asOffsetFrom(this.v0);
		if (this.rotationSign * sign < 0) {
			apexCoords = apexCoords.conjugate();
		}
		return this.v0.plus(apexCoords.times(offset));
	}

	vertex(vertexIndex: number): Coords2d {
		if (vertexIndex == 0) {
			return this.v0.copy();
		}
		return this.v1.copy();
	}
}

export class BaseAngle {

	coefficients: Array<number>;

	constructor(coefficients: Array<number>) {
		this.coefficients = coefficients;
	}

	plus(angleIndex: number, coeff: number): BaseAngle {
		const c = [this.coefficients[0], this.coefficients[1]];
		c[angleIndex] += coeff;
		return new BaseAngle(c);
	}

	vectorForParams(params: BilliardsParams): OffsetCoords2d {
		return params.vectorForRotationCoefficients(this.coefficients[0], this.coefficients[1]);
	}

	apexVectorForParams(params: BilliardsParams, conjugate: boolean): OffsetCoords2d {
		const vec = this.vectorForParams(params);
		let apex = params.apex.asOffsetFromOrigin();
		if (conjugate) {
			apex = apex.conjugate();
		}
		return vec.times(apex);
	}
}

export class Fan {

	startAngle: BaseAngle;
	length: number;

	segment: FanPathSegment;
	segmentIndex: number;

	constructor(segment: FanPathSegment, segmentIndex: number, length: number) {
		this.segment = segment;
		this.segmentIndex = segmentIndex;

		this.startAngle = new BaseAngle([0, 0]);
		this.length = length;
	}

	// Whether this fan is around base angle 0 or 1.
	angleIndex(): number {
		return (this.segment.firstAngleIndex() + this.segmentIndex) % 2;
	}

	// Whether this fan rotates upward or downward relative to the base triangle.
	rotationSign(): number {
		return 1 - 2 * (this.segment.pathIndex % 2);
	}
}

// A FanPathSegment is a sequence of fans with the same orientation,
// where "orientation" means whether reflections are proceeding
// "upward" or "downward" from the original triangle (upward = counterclockwise
// rotation around a0 or clockwise rotation around a1).
// A FanPathSegment begins and ends at a boundary spine edge.
export class FanPathSegment {

	fans: Array<Fan>;

	path: FanPath;
	pathIndex: number;

	constructor(path: FanPath, pathIndex: number, fanLengths: Array<number>) {
		this.path = path;
		this.pathIndex = pathIndex;

		const fans: Array<Fan> = [];
		for (let i = 0; i < fanLengths.length; i++) {
			const fan = new Fan(this, i, fanLengths[i]);
			fans.push(fan);
		}
		this.fans = fans;
	}

	// Whether the first fan rotates around base angle 0 or 1.
	firstAngleIndex(): number {
		return 1;
	}
}

// A FanPath represents a cyclic path as a sequence of fans.
export class FanPath {

	segments: Array<FanPathSegment>;

	// Each entry of fanLengths is a path segment. Each entry of the path
	// segment array is the length of a fan.
	constructor(fanLengths: Array<Array<number>>) {
		const segments: Array<FanPathSegment> = [];
		for (let i = 0; i < fanLengths.length; i++) {
			const segment = new FanPathSegment(this, i, fanLengths[i]);
			segments.push(segment);
		}
		this.segments = segments;
	}

	static fromString(s: string): FanPath {
		const fanLengths = s.split("|").map(segmentStr => {
			return segmentStr.trim().split(" ").map(fanStr => parseInt(fanStr));
		});
		return new FanPath(fanLengths);
	}
}
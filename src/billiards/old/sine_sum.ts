

import { Triangle2d } from "core/polygon";

import { ReflectedAngle } from "./reflected_triangle";

function sineEntryCompare(a: SineEntry, b: SineEntry): number {
	if (a.angleCoeffs[1] < b.angleCoeffs[1]) {
		return -1;
	}
	if (a.angleCoeffs[1] > b.angleCoeffs[1]) {
		return 1;
	}
	if (a.angleCoeffs[0] < b.angleCoeffs[0]) {
		return -1;
	}
	if (a.angleCoeffs[0] > b.angleCoeffs[0]) {
		return 1;
	}
	return 0;
}

// Represents a particular sine wave in the configuration.
// Given (t: Triangle2d), this corresponds to the function:
// coefficient * sin(
//   angleCoeffs[0] * t.angleAtVertex(0) +
//   angleCoeffs[1] * t.angleAtVertex(1)
// )
// Note that this function is invariant under the mappings
//   (angles: [number, number]) => [-angles[0], -angles[1]]
//   (coefficient: number) => -coefficient
// so in the constructor we select for the one that makes angles[0] positive,
// or the one that makes angles[1] positive if angles[0] is zero.
export class SineEntry {

	coefficient: number;
	// Invariants:
	// - angleCoeffs[0] >= 0
	// - If angleCoeffs[0] == 0, then angleCoeffs[1] >= 0
	angleCoeffs: [number, number];

	constructor(coefficient: number, angleCoeffs: [number, number]) {
		if (angleCoeffs[0] < 0 || (angleCoeffs[0] == 0 && angleCoeffs[1] < 0)) {
			this.coefficient = -coefficient;
			this.angleCoeffs = [-angleCoeffs[0], -angleCoeffs[1]];
		} else {
			this.coefficient = coefficient;
			this.angleCoeffs = [angleCoeffs[0], angleCoeffs[1]];
		}
	}

	static fromAngle(angle: ReflectedAngle): SineEntry {
		return new SineEntry(angle.reversed ? -1 : 1, [angle.coefficients[0], angle.coefficients[1]]);
	}

	valueOnTriangle(t: Triangle2d): number {
		const angle = this.angleCoeffs[0] * t.angleAtVertex(0) + this.angleCoeffs[1] * t.angleAtVertex(1);
		return this.coefficient * Math.sin(angle);
	}

	valueOnAngles(a0: number, a1: number): number {
		const angle = this.angleCoeffs[0] * a0 + this.angleCoeffs[1] * a1;
		return this.coefficient * Math.sin(angle);
	}

	toString(): string {
		let coefficient = "";
		if (this.coefficient != 1) {
			if (this.coefficient == -1) {
				coefficient = "-";
			} else {
				coefficient = `${this.coefficient.toString()} `;
			}
		}
		return `${coefficient}sin(${this.angleCoeffs[0]}, ${this.angleCoeffs[1]})`;
	}
}

function CoalesceSineEntries(entries: Array<SineEntry>): Array<SineEntry> {
	entries.sort(sineEntryCompare);
	const mergedEntries: Array<SineEntry> = [];
	let curEntry: SineEntry | null | undefined = null;
	let curCoeff = 0;
	for (let i = 0; i < entries.length; i++) {
		const entry = entries[i];
		if (entry.angleCoeffs[0] == 0 && entry.angleCoeffs[1] == 0) {
			continue;
		}
		if (curEntry == null || sineEntryCompare(curEntry, entry) != 0) {
			if (curEntry != null && curCoeff != 0) {
				mergedEntries.push(new SineEntry(curCoeff, curEntry.angleCoeffs));
			}
			curEntry = entry;
			curCoeff = entry.coefficient;
		} else {
			curCoeff += entry.coefficient;
		}
	}
	if (curEntry != null && curCoeff != 0) {
		mergedEntries.push(new SineEntry(curCoeff, curEntry.angleCoeffs));
	}
	return mergedEntries;
}

export class SineSum {

	entries: Array<SineEntry>;

	constructor(entries: Array<SineEntry>) {
		this.entries = CoalesceSineEntries(entries);
	}

	// Takes a set of angles and returns a SineSum representing the function:
	// (t: Triangle2d) => {
	//   sum( (angle: Angle in angles) => {
	//     sin(angle.angleCoeffs[0] * t.angleAtVertex(0) +
	//         angle.angleCoeffs[1] * t.angleAtVertex(1))
	//   })
	// }
	// in the form:
	// (t: Triangle2d) => {
	//   sum( )
	// }
	static fromEntries(sineEntries: Array<SineEntry>): SineSum {
		return new SineSum(sineEntries);
	}

	valueOnTriangle(t: Triangle2d): number {
		let total = 0;
		for (let i = 0; i < this.entries.length; i++) {
			total += this.entries[i].valueOnTriangle(t);
		}
		return total;
	}

	valueOnAngles(a0: number, a1: number): number {
		let total = 0;
		for (let i = 0; i < this.entries.length; i++) {
			total += this.entries[i].valueOnAngles(a0, a1);
		}
		return total;
	}

	toString(): string {
		// First sort by angles[1] (which has the most dynamic range
		// since it's the smallest angle).
		let index = 0;
		const betaStrings: Array<string> = [];
		while (index < this.entries.length) {
			const beta = this.entries[index].angleCoeffs[1];
			const betaString = beta.toString();
			const alphaStrings: Array<string> = [];
			while (index < this.entries.length && this.entries[index].angleCoeffs[1] == beta) {
				const alpha = this.entries[index].angleCoeffs[0];
				let alphaStr = alpha.toString();
				const coeff = this.entries[index].coefficient;
				if (coeff != 1) {
					alphaStr = `${alphaStr}<${coeff.toString()}>`;
				}
				alphaStrings.push(alphaStr);
				index += 1;
			}
			const alphaString = alphaStrings.join(" ");

			const mergedString = `SineEntry(${alphaString}, ${betaString})`;
			betaStrings.push("  " + mergedString);
		}
		if (betaStrings.length == 0) {
			return "SineSum()";
		}
		return "SineSum(\n" + betaStrings.join("\n") + ")";
	}
}
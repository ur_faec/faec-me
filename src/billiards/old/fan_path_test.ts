

import { Coords2d } from "core/geometry";

import { BilliardsParams } from "./billiards_params";
import { FeasibleSegmentsForApex, FeasibleSegmentTree } from "./fan_path";
import { FeasibleVectorRange, LinearConstraint2d } from "./feasible_vector_range";

// Given a line in the XY plane (ah + bv + c = 0), returns the coordinates of
// that line in the dual space used by FeasibleVectorRange.
export function DualForLineCoefficients(a: number, b: number, c: number): Coords2d {
	const h = (c - a) / b;
	const v = a / b;
	return Coords2d.fromXY(h, v);
}

// Given a line in the XY plane (y = slope * x + intercept), returns the
// coordinates of that line in the dual space used by FeasibleVectorRange.
export function DualForSlopeIntercept(slope: number, intercept: number): Coords2d {
	const constraint = LinearConstraint2d.fromSlopeAndIntercept(slope, intercept);
	return DualForLineCoefficients(constraint.a, constraint.b, constraint.c);
}

export function TryFeasibleVectorRange(): FeasibleVectorRange {

	const apex = Coords2d.fromXY(0.5, 0.4);
	const params = new BilliardsParams(apex);
	const vr = FeasibleVectorRange.fromParams(params);

	const containedCoords = DualForLineCoefficients(0, 1, -0.3);
	if (!vr.contains(containedCoords)) {
		console.log("Didn't contain point?", containedCoords);
		return vr;
	}
	const exteriorCoords = DualForLineCoefficients(0, 1, -0.5);
	if (vr.contains(exteriorCoords)) {
		console.log("Contained point?", exteriorCoords);
		return vr;
	}
	let reachableCoords = Coords2d.fromXY(4, 0.4);
	if (!vr.areCoordsReachable(reachableCoords)) {
		console.log("Coords not reachable?", reachableCoords);
	}

	vr.addBoundaryVertex(Coords2d.fromXY(4, 0.35), -1);
	if (!vr.contains(containedCoords)) {
		console.log("Didn't contain point?", containedCoords);
		return vr;
	}
	if (vr.contains(exteriorCoords)) {
		console.log("Contained point?", exteriorCoords);
		return vr;
	}
	if (vr.areCoordsReachable(reachableCoords)) {
		console.log("Coords reachable?", reachableCoords);
	}

	vr.addBoundaryVertex(Coords2d.fromXY(8, 0.25), 1);
	if (!vr.contains(containedCoords)) {
		console.log("Didn't contain point?", containedCoords);
		return vr;
	}
	if (vr.contains(exteriorCoords)) {
		console.log("Contained point?", exteriorCoords);
		return vr;
	}

	reachableCoords = Coords2d.fromXY(9, 0.2);
	if (vr.areCoordsReachable(reachableCoords)) {
		console.log("Coords reachable?", reachableCoords);
		return vr;
	}
	reachableCoords = Coords2d.fromXY(11, 0.26);
	if (!vr.areCoordsReachable(reachableCoords)) {
		console.log("Coords not reachable?", reachableCoords);
		return vr;
	}
	const crossedEdge = [Coords2d.fromXY(10, -1), Coords2d.fromXY(11, 0.26)];
	const nonCrossedEdge = [Coords2d.fromXY(8, 0.2), Coords2d.fromXY(9, 0.2)];
	if (vr.crossesSegment(nonCrossedEdge[0], nonCrossedEdge[1])) {
		console.log("Crossed segment?", nonCrossedEdge);
		return vr;
	}
	if (!vr.crossesSegment(crossedEdge[0], crossedEdge[1])) {
		console.log("Doesn't cross segment?", crossedEdge);
		return vr;
	}
	return vr;
	//PCoords2d.fromLineCoefficients();
}

export function TryFeasibleSegments(): Array<FeasibleSegmentTree> {
	const apex = Coords2d.fromXY(0.5, 0.4);
	return FeasibleSegmentsForApex(apex, 2);
}
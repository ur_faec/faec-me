

import { Coords2d } from "core/geometry";
import { Triangle2d } from "core/polygon";

import { EdgePath } from "./edge_path";

export type BoundaryCoords = {
	leftBoundary: Array<Coords2d>;
	rightBoundary: Array<Coords2d>;
};

export type BoundaryOffsets = {
	leftOffsets: Array<number>;
	rightOffsets: Array<number>;
};

function Mod3(n: number): number {
	return (n % 3 + 3) % 3;
}

export function BoundaryCoordsForPathAndTriangle(edgePath: EdgePath, t: Triangle2d): BoundaryCoords {
	const leftBoundary: Array<Coords2d> = [t.vertexCoords[0].copy()];
	const rightBoundary: Array<Coords2d> = [t.vertexCoords[1].copy()];
	let baseEdge = 0;
	let sign = 1;
	const tri = t.copy();
	for (let i = 0; i < edgePath.str.length; i++) {
		const turn = edgePath.str[i];
		const turnOffset = turn == "L" ? -1 : 1;
		const newBase = Mod3(baseEdge + sign * turnOffset);
		const boundaryVert = Mod3(baseEdge + 2);
		if (turn == "L") {
			rightBoundary.push(tri.vertexCoords[boundaryVert].copy());
		} else {
			leftBoundary.push(tri.vertexCoords[boundaryVert].copy());
		}
		tri.reflectThroughEdge(newBase);
		baseEdge = newBase;
		sign = -sign;
	}
	return { leftBoundary, rightBoundary };
}

export function BoundaryOffsetsForPathAndTriangle(edgePath: EdgePath, t: Triangle2d): BoundaryOffsets {
	const {
		leftBoundary,
		rightBoundary,
	} = BoundaryCoordsForPathAndTriangle(edgePath, t);
	const totalOffset = leftBoundary[leftBoundary.length - 1].asOffsetFrom(leftBoundary[0]);
	const totalCross = totalOffset.cross();
	const leftOffsets: Array<number> = [];
	const rightOffsets: Array<number> = [];
	for (let i = 0; i < leftBoundary.length; i++) {
		const curOffset = leftBoundary[i].asOffsetFrom(leftBoundary[0]);
		leftOffsets.push(curOffset.dot(totalCross));
	}
	for (let i = 0; i < rightBoundary.length; i++) {
		const curOffset = rightBoundary[i].asOffsetFrom(leftBoundary[0]);
		rightOffsets.push(curOffset.dot(totalCross));
	}
	return { leftOffsets, rightOffsets };
}
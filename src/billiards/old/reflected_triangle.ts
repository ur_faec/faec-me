

import { OffsetCoords2d } from "core/geometry";
import { Triangle2d } from "core/polygon";

import { BilliardsParams, DReal, DTCoords2d } from "./billiards_params";

function Xor(a: boolean, b: boolean): boolean {
	return (a && !b) || (b && !a);
}

// Encodes an angle obtained from the edge of a triangle after repeatedly
// reflecting it through its sides. The initial triangle base is considered
// zero, all other angles are measured relative to it as linear multiples of
// the base angles
export class ReflectedAngle {
	// Integer coefficients of the angles in a triangle.
	coefficients: [number, number];
	// If reversed is true, the real angle is
	// (coefficients[0] + 1) * tri.angleAtVertex(0) +
	//   (coefficients[1] + 1) * tri.angleAtVertex(1) +
	//   tri.angleAtVertex(2) =
	// coefficients[0] * tri.angleAtVertex(0) +
	//   coefficients[1] * tri.angleAtVertex(1) +
	//   pi
	reversed: boolean;

	constructor(coefficients: [number, number], reversed: boolean) {
		this.coefficients = [coefficients[0], coefficients[1]];
		this.reversed = reversed;
	}

	static zero(): ReflectedAngle {
		return new ReflectedAngle([0, 0], false);
	}

	valueOnTriangle(t: Triangle2d): number {
		const offset = this.reversed ? Math.PI : 0;
		return this.coefficients[0] * t.angleAtVertex(0) + this.coefficients[1] * t.angleAtVertex(1) + offset;
	}

	valueOnParams(params: BilliardsParams): DReal {
		const a0 = params.angleAtBaseVertex(0);
		const a1 = params.angleAtBaseVertex(1);
		const offset = this.reversed ? Math.PI : 0;
		return a0.timesConstant(this.coefficients[0]).plus(a1.timesConstant(this.coefficients[1])).plusConstant(offset);
	}

	plus(c1: number, c2: number): ReflectedAngle {
		return new ReflectedAngle([this.coefficients[0] + c1, this.coefficients[1] + c2], this.reversed);
	}

	reverse(): ReflectedAngle {
		return new ReflectedAngle([this.coefficients[0], this.coefficients[1]], !this.reversed);
	}

	minus(a: ReflectedAngle): ReflectedAngle {
		const c: [number, number] = [this.coefficients[0] - a.coefficients[0], this.coefficients[1] - a.coefficients[1]];
		return new ReflectedAngle(c, Xor(this.reversed, a.reversed));
	}

	toString(): string {
		const c = this.coefficients;
		return `ReflectedAngle([${c[0]}, ${c[1]}], ${this.reversed.toString()})`;
	}
}

export class ReflectedEdge {

	index: number; // Edge index in the original triangle.
	angle: ReflectedAngle;

	constructor(index: number, angle: ReflectedAngle) {
		this.index = index;
		this.angle = angle;
	}

	reverse(): ReflectedEdge {
		return new ReflectedEdge(this.index, this.angle.reverse());
	}

	valueOnTriangle(t: Triangle2d): OffsetCoords2d {
		return OffsetCoords2d.fromPolar(t.edge(this.index).length(), this.angle.valueOnTriangle(t));
	}

	valueOnParams(params: BilliardsParams): DTCoords2d {
		const angle = this.angle.valueOnParams(params);
		const length = params.edgeLengthAtIndex(this.index);
		return DTCoords2d.fromPolar(length, angle);
	}

	toString(): string {
		return `ReflectedEdge(${this.index.toString()}, ${this.angle.toString()})`;
	}
}

// A structure that tracks repeated reflections of a triangle in terms of
// the angle offset from the original, and can return the ReflectedAngle of its
// edges.
export class ReflectedTriangle {

	baseAngle: ReflectedAngle;
	neighbors: Array<ReflectedTriangle | null>;
	reflectedFrom: number | null | undefined; // The edge index we came from.

	constructor(baseAngle: ReflectedAngle) {
		this.baseAngle = baseAngle;
		this.neighbors = [null, null, null];
	}

	static zero(): ReflectedTriangle {
		return new ReflectedTriangle(ReflectedAngle.zero());
	}

	// Returns the angle of the given edge index, oriented counterclockwise
	// around the triangle.
	// Note that this means if the triangle has been reflected an odd number of
	// times the given angle will point "backwards" in the vertices, e.g.
	// edgeAngle(0) will point from vertexCoords[1] to vertexCoords[0].
	edgeAngle(edgeIndex: number): ReflectedAngle {
		if (edgeIndex == 0) {
			return this.baseAngle;
		}
		const sign = this.baseAngle.reversed ? -1 : 1;
		if (edgeIndex == 1) {
			return this.baseAngle.reverse().plus(0, -sign);
		}
		// edgeIndex == 2
		return this.baseAngle.plus(sign, 0).reverse();
	}

	edge(edgeIndex: number): ReflectedEdge {
		return new ReflectedEdge(edgeIndex, this.edgeAngle(edgeIndex));
	}

	reflectThroughEdgeIndex(edgeIndex: number): ReflectedTriangle {
		const neighbor = this.neighbors[edgeIndex];
		if (neighbor != null) {
			return neighbor;
		}
		let angle: ReflectedAngle;
		const sign = this.baseAngle.reversed ? -1 : 1;
		if (edgeIndex == 0) {
			angle = this.baseAngle.reverse();
		} else if (edgeIndex == 1) {
			angle = this.baseAngle.plus(0, -2 * sign).reverse();
		} else {
			angle = this.baseAngle.plus(2 * sign, 0).reverse();
		}
		const tri = new ReflectedTriangle(angle);
		tri.reflectedFrom = edgeIndex;
		tri.neighbors[edgeIndex] = this;
		this.neighbors[edgeIndex] = tri;
		return tri;
	}

	toString(): string {
		return `ReflectedTriangle(${this.baseAngle.toString()})`;
	}
}


import { RenderTarget } from "core/canvas";
import { ElementsView } from "core/elements_view";
import { Renderable, ImageRendering, Line } from "core/element_render";
import { Coords2d } from "core/geometry";
import { Observable } from "core/observable";
import { AxisRect, Size } from "core/polygon";

import { BilliardsParams } from "./billiards_params";

function blur(src: Float32Array, dest: Float32Array, size: Size) {
	const {
		width,
		height,
	} = size;
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			const index = y * width + x;
			let corners = 0;
			let sides = 0;
			if (x > 0) {
				if (y > 0) {
					corners += src[index - width - 1];
				}
				sides += src[index - 1];
				if (y + 1 < height) {
					corners += src[index + width - 1] / 9;
				}
			}
			if (x + 1 < width) {
				if (y > 0) {
					corners += src[index - width + 1];
				}
				sides += src[index + 1];
				if (y + 1 < height) {
					corners += src[index + width + 1];
				}
			}
			if (y > 0) {
				sides += src[index - width];
			}
			if (y + 1 < height) {
				sides += src[index + width];
			}
			dest[index] = (src[index] + sides / 2 + corners / 4) / 4;
		}
	}
}

function affineArrayMap(src: Float32Array, dest: Float32Array, coeff: number, delta: number) {
	for (let i = 0; i < src.length; i++) {
		dest[i] = coeff * src[i] + delta;
	}
}

export class BilliardsFlowModel extends ElementsView {

	apex: Observable<Coords2d>;
	billiardsParams: BilliardsParams;
	gridSize: Size;
	grid: Float32Array;

	constructor(renderTarget: RenderTarget, apex: Observable<Coords2d>, gridSize: Size) {
		super(renderTarget);

		this.apex = apex;
		this.billiardsParams = new BilliardsParams(apex.value);
		this.renderer.modelBounds = AxisRect.fromXYWH(0, 0, 1, 1);
		this.gridSize = gridSize;
		this.grid = this.computeThingie();

		apex.addObserver((oldValue, newValue) => {
			this.billiardsParams = new BilliardsParams(newValue);
			this._needsRedraw = true;
		});
	}

	phaseMap(position: number, angle: number, discIndex: number): {position: number;angle: number;} {
		return {
			position: position * position,
			angle: angle,
		};
	}

	computeThingie(): Float32Array {
		const interpolationFactor = 0.7;
		const regularization = 0.1;
		const weightCoeff = 1 - interpolationFactor - regularization;
		const angleCount = this.gridSize.height;
		const positionCount = this.gridSize.width;

		const domain = new Float32Array(angleCount * positionCount);
		const codomain = new Float32Array(angleCount * positionCount);
		domain.fill(1);
		for (let i = 0; i < 10; i++) {
			affineArrayMap(domain, codomain, interpolationFactor, regularization);
			for (let fromAngleIndex = 0; fromAngleIndex < angleCount; fromAngleIndex++) {
				const fromAngle = Math.PI * (fromAngleIndex + 0.5) / angleCount;
				for (let fromPositionIndex = 0; fromPositionIndex < positionCount; fromPositionIndex++) {
					const fromPosition = (fromPositionIndex + 0.5) / positionCount;
					const fromDensity = weightCoeff * domain[fromAngleIndex * positionCount + fromPositionIndex];

					const {
						position,
						angle,
					} = this.phaseMap(fromPosition, fromAngle, i % 2);
					const toPositionIndex = Math.floor(positionCount * position);
					const toAngleIndex = Math.floor(angleCount * angle / Math.PI);
					codomain[toAngleIndex * positionCount + toPositionIndex] += fromDensity;
				}
			}
			// Write the codomain density over the domain with a blur factor.
			blur(codomain, domain, this.gridSize);
		}
		return domain;
	}

	elementsToRender(frameRect: AxisRect): Array<Renderable> {
		console.log("BilliardsFlowModel.elementsToRender");

		const image = new ImageRendering(AxisRect.fromXYWH(0, 0, 1, 1), this.gridSize, (imageData: ImageData) => {
			const width = this.gridSize.width;
			const height = this.gridSize.height;
			//let renderScale = this.renderTarget.canvas.renderScale;
			for (let y = 0; y < height; y++) {
				for (let x = 0; x < width; x++) {
					const val = Math.floor(255.9 * this.grid[y * width + x]);


					const offset = y * width + x;
					//let modelCoords = pixelSpaceToModelSpace.transformCoords(
					//  Coords2d.fromXY(x / renderScale, y / renderScale));
					imageData.data[offset * 4 + 0] = val;
					imageData.data[offset * 4 + 1] = 0;
					imageData.data[offset * 4 + 2] = val;
					imageData.data[offset * 4 + 3] = 255;
				}
			}
		});

		const line = new Line(Coords2d.fromXY(0, 0), Coords2d.fromXY(1, 0));


		return [image, line];
	}
}
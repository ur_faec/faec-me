

import { RenderTarget } from "core/canvas";
import { Renderable, Line, Polygon, RenderableElementsRenderer,
	RenderableElementsRendererParams } from "core/element_render";
import { Coords2d, TangentCoords2d } from "core/geometry";
import { Observable } from "core/observable";
import { AxisRect, Triangle2d } from "core/polygon";

import { BilliardsParams } from "./billiards_params";
import { FanPathEdge, Fan, FanPath, FanPathSegment, FanPathNode } from "./fan_path";

export class BilliardsPathView {

	renderTarget: RenderTarget;
	renderer: RenderableElementsRenderer;
	renderParams: RenderableElementsRendererParams;

	_needsRedraw: boolean;

	_showSource: boolean;
	_unfoldingViewSize: number;

	_fanPath: FanPath | null = null;

	_apex: Observable<Coords2d>;
	_source: Observable<TangentCoords2d> | null;

	constructor(renderTarget: RenderTarget, apex: Observable<Coords2d>, source: Observable<TangentCoords2d> | null = null) {
		console.log("BilliardsPathView.constructor");
		// Internal UI state
		this.renderTarget = renderTarget;
		this.renderer = new RenderableElementsRenderer(renderTarget);
		this.renderParams = new RenderableElementsRendererParams();

		this.renderer.modelBounds = AxisRect.fromXYWH(-0.1, -0.1, 1.2, 1.2);

		this._apex = apex;
		apex.addObserver((oldValue, newValue) => {this._needsRedraw = true;});
		this._source = source;
		if (source != null) {
			source.addObserver((oldValue, newValue) => {
				if (this.showSource) {
					this._needsRedraw = true;
				}
			});
		}
		this._needsRedraw = true;

		this._showSource = true;
		this._unfoldingViewSize = 7.14;

		requestAnimationFrame(this.generateClockTick());
	}

	// Triangle: the triangle in which we're looking at billiards.
	get triangle(): Triangle2d {
		return Triangle2d.fromCoords(Coords2d.origin(), Coords2d.fromXY(1, 0), this._apex.value);
	}

	// Coords2d: The apex of the triangle. The base is always from (0,0) to (1,0).
	get apex(): Coords2d {
		return this._apex.value;
	}

	// RootedTCoords(2d): the starting position / vector from which to draw the
	// billiard path.
	get source(): TangentCoords2d | null {
		return this._source && this._source.value;
	}

	get fanPath(): FanPath | null {
		return this._fanPath;
	}

	set fanPath(fanPath: FanPath | null) {
		this._fanPath = fanPath;
		this._needsRedraw = true;
	}

	get showSource(): boolean {
		return this._showSource;
	}

	set showSource(showSource: boolean) {
		if (showSource != this._showSource) {
			this._showSource = showSource;
			this._needsRedraw = true;
		}
	}

	get unfoldingViewSize(): number {
		return this._unfoldingViewSize;
	}
	set unfoldingViewSize(unfoldingViewSize: number) {
		this._unfoldingViewSize = unfoldingViewSize;
		this._needsRedraw = true;
	}

	_advanceSource(): void {}

	ElementsForTriangleView(): Array<Renderable> {
		const triangle = this.triangle;

		const elements = [];

		// Render the triangle itself.
		const triRender = Polygon.fromTriangle(triangle);
		triRender.style.strokeStyle = '#ff33ee';
		triRender.style.lineWidth = '2';
		elements.push(triRender);

		return elements;
	}

	generateClockTick(): () => void {
		if (this._needsRedraw) {
			this.redraw();
		}
		return () => {
			requestAnimationFrame(this.generateClockTick());
		};
	}

	redraw(): void {
		this._needsRedraw = false;

		const diameter = this.unfoldingViewSize;
		let elements: Array<Renderable> = [];
		if (this.fanPath != null) {
			const fanPath: FanPath = this.fanPath;
			const params = new BilliardsParams(this.apex);
			elements = ElementsForFanPathUnfolding(params, fanPath);
			this.renderer.modelBounds = AxisRect.fromXYWH(-diameter / 12, -diameter / 3, diameter, diameter);
		}
		/* else {
		elements = this.ElementsForUnfoldingView(edgeSequence);
		this.renderer.modelBounds =
			AxisRect.fromXYWH(-diameter/2, -diameter/12, diameter, diameter);
		}*/
		const renderParams = this.renderParams;
		renderParams.elements = elements;
		this.renderer.render(renderParams);
	}

}

function ScaleUnitTo255(v: number): number {
	return Math.floor(v * 255.9);
}

// Takes intensity in the range 0-1.
export function ColorStringForRGB(r: number, g: number, b: number): string {
	const rs = ScaleUnitTo255(r);
	const gs = ScaleUnitTo255(g);
	const bs = ScaleUnitTo255(b);
	return `rgb(${rs},${gs},${bs})`;
}

export function ElementsForFan(startEdge: FanPathEdge, fan: Fan): Array<Renderable> {
	const elements: Array<Renderable> = [];
	const outerVertices: Array<Coords2d> = [];
	const fanCenter = startEdge.destCoords();

	outerVertices.push(startEdge.conjugateApex());
	let edge = startEdge;
	for (let i = 1; i < fan.length; i++) {
		edge = startEdge.copy();
		edge.advance(i);
		const apex = edge.conjugateApex();
		outerVertices.push(apex);

		const apexLine = new Line(fanCenter, apex);
		apexLine.style.strokeStyle = '#aaaaaa';
		elements.push(apexLine);
	}
	outerVertices.push(edge.apex());

	outerVertices.push(fanCenter);
	const poly = new Polygon(outerVertices);
	poly.style.strokeStyle = '#ff33ee';
	poly.style.lineWidth = '2';
	elements.push(poly);

	let crossLine = new Line(outerVertices[0], outerVertices[outerVertices.length - 2]);
	elements.push(crossLine);
	crossLine = new Line(outerVertices[1], outerVertices[outerVertices.length - 3]);
	elements.push(crossLine);
	return elements;
}

export function ElementsForFanPathSegment(startEdge: FanPathEdge, segment: FanPathSegment): {
	elements: Array<Renderable>;
	endEdge: FanPathEdge;
} {
	let elements: Array<Renderable> = [];
	const nextEdge = startEdge.copy();
	for (let i = 0; i < segment.fans.length; i++) {
		elements = elements.concat(ElementsForFan(nextEdge, segment.fans[i]));
		nextEdge.advance(segment.fans[i].length - 1);
	}
	return {
		elements: elements,
		endEdge: nextEdge,
	};
}

export function ElementsForFanPathUnfolding(params: BilliardsParams, fanPath: FanPath): Array<Renderable> {
	let elements: Array<Renderable> = [];
	let baseEdge = FanPathEdge.fromParams(params);
	let baseVert = baseEdge.sourceCoords();
	let apex = baseEdge.apex();
	let conjugateApex = baseEdge.conjugateApex();
	let topLine = new Line(baseVert, apex);
	topLine.style.strokeStyle = '#bbbbbb';
	topLine.style.lineWidth = '2';
	topLine.lineDash = [5, 3];
	elements.push(topLine);
	let bottomLine = new Line(baseVert, conjugateApex);
	bottomLine.style.strokeStyle = '#bbbbbb';
	bottomLine.lineDash = [5, 3];
	bottomLine.style.lineWidth = '2';
	elements.push(bottomLine);

	for (let i = 0; i < fanPath.segments.length; i++) {
		const segment = ElementsForFanPathSegment(baseEdge, fanPath.segments[i]);
		elements = elements.concat(segment.elements);
		baseEdge = segment.endEdge;
		baseEdge.flip();
	}

	baseVert = baseEdge.destCoords();
	apex = baseEdge.apex();
	conjugateApex = baseEdge.conjugateApex();
	topLine = new Line(baseVert, apex);
	topLine.style.strokeStyle = '#bbbbbb';
	topLine.style.lineWidth = '2';
	topLine.lineDash = [5, 3];
	elements.push(topLine);
	bottomLine = new Line(baseVert, conjugateApex);
	bottomLine.style.strokeStyle = '#bbbbbb';
	bottomLine.lineDash = [5, 3];
	bottomLine.style.lineWidth = '2';
	elements.push(bottomLine);

	return elements;
}

export function ElementsForFanPathNode(node: FanPathNode, depth: number): Array<Renderable> {
	const elements: Array<Renderable> = [];
	const xAxis = new Line(Coords2d.fromXY(-1, 0), Coords2d.fromXY(1, 0));
	xAxis.style.strokeStyle = '#aaaaaa';
	elements.push(xAxis);
	const yAxis = new Line(Coords2d.fromXY(0, -1), Coords2d.fromXY(0, 1));
	yAxis.style.strokeStyle = '#aaaaaa';
	elements.push(yAxis);
	AppendElementsForFanPathNode(node, depth, elements);
	return elements;
}

export function AppendElementsForFanPathNode(
	node: FanPathNode, maxDepth: number, accumulator: Array<Renderable>
): void {
	//let flippedRegion = node.pathRegions.valueForSign(-1);
	const region = node.pathRegions.valueForSign(1);
	if (region != null) {
		const v = region.vertices; //vectorRange.constraintIntersections;
		const poly = new Polygon(v);
		if (node.depth == 0) {
			poly.style.strokeStyle = '#ff33ee';
			poly.style.lineWidth = '2';
		} else {
			poly.style.strokeStyle = '#ff77ff';
		}
		accumulator.push(poly);
		if (node.depth < maxDepth) {
			const children = node.children();
			for (let i = 0; i < children.length; i++) {
				AppendElementsForFanPathNode(children[i], maxDepth, accumulator);
			}
		}
	}
	const flippedRegion = node.pathRegions.valueForSign(-1);
	if (flippedRegion != null) {
		const angle0 = node.params.angleAtBaseVertex(0).v;
		const angle1 = node.params.angleAtBaseVertex(1).v;
		const angleRatio = node.edge.rotationCounts[1] * angle0 / node.edge.rotationCounts[0] * angle1;
		const phase = Math.atan(angleRatio);
		// Scale the phase range to [-1, 1].
		const phaseOffset = 4 * phase / Math.PI - 1;
		const v = flippedRegion.vertices;
		const poly = new Polygon(v);
		const intensity = 10 / (11 + node.depth);
		poly.style.fillStyle = ColorStringForRGB(0.3 + 0.6 * intensity * (0.5 + 0.5 * phaseOffset), 0.3 + 0.6 * intensity * (0.5 + 0.2 * phaseOffset), 0.3 + 0.6 * intensity * (0.5 - 0.5 * phaseOffset));
		accumulator.push(poly);
	}
}
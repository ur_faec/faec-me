

import { Triangle2d } from "core/polygon";

import { ReflectedAngle, ReflectedEdge } from "./reflected_triangle";

function Mod3(n: number): number {
	return (n % 3 + 3) % 3;
}

// Convenience container for two order-independent triangle edges that can
// canonically transform between edge pairs and integers from 0 to 5.
export class EdgePair {

	// Invariant: edgeIndices[0] <= edgeIndices[1].
	edgeIndices: [number, number];

	constructor(edgeIndex1: number, edgeIndex2: number) {
		const vals = [Mod3(edgeIndex1), Mod3(edgeIndex2)];
		this.edgeIndices = [Math.min(vals[0], vals[1]), Math.max(vals[0], vals[1])];
	}

	asInt(): number {
		if (this.edgeIndices[0] == 0 && this.edgeIndices[1] == 0) {
			return 0;
		}
		if (this.edgeIndices[0] == 1 && this.edgeIndices[1] == 1) {
			return 1;
		}
		if (this.edgeIndices[0] == 2 && this.edgeIndices[1] == 2) {
			return 2;
		}
		if ((this.edgeIndices[0] == 0 && this.edgeIndices[1] == 1) || (this.edgeIndices[0] == 1 && this.edgeIndices[1] == 0)) {
			return 3;
		}
		if ((this.edgeIndices[0] == 1 && this.edgeIndices[1] == 2) || (this.edgeIndices[0] == 2 && this.edgeIndices[1] == 1)) {
			return 4;
		}
		return 5;
	}

	static fromInt(i: number): EdgePair {
		if (i == 0) {
			return new EdgePair(0, 0);
		}
		if (i == 1) {
			return new EdgePair(1, 1);
		}
		if (i == 2) {
			return new EdgePair(2, 2);
		}
		if (i == 3) {
			return new EdgePair(0, 1);
		}
		if (i == 4) {
			return new EdgePair(1, 2);
		}
		return new EdgePair(0, 2);
	}

	toString(): string {
		const edgeStrs: Array<string> = ["A^2", "B^2", "C^2", "AB", "BC", "AC"];
		return edgeStrs[this.asInt()];
	}

	lengthProductOnTriangle(t: Triangle2d): number {
		return t.edge(this.edgeIndices[0]).length() * t.edge(this.edgeIndices[1]).length();
	}
}

// ReflectedEdgeInnerProduct represents the inner product of two reflected
// edges, with the second one rotated by pi/2, i.e.:
// (t: Triangle2d) => {
//   edge1.valueOnTriangle(t).dot(edge2.valueOnTriangle(t).cross())
// }
// It does this by storing:
// - the pair of edge indices in the original triangle, which gives the
//   length of each edge
// - the ReflectedAngle between the two edges
// so the value can be computed as:
// (t: Triangle2d) => {
//    return edges.lengthProductOnTriangle(t) * sin(angle.valueOnTriangle(t));
// }
export class ReflectedEdgeInnerProduct {

	edgePair: EdgePair;
	// The angle from the first to the second vector. Note that because of
	// the implicit rotation by pi/2, this field is sign-sensitive even though
	// it wouldn't be if we were storing the angle between two raw vectors;
	// since we're actually storing the angle between the first vector and
	// the pi/2 rotation of the second one, this object is antisymmetric
	// with respect to the angle parameter (and the input edges).
	// Note also that this is NOT the angle from
	// edges.edgeIndices[0] to edges.edgeIndices[1] since EdgePair doesn't
	// preserve order; it is the signed angle between the original vectors,
	// whatever they were (they are not recoverable at this point).
	angle: ReflectedAngle;

	constructor(edgePair: EdgePair, angle: ReflectedAngle) {
		this.edgePair = edgePair;
		this.angle = angle;
	}

	static fromEdges(edge1: ReflectedEdge, edge2: ReflectedEdge): ReflectedEdgeInnerProduct {
		// We want:
		// cos(edge2.angle - edge1.angle + pi/2) =
		//   -sin(edge2.angle - edge1.angle) =
		//   sin(edge1.angle - edge2.angle).
		return new ReflectedEdgeInnerProduct(new EdgePair(edge1.index, edge2.index), edge1.angle.minus(edge2.angle));
	}

	toString(): string {
		const edgePair = this.edgePair;
		const angle = this.angle;
		return `EdgeInnerProduct(${edgePair.toString()}, ${angle.toString()})`;
	}

	valueOnTriangle(t: Triangle2d): number {
		return this.edgePair.lengthProductOnTriangle(t) * Math.sin(this.angle.valueOnTriangle(t));
	}
}
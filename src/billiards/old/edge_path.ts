

import { Coords2d, TangentCoords2d } from "core/geometry";
import { Triangle2d } from "core/polygon";

export class EdgePath {

	str: string;

	constructor(path: string) {
		this.str = CanonicalizeEdgePath(path);
	}
}

function Mod3(n: number): number {
	return (n % 3 + 3) % 3;
}

export function CanonicalizeEdgePath(edgePath: string): string {
	const candidates: Array<number> = [];
	let baseEdgeIndex = 0;
	let sign = 1;
	let lastTurn = edgePath[edgePath.length - 1];
	for (let i = 0; i < edgePath.length; i++) {
		const turn = edgePath[i];
		const turnSign = turn == "L" ? -1 : 1;
		if (baseEdgeIndex == 0 && sign == 1 && lastTurn == "L" && turn == "R") {
			candidates.push(i);
		}
		baseEdgeIndex = Mod3(baseEdgeIndex + turnSign * sign);
		lastTurn = turn;
		sign = -sign;
	}
	if (candidates.length == 0) {
		console.log("CanonicalizeEdgePath failed");
		return edgePath;
	}
	const candidateStrings = candidates.map((index: number) => {
		return edgePath.slice(index, edgePath.length) + edgePath.slice(0, index);
	});
	candidateStrings.sort();
	return candidateStrings[0];
}
// RRLLRRRRRRRRRRRRRRRLRLLRRLLLLLLLLLLLLLLLRL
// -1, 1, -8, -1, 1, -1, 8, 1,

class Fan {

	pathIndex: number;
	angleIndex: number; // 0 or 1
	orientation: number; // 1 for counterclockwise, -1 otherwise
	startOnBaseEdge: boolean;
	length: number; // >= 1

	constructor(pathIndex: number, angleIndex: number, orientation: number, startOnBaseEdge: boolean) {
		this.pathIndex = pathIndex;
		this.angleIndex = angleIndex;
		this.orientation = orientation;
		this.startOnBaseEdge = startOnBaseEdge;
		this.length = 1;
	}
}

export function FansForEdgePath(edgePath: string): Array<Fan> {
	// RLRRRLLLRLLLLRLRLRRR
	let curFan: Fan | null = null;
	const fans: Array<Fan> = [];
	let prevTurn = edgePath[edgePath.length - 1];
	let prevReflectingEdgeIndex = 0;
	let paritySign = 1;
	for (let i = 0; i < edgePath.length; i++) {
		const turn = edgePath[i];
		const turnSign = turn == "L" ? -1 : 1;
		const reflectingEdgeIndex = Mod3(prevReflectingEdgeIndex + turnSign * paritySign);
		if (reflectingEdgeIndex != 0 && prevReflectingEdgeIndex != 0) {
			if (curFan != null) {
				curFan.length++;
				fans.push(curFan);
			}
			//window.console.log(i, reflectingEdgeIndex, turnSign, paritySign);
			curFan = new Fan(i, reflectingEdgeIndex % 2, turnSign, true);
		} else if (prevTurn == turn || reflectingEdgeIndex == 0) {
			if (curFan != null) {
				curFan.length++;
			}
		} else {// prevTurn != turn, prevReflectingEdgeIndex == 0
			if (curFan != null) {
				curFan.length++;
				fans.push(curFan);
			}
			curFan = new Fan(i, reflectingEdgeIndex % 2, -turnSign, false);
			curFan.length++;
		}
		prevTurn = turn;
		paritySign = -paritySign;
		prevReflectingEdgeIndex = reflectingEdgeIndex;
	}
	if (curFan != null) {
		const fan: Fan = curFan;
		if (fans.length > 0) {
			fan.length += fans[0].pathIndex + 1;
		}
		fans.push(curFan);
	}
	return fans;
}

function PickCanonicalCountIndex(counts: Array<number>, indices: Array<number>) {
	let curOffset = 0;
	function countForIndex(i: number): number {
		return counts[(indices[i] + curOffset) % counts.length];
	}
	let remaining: Array<number> = [];
	for (let i = 0; i < indices.length; i++) {
		remaining.push(i);
	}
	while (remaining.length != 1) {
		// This isn't optimal cycle short-circuiting, but it's easy.
		if (curOffset >= Math.abs(indices[remaining[0]] - indices[remaining[1]])) {
			break;
		}
		let minVal: number = countForIndex(remaining[0]);
		for (let i = 1; i < remaining.length; i++) {
			const index = remaining[i];
			if (countForIndex(index) < minVal) {
				minVal = countForIndex(index);
			}
		}
		const newRemaining = [];
		for (let i = 0; i < remaining.length; i++) {
			const index = remaining[i];
			if (countForIndex(index) == minVal) {
				newRemaining.push(index);
			}
		}
		remaining = newRemaining;
		curOffset++;
	}
	return indices[remaining[0]];
}

export function CountsForEdgePath(edgePath: string): Array<number> {
	const result: Array<number> = [];
	let startIndices: Array<number> = [];
	let bestLength = 0;
	const fans = FansForEdgePath(edgePath);
	for (let i = 0; i < fans.length; i++) {
		const fan = fans[i];
		// Looking for a good starting fan: counterclockwise along angle 0
		// or clockwise along angle 1. We choose one of maximal length, with
		// ties broken by lex order on the signed int list
		if (fan.orientation == 1 && fan.angleIndex == 0 || fan.orientation == -1 && fan.angleIndex == 1) {
			if (fan.length > bestLength) {
				bestLength = fan.length;
				startIndices = [result.length];
			} else if (fan.length == bestLength) {
				startIndices.push(result.length);
			}
		}
		const offset = fan.startOnBaseEdge ? 0 : -1;
		const baseLength = Math.floor((fan.length + offset) / 2);
		result.push(fan.orientation * baseLength);
	}
	const sliceIndex = PickCanonicalCountIndex(result, startIndices);
	return result.slice(sliceIndex).concat(result.slice(0, sliceIndex));
	//return result;
}

export function EdgePathForEdgeSequence(edgeSequence: Array<number>): string {
	const pathArray = [];
	let prevEdge = 0;
	for (let i = 0; i < edgeSequence.length; i++) {
		if ((prevEdge + 1 + i % 2) % 3 == edgeSequence[i]) {
			pathArray.push('R');
		} else {
			pathArray.push('L');
		}
		prevEdge = edgeSequence[i];
	}
	return pathArray.join('');
}

export function EdgeSequenceForEdgePath(edgePath: string): Array<number> {
	const edgeSequence: Array<number> = [];
	let baseEdge = 0;
	for (let i = 0; i < edgePath.length; i++) {
		let offset = 1;
		if (edgePath[i] == 'L') {
			offset = -1;
		}
		if (i % 2 == 1) {
			offset = -offset;
		}
		baseEdge = (baseEdge + 3 + offset) % 3;
		edgeSequence.push(baseEdge);
	}
	return edgeSequence;
}

export function EdgePathIsClosed(edgePath: string): boolean {
	const turnCounts = [0, 0, 0];
	let turnIndex = 0;
	let turningDirection = edgePath[edgePath.length - 1];
	let hexDirection = 1;
	for (let i = 0; i < edgePath.length; i++) {
		if (edgePath[i] == turningDirection) {
			hexDirection = -hexDirection;
		} else {
			turnIndex = (turnIndex + hexDirection + 3) % 3;
			turningDirection = edgePath[i];
		}
		const turnSign = edgePath[i] == 'L' ? 1 : -1;
		turnCounts[turnIndex] += turnSign;
	}
	return (turnCounts[0] == 0 && turnCounts[1] == 0 && turnCounts[2] == 0);
}

export function EdgeSequenceIsClosed(
	edgeSequence: Array<number>
): boolean {
	return EdgePathIsClosed(EdgePathForEdgeSequence(edgeSequence));
}

// If edgeSequence admits a periodic billiard on triangle, returns a
// RootedTCoords of the path.
export function TangentForPeriodicPath(triangle: Triangle2d, edgeSequence: Array<number>): TangentCoords2d | null {
	const tri = triangle.copy();
	const leftBoundary = [tri.vertex(0)];
	const rightBoundary = [tri.vertex(1)];
	let prevEdge = 0;
	for (let i = 0; i < edgeSequence.length; i++) {
		const edgeDelta = (edgeSequence[i] - (prevEdge + 1) + 3) % 3; // always 0 or 1
		if (edgeDelta == i % 2) {
			// Right
			leftBoundary.push(tri.vertex(edgeSequence[i] + 1 - i % 2));
		} else {
			// Left
			rightBoundary.push(tri.vertex(edgeSequence[i] + i % 2));
		}
		tri.reflectThroughEdge(edgeSequence[i]);
		prevEdge = edgeSequence[i];
	}
	// The offset from the first to la
	const origin = leftBoundary[0];
	const offset = leftBoundary[leftBoundary.length - 1].asOffsetFrom(leftBoundary[0]);
	const offsetCross = offset.cross().normalized();
	function coordsCoeff(coords: Coords2d) {
		return coords.asOffsetFrom(origin).dot(offsetCross);
	}
	const leftCoeffs = leftBoundary.map(coordsCoeff);
	const rightCoeffs = rightBoundary.map(coordsCoeff);
	const minLeft = Math.min(...leftCoeffs);
	const maxRight = Math.max(...rightCoeffs);
	if (maxRight >= minLeft) {
		// No space between the left and right boundaries, no cycle.
		return null;
	}
	const centerCoeff = (minLeft + maxRight) / 2;
	const pathCoords = origin.plus(offsetCross.timesReal(centerCoeff));
	const pathTangent = offset.rootedAtCoords(pathCoords);
	// Now project back to the original triangle base.
	pathTangent.base = pathTangent.intersectionWith(triangle.rootedEdge(0));
	return pathTangent;
}
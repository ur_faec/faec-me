// @flow

/*
import $ from 'jquery';

import { CallbackPointsInterface } from 'faec/callback_points_interface';
import { RenderTarget } from 'faec/canvas';
import CanvasRender, {
  RenderableElementsRenderer, RenderableElementsRendererParams }
  from 'faec/element_render';
import { Coords2d, TangentCoords2d, OffsetCoords2d } from 'faec/geometry';
import { Rect, Triangle2d, TrianglePerimeterPoint } from 'faec/polygon';

import { CountsForEdgePath, EdgeSequenceForEdgePath, EdgeSequenceIsClosed,
  EdgePathIsClosed, TangentForPeriodicPath } from './edge_path';



export class BilliardsModel {
  _modelBounds: Rect;
  _renderers: [RenderableElementsRenderer, RenderableElementsRenderer];
  renderParams: RenderableElementsRendererParams;

  extra: Array<CanvasRender.Renderable>;
  _needsRedraw: boolean;
  _controlPointsInterface: ?CallbackPointsInterface;

  _viewModes: Array<string>;

  vForward: number;
  vRight: number;
  vAngle: number;

  drawDistance: number;
  skipOdd: boolean;
  decayParam: number;
  _showPath: boolean;
  //_showUnfolding: boolean;
  _unfoldingViewSize: number;

  _fanPath: ?FanPath;

  sequenceResults: Array<[number, number]>;

  _apex: Coords2d;
  _source: TangentCoords2d;

  highlightCount: number;


  edgePaths: Array<string>;
  originalEdgePaths: Array<string>;
  _reflectedEdgeSequence: ?Array<number>;

  constructor(renderTarget0: RenderTarget, renderTarget1: RenderTarget) {
    // Internal UI state
    this._modelBounds = Rect.fromDimensions(2, 2);

    this._renderers = [
      new RenderableElementsRenderer(renderTarget0),
      new RenderableElementsRenderer(renderTarget1),
    ];
    this.renderParams = new RenderableElementsRendererParams();
    this._needsRedraw = false;

    // Right panel supports "unfolding" and "dual"
    this._viewModes = ["", "dual"];

    this._apex = Coords2d.fromXY(0.6, 0.6);
    this._source = TangentCoords2d.fromBaseAndOffset(
      Coords2d.fromXY(0.5, 0.3),
      OffsetCoords2d.fromDxDy(1, 0)
    );

    this.vForward = 0;
    this.vRight = 0;
    this.vAngle = 0;

    this.drawDistance = 30;
    this.skipOdd = false;
    this.decayParam = 100;
    this._showPath = true;
    //this._showUnfolding = true;
    this._unfoldingViewSize = 7.14;

    this.edgePaths = [
      // Just here for reference, anything less than (1, 1) is acute-only
      "RLRLRL", // (0, 0, 0)

      // Winding number 1 with no backtracking:
      // (<= 1 backtrack means all middle indices are 0)
      "RRRLRRRLLLRLLL", // Big central triangle (1, 0, 1)
      "RRRRRLRRRLLLLLRLLL", // Left child (2, 0, 1)
      "RRRRRLRRRRRLLLLLRLLLLL", // Center child (2, 0, 2)

      "RRRRRRRLRRRLLLLLLLRLLL", // Left grandchild (3, 0, 1)
      "RRRRRRRLRRRRRLLLLLLLRLLLLL", // (3, 0, 2)
      //"RRRRRRRLRRRRRRRLLLLLLLRLLLLLLL", // (3, 0, 3)

      //"RRRRRRRRRLRRRLLLLLLLLLRLLL", // (4, 0, 1)
      "RRRRRRRRRLRRRRRLLLLLLLLLRLLLLL", // (4, 0, 2)
    ];
    this.originalEdgePaths = this.edgePaths.map((x) => x);
    this.highlightCount = 1;
  }

  // Triangle: the triangle in which we're looking at billiards.
  get triangle (): Triangle2d {
    return Triangle2d.fromCoords(
      Coords2d.origin(),
      Coords2d.fromXY(1, 0),
      this._apex);
  }

  // Coords2d: The apex of the triangle. The base is always from (0,0) to (1,0).
  get apex (): Coords2d {
    return this._apex;
  }

  set apex (apex: Coords2d) {
    this._apex = apex.copy();
    this._needsRedraw = true;
  }

  // RootedTCoords(2d): the starting position / vector from which to draw the
  // billiard path.
  get source (): TangentCoords2d {
    return this._source;
  }

  set source (source: TangentCoords2d) {
    this._source = source;
    this._needsRedraw = true;
  }

  get reflectedEdgeSequence (): ?Array<number> {
    return this._reflectedEdgeSequence;
  }

  set reflectedEdgeSequence (edgeSequence: ?Array<number>) {
    this._reflectedEdgeSequence = edgeSequence;
    this._fanPath = null;
    this._needsRedraw = true;
  }

  get fanPath (): ?FanPath {
    return this._fanPath;
  }

  set fanPath (fanPath: FanPath) {
    this._fanPath = fanPath;
    this._reflectedEdgeSequence = null;
    this._needsRedraw = true;
  }

  get showPath (): boolean {
    return this._showPath;
  }

  set showPath (showPath: boolean) {
    this._showPath = showPath;
    this._needsRedraw = true;
  }

  viewMode (index: number) {
    return this._viewModes[index];
  }

  setViewMode (index: number, mode: string) {
    this._viewModes[index] = mode;
    this._needsRedraw = true;
  }

  get modelBounds (): Rect {
    return this._modelBounds;
  }
  set modelBounds (modelBounds: Rect) {
    this._modelBounds = modelBounds;
    this._needsRedraw = true;
  }

  get unfoldingViewSize (): number {
    return this._unfoldingViewSize;
  }
  set unfoldingViewSize (unfoldingViewSize: number) {
    this._unfoldingViewSize = unfoldingViewSize;
    this._needsRedraw = true;
  }

  start () {

    this._createControlInterfaces();
    this._addEventListeners();

    requestAnimationFrame(this.generateClockTick());
  }

  _advanceSource () {
    var c = this.source.base;
    var t = this.source.offset.normalized();

    if (this.vForward != 0 || this.vRight != 0 || this.vAngle != 0) {
      let angleStart =
          Math.atan2(this.source.offset.dy, this.source.offset.dx);
      let angleDelta = 0;
      if (this.vForward != 0 || this.vRight != 0) {
        var vx = t.dx * this.vForward - t.dy * this.vRight;
        var vy = t.dy * this.vForward + t.dx * this.vRight;
        var v = TangentCoords2d.fromBaseAndOffset(
          c, OffsetCoords2d.fromDxDy(vx, vy));
        let vdist = v.offset.length();
        v.offset = v.offset.normalized();
        while (vdist > 0) {
          let {edge} = TriangleEdgeIntersection(this.triangle, v);
          let idist = v.coefficientOfIntersectionWith(edge);
          let intersection = v.base.plus(v.offset.timesReal(idist));
          if (vdist > idist) {
            v.base = intersection;
            v.offset = edge.reflectTCoords(v.offset);
            vdist -= idist;
            continue;
          }
          v.base = v.base.plus(v.offset.timesReal(vdist));
          break;
        }
        angleDelta =
            Math.atan2(v.offset.dy, v.offset.dx) - Math.atan2(vy, vx);
        this.source.base = v.base;
      }
      if (this.vAngle != 0 || angleDelta != 0) {
        this.source.offset =
            OffsetCoords2d.fromPolar(1, angleStart + angleDelta + this.vAngle);
      }
      this._needsRedraw = true;
    }
  }

  // Given a triangle and a list of points on its perimeter, returns
  // Renderable elements with a visualization of
  // (path: [TrianglePerimeterPoint]) -> [Renderable]
  ElementsForTriangleView (
      path: Array<TrianglePerimeterPoint>): Array<CanvasRender.Renderable> {
    let triangle = this.triangle;
    let startingCoords = this.source.base;
    let showPath = this.showPath;
    let decayParam = this.decayParam;
    let skipOdd = this.skipOdd;

    let elements = [];
    let totalLength = 0;

    // If we're rendering a path, also render the path source.
    if (showPath) {
      let sourceRender = CanvasRender.Circle.fromCenterAndRadius(
        startingCoords, 0.015
      );
      sourceRender.style.fillStyle = '#0088bb';
      elements.push(sourceRender);
      sourceRender = CanvasRender.Circle.fromCenterAndRadius(
        startingCoords, 0.05
      );
      sourceRender.style.strokeStyle = '#000000';
      elements.push(sourceRender);
    }

    // Render the triangle itself.
    let triRender = CanvasRender.Polygon.fromTriangle(triangle);
    triRender.style.strokeStyle = '#ff33ee';
    triRender.style.lineWidth = '2';
    elements.push(triRender);

    if (showPath) {
      let tri = triangle;
      let coordsList = path.map(
        (perimeterPoint) => tri.coordsForPerimeterPoint(perimeterPoint));
      let prevCoords = startingCoords;
      let backElements = [];
      for (let i = 0; i < coordsList.length; i++) {
        let length = coordsList[i].distanceFrom(prevCoords);
        totalLength += length;

        let decay = decayParam / (decayParam + totalLength * totalLength);
        let c = Math.floor(decay * 255 + 0.5);
        let stroke =
          `rgb(${Math.floor(220 - 3*c/4)},${Math.floor(220 - 3*c/4)},255)`;
        let line = CanvasRender.Line.fromEndpoints(
          prevCoords, coordsList[i]);
        if (!skipOdd || i % 2 == 0) {
          line.style.strokeStyle = stroke;
          elements.push(line);
        } else {
          line.style.strokeStyle = '#eeeeee';
          backElements.push(line);
        }
        prevCoords = coordsList[i];
      }
    }
    return elements;
  }

  _thingie () {
    for (let i = 0; i < this.edgePaths.length; i++) {
      if (!EdgePathIsClosed(this.edgePaths[i])) {
        window.console.log(
          `Warning: edge path ${i} (${this.edgePaths[i]}) is not closed`);
      }
    }
    let edgeSequences = this.edgePaths.map(EdgeSequenceForEdgePath);
    let cutoff = edgeSequences.length - this.highlightCount;
    this.extra = [];
    let h = Math.sqrt(3) / 3;
    let w = 1;
    let tri = this.triangle;
    //let xres = 100;
    //let yres = 50;
    //let dotRadius = 0.007;
    let xres = 200;
    let yres = 100;
    let dotRadius = 0.0045;
    let highlighted = 0;
    let sequenceCounts = edgeSequences.map(() => 0);
    for (let y = 1; y <= yres; y++) {
      for (let x = 0; x <= xres; x++) {
        for (let i = 0; i < edgeSequences.length; i++) {
        //for (let i = edgeSequences.length - 1; i >= 0; i--) {
          let edgeSequence = edgeSequences[i];
          let v = tri.vertexCoords;
          v[2] = Coords2d.fromXY(v[0].x + x * w / xres, v[0].y + y * h / yres);
          let margin = PathMargin(tri, edgeSequence);
          if (margin > 0) {
            let c = CanvasRender.Circle.fromCenterAndRadius(v[2], dotRadius);
            if (i >= cutoff) {
              c.style.fillStyle = '#bb8888';
              highlighted++;
            } else {
              let intensity = Math.min(Math.sqrt(margin), 1);
              let rg = Math.floor(128 * (1 - intensity));
              let b = Math.floor(rg + 255 * intensity);
              c.style.fillStyle = `rgb(${rg},${rg},${b}`;
            }
            this.extra.push(c);
            sequenceCounts[i]++;
            break;
          }
        }
      }
    }
    window.console.log(`${highlighted} points were highlighted`);
    this.sequenceResults = [];
    for (let i = 0; i < sequenceCounts.length; i++) {
      if (sequenceCounts[i] > 0) {
        this.sequenceResults.push([i, sequenceCounts[i]]);
      }
    }
    this._needsRedraw = true;
  }

  ElementsForUnfoldingView (
      edgeSequence: Array<number>): Array<CanvasRender.Renderable> {
    let elements: Array<CanvasRender.Renderable> = [];
    let tri = this.triangle;

    if (this.showPath) {
      let pathRender = CanvasRender.Line.fromEndpoints(
        this.source.base,
        this.source.base.plus(this.source.offset.timesReal(10))
      );
      pathRender.style.strokeStyle = 'rgb(20,20,255)';
      elements.push(pathRender);
    }

    if (EdgeSequenceIsClosed(edgeSequence)) {
      let cycleTangent = TangentForPeriodicPath(tri, edgeSequence);
      if (cycleTangent) {
        let cycleRender = CanvasRender.Line.fromEndpoints(
          cycleTangent.base,
          cycleTangent.base.plus(cycleTangent.offset)
        );
        cycleRender.style.strokeStyle = 'rgb(128,128,128)';
        cycleRender.style.lineWidth = '3';
        cycleRender.lineDash = [4, 4];
        elements.push(cycleRender);
      }
    }

    let triRender = CanvasRender.Polygon.fromTriangle(tri);
    triRender.style.strokeStyle = '#ff33ee';
    triRender.style.lineWidth = '2';
    elements.push(triRender);
    for (let i = 0; i < edgeSequence.length; i++) {
      tri.reflectThroughEdge(edgeSequence[i]);
      triRender = CanvasRender.Polygon.fromTriangle(tri);
      triRender.style.strokeStyle = '#ff33ee';
      triRender.style.lineWidth = '2';
      elements.push(triRender);
    }
    return elements;
  }

  KeyDown (event: KeyboardEvent) {
    if (event.metaKey) {
      return;
    }
    var speed = 0.01;
    if (event.shiftKey) {
      speed *= 0.05;
      if (event.ctrlKey) {
        speed *= 0.05;
      }
    }
    switch (event.which) {
    case 38: // up
    case 87: // W
      this.vForward = 1.5*speed;
      break;

    case 40: // down
    case 83: // S
      this.vForward = -1.5*speed;
      break;

    case 37: // left
    case 65: // A
      this.vAngle = 2.0*speed;
      break;

    case 39: // right
    case 68: // D
      this.vAngle = -2.0*speed;
      break;

    case 81: // Q
      this.vRight = speed;
      break;

    case 69: // E
      this.vRight = -speed;
      break;

    default:
      return;
    }
    event.preventDefault();
  }

  KeyUp (event: KeyboardEvent) {
    switch (event.which) {
    case 38: // up
    case 87: // W
      if (this.vForward > 0.0) {
        this.vForward = 0;
      }
      break;

    case 40: // down
    case 83: // S
      if (this.vForward < 0.0) {
        this.vForward = 0;
      }
      break;

    case 37: // left
    case 65: // A
      if (this.vAngle > 0.0) {
        this.vAngle = 0;
      }
      break;

    case 39: // right
    case 68: // D
      if (this.vAngle < 0.0) {
        this.vAngle = 0.0;
      }
      break;

    case 81: // Q
      if (this.vRight > 0.0) {
        this.vRight = 0.0;
      }
      break;

    case 69: // E
      if (this.vRight < 0.0) {
        this.vRight = 0.0;
      }
      break;
    }
  }

  generateClockTick() {
    this._advanceSource();
    if (this._needsRedraw) {
      this.redraw();
    }
    return () => {
      requestAnimationFrame(this.generateClockTick());
    };
  }

  redraw () {
    this._needsRedraw = false;

    let reflections = TriangleReflectionsThroughTangent(
      this.triangle, this.source, this.drawDistance);

    let elements = this.ElementsForTriangleView(reflections.intersections);
    if (this.extra) {
      elements = elements.concat(this.extra);
    }
    let renderParams = this.renderParams;
    renderParams.elements = elements;
    this._renderers[0].modelBounds = this.modelBounds;
    this._renderers[0].render(renderParams);

    let edgeSequence = reflections.edgeSequence;
    let scale = this.unfoldingViewSize;
    if (this.reflectedEdgeSequence) {
      edgeSequence = this.reflectedEdgeSequence;
    }
    if (this._viewModes[1] == "unfolding") {
      if (this.fanPath != null) {
        let fanPath: FanPath = this.fanPath;
        let params = new BilliardsParams(this.apex);
        renderParams.elements = ElementsForFanPathUnfolding(params, fanPath);
        this._renderers[1].modelBounds =
          Rect.fromXYWH(-scale/12, -scale/3, scale, scale);
      } else {
        renderParams.elements = this.ElementsForUnfoldingView(edgeSequence);
        this._renderers[1].modelBounds =
          Rect.fromXYWH(-scale/2, -scale/12, scale, scale);
      }
    } else if (this._viewModes[1] == "dual") {
      let tree = FanPathNode.rootForApex(this.apex);
      let elements = ElementsForFanPathNode(tree, 20);
      renderParams.elements = elements;
      this._renderers[1].modelBounds =
        Rect.fromXYWH(-scale/6, -scale/6, scale/3, scale/3);
    } else {
      renderParams.elements = [];
    }
    this._renderers[1].render(renderParams);
  }

  _createControls(): HTMLElement {
    var div: HTMLElement = $("<div></div>")
      .css({
        width: this._viewSize[0], height: '31px', margin: 'auto',
        position: 'absolute', left: '0px', top: `${this._viewSize[1]}px`})
      .html(`
        <form style="margin-top: 5px; float: left; width: 50%;"
            class="phase-controls">
          <div style="display: inline-block;float: left;">
            <label>
              <input style="margin:4px;" type="checkbox" checked="checked"
                  name="show-path">
              Show path
            </label>
          </div>
          <div style="display: inline-block;float: left; padding-left:20px;">
            <label>
              <input type="radio" name="draw-distance" value="3">
              3
            </label>
          </div>
          <div style="display: inline-block;float: left;">
            <label>
              <input type="radio" name="draw-distance" value="30"
                  checked="checked">
              30
            </label>
          </div>
          <div style="display: inline-block;float: left;">
            <label>
              <input type="radio" name="draw-distance" value="200">
              200
            </label>
          </div>
          <div style="display: inline-block;float: left; padding-left:20px;">
            <label>
              <input type="radio" name="right-panel" value="unfolding">
              Unfolding
            </label>
          </div>
          <div style="display: inline-block;float: left;">
            <label>
              <input type="radio" name="right-panel" value="dual"
                  checked="checked">
              Dual
            </label>
          </div>
          <div style="display: inline-block;float: left;">
            <label>
              <input type="radio" name="right-panel" value="reachable">
              Reachable
            </label>
          </div>
        </form>
        <div style="padding: 4px 6px; float: right;">
          <input name="edgePath" type="text" size="50">
        </div>
        <div style="padding: 4px 6px; float: right;">
          Zoom:
          <input name="zoom" type="range" min="1" max="10" step="0.01"
            value="7">
        </div>
        `)[0];
    return div;
  }

  _triangleEditCallback (canvasCoords: Coords2d): ?(Coords2d => void) {
    let modelToCanvas = this._renderers[0].modelToCanvasTransform();
    let canvasToModel = modelToCanvas.inverse();
    // This code assumes the base of the triangle is from (0,0) to (1,0)
    // in model space.
    var maxDist = canvasToModel.isoScale() * 30;
    var modelCoords = canvasToModel.transformCoords(canvasCoords);

    let originalApexCoords = this.apex.copy();
    let originalSource = this.source.copy();
    // Measure the fraction of the apex height.
    let originalSourceVert = originalSource.base.y / originalApexCoords.y;
    // Measure the intersection with the base when projecting down
    // from the apex.
    let originalSourceHorz = originalApexCoords.x +
      (originalSource.base.x - originalApexCoords.x) *
      (1.0 / (1.0 - originalSourceVert));
    if (modelCoords.distanceFrom(originalApexCoords) <= maxDist) {
      return (newCanvasCoords: Coords2d) => {
        var newModelCoords =
          canvasToModel.transformCoords(newCanvasCoords);
        if (newModelCoords.y < 0.01) {
          newModelCoords.y = 0.01;
        }
        if (newModelCoords.x < 0) {
          newModelCoords.x = 0;
        }
        if (newModelCoords.x > 1) {
          newModelCoords.x = 1;
        }
        var modelDelta = newModelCoords.asOffsetFrom(modelCoords);
        let newApexCoords = originalApexCoords.plus(modelDelta);

        let newSourceY = newApexCoords.y * originalSourceVert;
        let newSourceX = newApexCoords.x +
          (originalSourceHorz - newApexCoords.x) *
          (1.0 - originalSourceVert);

        this.apex = newApexCoords;
        this.source.base = Coords2d.fromXY(newSourceX, newSourceY);
        this.source.offset.dy = originalSource.offset.dy *
          (newApexCoords.y / originalApexCoords.y);
        this.source.offset = this.source.offset.normalized();
        this._needsRedraw = true;
      };
    }
  }

  _createControlInterfaces() {
    this._controlPointsInterface = new CallbackPointsInterface(
			this._renderers[0].canvas,
      (canvasCoords: Coords2d) => {
        return this._triangleEditCallback(canvasCoords);
      });
  }

  _addEventListeners() {
    $(this._container).find('input').change(() => {
      window.console.log("Input changed in container:", this._container);
      this.drawDistance = parseInt($(this._container)
        .find('input[name="draw-distance"]:checked')[0].value);
      this.showPath =
        $(this._container).find('input[name="show-path"]').is(':checked');

      let viewMode =
        $(this._container).find('input[name="right-panel"]:checked')[0].value;
      this.setViewMode(1, viewMode);
      let zoom = $(this._container).find('input[name="zoom"]')[0].value;
      this.unfoldingViewSize = 50 / zoom;
      this._needsRedraw = true;
    });
    $(this._container).find('input[name="zoom"]').on('input', () => {
      let zoom = $(this._container).find('input[name="zoom"]')[0].value;
      this.unfoldingViewSize = 50 / zoom;
    });
    $(this._container).find('.refresh').click((event) => {
      if (event.button == 0) {
        let edgePath = $(this._container)
          .find('input[name="edgePath"]')[0].value;
        window.console.log("New edge path: " + edgePath);
        if (edgePath.length > 0) {
          if (edgePath[0] == 'L' || edgePath[0] == 'R') {
            let edgeSequence = EdgeSequenceForEdgePath(edgePath);
            this.reflectedEdgeSequence = edgeSequence;
            let counts = CountsForEdgePath(edgePath);
            window.console.log("Counts:",counts);
          } else {
            let fanPath = FanPath.fromString(edgePath);
            window.console.log("Created fanPath from string:", fanPath);
            this.fanPath = fanPath;
          }
        } else {
          this.reflectedEdgeSequence = null;
          this._fanPath = null;
        }
      }
    });
    $(document).keydown((event) => {
      this.KeyDown(event);
    });
    $(document).keyup((event) => {
      this.KeyUp(event);
    });
  }
}

// Returns the (inner) edge rt intersects.
export function TriangleEdgeIntersection (
    triangle: Triangle2d, rt: TangentCoords2d): {
      edge: TangentCoords2d; edgeIndex: number;
    } {
  let bestEdgeIndex = -1;
  let bestCoeff = 0;
  for (var i = 0; i < 3; i++) {
    let edge = triangle.rootedEdge(i);
    let normal = OffsetCoords2d.fromDxDy(-edge.offset.dy, edge.offset.dx);
    if (normal.dot(rt.offset) >= 0) {
      continue;
    }
    let coeff = rt.coefficientOfIntersectionWith(edge);
    if (coeff >= 0 && (bestEdgeIndex < 0 || coeff < bestCoeff)) {
      bestEdgeIndex = i;
      bestCoeff = coeff;
    }
  }
  return {
    edge: triangle.rootedEdge(bestEdgeIndex),
    edgeIndex: bestEdgeIndex,
  };
}

// Computes reflections of the given tangent up to distance pathLength,
// returning the sequence of edges that were crossed and their location
// on the perimeter of the triangle.
export function TriangleReflectionsThroughTangent(
    triangle: Triangle2d, tangent: TangentCoords2d, pathLength: number): {
      edgeSequence: Array<number>,
      intersections: Array<TrianglePerimeterPoint>
    } {
  let totalLength = 0;
  let v: TangentCoords2d = tangent.copy();
  let prevCoords = v.base;

  let edgeSequence = [];
  let intersections = [];
  while (totalLength < pathLength) {
    let {edge, edgeIndex} = TriangleEdgeIntersection(triangle, v);
    if (edgeIndex < 0) {
      break;
    }
    edgeSequence.push(edgeIndex);
    let coeff = edge.coefficientOfIntersectionWith(v);
    let perimeterPoint = TrianglePerimeterPoint.fromEdgeIndexAndCoeff(
      edgeIndex, coeff
    );
    intersections.push(perimeterPoint);
    v.base = triangle.coordsForPerimeterPoint(perimeterPoint);
    v.offset = edge.reflectTCoords(v.offset);
    totalLength += v.base.distanceFrom(prevCoords);
    prevCoords = v.base;
  }
  return {edgeSequence, intersections};
}

export function PathMargin (
    triangle: Triangle2d, edgeSequence: Array<number>): number {
  let tri = triangle.copy();
  let leftBoundary = [];
  let rightBoundary = [];
  let prevEdge = 0;
  for (let i = 0; i < edgeSequence.length; i++) {
    let edgeDelta = (edgeSequence[i] - (prevEdge + 1) + 3) % 3; // always 0 or 1
    if (edgeDelta == i % 2) {
      // Right
      leftBoundary.push(tri.vertex(edgeSequence[i] + 1 - (i % 2)));
    } else {
      // Left
      rightBoundary.push(tri.vertex(edgeSequence[i] + (i % 2)));
    }
    tri.reflectThroughEdge(edgeSequence[i]);
    prevEdge = edgeSequence[i];
  }
  let offset = tri.vertexCoords[0]
    .asOffsetFrom(triangle.vertexCoords[0]);
  let lineCoeff = OffsetCoords2d.fromDxDy(-offset.dy, offset.dx).normalized();
  let o = Coords2d.origin();
  let minLeft = lineCoeff.dot(triangle.vertexCoords[0].asOffsetFrom(o));
  for (let i = 0; i < leftBoundary.length; i++) {
    let coeff = lineCoeff.dot(leftBoundary[i].asOffsetFrom(o));
    if (coeff < minLeft) {
      minLeft = coeff;
    }
  }
  let maxRight = lineCoeff.dot(triangle.vertexCoords[1].asOffsetFrom(o));
  for (let i = 0; i < rightBoundary.length; i++) {
    let coeff = lineCoeff.dot(rightBoundary[i].asOffsetFrom(o));
    if (coeff > maxRight) {
      maxRight = coeff;
    }
  }
  //window.console.log(`${minLeft}, ${maxRight}`);
  return minLeft - maxRight;
}
*/


import { Coords2d, OffsetCoords2d } from "core/geometry";
import { Triangle2d } from "core/polygon";

export function TriangleForApex(apex: Coords2d): Triangle2d {
	return Triangle2d.fromCoords(Coords2d.origin(), Coords2d.fromXY(1, 0), apex);
}

export function ApexForBaseAngles(a0: number, a1: number): Coords2d {
	const leftTangent = OffsetCoords2d.fromPolar(1, a0).rootedAtCoords(Coords2d.origin());
	const rightTangent = OffsetCoords2d.fromPolar(1, Math.PI - a1).rootedAtCoords(Coords2d.fromXY(1, 0));
	return leftTangent.intersectionWith(rightTangent);
}

export class DReal {

	v: number;
	dv: [number, number];

	constructor(v: number, dv: [number, number]) {
		this.v = v;
		this.dv = dv;
	}

	toString(): string {
		return `${this.v} [${this.dv[0]} ${this.dv[1]}]`;
	}

	static zero(): DReal {
		return new DReal(0, [0, 0]);
	}

	static one(): DReal {
		return new DReal(1, [0, 0]);
	}

	plus(r: DReal): DReal {
		return new DReal(this.v + r.v, [this.dv[0] + r.dv[0], this.dv[1] + r.dv[1]]);
	}

	negate(): DReal {
		return new DReal(-this.v, [-this.dv[0], -this.dv[1]]);
	}

	minus(r: DReal): DReal {
		return new DReal(this.v - r.v, [this.dv[0] - r.dv[0], this.dv[1] - r.dv[1]]);
	}

	plusConstant(c: number): DReal {
		return new DReal(this.v + c, this.dv);
	}

	times(r: DReal): DReal {
		const dOverX = this.v * r.dv[0] + this.dv[0] * r.v;
		const dOverY = this.v * r.dv[1] + this.dv[1] * r.v;
		return new DReal(this.v * r.v, [dOverX, dOverY]);
	}

	timesConstant(c: number): DReal {
		return new DReal(c * this.v, [c * this.dv[0], c * this.dv[1]]);
	}

	inverse(): DReal {
		const inverseNorm = 1.0 / (this.v * this.v);
		return new DReal(1.0 / this.v, [-inverseNorm * this.dv[0], -inverseNorm * this.dv[1]]);
	}

	cos(): DReal {
		const cos = Math.cos(this.v);
		const sin = Math.sin(this.v);
		return new DReal(cos, [-sin * this.dv[0], -sin * this.dv[1]]);
	}

	sin(): DReal {
		const cos = Math.cos(this.v);
		const sin = Math.sin(this.v);
		return new DReal(sin, [cos * this.dv[0], cos * this.dv[1]]);
	}

	static atan2(y: DReal, x: DReal): DReal {
		const angle = Math.atan2(y.v, x.v);
		const norm = x.v * x.v + y.v * y.v;
		const dAngleOverDx = -y.v / norm;
		const dAngleOverDy = x.v / norm;
		const dAngleOverD0 = dAngleOverDx * x.dv[0] + dAngleOverDy * y.dv[0];
		const dAngleOverD1 = dAngleOverDx * x.dv[1] + dAngleOverDy * y.dv[1];
		return new DReal(angle, [dAngleOverD0, dAngleOverD1]);
	}

	sqrt(): DReal {
		const sqrt = Math.sqrt(this.v);
		return new DReal(sqrt, [this.dv[0] / (2 * sqrt), this.dv[1] / (2 * sqrt)]);
	}
}

export class DTCoords2d {

	dx: DReal;
	dy: DReal;

	constructor(dx: DReal, dy: DReal) {
		this.dx = dx;
		this.dy = dy;
	}

	static zero(): DTCoords2d {
		return new DTCoords2d(DReal.zero(), DReal.zero());
	}

	static fromDxDy(dx: DReal, dy: DReal): DTCoords2d {
		return new DTCoords2d(dx, dy);
	}

	static fromPolar(magnitude: DReal, angle: DReal): DTCoords2d {
		const cos = angle.cos();
		const sin = angle.sin();
		return DTCoords2d.fromDxDy(cos.times(magnitude), sin.times(magnitude));
	}

	times(tc: DTCoords2d): DTCoords2d {
		return DTCoords2d.fromDxDy(this.dx.times(tc.dx).minus(this.dy.times(tc.dy)), this.dx.times(tc.dy).plus(this.dy.times(tc.dx)));
	}

	timesScalar(s: DReal): DTCoords2d {
		return DTCoords2d.fromDxDy(this.dx.times(s), this.dy.times(s));
	}

	timesConstantScalar(s: number): DTCoords2d {
		return DTCoords2d.fromDxDy(this.dx.timesConstant(s), this.dy.timesConstant(s));
	}

	complexInverse(): DTCoords2d {
		const inverseNorm = this.squaredLength().inverse();
		return new DTCoords2d(this.dx.times(inverseNorm), this.dy.times(inverseNorm).negate());
	}

	complexConjugate(): DTCoords2d {
		return new DTCoords2d(this.dx, this.dy.negate());
	}

	dividedBy(tc: DTCoords2d): DTCoords2d {
		return this.times(tc.complexInverse());
	}

	plus(tc: DTCoords2d): DTCoords2d {
		return DTCoords2d.fromDxDy(this.dx.plus(tc.dx), this.dy.plus(tc.dy));
	}

	plusConstant(tc: OffsetCoords2d): DTCoords2d {
		return DTCoords2d.fromDxDy(this.dx.plusConstant(tc.dx), this.dy.plusConstant(tc.dy));
	}

	minus(tc: DTCoords2d): DTCoords2d {
		return DTCoords2d.fromDxDy(this.dx.minus(tc.dx), this.dy.minus(tc.dy));
	}

	squaredLength(): DReal {
		return this.dx.times(this.dx).plus(this.dy.times(this.dy));
	}

	length(): DReal {
		const norm = this.squaredLength();
		const length = Math.sqrt(norm.v);
		return new DReal(length, [norm.dv[0] / (2 * length), norm.dv[1] / (2 * length)]);
	}

	toString(): string {
		return `DTCoords(${this.dx.toString()}, ${this.dy.toString()})`;
	}
}

export class BilliardsParams {
	// The only non-derived params.
	apex: Coords2d;

	// Caches for derived properties.
	_triangle: Triangle2d | null = null;

	_rotationCache: Array<Array<OffsetCoords2d>> = [];

	constructor(apex: Coords2d) {
		this.apex = apex;
	}

	triangle(): Triangle2d {
		if (this._triangle == null) {
			this._triangle = TriangleForApex(this.apex);
		}
		return this._triangle;
	}

	angleAtBaseVertex(vertexIndex: number): DReal {
		let D0 = this.D0();
		const D1 = this.D1();
		if (vertexIndex == 1) {
			D0 = DReal.one().minus(D0);
		}
		return DReal.atan2(D1, D0);
	}

	D0(): DReal {
		return new DReal(this.apex.x, [1, 0]);
	}

	D1(): DReal {
		return new DReal(this.apex.y, [0, 1]);
	}

	edgeNormAtIndex(edgeIndex: number): DReal {
		if (edgeIndex == 0) {
			return DReal.one();
		}
		const D0 = this.D0();
		const D1 = this.D1();
		const x = edgeIndex == 2 ? D0 : DReal.one().minus(D0);
		const y = D1;
		return x.times(x).plus(y.times(y));
	}

	edgeLengthAtIndex(edgeIndex: number): DReal {
		return this.edgeNormAtIndex(edgeIndex).sqrt();
	}

	vectorForAngleIndex(i: number): OffsetCoords2d {
		let v: OffsetCoords2d;
		if (i == 0) {
			v = this.apex.asOffsetFromOrigin();
		} else {
			v = OffsetCoords2d.fromDxDy(1 - this.apex.x, -this.apex.y);
		}
		return v.times(v).timesReal(1.0 / v.squaredLength());
	}

	rotationCache(): Array<Array<OffsetCoords2d>> {
		if (this._rotationCache == null) {
			this._rotationCache = [[OffsetCoords2d.fromDxDy(1, 0), this.vectorForAngleIndex(0)], [OffsetCoords2d.fromDxDy(1, 0), this.vectorForAngleIndex(1)]];
		}
		return this._rotationCache;
	}

	vectorForRotation(angleIndex: number, count: number): OffsetCoords2d {
		const rotationCache = this.rotationCache()[angleIndex];
		const absCount = Math.abs(count);
		while (rotationCache.length <= absCount) {
			const last = rotationCache[rotationCache.length - 1];
			rotationCache.push(last.times(rotationCache[1]));
		}
		return rotationCache[count].copy();
	}


	vectorForRotationCoefficients(c0: number, c1: number): OffsetCoords2d {
		const rotationCache = this.rotationCache();
		const cAbs = [Math.abs(c0), Math.abs(c1)];
		for (let i = 0; i < 2; i++) {
			while (cAbs[i] >= rotationCache[i].length) {
				const last = rotationCache[i][rotationCache[i].length - 1];
				rotationCache[i].push(last.times(rotationCache[i][1]));
			}
		}
		let r0 = rotationCache[0][cAbs[0]].copy();
		if (c0 < 0) {
			r0 = r0.conjugate();
		}
		let r1 = rotationCache[1][cAbs[1]].copy();
		if (c1 < 0) {
			r1 = r1.conjugate();
		}
		return r0.times(r1);
	}

	maxFanLength(angleIndex: number): number {
		const vertexAngle = this.angleAtBaseVertex(angleIndex);
		const fanAngle = 2 * vertexAngle.v;
		return Math.ceil(Math.PI / fanAngle);
	}
}
// @flow

/*
import $ from 'jquery';
import { CallbackPointsInterface } from 'faec/callback_points_interface';
import { IsoScaleTransform } from 'faec/coordinate_transform';
import CanvasRender, {
  RenderableElementsRenderer, RenderableElementsRendererParams }
  from 'faec/element_render';
import { Coords2d, TangentCoords2d, OffsetCoords2d } from 'faec/geometry';
import { Rect, Triangle2d } from 'faec/polygon';

import { TriangleForApex } from './billiards_params';
import { EdgeSequenceForEdgePath } from './edge_path';
import { BoundaryOffsetFunction, PathStudy } from './billiards_study';
import { EdgePath } from './edge_path';

function TriangleWithBaseAngles(a0: number, a1: number): Triangle2d {
  let v0 = Coords2d.origin();
  let t0 = TangentCoords2d.fromBaseAndOffset(
    v0, OffsetCoords2d.fromDxDy(Math.cos(a0), Math.sin(a0)));
  let v1 = Coords2d.fromXY(1, 0);
  let t1 = TangentCoords2d.fromBaseAndOffset(
    v1, OffsetCoords2d.fromDxDy(-Math.cos(a1), Math.sin(a1)));
  let apex = t0.intersectionWith(t1);
  return TriangleForApex(apex);
}


export class SineSumModel {
  _container: HTMLElement;
  _viewSize: [number, number];
  _canvasSize: [number, number];
  _canvasBounds: Rect;
  _modelBounds: Rect;
  _coordinateTransform: IsoScaleTransform;
  _renderers: [RenderableElementsRenderer, RenderableElementsRenderer];
  renderParams: RenderableElementsRendererParams;

  //_sineSum: SineSum;
  _boundaryOffsetFunction: BoundaryOffsetFunction;

  extra: Array<CanvasRender.Renderable>;
  _needsRedraw: boolean;
  _controlPointsInterface: ?CallbackPointsInterface;

  vForward: number;
  vRight: number;
  vAngle: number;

  drawDistance: number;
  skipOdd: boolean;
  decayParam: number;
  _showPath: boolean;
  _showUnfolding: boolean;
  _unfoldingViewSize: number;

  sequenceResults: Array<[number, number]>;

  _apex: Coords2d;
  _source: TangentCoords2d;

  highlightCount: number;


  edgePaths: Array<string>;
  originalEdgePaths: Array<string>;
  _reflectedEdgeSequence: ?Array<number>;

  constructor(container: HTMLElement, viewSize: [number, number]) {
    window.console.log("SineSumModel");
    // Internal UI state
    this._container = container;
    this._viewSize = viewSize;
    this._canvasSize = [viewSize[0] / 2, viewSize[1]];
    this._canvasBounds = new Rect(
      Coords2d.fromXY(0, this._canvasSize[1]),
      OffsetCoords2d.fromDxDy(this._canvasSize[0], -this._canvasSize[1])
    );
    this._modelBounds = Rect.fromDimensions(2, 2);

    let path = new EdgePath("RRLLRRRRRRRRRRRRRRRLRLLRRLLLLLLLLLLLLLLLRL");
    let pathStudy = new PathStudy(path);
    let boundaryOffsetFunction = pathStudy.constraintFunction(9, 15);
    this._boundaryOffsetFunction = boundaryOffsetFunction;

    this._renderers = [
      new RenderableElementsRenderer(this._canvasSize),
      new RenderableElementsRenderer(this._canvasSize),
    ];
    this.renderParams = new RenderableElementsRendererParams();
    this._needsRedraw = false;

    this._apex = Coords2d.fromXY(0.6, 0.6);
    this._source = TangentCoords2d.fromBaseAndOffset(
      Coords2d.fromXY(0.5, 0.3),
      OffsetCoords2d.fromDxDy(1, 0)
    );

    this.vForward = 0;
    this.vRight = 0;
    this.vAngle = 0;

    this.drawDistance = 30;
    this.skipOdd = false;
    this.decayParam = 100;
    this._showPath = true;
    this._showUnfolding = false;
    this._unfoldingViewSize = 7.14;

    this.edgePaths = [

    ];
    this.originalEdgePaths = this.edgePaths.map((x) => x);
    this.highlightCount = 1;
  }

  // Triangle: the triangle in which we're looking at billiards.
  get triangle (): Triangle2d {
    return Triangle2d.fromCoords(
      Coords2d.origin(),
      Coords2d.fromXY(1, 0),
      this._apex);
  }

  // Coords2d: The apex of the triangle. The base is always from (0,0) to (1,0).
  get apex (): Coords2d {
    return this._apex;
  }

  set apex (apex: Coords2d) {
    this._apex = apex.copy();
    this._needsRedraw = true;
  }

  get boundaryOffsetFunction (): BoundaryOffsetFunction {
    return this._boundaryOffsetFunction;
  }

  set boundaryOffsetFunction (boundaryOffsetFunction: BoundaryOffsetFunction) {
    this._boundaryOffsetFunction = boundaryOffsetFunction;
    this._needsRedraw = true;
  }

  // RootedTCoords(2d): the starting position / vector from which to draw the
  // billiard path.
  get source (): TangentCoords2d {
    return this._source;
  }

  set source (source: TangentCoords2d) {
    this._source = source;
    this._needsRedraw = true;
  }

  get reflectedEdgeSequence (): ?Array<number> {
    return this._reflectedEdgeSequence;
  }

  set reflectedEdgeSequence (edgeSequence: ?Array<number>) {
    this._reflectedEdgeSequence = edgeSequence;
    this._needsRedraw = true;
  }

  get showPath (): boolean {
    return this._showPath;
  }

  set showPath (showPath: boolean) {
    this._showPath = showPath;
    this._needsRedraw = true;
  }

  get showUnfolding (): boolean {
    return this._showUnfolding;
  }

  set showUnfolding (showUnfolding: boolean) {
    this._showUnfolding = showUnfolding;
    this._needsRedraw = true;
  }

  get modelBounds (): Rect {
    return this._modelBounds;
  }
  set modelBounds (modelBounds: Rect) {
    var transform = IsoScaleTransform.forCenteredRect(
      this._canvasBounds, modelBounds);
    this._coordinateTransform = transform;
    var {origin, diagonal} = this._canvasBounds;
    var mappedOrigin = transform.transformCoords(origin);
    var mappedDiagonal = transform.transformOffset(diagonal);
    this._modelBounds = new Rect(mappedOrigin, mappedDiagonal);
    //mappedDiagonal.timesReal(1.0 / 3.0);
    //mappedOrigin.plus(mappedDiagonal);
    this._needsRedraw = true;
  }

  get unfoldingViewSize (): number {
    return this._unfoldingViewSize;
  }
  set unfoldingViewSize (unfoldingViewSize: number) {
    this._unfoldingViewSize = unfoldingViewSize;
    this._needsRedraw = true;
  }

  start () {
    $(this._container).css(
      {'width': `${this._viewSize[0]+4}px`, 'margin': 'auto',
        'position': 'relative'});
    $(this._container).append(this._renderers[0].canvas);
    $(this._container).append(this._renderers[1].canvas);
    $(this._container).append(this._createControls());

    this._createControlInterfaces();
    this._addEventListeners();

    window.console.log("Start");
    requestAnimationFrame(this.generateClockTick());
  }

  KeyDown (event: KeyboardEvent) {
    if (event.metaKey) {
      return;
    }
    var speed = 0.01;
    if (event.shiftKey) {
      speed *= 0.05;
      if (event.ctrlKey) {
        speed *= 0.05;
      }
    }
    switch (event.which) {
    case 38: // up
    case 87: // W
      this.vForward = 1.5*speed;
      break;

    case 40: // down
    case 83: // S
      this.vForward = -1.5*speed;
      break;

    case 37: // left
    case 65: // A
      this.vAngle = 2.0*speed;
      break;

    case 39: // right
    case 68: // D
      this.vAngle = -2.0*speed;
      break;

    case 81: // Q
      this.vRight = speed;
      break;

    case 69: // E
      this.vRight = -speed;
      break;

    default:
      return;
    }
    event.preventDefault();
  }

  KeyUp (event: KeyboardEvent) {
    switch (event.which) {
    case 38: // up
    case 87: // W
      if (this.vForward > 0.0) {
        this.vForward = 0;
      }
      break;

    case 40: // down
    case 83: // S
      if (this.vForward < 0.0) {
        this.vForward = 0;
      }
      break;

    case 37: // left
    case 65: // A
      if (this.vAngle > 0.0) {
        this.vAngle = 0;
      }
      break;

    case 39: // right
    case 68: // D
      if (this.vAngle < 0.0) {
        this.vAngle = 0.0;
      }
      break;

    case 81: // Q
      if (this.vRight > 0.0) {
        this.vRight = 0.0;
      }
      break;

    case 69: // E
      if (this.vRight < 0.0) {
        this.vRight = 0.0;
      }
      break;
    }
  }

  generateClockTick() {
    if (this._needsRedraw) {
      this.redraw();
    }
    return () => {
      requestAnimationFrame(this.generateClockTick());
    };
  }

  redraw () {

    this._needsRedraw = false;

    window.console.log("Hello?");
    let imagePosition =
      this._coordinateTransform.transformCoords(Coords2d.origin());

    if (this._renderers[0].canvas == null) {
      return;
    }
    let canvas = this._renderers[0].canvas;
    let plotFunction = this.boundaryOffsetFunction;
    window.console.log(`Plot function: ${plotFunction.toString()}`);
    let pixelSpaceToModelSpace = IsoScaleTransform.forCenteredRect(
      Rect.fromXYWH(0, 0, canvas.width, canvas.height), this.modelBounds);
    let fullFunction = new CanvasRender.ImageRendering(
      imagePosition, canvas.width, canvas.height,
      (imageData: ImageData) => {
        let width = canvas.width;
        let height = canvas.height;
        let v00 = pixelSpaceToModelSpace.transformCoords(Coords2d.origin());
        let vnn = pixelSpaceToModelSpace.transformCoords(
          Coords2d.fromXY(width - 1, height - 1)
        );
        window.console.log(`Corners: ${v00.toString()}, ${vnn.toString()}`);
        for (let y = 0; y < height; y++) {
          for (let x = 0; x < width; x++) {
            let offset = y * width + x;
            let modelCoords =
              pixelSpaceToModelSpace.transformCoords(Coords2d.fromXY(x, y));
            let triangle = TriangleWithBaseAngles(modelCoords.x, modelCoords.y);
            //let sineSum = plotFunction.sineSums[0];
            let value = //sineSum.valueOnAngles(modelCoords.x, modelCoords.y);
              //sineSum.valueOnTriangle(triangle);
              plotFunction.valueOnTriangle(triangle);
            let intensity = (value + 1) / 2;
            if (value >= 0) {
              imageData.data[offset*4 + 0] = 0;//(1 - intensity) * 255;
              imageData.data[offset*4 + 1] = (1 - intensity) * 255;//0;
              imageData.data[offset*4 + 2] = intensity * 255;
              imageData.data[offset*4 + 3] = 255;
            } else {
              imageData.data[offset*4 + 0] = 40;
              imageData.data[offset*4 + 1] = 30;
              imageData.data[offset*4 + 2] = 30;
              imageData.data[offset*4 + 3] = 255;
            }
          }
        }
      }
    );
    let params = this.renderParams;
    params.elements = [fullFunction];//elements;
    this._renderers[0].modelBounds = this.modelBounds;
    this._renderers[0].render(params);

    let partialFunction = new CanvasRender.ImageRendering(
      imagePosition, canvas.width, canvas.height,
      (imageData: ImageData) => {
        let width = canvas.width;
        let height = canvas.height;
        for (let y = 0; y < height; y++) {
          for (let x = 0; x < width; x++) {
            let offset = y * width + x;
            let modelCoords =
              pixelSpaceToModelSpace.transformCoords(Coords2d.fromXY(x, y));
            let sineSum = plotFunction.sineSums[0];
            let value = sineSum.valueOnAngles(modelCoords.x, modelCoords.y);
            let intensity = (value + 1) / 2;
            if (value >= 0) {
              imageData.data[offset*4 + 0] = 0;//(1 - intensity) * 255;
              imageData.data[offset*4 + 1] = (1 - intensity) * 255;//0;
              imageData.data[offset*4 + 2] = intensity * 255;
              imageData.data[offset*4 + 3] = 255;
            } else {
              imageData.data[offset*4 + 0] = 40;
              imageData.data[offset*4 + 1] = 30;
              imageData.data[offset*4 + 2] = 30;
              imageData.data[offset*4 + 3] = 255;
            }
          }
        }
      }
    );

    params.elements = [partialFunction];//elements;
    this._renderers[1].modelBounds = this.modelBounds;
    this._renderers[1].render(params);
  }

  _createControls() {
    var div = $("<div></div>").css({
      width: this._viewSize[0], height: '31px', margin: 'auto',
      position: 'absolute', left: '0px', top: `${this._viewSize[1]}px`});
    div.html(`
      <form style="margin-top: 5px; float: left; width: 50%;"
          class="phase-controls">
        <div style="display: inline-block;float: left;">
          <label>
            <input style="margin:4px;" type="checkbox" checked="checked"
                name="show-path">
            Show path
          </label>
        </div>
        <div style="display: inline-block;float: left; padding-left:20px;">
          <label>
            <input type="radio" name="draw-distance" value="3">
            3
          </label>
        </div>
        <div style="display: inline-block;float: left;">
          <label>
            <input type="radio" name="draw-distance" value="30"
                checked="checked">
            30
          </label>
        </div>
        <div style="display: inline-block;float: left;">
          <label>
            <input type="radio" name="draw-distance" value="200">
            200
          </label>
        </div>
        <div style="display: inline-block;float: left;">
          <label>
            <input style="margin:2px 8px;" type="checkbox"
                name="show-unfolding">
            Show unfolding
          </label>
        </div>
      </form>
      <div style="padding: 4px 6px; float: right;">
        <input name="edgePath" type="text" size="50">
      </div>
      <div style="padding: 4px 6px; float: right;">
        Zoom:
        <input name="zoom" type="range" min="1" max="10" step="0.01"
            value="7">
      </div>
      `);
    return div;
  }

  _triangleEditCallback (canvasCoords: Coords2d) {
    // This code assumes the base of the triangle is from (0,0) to (1,0)
    // in model space.
    var maxDist = this._coordinateTransform.isoScale() * 30;
    var modelCoords =
        this._coordinateTransform.transformCoords(canvasCoords);

    let originalApexCoords = this.apex.copy();
    let originalSource = this.source.copy();
    // Measure the fraction of the apex height.
    let originalSourceVert = originalSource.base.y / originalApexCoords.y;
    // Measure the intersection with the base when projecting down
    // from the apex.
    let originalSourceHorz = originalApexCoords.x +
      (originalSource.base.x - originalApexCoords.x) *
      (1.0 / (1.0 - originalSourceVert));
    if (modelCoords.distanceFrom(originalApexCoords) <= maxDist) {
      return (newCanvasCoords: Coords2d) => {
        var newModelCoords =
            this._coordinateTransform.transformCoords(newCanvasCoords);
        if (newModelCoords.y < 0.01) {
          newModelCoords.y = 0.01;
        }
        if (newModelCoords.x < 0) {
          newModelCoords.x = 0;
        }
        if (newModelCoords.x > 1) {
          newModelCoords.x = 1;
        }
        var modelDelta = newModelCoords.asOffsetFrom(modelCoords);
        let newApexCoords = originalApexCoords.plus(modelDelta);

        let newSourceY = newApexCoords.y * originalSourceVert;
        let newSourceX = newApexCoords.x +
          (originalSourceHorz - newApexCoords.x) *
          (1.0 - originalSourceVert);

        this.apex = newApexCoords;
        this.source.base = Coords2d.fromXY(newSourceX, newSourceY);
        this.source.offset.dy = originalSource.offset.dy *
          (newApexCoords.y / originalApexCoords.y);
        this.source.offset = this.source.offset.normalized();
        this._needsRedraw = true;
      };
    }
  }

  _createControlInterfaces() {
    if (this._renderers[0].canvas != null) {
      this._controlPointsInterface = new CallbackPointsInterface(
        (canvasCoords: Coords2d) => {
          return this._triangleEditCallback(canvasCoords);
        }, this._renderers[0].canvas);
    }
  }

  _addEventListeners() {
    $(this._container).find('input').change(() => {
      window.console.log("Input changed in container:", this._container);
      this.drawDistance = parseInt($(this._container)
        .find('input[name="draw-distance"]:checked')[0].value);
      this.showPath =
        $(this._container).find('input[name="show-path"]').is(':checked');
      this.showUnfolding =
        $(this._container).find('input[name="show-unfolding"]').is(':checked');
      let zoom = $(this._container).find('input[name="zoom"]')[0].value;
      this.unfoldingViewSize = 50 / zoom;
      this._needsRedraw = true;
    });
    $(this._container).find('input[name="zoom"]').on('input', () => {
      let zoom = $(this._container).find('input[name="zoom"]')[0].value;
      this.unfoldingViewSize = 50 / zoom;
    });
    $(this._container).find('.refresh').click((event) => {
      if (event.button == 0) {
        let edgePath = $(this._container)
          .find('input[name="edgePath"]')[0].value;
        if (edgePath.length > 0) {
          let edgeSequence = EdgeSequenceForEdgePath(edgePath);
          this.reflectedEdgeSequence = edgeSequence;
        } else {
          this.reflectedEdgeSequence = null;
        }
      }
    });
    $(document).keydown((event) => {
      this.KeyDown(event);
    });
    $(document).keyup((event) => {
      this.KeyUp(event);
    });
  }
}
*/
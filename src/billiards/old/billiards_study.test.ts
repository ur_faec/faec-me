

import { Coords2d, OffsetCoords2d } from "core/geometry";
import { Triangle2d } from "core/polygon";

import { BoundaryCoords, BoundaryOffsets } from "./billiards_calc";
import { BoundaryCoordsForPathAndTriangle, BoundaryOffsetsForPathAndTriangle } from "./billiards_calc";
import { TriangleForApex } from "./billiards_params";
import { PathStudy, BoundaryVertexIndex } from "./billiards_study";
import { EdgePath, EdgeSequenceForEdgePath, TangentForPeriodicPath } from "./edge_path";
import { ReflectedAngle, ReflectedEdge } from "./reflected_triangle";
import { ReflectedEdgeInnerProduct } from "./edge_inner_product";
import { SineEntry } from "./sine_sum";

type TangentForPeriodicPathTestCase = {
	pathStr: string;
	apex: Coords2d;
	constraintLeft: number;
	constraintRight: number;
	hasPath: boolean;
};

function TangentForPeriodicPathTest(test: TangentForPeriodicPathTestCase) {
	return () => {
		const triangle = TriangleForApex(test.apex);
		const edgeSequence = EdgeSequenceForEdgePath(test.pathStr);
		const tangent = TangentForPeriodicPath(triangle, edgeSequence);
		const tangentExists = tangent != null;

		expect(tangentExists).toEqual(test.hasPath);
	};
}

test("Left(2) and Right(4) should be positively separated", TangentForPeriodicPathTest({
	pathStr: "RRRLRRRLLLRLLL",
	apex: Coords2d.fromXY(0.5, 0.35),
	constraintLeft: 2,
	constraintRight: 4,
	hasPath: true,
}));

test("Left(2) and Right(4) should be negatively separated", TangentForPeriodicPathTest({
	pathStr: "RRRLRRRLLLRLLL",
	apex: Coords2d.fromXY(0.5, 0.34),
	constraintLeft: 2,
	constraintRight: 4,
	hasPath: false,
}));


class BoundaryCompare {

	edgePath: EdgePath;
	triangle: Triangle2d;
	explicitBoundary: BoundaryCoords;
	implicitBoundary: BoundaryCoords;

	constructor(edgePath: EdgePath, triangle: Triangle2d) {
		this.edgePath = edgePath;
		this.triangle = triangle;
		this.explicitBoundary = BoundaryCoordsForPathAndTriangle(edgePath, triangle);

		const pathStudy = new PathStudy(edgePath);
		const leftBoundary: Array<Coords2d> = [triangle.vertexCoords[0]];
		const rightBoundary: Array<Coords2d> = [triangle.vertexCoords[1]];
		for (let i = 0; i < pathStudy.leftEdges.length; i++) {
			const leftBoundaryEdge = pathStudy.leftEdges[i];
			const offset: OffsetCoords2d = leftBoundaryEdge.reflectedEdge.valueOnTriangle(triangle);
			const coords = leftBoundary[leftBoundary.length - 1].plus(offset);
			leftBoundary.push(coords);
		}
		for (let i = 0; i < pathStudy.rightEdges.length; i++) {
			const rightBoundaryEdge = pathStudy.rightEdges[i];
			const offset: OffsetCoords2d = rightBoundaryEdge.reflectedEdge.valueOnTriangle(triangle);
			const coords = rightBoundary[rightBoundary.length - 1].plus(offset);
			rightBoundary.push(coords);
		}
		this.implicitBoundary = { leftBoundary, rightBoundary };
	}

	match(): boolean {
		function arraysMatch(a1: Array<Coords2d>, a2: Array<Coords2d>): boolean {
			if (a1.length != a2.length) {
				return false;
			}
			let totalDistance = 0;
			for (let i = 0; i < a1.length; i++) {
				const offset = a1[i].asOffsetFrom(a2[i]);
				totalDistance += offset.length();
			}
			return totalDistance < 0.0000001;
		}
		return (arraysMatch(this.explicitBoundary.leftBoundary, this.implicitBoundary.leftBoundary) && arraysMatch(this.explicitBoundary.rightBoundary, this.implicitBoundary.rightBoundary));
	}
}

type BoundaryCompareTestCase = {
	path: EdgePath;
	triangle: Triangle2d;
};

function BoundaryCompareTest(test: BoundaryCompareTestCase) {
	return () => {
		const boundaryCompare = new BoundaryCompare(test.path, test.triangle);
		expect(boundaryCompare.match()).toBe(true);
	};
}

test("Explicitly computed boundaries should match the ones in PathStudy", BoundaryCompareTest({
	path: new EdgePath("RLRLLRLRRRRLRLLRLRRRRRLRLLLLLLLLRLRRRRRRLRLLLLLLLLRLRRRR"),
	triangle: TriangleForApex(Coords2d.fromXY(0.10422091619622, 0.29821738815274)),
}));

test("Explicitly computed boundaries should match the ones in PathStudy", BoundaryCompareTest({
	path: new EdgePath("RLRLLRLRRRRLRLLRLRRRRRLRLLLLLLLLRLRRRRRRLRLLLLLLLLRLRRRR"),
	triangle: TriangleForApex(Coords2d.fromXY(0.3, 0.05)),
}));

test("Explicitly computed boundaries should match the ones in PathStudy", BoundaryCompareTest({
	path: new EdgePath("RRRRRRRRRLRRLRRRRRRRRRRLRRLRRRRRRRRRLLLRLLLLLRRLLLLLLLLLRLLRL" + "LLLLLLLLRRLLLLLRLLL"),
	triangle: TriangleForApex(Coords2d.fromXY(0.4013532, 0.01503253)),
}));


type EdgeInnerProductTestCase = {
	triangle: Triangle2d;
	edges: [ReflectedEdge, ReflectedEdge];
};

function EdgeInnerProductTest(test: EdgeInnerProductTestCase) {
	return () => {
		const edge1 = test.edges[0];
		const edge2 = test.edges[1];
		const edgeValue1 = edge1.valueOnTriangle(test.triangle);
		const edgeValue2 = edge2.valueOnTriangle(test.triangle);
		const innerProductValue = edgeValue1.dot(edgeValue2.cross());
		const innerProduct = ReflectedEdgeInnerProduct.fromEdges(edge1, edge2);
		const implicitValue = innerProduct.valueOnTriangle(test.triangle);
		// Expect accuracy to 5 decimals.
		expect(implicitValue).toBeCloseTo(innerProductValue, 5);
	};
}

test("ReflectedEdgeInnerProduct should produce the same value as its inputs", EdgeInnerProductTest({
	triangle: TriangleForApex(Coords2d.fromXY(0.10422091619622, 0.29821738815274)),
	edges: [new ReflectedEdge(2, new ReflectedAngle([1, 2], false)), new ReflectedEdge(2, new ReflectedAngle([-1, 0], true))],
}));

test("ReflectedEdgeInnerProduct should produce the same value as its inputs", EdgeInnerProductTest({
	triangle: TriangleForApex(Coords2d.fromXY(0.10422091619622, 0.29821738815274)),
	edges: [new ReflectedEdge(0, new ReflectedAngle([-6, -10], false)), new ReflectedEdge(2, new ReflectedAngle([-1, 0], true))],
}));

test("ReflectedEdgeInnerProduct should produce the same value as its inputs", EdgeInnerProductTest({
	triangle: TriangleForApex(Coords2d.fromXY(0.2, 0.01)),
	edges: [new ReflectedEdge(2, new ReflectedAngle([-9, -10], true)), new ReflectedEdge(2, new ReflectedAngle([-1, 0], true))],
}));


export class EdgeInnerProductCompare {

	edgePath: EdgePath;
	triangle: Triangle2d;

	explicitOffsets: BoundaryOffsets;
	implicitOffsets: BoundaryOffsets;

	constructor(edgePath: EdgePath, triangle: Triangle2d) {
		this.edgePath = edgePath;
		this.triangle = triangle;

		const pathStudy = new PathStudy(edgePath);

		// Given (path: string, t: Triangle2d, )
		// Compute, for each boundary:
		//   for each vertex:
		//     InnerProduct(
		//       vertex.asOffsetFrom(leftBoundary[0]),
		//         PathOffset(path, t).cross()
		//     )
		// We compute this in two different ways: the "explicit" way where we run
		// the preceding exactly on the coordinates of the boundary,
		// and the "implicit" where we represent the vertices and offsets as
		// sums of ReflectedEdges, then compute InnerProduct by distributing the
		// multiplication termwise to get a SineSum, and calling valueOnTriangle
		// to get the final scalar.
		this.explicitOffsets = BoundaryOffsetsForPathAndTriangle(edgePath, triangle);

		const innerProductSum = (innerProducts: Array<ReflectedEdgeInnerProduct>) => {
			let total = 0;
			for (let i = 0; i < innerProducts.length; i++) {
				total += innerProducts[i].valueOnTriangle(this.triangle);
			}
			return total;
		};

		const implicitLeftOffsets: Array<number> = [];
		for (let i = 0; i <= pathStudy.leftEdges.length; i++) {
			const innerProducts = pathStudy.innerProductsForBoundaryVertex(BoundaryVertexIndex.left(i));
			implicitLeftOffsets.push(innerProductSum(innerProducts));
		}
		const implicitRightOffsets: Array<number> = [];
		for (let i = 0; i <= pathStudy.rightEdges.length; i++) {
			const innerProducts = pathStudy.innerProductsForBoundaryVertex(BoundaryVertexIndex.right(i));
			implicitRightOffsets.push(innerProductSum(innerProducts));
		}
		this.implicitOffsets = {
			leftOffsets: implicitLeftOffsets,
			rightOffsets: implicitRightOffsets,
		};
	}

	match(): boolean {
		const arrayMatch = function (a: Array<number>, b: Array<number>) {
			if (a.length != b.length) {
				return false;
			}
			let err = 0;
			for (let i = 0; i < a.length; i++) {
				err += Math.abs(a[i] - b[i]);
			}
			return err < 0.00000001;
		};
		return (arrayMatch(this.explicitOffsets.leftOffsets, this.implicitOffsets.leftOffsets) && arrayMatch(this.explicitOffsets.rightOffsets, this.implicitOffsets.rightOffsets));
	}
}

type EdgeInnerProductCompareTestCase = {
	path: EdgePath;
	triangle: Triangle2d;
};

function EdgeInnerProductCompareTest(test: EdgeInnerProductCompareTestCase) {
	return () => {
		const compare = new EdgeInnerProductCompare(test.path, test.triangle);
		expect(compare.match()).toBe(true);
	};
}

test("PathStudy.innerProductsForBoundaryVertex should evaluate to the same" + "values as its raw inputs", EdgeInnerProductCompareTest({
	path: new EdgePath("RLRLRLRLRLRL"),
	triangle: TriangleForApex(Coords2d.fromXY(0.5, 0.25)),
}));

test("PathStudy.innerProductsForBoundaryVertex should evaluate to the same" + "values as its raw inputs", EdgeInnerProductCompareTest({
	path: new EdgePath("RLRLLRLRRRRLRLLRLRRRRRLRLLLLLLLLRLRRRRRRLRLLLLLLLLRLRRRR"),
	triangle: TriangleForApex(Coords2d.fromXY(0.10422091619622, 0.29821738815274)),
}));

test("PathStudy.innerProductsForBoundaryVertex should evaluate to the same" + "values as its raw inputs", EdgeInnerProductCompareTest({
	path: new EdgePath("RLRLLRLRRRRLRLLRLRRRRRLRLLLLLLLLRLRRRRRRLRLLLLLLLLRLRRRR"),
	triangle: TriangleForApex(Coords2d.fromXY(0.3, 0.05)),
}));


type SineEntryTestCase = {
	triangle: Triangle2d;
	sineAngle: ReflectedAngle;
};

function SineEntryTest(test: SineEntryTestCase) {
	return () => {
		const rawAngle = test.sineAngle.valueOnTriangle(test.triangle);
		const sineEntry = SineEntry.fromAngle(test.sineAngle);
		expect(sineEntry.valueOnTriangle(test.triangle)).toBeCloseTo(Math.sin(rawAngle), 5);
	};
}

test("SineEntry should produce the same values as Math.sin(reflectedAngle)", SineEntryTest({
	triangle: TriangleForApex(Coords2d.fromXY(0.10422091619622, 0.29821738815274)),
	sineAngle: new ReflectedAngle([1, 2], false),
}));

test("SineEntry should produce the same values as Math.sin(reflectedAngle)", SineEntryTest({
	triangle: TriangleForApex(Coords2d.fromXY(0.10422091619622, 0.29821738815274)),
	sineAngle: new ReflectedAngle([-9, 15], true),
}));

test("SineEntry should produce the same values as Math.sin(reflectedAngle)", SineEntryTest({
	triangle: TriangleForApex(Coords2d.fromXY(0.2, 0.01)),
	sineAngle: new ReflectedAngle([-30, -5], false),
}));

class BoundaryFunctionCompare {

	edgePath: EdgePath;
	triangle: Triangle2d;

	explicitOffsets: BoundaryOffsets;
	implicitOffsets: BoundaryOffsets;

	constructor(edgePath: EdgePath, triangle: Triangle2d) {
		this.edgePath = edgePath;
		this.triangle = triangle;

		this.explicitOffsets = BoundaryOffsetsForPathAndTriangle(edgePath, triangle);

		const pathStudy = new PathStudy(edgePath);

		const implicitLeftOffsets: Array<number> = [];
		for (let i = 0; i <= pathStudy.leftEdges.length; i++) {
			const boundaryFunction = pathStudy.offsetFunctionForBoundaryVertex(BoundaryVertexIndex.left(i));
			implicitLeftOffsets.push(boundaryFunction.valueOnTriangle(triangle));
		}
		const implicitRightOffsets: Array<number> = [];
		for (let i = 0; i <= pathStudy.rightEdges.length; i++) {
			const boundaryFunction = pathStudy.offsetFunctionForBoundaryVertex(BoundaryVertexIndex.right(i));
			implicitRightOffsets.push(boundaryFunction.valueOnTriangle(triangle));
		}
		this.implicitOffsets = {
			leftOffsets: implicitLeftOffsets,
			rightOffsets: implicitRightOffsets,
		};
	}

	match(): boolean {
		const arrayMatch = function (a: Array<number>, b: Array<number>) {
			if (a.length != b.length) {
				return false;
			}
			let err = 0;
			for (let i = 0; i < a.length; i++) {
				err += Math.abs(a[i] - b[i]);
			}
			return err < 0.00000001;
		};
		return (arrayMatch(this.explicitOffsets.leftOffsets, this.implicitOffsets.leftOffsets) && arrayMatch(this.explicitOffsets.rightOffsets, this.implicitOffsets.rightOffsets));
	}
}

type BoundaryFunctionCompareTestCase = {
	path: EdgePath;
	triangle: Triangle2d;
};

function BoundaryFunctionCompareTest(test: BoundaryFunctionCompareTestCase) {
	return () => {
		const boundaryFunctionCompare = new BoundaryFunctionCompare(test.path, test.triangle);
		expect(boundaryFunctionCompare.match()).toBe(true);
	};
}

test("BoundaryFunction should produce the same offsets as explicit calculation", BoundaryFunctionCompareTest({
	path: new EdgePath("RLRLRLRLRLRL"),
	triangle: TriangleForApex(Coords2d.fromXY(0.5, 0.25)),
}));

test("BoundaryFunction should produce the same offsets as explicit calculation", BoundaryFunctionCompareTest({
	path: new EdgePath("RLRLLRLRRRRLRLLRLRRRRRLRLLLLLLLLRLRRRRRRLRLLLLLLLLRLRRRR"),
	triangle: TriangleForApex(Coords2d.fromXY(0.10422091619622, 0.29821738815274)),
}));

test("BoundaryFunction should produce the same offsets as explicit calculation", BoundaryFunctionCompareTest({
	path: new EdgePath("RLRLLRLRRRRLRLLRLRRRRRLRLLLLLLLLRLRRRRRRLRLLLLLLLLRLRRRR"),
	triangle: TriangleForApex(Coords2d.fromXY(0.3, 0.05)),
}));
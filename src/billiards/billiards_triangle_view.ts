

import { CallbackPointsInterface } from "core/callback_points_interface";
import { RenderTarget } from "core/canvas";
import { Circle, Renderable, Polygon, Line, RenderableElementsRenderer, RenderableElementsRendererParams } from "core/element_render";
import { Coords2d, TangentCoords2d, OffsetCoords2d } from "core/geometry";
import { Observable } from "core/observable";
import { AxisRect, Triangle2d, TrianglePerimeterPoint } from "core/polygon";

export class BilliardsTriangleView {

	renderTarget: RenderTarget;
	renderer: RenderableElementsRenderer;
	renderParams: RenderableElementsRendererParams;

	extra: Array<Renderable> = [];
	_needsRedraw: boolean;
	_controlPointsInterface: CallbackPointsInterface | null = null;

	// Keyboard control state.
	vForward: number;
	vRight: number;
	vAngle: number;

	_drawDistance: number;
	skipOdd: boolean;
	decayParam: number;
	_showSource: boolean;

	sequenceResults: Array<[number, number]> = [];

	_apex: Observable<Coords2d>;
	_source: Observable<TangentCoords2d> | null;

	constructor(
		renderTarget: RenderTarget,
		apex: Observable<Coords2d>,
		source: Observable<TangentCoords2d> | null = null
	) {
		console.log("BilliardsTriangleView.constructor");
		// Internal UI state
		this.renderTarget = renderTarget;
		this.renderer = new RenderableElementsRenderer(renderTarget);
		this.renderParams = new RenderableElementsRendererParams();

		this.renderer.modelBounds = AxisRect.fromXYWH(-0.1, -0.1, 1.2, 1.2);

		this._apex = apex;
		apex.addObserver((oldValue, newValue) => {this._needsRedraw = true;});
		this._source = source;
		if (source != null) {
			source.addObserver((oldValue, newValue) => {this._needsRedraw = true;});
		}
		this._needsRedraw = true;

		this.vForward = 0;
		this.vRight = 0;
		this.vAngle = 0;

		this._drawDistance = 30;
		this.skipOdd = false;
		this.decayParam = 100;
		this._showSource = true;

		this._addEventListeners();

		requestAnimationFrame(this.generateClockTick());
	}

	// Triangle: the triangle in which we're looking at billiards.
	get triangle(): Triangle2d {
		return Triangle2d.fromCoords(Coords2d.origin(), Coords2d.fromXY(1, 0), this._apex.value);
	}

	// Coords2d: The apex of the triangle. The base is always from (0,0) to (1,0).
	get apex(): Coords2d {
		return this._apex.value;
	}

	// RootedTCoords(2d): the starting position / vector from which to draw the
	// billiard path.
	get source(): TangentCoords2d | null {
		return this._source && this._source.value;
	}

	get drawDistance(): number {
		return this._drawDistance;
	}

	set drawDistance(drawDistance: number) {
		this._drawDistance = drawDistance;
		this._needsRedraw = true;
	}

	get showSource(): boolean {
		return this._showSource;
	}

	set showSource(showSource: boolean) {
		if (showSource != this._showSource) {
			this._showSource = showSource;
			this._needsRedraw = true;
		}
	}

	_advanceSource(): void {
		if (this.source == null) {
			return;
		}
		if (this.vForward != 0 || this.vRight != 0 || this.vAngle != 0) {
			const oldSource: TangentCoords2d = this.source;
			let newBase: Coords2d | null = null;
			let newOffset: OffsetCoords2d | null = null;

			const c = oldSource.base;
			const t = oldSource.offset.normalized();
			const angleStart = Math.atan2(t.dy, t.dx);
			let angleDelta = 0;
			if (this.vForward != 0 || this.vRight != 0) {
				const vx = t.dx * this.vForward - t.dy * this.vRight;
				const vy = t.dy * this.vForward + t.dx * this.vRight;
				const v = TangentCoords2d.fromBaseAndOffset(c, OffsetCoords2d.fromDxDy(vx, vy));
				let vdist = v.offset.length();
				v.offset = v.offset.normalized();
				while (vdist > 0) {
					const {
						edge,
					} = TriangleEdgeIntersection(this.triangle, v);
					const idist = v.coefficientOfIntersectionWith(edge);
					const intersection = v.base.plus(v.offset.timesReal(idist));
					if (vdist > idist) {
						v.base = intersection;
						v.offset = edge.reflectTCoords(v.offset);
						vdist -= idist;
						continue;
					}
					v.base = v.base.plus(v.offset.timesReal(vdist));
					break;
				}
				angleDelta = Math.atan2(v.offset.dy, v.offset.dx) - Math.atan2(vy, vx);
				newBase = v.base;
			}
			if (this.vAngle != 0 || angleDelta != 0) {
				newOffset = OffsetCoords2d.fromPolar(1, angleStart + angleDelta + this.vAngle);
			}
			const newSource = TangentCoords2d.fromBaseAndOffset(newBase || oldSource.base, newOffset || oldSource.offset);
			if (this._source) {
				this._source.set(newSource);
			}
			// TODO: this should be set thru the listener
			this._needsRedraw = true;
		}
	}

	ElementsForTriangleView(): Array<Renderable> {
		const triangle = this.triangle;
		const showSource = this.showSource;
		const decayParam = this.decayParam;
		const skipOdd = this.skipOdd;

		const elements = [];
		let totalLength = 0;

		// If we're rendering a path, also render the path source.
		if (showSource && this._source != null) {
			const source = this._source.value;
			let sourceRender = Circle.fromCenterAndRadius(source.base, 0.015);
			sourceRender.style.fillStyle = '#0088bb';
			elements.push(sourceRender);
			sourceRender = Circle.fromCenterAndRadius(source.base, 0.05);
			sourceRender.style.strokeStyle = '#000000';
			elements.push(sourceRender);
		}

		// Render the triangle itself.
		const triRender = Polygon.fromTriangle(triangle);
		triRender.style.strokeStyle = '#ff33ee';
		triRender.style.lineWidth = '2';
		elements.push(triRender);

		if (showSource && this._source != null) {
			const source = this._source.value;
			const reflections = TriangleReflectionsThroughTangent(this.triangle, source, this.drawDistance);
			const path = reflections.intersections;

			const coordsList = path.map(perimeterPoint => triangle.coordsForPerimeterPoint(perimeterPoint));
			let prevCoords = source.base;
			const backElements = [];
			for (let i = 0; i < coordsList.length; i++) {
				const length = coordsList[i].distanceFrom(prevCoords);
				totalLength += length;

				const decay = decayParam / (decayParam + totalLength * totalLength);
				const c = Math.floor(decay * 255 + 0.5);
				const stroke = `rgb(${Math.floor(220 - 3 * c / 4)},${Math.floor(220 - 3 * c / 4)},255)`;
				const line = Line.fromEndpoints(prevCoords, coordsList[i]);
				if (!skipOdd || i % 2 == 0) {
					line.style.strokeStyle = stroke;
					elements.push(line);
				} else {
					line.style.strokeStyle = '#eeeeee';
					backElements.push(line);
				}
				prevCoords = coordsList[i];
			}
		}
		return elements;
	}

	KeyDown(event: KeyboardEvent): void {
		if (event.metaKey) {
			return;
		}
		let speed = 0.01;
		if (event.shiftKey) {
			speed *= 0.05;
			if (event.ctrlKey) {
				speed *= 0.05;
			}
		}
		switch (event.which) {
		case 38:
		case 87:
			// W
			this.vForward = 1.5 * speed;
			break;

		case 40:
		case 83:
			// S
			this.vForward = -1.5 * speed;
			break;

		case 37:
		case 65:
			// A
			this.vAngle = 2.0 * speed;
			break;

		case 39:
		case 68:
			// D
			this.vAngle = -2.0 * speed;
			break;

		case 81:
			// Q
			this.vRight = speed;
			break;

		case 69:
			// E
			this.vRight = -speed;
			break;

		default:
			return;

		}
		event.preventDefault();
	}

	KeyUp(event: KeyboardEvent): void {
		switch (event.which) {
		case 38:case 87:
			// W
			if (this.vForward > 0.0) {
				this.vForward = 0;
			}

			break;

		case 40:case 83:
			// S
			if (this.vForward < 0.0) {
				this.vForward = 0;
			}

			break;

		case 37:case 65:
			// A
			if (this.vAngle > 0.0) {
				this.vAngle = 0;
			}

			break;

		case 39:case 68:
			// D
			if (this.vAngle < 0.0) {
				this.vAngle = 0.0;
			}

			break;

		case 81:
			// Q
			if (this.vRight > 0.0) {
				this.vRight = 0.0;
			}

			break;

		case 69:
			// E
			if (this.vRight < 0.0) {
				this.vRight = 0.0;
			}

			break;

		}
	}

	generateClockTick(): () => void {
		this._advanceSource();
		if (this._needsRedraw) {
			this.redraw();
		}
		return () => {
			requestAnimationFrame(this.generateClockTick());
		};
	}

	redraw(): void {
		this._needsRedraw = false;

		let elements = this.ElementsForTriangleView();
		if (this.extra) {
			elements = elements.concat(this.extra);
		}
		const renderParams = this.renderParams;
		renderParams.elements = elements;
		this.renderer.render(renderParams);
	}

	_triangleEditCallback(canvasCoords: Coords2d): ((arg0: Coords2d) => void) | null {
		const modelToCanvas = this.renderer.modelToCanvasTransform();
		const canvasToModel = modelToCanvas.inverse();
		// This code assumes the base of the triangle is from (0,0) to (1,0)
		// in model space.
		const maxDist = canvasToModel.isoScale() * 30;
		const modelCoords = canvasToModel.transformCoords(canvasCoords);

		const originalApexCoords: Coords2d = this.apex;
		const originalSource: TangentCoords2d | null = this.source;
		if (modelCoords.distanceFrom(originalApexCoords) <= maxDist) {
			return (newCanvasCoords: Coords2d) => {
				const newModelCoords = canvasToModel.transformCoords(newCanvasCoords);
				if (newModelCoords.y < 0.01) {
					newModelCoords.y = 0.01;
				}
				if (newModelCoords.x < 0) {
					newModelCoords.x = 0;
				}
				if (newModelCoords.x > 1) {
					newModelCoords.x = 1;
				}
				const modelDelta = newModelCoords.asOffsetFrom(modelCoords);
				const newApexCoords = originalApexCoords.plus(modelDelta);

				this._apex.set(newApexCoords);

				if (originalSource != null) {
					const oldSource: TangentCoords2d = originalSource;
					const yCoeff = newApexCoords.y / originalApexCoords.y;

					// Measure the fraction of the apex height.
					const oldSourceVert = oldSource.base.y / originalApexCoords.y;
					// Measure the intersection with the base when projecting down
					// from the apex.
					const oldSourceHorz = originalApexCoords.x + (oldSource.base.x - originalApexCoords.x) * (1.0 / (1.0 - oldSourceVert));
					const newSourceX = newApexCoords.x + (oldSourceHorz - newApexCoords.x) * (1.0 - oldSourceVert);
					const newSourceY = newApexCoords.y * oldSourceVert;

					const newBase = Coords2d.fromXY(newSourceX, newSourceY);
					const newOffset = OffsetCoords2d.fromDxDy(oldSource.offset.dx, yCoeff * oldSource.offset.dy);

					// This is always true if originalSource != null, but the
					// typechecker doesn't know that...
					if (this._source != null) {
						this._source.set(TangentCoords2d.fromBaseAndOffset(newBase, newOffset));
					}
				}
			};
		}
		return null;
	}

	_addEventListeners(): void {
		this.renderTarget.canvas.addPositionListener((canvasCoords: Coords2d) => {
			return this._triangleEditCallback(canvasCoords);
		});

		document.addEventListener("keydown", KeyDownCallback(this));
		document.addEventListener("keyup", KeyUpCallback(this));
	}
}

type KeyboardCallback = (this: Document, ev: KeyboardEvent) => void;

function KeyDownCallback(view: BilliardsTriangleView): KeyboardCallback {
	function callback(this: Document, ev: KeyboardEvent): void {
		view.KeyDown(ev);
	}
	return callback;
}

function KeyUpCallback(view: BilliardsTriangleView): KeyboardCallback {
	function callback(this: Document, ev: KeyboardEvent): void {
		view.KeyUp(ev);
	}
	return callback;
}

// Returns the (inner) edge rt intersects.
function TriangleEdgeIntersection(triangle: Triangle2d, rt: TangentCoords2d): {
	edge: TangentCoords2d;edgeIndex: number;
} {
	let bestEdgeIndex = -1;
	let bestCoeff = 0;
	for (let i = 0; i < 3; i++) {
		const edge = triangle.rootedEdge(i);
		const normal = OffsetCoords2d.fromDxDy(-edge.offset.dy, edge.offset.dx);
		if (normal.dot(rt.offset) >= 0) {
			continue;
		}
		const coeff = rt.coefficientOfIntersectionWith(edge);
		if (coeff >= 0 && (bestEdgeIndex < 0 || coeff < bestCoeff)) {
			bestEdgeIndex = i;
			bestCoeff = coeff;
		}
	}
	return {
		edge: triangle.rootedEdge(bestEdgeIndex),
		edgeIndex: bestEdgeIndex,
	};
}

// Computes reflections of the given tangent up to distance pathLength,
// returning the sequence of edges that were crossed and their location
// on the perimeter of the triangle.
function TriangleReflectionsThroughTangent(triangle: Triangle2d, tangent: TangentCoords2d, pathLength: number): {
	edgeSequence: Array<number>;
	intersections: Array<TrianglePerimeterPoint>;
} {
	let totalLength = 0;
	const v: TangentCoords2d = tangent.copy();
	let prevCoords = v.base;

	const edgeSequence = [];
	const intersections = [];
	while (totalLength < pathLength) {
		const {
			edge,
			edgeIndex,
		} = TriangleEdgeIntersection(triangle, v);
		if (edgeIndex < 0) {
			break;
		}
		edgeSequence.push(edgeIndex);
		const coeff = edge.coefficientOfIntersectionWith(v);
		const perimeterPoint = TrianglePerimeterPoint.fromEdgeIndexAndCoeff(edgeIndex, coeff);
		intersections.push(perimeterPoint);
		v.base = triangle.coordsForPerimeterPoint(perimeterPoint);
		v.offset = edge.reflectTCoords(v.offset);
		totalLength += v.base.distanceFrom(prevCoords);
		prevCoords = v.base;
	}
	return { edgeSequence, intersections };
}
import { JestObjectEquality } from "core/testing";
import { Orientation } from "billiards/singularity";
import { TurnPath } from "billiards/turn_path";

expect.extend(JestObjectEquality);

test("Test reduced turn paths", () => {
	const oneRepetition = new TurnPath(
		Orientation.Forward, [1, 2, 3, 4]);
	const twoRepetitions = new TurnPath(
		Orientation.Forward, [1, 2, 3, 4, 1, 2, 3, 4]);
	const threeRepetitions = new TurnPath(
		Orientation.Forward, [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4]);

	// reducing a single repetition should return the exact same
	// object (strict equality).
	expect(oneRepetition.reduced()).toEqual(oneRepetition);

	// more than one repetition should just return an equivalent object
	// (as determined by a.equals(b)).
	expect(twoRepetitions.reduced()).toEqualObject(oneRepetition);
	expect(threeRepetitions.reduced()).toEqualObject(oneRepetition);

	const oddRepetition = new TurnPath(
		Orientation.Forward, [1, 2, 3, 1, 2, 3]);
	// no reduction should happen with odd-length components, so the
	// returned object should strictly equal the original.
	expect(oddRepetition.reduced()).toEqual(oddRepetition);
});
export enum Side {
	Left = 0,
	Right
}

// The singularities S0 and S1 correspond to the left and
// right vertices of the base edge of a triangle.
// Orientation specifies a direction between the two vertices:
// Forward for left to right / widdershins, Backward for
// right to left / clockwise.
export class Singularity {
	static S0: Singularity = new Singularity();
	static S1: Singularity = new Singularity();

	private constructor() { }

	toString(): string {
		if (this == Singularity.S0) {
			return "S0";
		}
		return "S1";
	}
}

export class Orientation {
	static Forward = new Orientation();
	static Backward = new Orientation();
		
	private constructor() { }

	static from(s: Singularity): Orientation {
		if (s === Singularity.S0) {
			return Orientation.Forward;
		}
		return Orientation.Backward;
	}

	static to(s: Singularity): Orientation {
		if (s === Singularity.S0) {
			return Orientation.Backward;
		}
		return Orientation.Forward;
	}

	to(): Singularity {
		if (this === Orientation.Forward) {
			return Singularity.S1;
		}
		return Singularity.S0;
	}

	from(): Singularity {
		if (this === Orientation.Forward) {
			return Singularity.S0;
		}
		return Singularity.S1;
	}

	reversed(): Orientation {
		if (this === Orientation.Forward) {
			return Orientation.Backward;
		}
		return Orientation.Forward;
	}

	static deserialize(x: number): Orientation {
		if (x == 0) {
			return Orientation.Forward;
		}
		return Orientation.Backward;
	}

	serialize(): number {
		if (this === Orientation.Forward) {
			return 0;
		}
		return 1;
	}
}



// S2<T> is a map from Singularity.{S0, S1} to T.
// the... name is not my favorite, but it's a common construction
// with a clear intuition that simplifies a lot of calculations so
// something overly abstract instead of a short nickname seems like
// a poor tradeoff. but we'd still like to find something both
// simple _and_ precise.
export class S2<T> {
	s0: T;
	s1: T;

	constructor(s0: T, s1: T) {
		this.s0 = s0;
		this.s1 = s1;
	}

	at(s: Singularity): T {
		if (s == Singularity.S0) {
			return this.s0;
		}
		return this.s1;
	}

	awayFrom(s: Singularity): T {
		if (s == Singularity.S0) {
			return this.s1;
		}
		return this.s0;
	}
}
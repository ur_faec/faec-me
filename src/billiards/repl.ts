import chalk from "chalk";
import readline from "readline";
import fs from "fs";
import path from "path";

import { Orientation, S2 } from "billiards/singularity";
import { BilliardsContext } from "billiards/context";
import { FeasibleCycle } from "billiards/boundaries";
import { TurnPath } from "billiards/turn_path";
import { TurnCycle } from "billiards/turn_cycle";
import { Rational } from "core/algebra/rational";

const swiftDateOffset = 978307200;

function DrawPrompt(): void {
	process.stdout.write("🕸  > ");
}

export function Repl(): void {
	console.log(chalk.blue("hello"));
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
		terminal: false,
	});
	const repl = new BilliardsRepl();

	DrawPrompt();

	rl.on('line', function(line: string) {
		repl.command(line.trim());
		DrawPrompt();
	});
}

class BilliardsRepl {
	constructor() {
	}

	command(c: string) {
		if (c == "list") {
			console.log("  point sets:\n");
			list();
		} else if (c == "search") {
			DoSearch();
		} else {
			console.log("i don't know that one");
		}
	}
}

function DoSearch() {
	const r0 = new Rational(BigInt(6), BigInt(5));
	const r = new S2(r0, r0);
	const data = new BilliardsContext(r);
	const path = new TurnPath(Orientation.Forward, [-2, 2, 2, -2]);
	const cycle = TurnCycle.repeatingTurnPath(path);
	const result = FeasibleCycle(cycle, data);
	console.log(`\n${chalk.blue("hi fae")}\n`);
	console.log(`feasible: ${result}\n`);
}

export interface PointSetMetadata {
	count?: number,
	density?: number,
	created?: number,
}

function list() {
	const rootDir = process.env.BILLIARDS_ROOT || "data";
	const pointSetDir = path.join(rootDir, "pointset");
	//const pointSets: Array<string> = [];
	//let metadataLookup = {};
	let entries: Array<string> = [];
	try {
		entries = fs.readdirSync(pointSetDir, "utf8");
	} catch (err) {
		console.log(`couldn't read pointset directory '${pointSetDir}`);
		return;
	}
	for (let i = 0; i < entries.length; i++) {
		const entry = entries[i];
		const entryPath = path.join(pointSetDir, entry);
		try {
			if (fs.statSync(entryPath).isDirectory()) {
				const metadataPath = path.join(entryPath, "metadata.json");
				const fileContents = fs.readFileSync(metadataPath, 'utf8');
				const metadata: PointSetMetadata = JSON.parse(fileContents);
				const created = metadata.created;
				let formattedDate = "";
				if (created) {
					const date = new Date((created + swiftDateOffset) * 1000);
					formattedDate = `${date.getFullYear()} / ${date.getMonth()} / ${date.getDate()}`;
				}
				console.log(`${entry}  ${metadata.count}  ${metadata.density}  ${formattedDate}`);
			}
			//pointSets.push(entry);
		} catch (err) {
			console.log(`couldn't read pointset '${entry}': ${err}`);
		}
	}
}

export function MetadataForPointSetName(name: string): PointSetMetadata | Error {
	const rootDir = process.env.BILLIARDS_ROOT || "data";
	const metadataPath = path.join(rootDir, "pointset", name, "metadata.json");
	try {
		const fileContents = fs.readFileSync(metadataPath, 'utf8');
		const metadata: PointSetMetadata = JSON.parse(fileContents);
		if (metadata.created) {
			metadata.created = (metadata.created + swiftDateOffset) * 1000;
		}
		return metadata;
	} catch (err) {
		console.log(`couldn't read pointset '${name}': ${err}`);
		return err;
	}
}

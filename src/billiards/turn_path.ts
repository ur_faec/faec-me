import { Orientation } from "./singularity";

export class TurnPath {
	initialOrientation: Orientation;
	turns: Array<number>;

	constructor(
		initialOrientation: Orientation,
		turns: Array<number>
	) {
		this.initialOrientation = initialOrientation;
		this.turns = turns;
	}

	toString(): string {
		return `${this.initialOrientation.from()}->${this.turns}`;
	}

	static deserialize(serialized: SerializedTurnPath): TurnPath {
		return new TurnPath(
			Orientation.deserialize(serialized.initialOrientation),
			serialized.turns);
	}

	serialize(): SerializedTurnPath {
		return {
			initialOrientation: this.initialOrientation.serialize(),
			turns: this.turns,
		};
	}

	equals(path: TurnPath): boolean {
		if (this.initialOrientation != path.initialOrientation) {
			return false;
		}
		if (this.turns.length != path.turns.length) {
			return false;
		}
		for (let i = 0; i < this.turns.length; i++) {
			if (this.turns[i] != path.turns[i]) {
				return false;
			}
		}
		return true;
	}

	// a TurnPath is reduced if it is cannot be
	// produced by repeating a shorter TurnPath an integer
	// number of times.
	// (note that such a repeated TurnPath would need to
	// be even length, since in order for a repetition to make
	// sense it must start from the same singularity.)
	// reduced returns the reduced TurnPath that generates this one.
	reduced(): TurnPath {
		outerLoop:
		for (let i = 2; i <= this.turns.length / 2; i += 2) {
			for (let j = 0; j < this.turns.length - i; j++) {
				if (this.turns[j] != this.turns[i + j]) {
					continue outerLoop;
				}
			}
			return new TurnPath(
				this.initialOrientation,
				this.turns.slice(0, i));
		}
		return this;
	}
}

export interface SerializedTurnPath {
	initialOrientation: number;
	turns: Array<number>;
}


import { Sign } from "core/arithmetic";
import { TurnPath } from "./turn_path";
import { Orientation, Singularity } from "./singularity";

function IsS2(segment: TurnCycleSegment): boolean {
	return (segment.turnDegrees.length == 2
		&& segment.turnDegrees[0] == 2
		&& segment.turnDegrees[1] == 2);
}

function IsA2_2(segments: Array<TurnCycleSegment>): boolean {
	return (segments.length == 2
		&& IsS2(segments[0])
		&& IsS2(segments[1]));
}

export class TurnCycle {
	length: number;
	weight: number;
	segments: Array<TurnCycleSegment>;

	private constructor(segments: Array<TurnCycleSegment>) {
		this.length = segments.reduce(
			(sum, segment) => sum + segment.turnDegrees.length,
			0);
		
		const segmentWeights = segments.map(
			segment => segment.turnDegrees.reduce((a, b) => a + b, 0));
		this.weight = segmentWeights.reduce((a, b) => a + b, 0);

		this.segments = CanonicallyOrderSegments(segments);
		/*if (IsA2_2(this.segments)) {
			console.log("A(2,2)");
		}
		if (IsA2_2(segments) != IsA2_2(this.segments)) {
			console.log(`Mismatch:`, segments, this.segments);
		}*/
	}

	// returns a (not particularly canonical, tho it is well-defined
	// within a particular rotation class) turn path that generates
	// this cycle.
	asTurnPath(): TurnPath {
		const initialOrientation = this.segments[0].initialOrientation;
		const turns: Array<number> = [];
		// In a TurnCycle, turn sign is always positive.
		// In a TurnPath, where turn sign indicates the geometric orientation
		// (positive = widdershins, negative = clockwise), sign alternates every
		// step, except at segment boundaries where it stays the same.
		// the initial sign is chosen so that a positive first turn corresponds to
		// moving clockwise around the target singularity.
		let sign = 1;
		for (const segment of this.segments) {
			for (const degree of segment.turnDegrees) {
				turns.push(sign * degree);
				sign = -sign;
			}
			// reverse it again to cancel out the reversal in the last turn
			sign = -sign;
		}
		
		return new TurnPath(initialOrientation, turns);
	}

	static repeatingTurnPath(turnPath: TurnPath): TurnCycle {
		// start out removing any existing repetitions so we can assume
		// any rotation with minimal order is the unique rotation with that order.
		turnPath = turnPath.reduced();
		const segmentBoundaries = segmentBoundariesForTurns(turnPath.turns);
		const initialOrientation = (segmentBoundaries[0] % 2 == 0)
			? turnPath.initialOrientation
			: turnPath.initialOrientation.reversed();
		let orientation = initialOrientation;
		const segments: Array<TurnCycleSegment> = [];
		for (let i = 0; i < segmentBoundaries.length; i++) {
			const start = segmentBoundaries[i];
			const end = segmentBoundaries[(i + 1) % segmentBoundaries.length];
			const length = (end > start)
				? (end - start)
				: (turnPath.turns.length + end - start);
			const turnDegrees: Array<number> = [];
			for (let j = 0; j < length; j++) {
				const turnIndex = (start + j) % turnPath.turns.length;
				turnDegrees.push(Math.abs(turnPath.turns[turnIndex]));
			}
			segments.push(
				new TurnCycleSegment(orientation, turnDegrees));

			orientation =
				(length % 2 == 0) ? orientation : orientation.reversed();
		}
		return new TurnCycle(segments);
	}

	toString(): string {
		const startSingularity = this.segments[0].initialOrientation.from();
		const segmentStrs = this.segments.map(s => s.turnDegrees.join(" "));
		return startSingularity.toString() + " " + segmentStrs.join(" | ");
	}

	static deserialize(obj: SerializedTurnCycle): TurnCycle {
		const segments = obj.segments.map(TurnCycleSegment.deserialize);
		return new TurnCycle(segments);
	}

	serialize(): SerializedTurnCycle {
		const segments = this.segments.map(s => s.serialize());
		return { segments };
	}

}


export enum ComparisonResult {
	Less,
	Equal,
	Greater
}

function Compare(a: number, b: number): ComparisonResult {
	if (a > b) {
		return ComparisonResult.Greater;
	}
	if (a < b) {
		return ComparisonResult.Less;
	}
	return ComparisonResult.Equal;
}

export class TurnCycleSegment {
	initialOrientation: Orientation;
	turnDegrees: Array<number>;

	constructor(initialOrientation: Orientation, turnDegrees: Array<number>) {
		this.initialOrientation = initialOrientation;
		this.turnDegrees = turnDegrees;
	}

	reversed(): TurnCycleSegment {
		const reversedDegrees = this.turnDegrees.slice().reverse();
		return new TurnCycleSegment(
			(reversedDegrees.length % 2 == 0)
				?	this.initialOrientation.reversed()
				: this.initialOrientation,
			reversedDegrees);
	}

	static deserialize(serialized: SerializedTurnCycleSegment): TurnCycleSegment {
		const turnDegrees =
			serialized.turnDegrees ? serialized.turnDegrees : serialized.turns;
		if (!turnDegrees) {
			throw "Turn cycle segment must have 'turnDegrees' or 'turns'.";
		}
		return new TurnCycleSegment(
			Orientation.deserialize(serialized.initialOrientation),
			turnDegrees);
	}

	serialize(): SerializedTurnCycleSegment {
		return {
			initialOrientation: this.initialOrientation.serialize(),
			turnDegrees: this.turnDegrees,
		};
	}

	// implements a somewhat arbitrary total order on
	// TurnCycleSegments,
	compareTo(segment: TurnCycleSegment): ComparisonResult {
		const lengthComparison = Compare(this.turnDegrees.length, segment.turnDegrees.length);
		if (lengthComparison != ComparisonResult.Equal) {
			return lengthComparison;
		}

		const sum = (a: number, b: number) => a + b;
		const totalTurnsThis = this.turnDegrees.reduce(sum, 0);
		const totalTurnsSegment = segment.turnDegrees.reduce(sum, 0);
		const totalComparison =
			Compare(totalTurnsThis, totalTurnsSegment);
		if (totalComparison != ComparisonResult.Equal) {
			return totalComparison;
		}

		const squaredSum = (a: number, b: number) => a + b * b;
		const totalSquaredTurnsThis = this.turnDegrees.reduce(squaredSum, 0);
		const totalSquaredTurnsSegment = segment.turnDegrees.reduce(squaredSum, 0);
		const totalSquaredComparison =
			Compare(totalSquaredTurnsThis, totalSquaredTurnsSegment);
		if (totalSquaredComparison != ComparisonResult.Equal) {
			return totalComparison;
		}

		for (let i = 0; i < this.turnDegrees.length; i++) {
			const turnComparison =
				Compare(this.turnDegrees[i], segment.turnDegrees[i]);
			if (turnComparison != ComparisonResult.Equal) {
				return turnComparison;
			}
		}

		if (this.initialOrientation == segment.initialOrientation) {
			return ComparisonResult.Equal;
		}
		if (this.initialOrientation == Orientation.Forward) {
			// forward precedes backward
			return ComparisonResult.Less;
		}
		return ComparisonResult.Greater;
	}
}

function MinimumRotation(
	segments: Array<TurnCycleSegment>
): Array<TurnCycleSegment> {
	let bestIndex = 0;
	for (let i = 1; i < segments.length; i++) {
		for (let j = 0; j < segments.length; j++) {
			const comparison =
				segments[(i + j) % segments.length].compareTo(
					segments[(bestIndex + j) % segments.length]);
			if (comparison === ComparisonResult.Less) {
				bestIndex = i;
				break;
			}
			if (comparison === ComparisonResult.Greater) {
				break;
			}
		}
	}
	return segments.slice(bestIndex).concat(segments.slice(0, bestIndex));
}

function CanonicallyOrderSegments(
	segments: Array<TurnCycleSegment>
): Array<TurnCycleSegment> {
	const reversedSegments: Array<TurnCycleSegment> = [];
	for (let i = 0; i < segments.length; i++) {
		const segment = segments[segments.length - 1 - i];
		reversedSegments.push(segment.reversed());
	}

	const bestForward = MinimumRotation(segments);
	const bestReversed = MinimumRotation(reversedSegments);

	for (let i = 0; i < bestForward.length; i++) {
		const comparison = bestForward[i].compareTo(bestReversed[i]);
		if (comparison === ComparisonResult.Less) {
			return bestForward;
		}
		if (comparison === ComparisonResult.Greater) {
			return bestReversed;
		}
	}

	// if we make it here, bestForward and bestReversed are the
	// same.
	return bestForward;
}

// this interface has both "turns" and "turnDegrees" because earlier
// versions serialized the segments as TurnPaths and we still need
// to be able to load that data for now. exactly one of turns and
// turnDegrees should be non-null.
export interface SerializedTurnCycleSegment {
	initialOrientation: number;
	turns?: Array<number>;
	turnDegrees?: Array<number>;
}


export interface SerializedTurnCycle {
	segments: Array<SerializedTurnCycleSegment>;
}

export function SerializeTurnCycles(
	cycles: Record<number, TurnCycle>
): Record<number, SerializedTurnCycle> {
	const serialized: Record<number, SerializedTurnCycle> = {};
	for (const [key, value] of Object.entries(cycles)) {
		serialized[key as unknown as number] = value.serialize();
	}
	return serialized;
}


function segmentBoundariesForTurns(turns: Array<number>): Array<number> {
	const flipIndices: Array<number> = [];
	let lastSign = Sign.of(turns[turns.length - 1]);
	for (let i = 0; i < turns.length; i++) {
		const sign = Sign.of(turns[i]);
		if (sign === lastSign) {
			flipIndices.push(i);
		}
		lastSign = sign;
	}
	return flipIndices;
}
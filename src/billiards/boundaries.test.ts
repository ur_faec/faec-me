import { JestObjectEquality } from "core/testing";

import { Complex, Rational } from "core/algebra/common";

import {
	Orientation, BilliardsContext, QuadEmbedding, Singularity,
	TurnPath, TurnPathBoundaries,
} from "billiards/common";

expect.extend(JestObjectEquality);

function Q(num: number, den: number = 1): Rational {
	return new Rational(num, den);
}

test("TurnPathBoundaries with r = (1, 1)", () => {
	const data = BilliardsContext.fromRadii(Q(1), Q(1));
	const embedding = QuadEmbedding.default(data);
	const path = new TurnPath(Orientation.Forward, [-2, 2, 2, -2]);
	const boundaries = TurnPathBoundaries(path, embedding);
	expect(boundaries).toBeDefined();
	if (boundaries !== undefined) {
		expect(boundaries.left.length).toEqual(5);
		expect(boundaries.right.length).toEqual(5);
		expect(boundaries.center.length).toEqual(6);

		expect(boundaries.center[0]).toEqualObject(
			new Complex(Q(-1), Q(0)));
		expect(boundaries.left[0]).toEqualObject(
			new Complex(Q(0), Q(1)));
		expect(boundaries.right[0]).toEqualObject(
			new Complex(Q(0), Q(-1)));

		expect(boundaries.center[1]).toEqualObject(
			new Complex(Q(1), Q(0)));
		expect(boundaries.left[1]).toEqualObject(
			new Complex(Q(2), Q(1)));
		expect(boundaries.right[1]).toEqualObject(
			new Complex(Q(2), Q(-1)));

		expect(boundaries.center[2]).toEqualObject(
			new Complex(Q(3), Q(0)));
		expect(boundaries.left[2]).toEqualObject(
			new Complex(Q(4), Q(1)));
		expect(boundaries.right[2]).toEqualObject(
			new Complex(Q(4), Q(-1)));

		expect(boundaries.center[3]).toEqualObject(
			new Complex(Q(5), Q(0)));
		expect(boundaries.left[3]).toEqualObject(
			new Complex(Q(6), Q(1)));
		expect(boundaries.right[3]).toEqualObject(
			new Complex(Q(6), Q(-1)));

		expect(boundaries.center[4]).toEqualObject(
			new Complex(Q(7), Q(0)));
		expect(boundaries.left[4]).toEqualObject(
			new Complex(Q(8), Q(1)));
		expect(boundaries.right[4]).toEqualObject(
			new Complex(Q(8), Q(-1)));

		expect(boundaries.center[5]).toEqualObject(
			new Complex(Q(9), Q(0)));
	}
});

test("TurnPathBoundaries with r = (6/5, 6/5)", () => {
	const r = Q(6, 5);
	const data = BilliardsContext.fromRadii(r, r);
	const embedding = QuadEmbedding.default(data);
	const path = new TurnPath(Orientation.Forward, [-2, 2, 2, -2]);
	const boundaries = TurnPathBoundaries(path, embedding);
	expect(data.turnCoeffs.at(Singularity.S0).pow(2)).toEqualObject(
		new Complex(Q(-3479, 3721), Q(1320, 3721)));
	expect(boundaries).toBeDefined();
	if (boundaries !== undefined) {
		expect(boundaries.left.length).toEqual(5);
		expect(boundaries.right.length).toEqual(5);
		expect(boundaries.center.length).toEqual(6);

		expect(boundaries.center[0]).toEqualObject(
			new Complex(r.neg(), Q(0)));
		expect(boundaries.left[0]).toEqualObject(
			new Complex(Q(0), Q(1)));
		expect(boundaries.right[0]).toEqualObject(
			new Complex(Q(0), Q(-1)));

		expect(boundaries.center[1]).toEqualObject(
			new Complex(r, Q(0)));
		expect(boundaries.left[1]).toEqualObject(
			new Complex(Q(2160, 3721), Q(1584, 3721)));
		/*expect(boundaries.right[1]).toEqualObject(
			new Complex(Q(2), Q(-1)));

		expect(boundaries.center[2]).toEqualObject(
			new Complex(Q(3), Q(0)));
		expect(boundaries.left[2]).toEqualObject(
			new Complex(Q(4), Q(1)));
		expect(boundaries.right[2]).toEqualObject(
			new Complex(Q(4), Q(-1)));

		expect(boundaries.center[3]).toEqualObject(
			new Complex(Q(5), Q(0)));
		expect(boundaries.left[3]).toEqualObject(
			new Complex(Q(6), Q(1)));
		expect(boundaries.right[3]).toEqualObject(
			new Complex(Q(6), Q(-1)));

		expect(boundaries.center[4]).toEqualObject(
			new Complex(Q(7), Q(0)));
		expect(boundaries.left[4]).toEqualObject(
			new Complex(Q(8), Q(1)));
		expect(boundaries.right[4]).toEqualObject(
			new Complex(Q(8), Q(-1)));

		expect(boundaries.center[5]).toEqualObject(
			new Complex(Q(9), Q(0)));*/
	}
});
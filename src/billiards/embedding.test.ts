import { Rational } from "core/algebra/rational";
import { Complex } from "core/algebra/complex";
import { JestObjectEquality } from "core/testing";

import { BilliardsContext } from "billiards/context";
import { QuadEmbedding } from "billiards/embedding";
import { Singularity } from "billiards/singularity";

expect.extend(JestObjectEquality);

function Q(a: number, b: number = 1): Rational {
	return new Rational(a, b);
}

test("Base angles (Pi/2, Pi/2)", () => {
	const ctx = BilliardsContext.fromRadii(Rational.one, Rational.one);
	const q0 = QuadEmbedding.default(ctx);
	const q1 = q0.turn(Singularity.S1, -2);
	const q2 = q1.turn(Singularity.S0, 2);
	expect(q0.center).toEqualObject(new Complex(Q(0), Q(0)));
	expect(q2.center).toEqualObject(new Complex(Q(4), Rational.zero));
});

test("Radii (6/5, 6/5)", () => {

});

//test("")
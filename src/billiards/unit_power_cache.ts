import {OrderedField} from "core/algebra/field";
import {Complex} from "core/algebra/complex";

// AngleBound is a bound that can be applied while calculating
// powers. It's very common that we will throw away a power if it
// turns by more than pi or 2 pi, and in that case we don't want
// to do an expensive computation or cache unused values.
export enum AngleBound {
		PI = 0,
		TWOPI
}

// There is more machinery here than maybe seems reasonable, but
// accuracy of floating-point estimates is _massively_ better when
// we compute exponents the clever way.
export class UnitPowerCache<K extends OrderedField<K>> {
	base: Complex<K>;

	_cache: Array<Complex<K> | undefined>;

	// the maximum exponent that has been computed and written to the
	//  cache so far.
	_maxPower: number;

	// _logBound[AngleBound.{PI, TWOPI}] is the smallest integer
	// n such that base^n spans at least {Pi, 2*Pi}, or nil if that
	// value hasn't been found yet.
	// no feasible path can have a turn of magnitude greater than
	// _logBound[AngleBound.PI], but we sometimes like to consider
	// looser notions of stability that can go up to 2 Pi.
	_logBound: Array<number | null> = [null, null];

	constructor(r: K) {
		const K = r.T;
		const z = new Complex(r, K.one);    // z = r + I
		const base = z.dividedBy(z.conjugate());   // (r + I) / (r - I)
		const cache = [new Complex(K.one, K.zero), base];

		this.base = base;
		this._cache = cache;
		this._maxPower = 1;
	}

	// input invariant: power <= 2 * _maxPower
	// output invariant: if _logBound[.PI] and / or _logBound[.TWOPI]
	// are at most power, then they will be non-null when _writeToCache
	// returns.
	_writeToCache(power: number, value: Complex<K>): void {
		while (this._cache.length <= power) {
			this._cache.push(undefined);
		}
		if (power > this._maxPower) {
			// if this is higher than previously stored values, we might need
			// to update _logBound.
			let updatingBound: AngleBound | undefined;
			const zero = value.x.T.zero;
			if (this._logBound[AngleBound.PI] === null) {
				if (value.y.lessOrEqual(zero)) {
					updatingBound = AngleBound.PI;
				}
			} else if (this._logBound[AngleBound.TWOPI] === null) {
				if (value.y.greaterOrEqual(zero)) {
					updatingBound = AngleBound.TWOPI;
				}
			}
			if (updatingBound !== undefined) {
				this._logBound[updatingBound] =
					binarySearch(this._maxPower, power, (curPower:number) => {
						// p0 and p1 are guaranteed to be <= _maxPower by
						// the input invariant so it is safe to call
						// recursively here.
						// the worst-case recursion fanout is log-squared,
						// we could bring it down to log by basing all
						// products on the existing _maxPower but this is
						// not a bottleneck right now and the
						// floating-point stability is better this way.
						const p0 = Math.floor(curPower / 2);
						const p1 = p0 + curPower % 2;
						const curValue = this.pow(p0).times(this.pow(p1));
						this._cache[curPower] = curValue;
						if (updatingBound == AngleBound.PI) {
							return curValue.y.lessOrEqual(zero);
						}
						return curValue.y.greaterOrEqual(zero);
					});
			}
			this._maxPower = power;
		}
		this._cache[power] = value;
	}

	powerSatisfiesBound(
		power: number, bound: AngleBound | undefined = undefined
	): boolean {
		if (bound === undefined) {
			return true;
		}
		const magnitude = Math.abs(power);
		if (bound === AngleBound.PI) {
			const logPi = this._logBound[AngleBound.PI];
			if (logPi !== null) {
				return magnitude <= logPi;
			}
		}
		const log2Pi = this._logBound[AngleBound.TWOPI];
		if (log2Pi !== null) {
			return magnitude <= log2Pi;
		}
		const logPi = this._logBound[AngleBound.PI];
		if (logPi !== null) {
			// assume the most permissive match (highest possible bound)
			// until we actually compute that high.
			return magnitude <= 2 * logPi;
		}
		// otherwise we have no information, so we match with anything.
		return true;
	}

	maxTurnMagnitudeForBound(angleBound: AngleBound): number {
		while (true) {
			const value = this._logBound[angleBound];
			if (value !== null) {
				return value;
			}
			this.power(this._maxPower * 2, angleBound);
		}
	}

	pow(n: number): Complex<K> {
		// power is guaranteed to return non-null if it is given no angle bound
		return this.power(n) as Complex<K>;
	}

	power(
		n: number, angleBound: AngleBound | undefined = undefined
	): Complex<K> | undefined {
		if (n < 0) {
			return this.power(-n, angleBound)?.conjugate();
		}
		// we check this again below, but if we already
		// know enough to stop now we can avoid the cost
		// of computing the result
		if (!this.powerSatisfiesBound(n, angleBound)
		) {
			return undefined;
		}
		if (n >= this._cache.length || this._cache[n] === undefined) {
			const e0 = Math.floor(n / 2);
			const e1 = e0 + (n % 2);
			const v0 = this.power(e0, angleBound);
			const v1 = this.power(e1, angleBound);
			if (v0 === undefined || v1 === undefined) {
				return undefined;
			}
			this._writeToCache(n, v0.times(v1));
		}
		if (!this.powerSatisfiesBound(n, angleBound)) {
			return undefined;
		}
		return this._cache[n];
	}
}

function binarySearch(
	from: number, to: number, highEnough: (power: number) => boolean
): number {
	if (from == to) {
		return from;
	}
	const center = Math.floor((from + to) / 2);
	if (highEnough(center)) {
		return binarySearch(from, center, highEnough);
	}
	return binarySearch(center + 1, to, highEnough);
}


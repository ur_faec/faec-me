import { Coords2d } from "core/geometry";
import { PointSetResponse } from "./server";
import { TurnCycle } from "./turn_cycle";

type PointSetInfo = {
	points: Array<Coords2d>;
	cycles: Record<number, TurnCycle>;
}


export async function FetchPointSet(
	name: string
): Promise<PointSetInfo> {
	const rawResponse = await fetch(`http://127.0.0.1:4014/pointset/${name}`);
	
	const response: PointSetResponse = await rawResponse.json(); //extract JSON from the http response
	if (response.error) {
		return Promise.reject(response.error);
	}
	if (response.points) {
		const points: Array<Coords2d> = [];
		for (const point of response.points) {
			const x = NumberForFractionString(point.x);
			const y = NumberForFractionString(point.y);
			if (x == null || y == null) {
				console.log("truncated point set?");
				break;
			}
			points.push(Coords2d.fromXY(x, y));
		}
		const responseCycles = response.cycles ?? {};
		const cycles: Record<number, TurnCycle> = {};
		//	response.cycles ?? {};
		for (const [key, value] of Object.entries(responseCycles)) {
			cycles[key as unknown as number] = TurnCycle.deserialize(value);
		}
		return Promise.resolve({points, cycles});
	}
	return Promise.reject("invalid response");
	//element.innerText =
	//	`${metadata.name} ${metadata.count} ${metadata.density} ${formattedDate}`;
}
//userAction();

function NumberForFractionString(str: string): number | null {
	const pieces = str.split("/");
	if (pieces.length != 2) {
		return null;
	}
	const numerator = parseInt(pieces[0]);
	const denominator = parseInt(pieces[1]);
	return numerator / denominator;
}
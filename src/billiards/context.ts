import {OrderedField} from "core/algebra/field";
import {Complex} from "core/algebra/complex";
import {S2, Singularity} from "billiards/singularity";
import {UnitPowerCache} from "billiards/unit_power_cache";

// BilliardContext computes / caches embedding-independent metadata about
// the fundamental quad and the apex parameters that generated it.
// Given parameters r0, r1 in the base field, we construct the
// fundamental quadrilateral as the convex closure of the following
// vertices:
//   - The _base vertices_: S0 = (-r0, 0), S1 = (r1, 0)
//   - The _apex_ (0, 1)
//   - The _conjugate apex_ (0 -1).
//
// The most common calculations with this structure involve rotation
// around Sk (k = 0,1) by powers of (r0 + i) / (r0 - i) 
export class BilliardsContext<K extends OrderedField<K>> {
	r: S2<K>;

	// The coordinates of the base vertex singularities in the source domain,
	// which are defined to be (-r0, 0) and (r1, 0).
	singularities: S2<Complex<K>>;

	turnCoeffs: S2<UnitPowerCache<K>>;

	constructor(r: S2<K>) {
		const r0 = r.at(Singularity.S0);
		const r1 = r.at(Singularity.S1);
		const zero = r0.T.zero;

		this.r = r;
		this.singularities = new S2(
			new Complex(r0.neg(), zero),
			new Complex(r1, zero));
		this.turnCoeffs = new S2(
			new UnitPowerCache(r0),
			new UnitPowerCache(r1));
	}

	static fromRadii<K extends OrderedField<K>>(r0: K, r1: K): BilliardsContext<K> {
		return new BilliardsContext(new S2(r0, r1));
	}

	static fromApex<K extends OrderedField<K>>(x: K, y: K): BilliardsContext<K> {
		const one = x.T.one;
		const r0 = x.dividedBy(y);
		const r1 = one.minus(x).dividedBy(y);
		return new BilliardsContext(new S2(r0, r1));
	}
}


import { JestObjectEquality } from "core/testing";

import { Rational } from "core/algebra/rational";
import { Complex } from "core/algebra/complex";

import { UnitPowerCache, AngleBound } from "./unit_power_cache";

expect.extend(JestObjectEquality);

test("basic calculation", () => {
	const r = new Rational(4, 3);
	const z = new Complex(r, Rational.one).dividedBy(
		new Complex(r, Rational.one.neg()));
	const cache = new UnitPowerCache(r);
	
	expect(cache.pow(2)).toEqualObject(z.times(z));

	let prod = new Complex(Rational.one, Rational.zero);
	for (let i = 0; i < 10; i++) {
		prod = prod.times(z);
	}
	expect(cache.pow(10)).toEqualObject(prod);
});

test("angle boundaries", () => {
	const cache = new UnitPowerCache(Rational.one);
	expect(cache.powerSatisfiesBound(10, AngleBound.PI)).toBeTruthy();
	expect(cache.power(3, AngleBound.PI)).toBeUndefined();
	expect(cache.powerSatisfiesBound(10, AngleBound.PI)).toBeFalsy();
	expect(cache.power(2, AngleBound.PI))
		.toEqualObject(new Complex(Rational.one.neg(), Rational.zero));

	expect(cache.power(3, AngleBound.TWOPI))
		.toEqualObject(new Complex(Rational.zero, Rational.one.neg()));
	expect(cache.power(5, AngleBound.TWOPI)).toBeUndefined();
	expect(cache.power(4, AngleBound.TWOPI))
		.toEqualObject(new Complex(Rational.one, Rational.zero));
});
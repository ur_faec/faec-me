import express from "express";
import { promises as fs } from "fs";
import path from "path";

import { Rational } from "core/algebra/rational";
import { FeasibleCycle, BilliardsContext, S2, SerializedTurnCycle,
	SerializeTurnCycles, TurnPath, TurnCycle } from "billiards/common";

export type PointSetResponse = {
	points?: Array<{x: string, y: string}>;
	cycles?: Record<number, SerializedTurnCycle>;
	error?: string; 
}

type PointArray = Array<{x: Rational, y: Rational}>

function HandlePointSetRequest(
	name: string,
	request: express.Request,
	response: express.Response,
	next: express.NextFunction
): void {
	const result: PointSetResponse = {};

	Promise.allSettled([
		ReadPointSet(name),
		ReadPointSetCycles(name)]
	).then(results => {
		if (results[0].status == "rejected") {
			result.error = results[0].reason;
			response.status(404);
		} else {

			const points = results[0].value;
			//const turnPath = new TurnPath(Orientation.Forward, [-2, 2, 2, -2]);
			//const filtered = filterPointsAndStuff(points, turnPath);

			result.points = SerializePoints(points);//(filtered);
		}

		if (results[1].status == "fulfilled") {
			result.cycles = SerializeTurnCycles(results[1].value);
		}

		response.end(JSON.stringify(result));
		next();
	});
}

function SerializePoints(points: PointArray): Array<{x: string, y: string}> {
	const result: Array<{x: string, y: string}> = [];
	for (const point of points) {
		result.push({
			x: point.x.toString(),
			y: point.y.toString(),
		});
	}
	return result;
}

function filterPointsAndStuff(points: PointArray, turnPath: TurnPath): PointArray {
	const filtered: PointArray = [];
	for (const point of points) {
		if (IncludePoint(point, turnPath)) {
			filtered.push(point);
		}
	}
	return filtered;
}

function IncludePoint(p: {x: Rational, y: Rational}, turnPath: TurnPath): boolean {
	const r0 = p.x.dividedBy(p.y);
	const r1 = Rational.one.minus(p.x).dividedBy(p.y);
	const quadData = new BilliardsContext(new S2(r0, r1));
	const cycle = TurnCycle.repeatingTurnPath(turnPath);
	//return true;
	return FeasibleCycle(cycle, quadData);
}

export function BilliardsServer(): void {
	const calls = express();
	calls.use(function(req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		next();
	});
	calls.get("/hello", function(req, res): void {
		res.end("hi there\n");
	});
	calls.get("/pointset/:name", function(req, res, next) {
		HandlePointSetRequest(req.params.name, req, res, next);
	});
	calls.get("*", function(req, res): void {
		res.end();
	});
	const server = calls.listen(4014, "localhost", function () {
		if (!server) {
			return;
		}
		const address = server.address();
		if (!address) { return; }
		if (typeof address === "string") { return; }
		const host = address.address;
		const port = address.port;
		console.log("billiards server listening at http://%s:%s", host, port);
	});
	console.log("🕷 🕸 ✨");
}

function fixFractionStr(str: string): string {
	return str.replace("\\/", "/");
}

async function ReadPointSet(name: string): Promise<PointArray> {
	const rootDir = process.env.BILLIARDS_ROOT || "data";
	const pointsPath = path.join(rootDir, "pointset", name, "elements.json");

	try {
		const data = await fs.readFile(pointsPath, 'utf8');
		const rawArray: Array<{ x: string; y: string; }> = JSON.parse(data);
		const points: Array<{ x: Rational; y: Rational; }> = [];
		for (let i = 0; i < rawArray.length; i++) {
			const xStr = fixFractionStr(rawArray[i].x);
			const yStr = fixFractionStr(rawArray[i].y);
			if (!xStr || !yStr) {
				console.log(`Invalid point entry ${name}[${i}]`);
				break;
			}
			const x = Rational.fromString(xStr);
			const y = Rational.fromString(yStr);
			if (x !== undefined && y !== undefined) {
				points.push({ x, y });
			}
		}
		return Promise.resolve(points);
	}
	catch (e) {
		return Promise.reject("no such pointset");
	}
}

async function ReadPointSetCycles(name: string): Promise<Record<number, TurnCycle>> {
	const rootDir = process.env.BILLIARDS_ROOT || "data";
	const cyclesPath = path.join(rootDir, "pointset", name, "cycles");

	try {
		const data = await fs.readFile(cyclesPath, 'utf8');
		const json: Record<string, SerializedTurnCycle> = JSON.parse(data);
		const cycles: Record<number, TurnCycle> = {};
		for (const key of Object.keys(json)) {
			const value = json[key];
			const i = parseInt(key);
			cycles[i] = TurnCycle.deserialize(value);
		}
		return Promise.resolve(cycles);
	}
	catch (e) {
		return Promise.reject("no known cycles");
	}
}


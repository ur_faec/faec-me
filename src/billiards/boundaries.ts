import {OrderedField} from "core/algebra/field";
import {Complex} from "core/algebra/complex";
import { BilliardsContext } from "billiards/context";
import { QuadEmbedding } from "billiards/embedding";
import {Side} from "billiards/singularity";
import { TurnCycle } from "billiards/turn_cycle";
import {TurnPath} from "billiards/turn_path";
import {AngleBound} from "billiards/unit_power_cache";

type Boundaries<K extends OrderedField<K>> = {
	left: Array<Complex<K>>;
	right: Array<Complex<K>>;
	center: Array<Complex<K>>;
}

export function TurnPathBoundaries<K extends OrderedField<K>>(
	turnPath: TurnPath,
	startingFrom: QuadEmbedding<K>,
	angleBound: AngleBound = AngleBound.PI,
): Boundaries<K> | undefined {
	const left: Array<Complex<K>> = [];
	const right: Array<Complex<K>> = [];
	const center: Array<Complex<K>> = [];
	let quad = startingFrom;
	let orientation = turnPath.initialOrientation;
	center.push(quad.singularity(orientation.from()));
	left.push(quad.apexFromSingularity(orientation.from(), Side.Left));
	right.push(quad.apexFromSingularity(orientation.from(), Side.Right));
	for (const turn of turnPath.turns) {
		const newQuad = quad.turnWithBound(orientation.to(), turn, angleBound);
		if (newQuad === undefined) {
			return undefined;
		}
		quad = newQuad;
		orientation = orientation.reversed();

		center.push(quad.singularity(orientation.from()));
		left.push(quad.apexFromSingularity(orientation.from(), Side.Left));
		right.push(quad.apexFromSingularity(orientation.from(), Side.Right));
	}
	center.push(quad.singularity(orientation.to()));
	return { left, right, center };
}

export function FeasibleCycle<K extends OrderedField<K>>(
	cycle: TurnCycle, ctx: BilliardsContext<K>
): boolean {
	const path = cycle.asTurnPath();
	const embedding = QuadEmbedding.default(ctx);
	const boundaries = TurnPathBoundaries(path, embedding);
	if (boundaries === undefined) {
		return false;
	}
	console.log(JSON.stringify(boundaries, null, 2));
	const offset = boundaries.left[boundaries.left.length - 1].minus(boundaries.left[0]);
	console.log(`offset: ${offset}`);
	const leftHeights = boundaries.left.map(p => {
		return offset.y.neg().times(p.x).plus(offset.x.times(p.y));
	});
	const rightHeights = boundaries.right.map(p => {
		return offset.y.neg().times(p.x).plus(offset.x.times(p.y));
	});
	const centerHeights = boundaries.center.map(p => {
		return offset.y.neg().times(p.x).plus(offset.x.times(p.y));
	});
	for (let i = 0; i < path.turns.length; i++) {
		if (path.turns[i] < 0) {
			// The singularity is a lower (right-side) constraint
			rightHeights.push(centerHeights[i]);
		} else {
			// The singularity is an upper (left-side) constraint
			leftHeights.push(centerHeights[i]);
		}
	}
	const minLeft = leftHeights.reduce(
		(min, cur) => { return cur.lessThan(min) ? cur : min; });
	const maxRight = rightHeights.reduce(
		(max, cur) => { return cur.greaterThan(max) ? cur : max; });
	return minLeft.greaterThan(maxRight);
}


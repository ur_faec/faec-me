import {OrderedField} from "core/algebra/field";
import {Complex} from "core/algebra/complex";
import {Singularity, Side} from "billiards/singularity";
import {AngleBound} from "billiards/unit_power_cache";
import {BilliardsContext} from "billiards/context";

// A (orientation-preserving) scale-isometry embedding of the fundamental
// quad into the plane is uniquely determined by where it sends any two
// distinct points. We choose the particular reference points (0, 0)
// (the intersection of the diagonals) and (0, 1) (the apex in the upper
// half plane).
export class QuadEmbedding<K extends OrderedField<K>> {
	// Technically this is not the geometric center, it's the intersection
	// of the quad's two diagonals, which is at (0, 0) in the original
	// coordinate system. But calling it "center" is intuitive and less
	// confusing than calling it "origin" so we're doing it anyway.
	center: Complex<K>;
	scale: Complex<K>;

	_quadData: BilliardsContext<K>;

	constructor(
		center: Complex<K>, scale: Complex<K>, quadData: BilliardsContext<K>
	) {
		this.center = center;
		this.scale = scale;
		this._quadData = quadData;
	}

	static default<K extends OrderedField<K>>(quadData: BilliardsContext<K>): QuadEmbedding<K> {
		const K = quadData.r.at(Singularity.S0).T;
		return new QuadEmbedding(
			new Complex(K.zero, K.zero),
			new Complex(K.one, K.zero),
			quadData);
	}

	// The (embedded) coordinates of the given singularity.
	singularity(s: Singularity): Complex<K> {
		return this.center.plus(this.singularityOffset(s));
	}

	// The offset of the given singularity from this.center.
	singularityOffset(s: Singularity): Complex<K> {
		const domainCoords = this._quadData.singularities.at(s);
		return this.scale.times(domainCoords);
	}

	apexFromSingularity(s: Singularity, side: Side): Complex<K> {
		const reversed =
			(s === Singularity.S0 && side === Side.Right)
			|| (s === Singularity.S1 && side === Side.Left);
		const offset = reversed
			? new Complex(this.scale.y, this.scale.x.neg())
			: new Complex(this.scale.y.neg(), this.scale.x);
		return this.center.plus(offset);
	}

	turn(s: Singularity, degree: number): QuadEmbedding<K> {
		// turnWithBound called with no angle bound is guaranteed to return
		// a result.
		return this.turnWithBound(s, degree) as QuadEmbedding<K>;
	}

	turnWithBound(
		s: Singularity, degree: number,
		angleBound: AngleBound | undefined = undefined
	): QuadEmbedding<K> | undefined {

		const turnCoeff =
			this._quadData.turnCoeffs.at(s).power(degree, angleBound);
		if (turnCoeff === undefined) {
			return undefined;
		}

		// sigh...
		const one = new Complex(turnCoeff.x.T.one, turnCoeff.x.T.zero);

		// The vector to the central singularity from this.center
		const sVec = this._quadData.singularities.at(s).times(this.scale);

		// The vector from the current center to the new one.
		const delta = sVec.times(one.minus(turnCoeff));

		const newCenter = this.center.plus(delta);
		const newScale = this.scale.times(turnCoeff);
		return new QuadEmbedding(newCenter, newScale, this._quadData);
	}
}


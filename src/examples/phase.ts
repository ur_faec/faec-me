// This is the code that runs the shader on the front page of faec.me

import { Canvas } from "core/canvas";
import { Complex } from "core/complex";
import { ComplexPoly } from "core/complex_poly";
import { Coords2d } from "core/geometry";
import { AxisRect, Size } from "core/polygon";
import { PhaseView } from "phase/phase_view";

const epsilon = 0.01;
const G = 1;
const M = 1;
const m = 0.001;
// gravitational force is proportionate to G*M*m / |r|^2
// how many steps of the physical model to compute each frame
const stepsPerFrame = 12000;
// the change in the time parameter t with each step of the
// physical model.
const dt = 1.0 / 1000000.0;

function measure(z: Complex) {
	// all assumed to be fixed unit masses
	const gCenters = [
		Complex.fromReal(-0.5),
		Complex.fromReal(0.5),
		//Complex.fromReal(0),
	];
	let potentialSum = 0;
	let accelerationSum = Complex.zero();
	for (const center of gCenters) {
		const r = center.minus(z);
		const rAbs = r.abs() + epsilon;

		const acceleration =
			r.timesScalar(G * M / (rAbs * rAbs * rAbs));

		potentialSum -= 1/rAbs;
		accelerationSum = accelerationSum.plus(acceleration);
	}
	return {
		potential: potentialSum,
		acceleration: accelerationSum,
	};
}

function HexagonVertices(): Array<Complex> {
	const angleCoeff = 2 * Math.PI / 6.0;
	const angles = [
		0, angleCoeff, 2*angleCoeff, 3*angleCoeff, 4*angleCoeff, 5*angleCoeff];
	return angles.map((angle: number): Complex => {
		return Complex.fromPolar(1, angle);
	});
}

export function Thingie(
	canvasSize: Size,
	canvasElement: HTMLCanvasElement
): PhaseView {
	const canvas = new Canvas(canvasSize, canvasElement);
	const view = new PhaseView(canvas.asRenderTarget(true), false);
	view.modelBounds = AxisRect.fromCenterAndDimensions(Coords2d.origin(), 3.5, 3.5);
	view.renderParams.showCartesianGrid = false;
	view.renderParams.showPhaseGrid = false;//true;//false;
	view.renderParams.showAbsGrid = false;

	const h = Math.sqrt(3.0) / 2.0;
	const pairCenters = [
		new Complex(-0.5, -h / 3),
		new Complex(0.5, -h / 3),
		new Complex(0.0, 2 * h / 3),
	];

	const hex = HexagonVertices();
	console.log("Hexagon:", hex);

	const blip = {
		center: new Complex(1, 1),
		angle: 3,
		energy: 0,
		velocity: new Complex(0.0, 0.6),
	};

	const { potential, acceleration } = measure(blip.center);
	blip.energy = potential + 0.1;
	const initialSpeed = blip.velocity.abs();
	const initialKinetic = initialSpeed * initialSpeed / 2;
	blip.energy = initialKinetic + potential;

	console.log("initial potential:", potential, "initial acceleration:", acceleration, "initial energy:", blip.energy);
	
	// 

	//let t = 0;
	let frame = 0;
	function nextFrameCallback(): () => void {
		let t = frame * (stepsPerFrame * dt);
		const theta = 0.0002 * t;
		const zeroes: Array<Complex> = [];
		const poles: Array<Complex> = [];

		// update blip
		// k = v^2 -> v = sqrt(k)
		for (let i = 0; i < stepsPerFrame; i++) {
			const { potential, acceleration } = measure(blip.center);
			const speed = blip.velocity.abs();
			const kinetic = speed * speed / 2;
			const inferredKinetic = blip.energy - potential;
			let velocity = blip.velocity;
			if (kinetic > inferredKinetic) {
				const inferredSpeed = Math.sqrt(2*inferredKinetic);
				velocity = velocity.timesScalar(inferredSpeed / speed);
			}
			//const kinetic = blip.energy - potential;
			//const absSpeed = Math.sqrt(Math.abs(kinetic));
			//const speed = (kinetic >= 0) ? absSpeed : -absSpeed;
			//const velocity = Complex.fromPolar(speed, blip.angle);

			//const acceleration = force.timesScalar(G);

			if (frame % 60 == 1 && i == 0) {
				console.log("acceleration:", acceleration, "potential:", potential, "kinetic:", kinetic, "total:", potential + kinetic);
			}
	
			const newCenter = blip.center.plus(velocity.timesScalar(dt));
			const newVelocity = velocity.plus(acceleration.timesScalar(dt));
			//const newAngle = Math.atan2(newVelocity.y, newVelocity.x);

			blip.center = newCenter;
			blip.velocity = newVelocity;
			//blip.angle = newAngle;

			t += dt;
		}
		frame++;

		// kinetic = m*v^2 / 2
		//   = p^2 / m / 2
		//   v^2 = 2*kinetic/m
		//   v = sqrt(2*kinetic/m)
		//const blipRadius = 3;
		const blipCoords = blip.center;
		/*new Complex(
			blipRadius*Math.cos(11*theta),
			blipRadius*Math.sin(10*theta)
		);*/
		zeroes.push(blipCoords);
		//zeroes.push(Complex.zero());
		//zeroes.push(Complex.zero());
		//zeroes.push(blipCoords);
		//zeroes.push(blipCoords);
			
		//newCenter);

		// update triangle
		const angles = [-theta * 40, -theta * 50, -theta * 70, theta * 90];
		const angleVectors = angles.map((angle: number): Complex => {
			return Complex.fromPolar(1, angle);
		});

		const pairRadius = 0.01;
		const rotationCoeff = Complex.fromPolar(1, theta * 5);
		for (let i = 0; i < pairCenters.length; i++) {
			const center = pairCenters[i];
			const rotatedCenter = center.times(rotationCoeff);
			const offset = angleVectors[i % angleVectors.length].timesScalar(pairRadius);
			zeroes.push(rotatedCenter.plus(offset));
			poles.push(rotatedCenter.minus(offset));
		}

		//console.log("zero count:", zeroes.length, "")
		view.numerator = ComplexPoly.fromZeroes(zeroes);
		view.denominator = ComplexPoly.fromZeroes(poles);
		return () => {
			requestAnimationFrame(nextFrameCallback());
		};
	}

	view.renderParams.absScale = 1;
	view.renderParams.cartesianScale = 1;

	requestAnimationFrame(nextFrameCallback());
	return view;
}

export function Thingie2(
	canvasSize: Size,
	canvasElement: HTMLCanvasElement
): PhaseView {
	const canvas = new Canvas(canvasSize, canvasElement);
	const view = new PhaseView(canvas.asRenderTarget(true), false);
	view.modelBounds = AxisRect.fromCenterAndDimensions(Coords2d.origin(), 3.5, 3.5);
	view.renderParams.showCartesianGrid = true;//false;
	view.renderParams.showPhaseGrid = false;//true;//false;
	view.renderParams.showAbsGrid = false;

	const h = Math.sqrt(3.0) / 2.0;
	const pairCenters = [
		new Complex(-0.5, -h / 3),
		new Complex(0.5, -h / 3),
		new Complex(0.0, 2 * h / 3),
	];

	const hex = HexagonVertices();
	console.log("Hexagon:", hex);
	
	let t = 0;
	function nextFrameCallback(): () => void {
		const theta = 0.0002 * t;
		t++;
		const zeroes: Array<Complex> = [];
		const poles: Array<Complex> = [];

		const blipRadius = 3;
		const blipCoords = new Complex(
			blipRadius*Math.cos(11*theta),
			blipRadius*Math.sin(10*theta)
		);
		zeroes.push(blipCoords);
		zeroes.push(Complex.zero());
		zeroes.push(Complex.zero());
		//zeroes.push(blipCoords);
		//zeroes.push(blipCoords);

		// update triangle
		const angles = [-theta * 40, -theta * 50, -theta * 70, theta * 90];
		const angleVectors = angles.map((angle: number): Complex => {
			return Complex.fromPolar(1, angle);
		});

		const pairRadius = 0.25;
		const triangleScale = 1.2;
		const rotationCoeff = Complex.fromPolar(1, theta * 5);
		for (let i = 0; i < 3; i++) {
			const center = pairCenters[i].timesScalar(triangleScale);
			const rotatedCenter = center.times(rotationCoeff);
			poles.push(rotatedCenter);
			const offset = angleVectors[i % angleVectors.length].timesScalar(pairRadius);
			for (let j = 0; j < hex.length; j++) {
				const rotatedOffset = offset.times(hex[j]);
				const coords = rotatedCenter.plus(rotatedOffset);
				if (j % 2 == 0) {
					zeroes.push(coords);
				} else {
					poles.push(coords);
				}
			}
		}
		//console.log("zero count:", zeroes.length, "")
		view.numerator = ComplexPoly.fromZeroes(zeroes);
		view.denominator = ComplexPoly.fromZeroes(poles);
		return () => {
			requestAnimationFrame(nextFrameCallback());
		};
	}

	view.renderParams.absScale = 1;
	view.renderParams.cartesianScale = 1;

	requestAnimationFrame(nextFrameCallback());
	return view;
}

export function ViewportSize(): Size {
	const doc = document;//, w = window;
	const docEl = (doc.compatMode && doc.compatMode === 'CSS1Compat')?
		doc.documentElement: doc.body;
	
	const width = docEl.clientWidth;
	const height = docEl.clientHeight;
	
	// mobile zoomed in?
	/*if ( w.innerWidth && width > w.innerWidth ) {
			width = w.innerWidth;
			height = w.innerHeight;
	}*/
	
	return Size.fromWH(width, height);
}
// @flow

/*import $ from "jquery";
import { CallbackPointsInterface } from 'faec/callback_points_interface';
import { IsoScaleTransform, TranslationTransform3d, CompositionTransform3d,
  CoordinateTransform3d }
  from 'faec/coordinate_transform';
import { Coords2d, OffsetCoords2d, Coords3d, OffsetCoords3d }
  from 'faec/geometry';
import { GluedFace, TriangleTetraGluing } from 'faec/gluing';
import { Rect, Triangle2d, Triangle3d } from 'faec/polygon';
import { Quaternion } from 'faec/quaternion';
import { RotationTransform3d } from 'faec/rotation_transform';

import { TriangleGluingRenderer, TriangleGluingRendererParams,
  GluedEmbeddingRenderer, GluedEmbeddingRendererParams }
  from './triangle_gluing_renderer';

export class TriangleGluingModel {
  _container: HTMLElement;
  _viewSize: [number, number];
  _canvasSize: [number, number];

  _canvasBounds: Rect;
  _gluingBounds: Rect;
  _embeddingBounds: Rect;

  _gluingInterface: CallbackPointsInterface;
  _embeddingInterface: CallbackPointsInterface;

  _gluingRenderer: TriangleGluingRenderer;
  _embeddingRenderer: GluedEmbeddingRenderer;
  _needsRedraw: boolean;

  _triangle: Triangle2d;
  _gluing: TriangleTetraGluing;
  _gluedFaces: Array<GluedFace>;
  //_embedding: Array<Triangle3d>;

  _distance: number;
  _rotation: Quaternion;
  _coordinateTransform: IsoScaleTransform;

  _gradientActive: boolean;

  editMode: string;

  constructor(container: HTMLElement, viewSize: [number, number]) {
    // Internal UI state
    this._viewSize = viewSize;
    this._container = container;
    this._canvasSize = [viewSize[0] / 2, viewSize[1]];
    this._canvasBounds = new Rect(
      Coords2d.fromXY(0, this._canvasSize[1]),
      OffsetCoords2d.fromDxDy(this._canvasSize[0], -this._canvasSize[1])
    );

    this._needsRedraw = false;

    this.gluingBounds = Rect.fromDimensions(2, 2);
    this.editMode = 'triangle';

    // Internal array to store the embedded 3-d triangles.
    this._distance = 3;
    this._triangle = Triangle2d.fromCoords(
      Coords2d.origin(),
      Coords2d.fromXY(1, 0),
      Coords2d.fromXY(0.5, Math.sqrt(3)/4)
    );
    this._rotation = Quaternion.fromReal(1);
    this._gluing = TriangleTetraGluing.fromEdgeIndexAndGluePoint(0, 0);
    this._refreshEmbedding();
  }

  get triangle (): Triangle2d {
    return this._triangle;
  }

  set triangle (triangle: Triangle2d) {
    this._triangle = triangle;
    this._needsRedraw = true;
    this._refreshEmbedding();
    // Update the interface or anything?
  }

  get gluing (): TriangleTetraGluing {
    return this._gluing;
  }

  set gluing (gluing: TriangleTetraGluing) {
    this._gluing = gluing;
    this._refreshEmbedding();
    this._needsRedraw = true;
  }

  _refreshEmbedding () {
    if (this._gluing && this._triangle) {
      this._gluedFaces = this._gluing.gluedFacesForTriangle(this._triangle);
      this._needsRedraw = true;
      this._gradientActive = true;
    }
  }

  _potentialForGluedFace (face: GluedFace): number {
    if (!face.embedding) {
      return 0;
    }
    const tri: Triangle3d = face.embedding;
    let total = 0;
    for (let i = 0; i < 3; i++) {
      let localEdge = tri.rootedEdge(i);
      if (face.connections[i]) {
        let conn = face.connections[i];
        let neighbor = conn.traverse(face.edge(i));
        if (neighbor.face.embedding) {
          let neighborTri: Triangle3d = neighbor.face.embedding;
          let neighborEdge =
            neighborTri.rootedEdge(neighbor.edgeIndex).reversed();
          let vec1 = neighborEdge.base.asOffsetFrom(localEdge.base);
          let localEndpoint = localEdge.base.plus(localEdge.offset);
          let neighborEndpoint =
            neighborEdge.base.plus(neighborEdge.offset);
          let vec2 = neighborEndpoint.asOffsetFrom(localEndpoint);
          total += vec1.squaredLength() + vec2.squaredLength();
        }
      }
    }
    return total;
  }

  _gradientForGluedFace (face: GluedFace): Array<OffsetCoords3d> {
    let gradient = [
      OffsetCoords3d.zero(), OffsetCoords3d.zero(), OffsetCoords3d.zero()];
    if (!face.embedding) {
      return gradient;
    }
    const tri: Triangle3d = face.embedding;
    for (let i = 0; i < 3; i++) {
      let localEdge = tri.rootedEdge(i);
      if (face.connections[i]) {
        let conn = face.connections[i];
        let neighbor = conn.traverse(face.edge(i));
        if (neighbor.face.embedding) {
          let neighborTri: Triangle3d = neighbor.face.embedding;
          let neighborEdge =
            neighborTri.rootedEdge(neighbor.edgeIndex).reversed();

          let vec1 = neighborEdge.base.asOffsetFrom(localEdge.base);
          gradient[i] = gradient[i].plus(vec1.timesReal(2));

          let localEndpoint = localEdge.base.plus(localEdge.offset);
          let neighborEndpoint =
            neighborEdge.base.plus(neighborEdge.offset);
          let vec2 = neighborEndpoint.asOffsetFrom(localEndpoint);
          let nexti = (i+1) % 3;
          gradient[nexti] = gradient[nexti].plus(vec2.timesReal(2));
        }
      }
    }
    return gradient;
  }

  _gradientMotionForGluedFace (face: GluedFace) {
    if (!face.embedding) {
      return {
        offset: OffsetCoords3d.zero(),
        rotation: OffsetCoords3d.zero(),
      };
    }
    const tri: Triangle3d = face.embedding;
    let triCenter = tri.center();
    let centeredVerts = [
      tri.vertexCoords[0].asOffsetFrom(triCenter),
      tri.vertexCoords[1].asOffsetFrom(triCenter),
      tri.vertexCoords[2].asOffsetFrom(triCenter),
    ];
    let gradient = this._gradientForGluedFace(face);
    let gradientSum =
      gradient[0].plus(gradient[1]).plus(gradient[2]);
    let centeringOffset = gradientSum.timesReal(-1/3);
    let centeredGradient = [
      gradient[0].plus(centeringOffset),
      gradient[1].plus(centeringOffset),
      gradient[2].plus(centeringOffset),
    ];

    // centeredGradient has 3 translational degrees of freedom factored out.
    // The remaining components are all rotation / scale, but for a rigid
    // motion we can only do rotation, so extract out the rotation
    // components as the torques of the remaining forces.
    let torque = OffsetCoords3d.zero();
    for (let i = 0; i < 3; i++) {
      torque = torque.plus(centeredVerts[i].cross(centeredGradient[i]));
    }
    return {
      offset: gradientSum,
      rotation: torque,
    };
  }

  embeddedTrianglesForGluing(
      triangle: Triangle2d, gluing: TriangleTetraGluing): Array<Triangle3d> {
    let gluedFaces = this.gluing.gluedFacesForTriangle(triangle);
    let triangles: Array<Triangle3d> = [];
    for (let i = 0; i < gluedFaces.length; i++) {
      const f = gluedFaces[i];
      if (f.embedding) {
        triangles.push(f.embedding);
      }
    }
    return triangles;
  }

  get gluingBounds (): Rect {
    return this._gluingBounds;
  }
  set gluingBounds (gluingBounds: Rect) {
    var transform =
        IsoScaleTransform.forCenteredRect(this._canvasBounds, gluingBounds);
    this._coordinateTransform = transform;
    var {origin, diagonal} = this._canvasBounds;
    var mappedOrigin = transform.transformCoords(origin);
    var mappedDiagonal = transform.transformOffset(diagonal);
    this._gluingBounds = new Rect(mappedOrigin, mappedDiagonal);
    mappedDiagonal = mappedDiagonal.timesReal(1.0 / 3.0);
    mappedOrigin = mappedOrigin.plus(mappedDiagonal);
    this._embeddingBounds = new Rect(mappedOrigin, mappedDiagonal);
    this._needsRedraw = true;
  }

  start() {
    window.console.log("TriangleGluingModel.start");
    this._gluingRenderer = new TriangleGluingRenderer(this._canvasSize);
    this._embeddingRenderer = new GluedEmbeddingRenderer(this._canvasSize);
    $(this._container).css({
      'width': `${this._viewSize[0]}px`, 'margin': 'auto',
      'position': 'relative'});
    $(this._container).append(this._gluingRenderer.canvas);
    $(this._container).append(this._embeddingRenderer.canvas);
    $(this._container).append(this._createControls());

    this._createControlInterfaces();
    this._addEventListeners();

    requestAnimationFrame(this.generateClockTick());
  }

  reset() {
  }

  generateClockTick() {
    if (this._gradientActive) {
      let delta = this._advanceGradient();
      window.console.log(`${delta}`);
      this._needsRedraw = true;
      if (delta >= -0.000001) {
        this._gradientActive = false;
      }
    }
    //window.console.log(`advanceGradient delta ${delta}`);
    if (this._needsRedraw) {
      this.redraw();
    }
    return () => {
      requestAnimationFrame(this.generateClockTick());
    };
  }

  _advanceGradient() {
    let embeddings: Array<?Triangle3d> = [];
    let before = 0;
    for (let i = 0; i < this._gluedFaces.length; i++) {
      const face = this._gluedFaces[i];
      before += this._potentialForGluedFace(face);
      if (face.embedding) {
        const embedding: Triangle3d = face.embedding.copy();
        const motion = this._gradientMotionForGluedFace(face);
        const coeff = 0.1;
        const oldCenter = embedding.center();
        const offset = motion.offset.timesReal(coeff);
        const newCenter = oldCenter.plus(offset);
        let transforms: Array<CoordinateTransform3d> = [];
        transforms.push(TranslationTransform3d.fromOffset(
          Coords3d.origin().asOffsetFrom(oldCenter)));
        if (motion.rotation.length() > 0.000001) {
          transforms.push(
            RotationTransform3d.rotationAroundAxisByAngle(
              motion.rotation, motion.rotation.length() * coeff));
        }
        transforms.push(
          TranslationTransform3d.fromOffset(
            newCenter.asOffsetFrom(Coords3d.origin())));
        const transform = new CompositionTransform3d(transforms);
        for (let j = 0; j < 3; j++) {
          embedding.vertexCoords[j] =
            transform.transformCoords(embedding.vertexCoords[j]);
        }
        embeddings.push(embedding);
      } else {
        embeddings.push(null);
      }
    }
    for (let i = 0; i < this._gluedFaces.length; i++) {
      this._gluedFaces[i].embedding = embeddings[i];
    }
    let after = 0;
    for (let i = 0; i < this._gluedFaces.length; i++) {
      after += this._potentialForGluedFace(this._gluedFaces[i]);
    }
    return after - before;
  }

  redraw () {
    this._needsRedraw = false;
    let gluingParams = new TriangleGluingRendererParams();
    gluingParams.triangle = this._triangle;

    this._gluingRenderer.modelBounds = this.gluingBounds;
    this._gluingRenderer.render(gluingParams);

    let embeddingParams = new GluedEmbeddingRendererParams();
    embeddingParams.triangle = this._triangle;
    let embedding: Array<Triangle3d> = [];
    for (let i = 0; i < this._gluedFaces.length; i++) {
      const face = this._gluedFaces[i];
      //let motion = this._gradientMotionForGluedFace(face);
      if (face.embedding) {
        embedding.push(face.embedding);
      }
    }
    embeddingParams.embedding = embedding;
    var centerOffset = OffsetCoords3d.fromDxDyDz(0, 0, this._distance);
    //window.console.log("TriangleGluingModel.redraw");


    //options.distance = this._distance;
    //options.rotation = this._rotation;
    this._embeddingRenderer.modelTransform = new CompositionTransform3d([
      RotationTransform3d.fromQuaternion(this._rotation),
      TranslationTransform3d.fromOffset(centerOffset),
    ]);
    this._embeddingRenderer.viewportBounds = this._embeddingBounds;
    this._embeddingRenderer.render(embeddingParams);
  }

  _createControls() {
    // TODO: I'm really mad at myself for resorting to this. Fix it.
    var div = $("<div></div>").css({
      width: this._viewSize[0], height: '31px', margin: 'auto',
      position: 'absolute', left: '0px', top: `${this._viewSize[1]}px`});
    div.html(`
      <form style="margin-top: 5px; float: left; width: 50%;"
          class="gluing-controls">
        <div style="display: inline-block;float: left; padding-left:20px;">
        <label>
          <input type="radio" name="edit-mode" value="triangle"
              checked="checked">
          Edit triangle
        </label>
        </div>
        <div style="display: inline-block;float: left;">
        <label>
          <input type="radio" name="edit-mode" value="gluing">
          Edit gluing
        </label>
        </div>
      </form>`);
    return div;
  }

  _triangleEditCallback (canvasCoords: Coords2d) {
    var maxDist = this._coordinateTransform.isoScale() * 30;
    var modelCoords =
        this._coordinateTransform.transformCoords(canvasCoords);

    var index = this.triangle.nearestVertexTo(modelCoords);
    var targetCoords = this.triangle.vertexCoords[index].copy();
    if (modelCoords.distanceFrom(targetCoords) <= maxDist) {
      return (newCanvasCoords: Coords2d) => {
        var newModelCoords =
            this._coordinateTransform.transformCoords(newCanvasCoords);
        var modelDelta = newModelCoords.asOffsetFrom(modelCoords);
        this.triangle.vertexCoords[index] =
          targetCoords.plus(modelDelta);
        this._refreshEmbedding();
      };
    }
  }

  _gluingEditCallback (canvasCoords: Coords2d) {
    var maxDist = this._coordinateTransform.isoScale() * 30;
    var modelCoords =
        this._coordinateTransform.transformCoords(canvasCoords);
    var gluingCoords = this.triangle.coordsForPerimeterPoint(
      this.gluing.perimeterPoint
    );

    if (modelCoords.distanceFrom(gluingCoords) <= maxDist) {
      return (newCanvasCoords: Coords2d) => {
        var newModelCoords =
            this._coordinateTransform.transformCoords(newCanvasCoords);
        // Now want to find the closest point on the perimeter of the
        // triangle.
        var nearest =
            this._triangle.nearestPerimeterPointTo(newModelCoords);
        //this.gluing = TriangleTetraGluing.fromPerimeterPoint(nearest);
        if (nearest) {
          this.gluing.perimeterPoint = nearest;
        }
        this._refreshEmbedding();
      };
    }
  }

  _createControlInterfaces() {
    this._gluingInterface = new CallbackPointsInterface(
      (canvasCoords) => {
        if (this.editMode == 'triangle') {
          return this._triangleEditCallback(canvasCoords);
        } else if (this.editMode == 'gluing') {
          return this._gluingEditCallback(canvasCoords);
        }
      }, this._gluingRenderer.canvas);

    this._embeddingInterface = new CallbackPointsInterface(
      (canvasCoords) => {
        var lastCoords = canvasCoords;
        return (newCanvasCoords) => {
          var delta = newCanvasCoords.asOffsetFrom(lastCoords);
          if (delta.length() == 0) {
            return;
          }
          lastCoords = newCanvasCoords;

          var theta = delta.length() * Math.PI / 150.0;
          var rotationAxis = delta.cross().normalized();

          var cos = Math.cos(theta / 2);
          var sin = Math.sin(theta / 2);
          var q = Quaternion.fromRIJK(
            cos, sin * rotationAxis.dx, -sin * rotationAxis.dy, 0);
          var rotation = q.iTimes(this._rotation);
          this._rotation = rotation;
          this._needsRedraw = true;
        };
      }, this._embeddingRenderer.canvas);
  }

  _addEventListeners() {
    $(this._container).find('input').change(() => {
      window.console.log("Input changed in container:", this._container);
      this.editMode =
          $(this._container)
            .find('input[name="edit-mode"]:checked')[0].value;
      window.console.log(`edit mode: ${this.editMode}`);
      this._needsRedraw = true;
    });
  }
}
*/
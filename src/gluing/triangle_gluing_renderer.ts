/*

import { IsoScaleTransform, CoordinateProjection } from "core/coordinate_transform";
import { TangentCoords2d, Coords2d, Coords3d, OffsetCoords3d, TangentCoords3d }
	from "core/geometry";
import { PlaneRenderer } from "core/plane_renderer";
import { AxisRect, Triangle2d, Triangle3d } from "core/polygon";
import { ThreeSpaceRenderer } from "core/three_space_renderer";

function _drawRootedTCoords(ctx: CanvasRenderingContext2D, c: TangentCoords2d) {
	ctx.beginPath();
	ctx.moveTo(c.base.x, c.base.y);
	ctx.lineTo(c.base.x + c.offset.dx, c.base.y + c.offset.dy);
	ctx.stroke();
}

export class TriangleGluingRendererParams {

	triangle: Triangle2d | null | undefined;

	constructor() {}
}

export class TriangleGluingRenderer extends PlaneRenderer<TriangleGluingRendererParams> {
	_render(
		ctx: CanvasRenderingContext2D,
		rect: AxisRect,
		transform: IsoScaleTransform,
		params: TriangleGluingRendererParams
	) {
		if (params.triangle) {
			const t = params.triangle.copy();
			t.applyTransform(transform);
			const v = t.vertexCoords;
			ctx.strokeStyle = '#ff33ee';
			ctx.lineWidth = 2;
			ctx.beginPath();
			ctx.moveTo(v[0].x, v[0].y);
			ctx.lineTo(v[1].x, v[1].y);
			ctx.lineTo(v[2].x, v[2].y);
			ctx.closePath();
			ctx.stroke();

			const inner = [];
			for (let i = 0; i < 3; i++) {
				const edge = t.rootedEdge(i);
				inner.push(edge.base.plus(edge.offset.timesReal(0.5)));
			}
			const ti = Triangle2d.fromCoords(inner[0], inner[1], inner[2]);

			for (let i = 0; i < 3; i++) {
				const edge = ti.rootedEdge(i);
				const guide = edge.copy();
				guide.base = guide.base.plus(guide.offset.reversed());
				guide.offset = guide.offset.timesReal(-3);

				ctx.lineWidth = 1;
				ctx.strokeStyle = '#bbbbbb';
				if (ctx.setLineDash) {
					ctx.setLineDash([]);
				}
				_drawRootedTCoords(ctx, guide);

				ctx.strokeStyle = '#555555';
				if (ctx.setLineDash) {
					ctx.setLineDash([2, 2]);
				}
				_drawRootedTCoords(ctx, edge);
			}

		}
	}
}

const redLight = OffsetCoords3d.fromDxDyDz(-4, -4, 5).normalized();
const greenLight = OffsetCoords3d.fromDxDyDz(5, -2, 5).normalized();
const blueLight = OffsetCoords3d.fromDxDyDz(-1, 5, 5).normalized();

function _colorForNormalVector(normal: OffsetCoords3d) {
	const red = 0.6 + 0.3 * Math.max(-redLight.dot(normal), 0);
	const green = 0.6 + 0.2 * Math.max(-greenLight.dot(normal), 0);
	const blue = 0.65 + 0.2 * Math.max(-blueLight.dot(normal), 0);
	return {
		r: Math.round(red * 255),
		g: Math.round(green * 255),
		b: Math.round(blue * 255),
	};
}

type NormalTri = {verts: [Coords2d], normal: OffsetCoords3d};

// Expects a list of objects with keys verts : Coords2d list and
// normal : OffsetCoords3d
function _fillTriangles(ctx: CanvasRenderingContext2D, triangles: [NormalTri]) {
	for (const tri of triangles) {
		const { verts, normal } = tri;
		const {
			r,
			g,
			b,
		} = _colorForNormalVector(normal);
		ctx.fillStyle = `rgb(${r},${g},${b})`;
		ctx.beginPath();
		ctx.moveTo(verts[0].x, verts[0].y);
		ctx.lineTo(verts[1].x, verts[1].y);
		ctx.lineTo(verts[2].x, verts[2].y);
		ctx.closePath();
		ctx.fill();
	}
}

function _outlineTriangles(ctx: CanvasRenderingContext2D, triangles) {
	for (const {
		verts,
	} of triangles) {
		ctx.beginPath();
		ctx.moveTo(verts[0].x, verts[0].y);
		ctx.lineTo(verts[1].x, verts[1].y);
		ctx.lineTo(verts[2].x, verts[2].y);
		ctx.closePath();
		ctx.stroke();
	}
}

function _projectTriangle(triangle: Triangle3d, projection: CoordinateProjection): Triangle2d {
	const v = triangle.vertexCoords;
	const transformed = Triangle2d.fromCoords(projection.transformCoords(v[0]), projection.transformCoords(v[1]), projection.transformCoords(v[2]));
	return transformed;
}

export class GluedEmbeddingRendererParams {

	triangle: Triangle2d | null | undefined;
	//gluing:
	embedding: Array<Triangle3d>;
	constructor() {
		this.embedding = [];
	}
}

export class GluedEmbeddingRenderer extends ThreeSpaceRenderer<GluedEmbeddingRendererParams> {


	_render(ctx: CanvasRenderingContext2D, projection: CoordinateProjection, params: GluedEmbeddingRendererParams) {
		const inverseModelTransform = projection.transform3d.inverse();
		const viewTangent = TangentCoords3d.fromBaseAndOffset(inverseModelTransform.transformCoords(Coords3d.origin()), inverseModelTransform.transformOffset(OffsetCoords3d.fromDxDyDz(0, 0, 1)));

		const facingViewer = [];
		const facingAway = [];
		for (const triangle of params.embedding) {
			const normal = triangle.normal();
			const offset = triangle.vertexCoords[0].asOffsetFrom(viewTangent.base);
			const screenTriangle = _projectTriangle(triangle, projection);
			const fill = normal.dot(offset) <= 0;
			if (fill) {
				facingViewer.push({
					verts: screenTriangle.vertexCoords,
					normal: normal });
			} else {
				facingAway.push({
					verts: screenTriangle.vertexCoords,
					normal: normal });
			}
		}
		_fillTriangles(ctx, facingViewer);
		ctx.strokeStyle = '#777777';
		ctx.lineWidth = 1;
		if (ctx.setLineDash) {
			ctx.setLineDash([2, 2]);
		}
		_outlineTriangles(ctx, facingAway);
		ctx.strokeStyle = '#ff33ee';
		ctx.lineWidth = 2;
		if (ctx.setLineDash) {
			ctx.setLineDash([]);
		}
		_outlineTriangles(ctx, facingViewer);
	}
}

*/
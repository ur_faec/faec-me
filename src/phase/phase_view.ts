

import { CallbackPointsInterface, PointsUpdateCallback }
	from "core/callback_points_interface";
import { RenderTarget } from "core/canvas";
import { Complex } from "core/complex";
import { ComplexPoly, ComplexSingularity, Meromorphic }
	from "core/complex_poly";
import { IsoScaleTransform } from "core/coordinate_transform";
import { Coords2d, OffsetCoords2d } from "core/geometry";
import { AxisRect } from "core/polygon";
import { AllShaders } from "core/shaders/all_shaders";

import { DPhasePlaneShaderRenderer, DPhasePlaneShaderRendererParams }
	from "./dphase_render";

const _paletteShader = AllShaders.fragment.hsvDefault;
//const _paletteShader = AllShaders.fragment.hsvDouble;
//const _paletteShader = AllShaders.fragment.hsvSplit6;
//const _paletteShader = AllShaders.fragment.hsvSplit12;
const MAX_DEGREE = 8;

// returns a transform that maps coordinates in renderBounds to
// coordinates in modelBounds.
function transformForBounds(renderBounds: AxisRect, modelBounds: AxisRect) {
	return IsoScaleTransform.transformOnto(
		renderBounds, modelBounds, true);	
}

type CoordsCallback = (renderCoords: Coords2d) => void;
type ControlMode = "singularities" | "constant" | "numConstant" | "pan";

type PaletteMode = "default" | "doubled" | "grad6" | "split3" | "split6" | "split12" | "trans";

function shaderForPaletteMode(mode: PaletteMode): string {
	switch (mode) {
	case "doubled":
		return AllShaders.fragment.hsvDouble;
	case "grad6":
		return AllShaders.fragment.hsvGrad6;
	case "split3":
		return AllShaders.fragment.hsvSplit3;
	case "split6":
		return AllShaders.fragment.hsvSplit6;
	case "split12":
		return AllShaders.fragment.hsvSplit12;
	case "trans":
		return AllShaders.fragment.hsvTrans;
	default:
		return AllShaders.fragment.hsvDefault;
	}
}

// Top-level properties: numerator, denominator
export class PhaseView {

	renderParams: DPhasePlaneShaderRendererParams;
	_renderTarget: RenderTarget;
	_renderer: DPhasePlaneShaderRenderer;
	_paletteMode: PaletteMode = "default";

	_modelBounds: AxisRect;
	//_coordinateTransform: IsoScaleTransform;
	_needsRedraw: boolean;
	_redrawCounter: number;

	controlMode: ControlMode = "singularities";
	_controlPointsInterface: CallbackPointsInterface | null;

	_numerator: ComplexPoly;
	_denominator: ComplexPoly;
	_constantTerm: Complex;
	_phaseOffset: number;
	_offsetCoords: Coords2d;

	constructor(renderTarget: RenderTarget, allowInteraction: boolean = true) {
		this._renderTarget = renderTarget;
		this.renderParams = new DPhasePlaneShaderRendererParams();
		this._renderer = new DPhasePlaneShaderRenderer(
			renderTarget.canvas, shaderForPaletteMode(this._paletteMode));

		this._redrawCounter = 1000;
		this._needsRedraw = true;
		this._controlPointsInterface = null;

		// Exposed properties
		this._numerator = ComplexPoly.fromReal(1);
		this._denominator = ComplexPoly.fromReal(1);
		this._constantTerm = Complex.zero();
		this._phaseOffset = 0;
		this._modelBounds = AxisRect.fromXYWH(-1, -1, 2, 2);
		this._renderer.renderFrom(this._modelBounds);

		// The coordinates constantOffset was derived from.
		this._offsetCoords = Coords2d.fromXY(0, 0);

		console.log(`allowInteraction: ${allowInteraction}`);
		if (allowInteraction) {
			this._createControlInterfaces();
		}

		requestAnimationFrame(this.generateClockTick());
	}

	get paletteMode(): PaletteMode {return this._paletteMode;}
	set paletteMode(mode: PaletteMode) {
		this._paletteMode = mode;
		this._renderer = new DPhasePlaneShaderRenderer(
			this._renderTarget.canvas, shaderForPaletteMode(mode));
		this._renderer.renderFrom(this._modelBounds);
		this._needsRedraw = true;
	}

	get numerator(): ComplexPoly {return this._numerator;}
	set numerator(numerator: ComplexPoly) {
		this._numerator = numerator;
		this._needsRedraw = true;
	}

	get denominator(): ComplexPoly {return this._denominator;}
	set denominator(denominator: ComplexPoly) {
		this._denominator = denominator;
		this._needsRedraw = true;
	}

	get constantTerm(): Complex {return this._constantTerm;}
	set constantTerm(constantTerm: Complex) {
		this._constantTerm = constantTerm;
		this._needsRedraw = true;
	}

	// A number in the range 0-1. The real function is multiplied by
	// e^{2 * pi * phaseOffset]}
	get phaseOffset(): number {return this._phaseOffset;}
	set phaseOffset(offset: number) {
		this._phaseOffset = offset;
		this._needsRedraw = true;
	}

	get modelBounds(): AxisRect {
		return this._modelBounds;
	}
	set modelBounds(modelBounds: AxisRect) {
		const transform = IsoScaleTransform.transformOnto(
			this._renderTarget.bounds, modelBounds, true);
		this._modelBounds = this._renderTarget.bounds.applyTransform(transform);
		this._renderer.renderFrom(this._modelBounds);
		this._needsRedraw = true;
	}

	// the (model-space) position of the center of the view
	get center(): Coords2d {
		return this._modelBounds.center();
	}
	set center(center: Coords2d) {
		const oldCenter = this._modelBounds.center();
		const delta = center.asOffsetFrom(oldCenter);
		this._modelBounds.origin =
			this._modelBounds.origin.plus(delta);
		this._needsRedraw = true;
	}

	addZero(): void {
		this._commitConstantTerm();
		if (this.numerator.degree < MAX_DEGREE) {
			const zeroes = this.numerator.computeZeroes();
			const newPoint = this.center.plus(
				OffsetCoords2d.fromDxDy(this._modelBounds.size.width/6, 0));
			zeroes.push(newPoint.asComplex());
			this.numerator = ComplexPoly.fromZeroes(zeroes);

		}
	}

	removeZero(): void {
		this._commitConstantTerm();
		if (this.numerator.degree > 1) {
			const zeroes = this.numerator.computeZeroes();
			zeroes.pop();
			this.numerator = ComplexPoly.fromZeroes(zeroes);
		} else if (this.numerator.degree > 0) {
			this.numerator = ComplexPoly.one();
			this.constantTerm = Complex.zero();
		}
	}

	addPole(): void {
		this._commitConstantTerm();
		if (this.denominator.degree < MAX_DEGREE) {
			const poles = this.denominator.computeZeroes();
			const newPoint = this.center.plus(
				OffsetCoords2d.fromDxDy(-this._modelBounds.size.width/6, 0));
			poles.push(newPoint.asComplex());
			this.denominator = ComplexPoly.fromZeroes(poles);
		}
	}

	removePole(): void {
		this._commitConstantTerm();
		if (this._denominator.degree > 1) {
			const poles = this.denominator.computeZeroes();
			poles.pop();
			this.denominator = ComplexPoly.fromZeroes(poles);
		} else if (this.denominator.degree > 0) {
			this.denominator = ComplexPoly.one();
		}
	}

	invert(): void {
		this._commitConstantTerm();
		const oldNumerator = this.numerator;
		const oldDenominator = this.denominator;
		this.numerator = oldDenominator;
		this.denominator = oldNumerator;
	}

	generateClockTick(): () => void {
		if (this._needsRedraw || this._redrawCounter <= 0) {
			this.redraw();
		} else {
			this._redrawCounter--;
		}
		return () => {
			requestAnimationFrame(this.generateClockTick());
		};
	}

	redraw(): void {
		this._needsRedraw = false;
		this._redrawCounter = 1000;
		//this._recomputeRenderedFunction();
		const phaseCoeff = Complex.fromPolar(1, 2 * Math.PI * this.phaseOffset);
		const numerator = this._numerator.copy();
		const denominator = this._denominator.copy();
		const offset = this._denominator.copy().iTimesComplex(this._constantTerm);
		const correctedNum = normalizedLeadingCoeff(numerator.iPlus(offset));

		const params = this.renderParams;
		params.numerator = correctedNum.iTimesComplex(phaseCoeff);
		params.denominator = denominator;

		//this._renderer.bindParams(params);
		this._renderer.render(params);
	}

	// merges the (scalar) constant term into the numerator and resets
	// it to zero.
	_commitConstantTerm(): void {
		if (isNaN(this.constantTerm.x) || isNaN(this.constantTerm.y)) {
			this.constantTerm = Complex.zero();
		}
		if (this.constantTerm.x != 0 || this.constantTerm.y != 0) {
			const offset =
				this._denominator.copy().iTimesComplex(this.constantTerm);
			this.numerator = normalizedLeadingCoeff(this._numerator.copy().iPlus(offset));
			this.constantTerm = Complex.zero();
		}
	}

	_createControlInterfaces(): void {
		const canvas = this._renderTarget.canvas;
		function nearestCoordsIndex(
			z: Complex,
			coordsList: Array<ComplexSingularity>,
			maxValidDistance: number
		): number {
			let nearest = null;
			let bestDistance = 0;
			const targetCoords = Coords2d.fromComplex(z);
			for (let i = 0; i < coordsList.length; i++) {
				const coords = Coords2d.fromComplex(coordsList[i].position);
				const dist = targetCoords.distanceFrom(coords);
				if (nearest == null || dist < bestDistance) {
					nearest = i;
					bestDistance = dist;
				}
			}
			if (nearest == null || bestDistance > maxValidDistance) {
				return -1;
			}
			return nearest;
		}


		this._controlPointsInterface = new CallbackPointsInterface(
			canvas,
			(canvasCoords: Coords2d): PointsUpdateCallback | null => {
				const transform = transformForBounds(
					this._renderTarget.bounds, this.modelBounds);
				const modelCoords =
					transform.transformCoords(canvasCoords);
				const z = modelCoords.asComplex();
				if (this.controlMode == "singularities") {
					// if we are switching modes from editing constant term to
					// editing singularities, add the constant term back into
					// the numerator so we can start from the new zeroes.
					this._commitConstantTerm();
					const f = Meromorphic.fromNumeratorAndDenominator(
						this._numerator, this._denominator);
					const singularities = f.singularities();
					// Get the nearest singularity index, if any.
					const nearest = nearestCoordsIndex(
						z, singularities, transform.isoScale() * 30);
					if (nearest < 0) {
						return null;
					}
	
					const position: Complex = singularities[nearest].position;
					const initialPosition: Complex = position;
					console.log(
						`Given input point ${String(z)} ` +
						`chose singularity ${nearest}: ${String(initialPosition)}`);
					// Track the mouse to make its position zero by moving the
					// chosen zero independently of the others.
					return (newCanvasCoords: Coords2d): void => {
						const canvasDelta =
							newCanvasCoords.asOffsetFrom(canvasCoords);
						const delta = transform.transformOffset(canvasDelta);
						singularities[nearest].position =
							initialPosition.plus(delta.asComplex());
						const newF = Meromorphic.fromSingularities(singularities);
						this._numerator = newF.numerator;
						this._denominator = newF.denominator;
						this._needsRedraw = true;
					};
				} else if (this.controlMode == "constant") {
					this.setConstantTermFromPosition(z);
					return (newCanvasCoords: Coords2d): void => {
						const newModelCoords = transform.transformCoords(newCanvasCoords);
						const newZ = newModelCoords.asComplex();
						this.setConstantTermFromPosition(newZ);
					};
				} else if (this.controlMode == "numConstant") {
					this.setNumConstantTermFromPosition(z);
					return (newCanvasCoords: Coords2d): void => {
						const newModelCoords = transform.transformCoords(newCanvasCoords);
						const newZ = newModelCoords.asComplex();
						this.setNumConstantTermFromPosition(newZ);
					};
				} else {	// controlMode == "pan"
					console.log("pan");
					const originalBounds = this.modelBounds;
					return (newCanvasCoords: Coords2d): void => {
						const newModelCoords = transform.transformCoords(newCanvasCoords);
						const delta = newModelCoords.asOffsetFrom(modelCoords);
						const newBounds = AxisRect.fromOriginAndSize(
							originalBounds.origin.plus(delta.timesReal(-1)),
							originalBounds.size);
						this.modelBounds = newBounds;
					};
				}
			});
	}

	setConstantTermFromPosition(z: Complex): void {
		const num = this._numerator.evaluateAt(z);
		const den = this._denominator.evaluateAt(z);
		const fz = num.dividedBy(den);
		if (!isNaN(fz.x) && !isNaN(fz.y)) {
			this.constantTerm = fz.negate();
		}
	}

	setNumConstantTermFromPosition(z: Complex): void {
		this._commitConstantTerm();
		const num = this._numerator.evaluateAt(z);
		this.numerator = this.numerator.iPlusComplex(num.negate());
	}
}

function normalizedLeadingCoeff(p: ComplexPoly): ComplexPoly {
	const c = p.leadingCoefficient();
	if (c.x * c.x + c.y * c.y > 0) {
		return p.copy().iTimesComplex(c.inverse());
	}
	return p;
}

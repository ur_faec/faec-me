

import { Canvas } from "core/canvas";
import { Complex } from "core/complex";
import { PlaneShaderRenderer, PlaneShaderRendererParams }
	from "core/plane_shader_renderer";
import { AllShaders } from "core/shaders/all_shaders";

export class BilliardsPhaseRendererParams extends PlaneShaderRendererParams {

	hDelta: Array<Complex>;
	vDelta: Array<Complex>;
	center: Array<Complex>;

	constructor() {
		super();
		this.hDelta = [Complex.one(), Complex.zero()];
		this.vDelta = [Complex.zero(), Complex.one()];
		this.center = [Complex.zero(), Complex.zero()];
	}
}

// A Renderer for low-degree complex rational functions.
export class BilliardsPhaseRenderer
	extends PlaneShaderRenderer<BilliardsPhaseRendererParams>
{
	_paletteFragShader: string;

	constructor(canvas: Canvas, paletteFragShader: string) {
		const vertexShader = AllShaders.vertex.identity;
		const fragmentShader =
			AllShaders.fragment.header
			+ AllShaders.fragment.d2complex
			+ AllShaders.fragment.billiardsPhase
			+ AllShaders.fragment.d2GridOverlay
			+ paletteFragShader;
		super(canvas, vertexShader, fragmentShader);
		this._paletteFragShader = "";
	}

	willRender(params: BilliardsPhaseRendererParams): void {
		super.willRender(params);

		const gl = this._gl;
		const shaderProgram = this._shaderProgram;
		if (gl == null || shaderProgram == null) {
			return;
		}

		const dxdh = params.hDelta[0];
		const dydh = params.hDelta[1];
		const dxdv = params.vDelta[0];
		const dydv = params.vDelta[1];

		const uDxDh = gl.getUniformLocation(shaderProgram, "uDxDh");
		const uDyDh = gl.getUniformLocation(shaderProgram, "uDyDh");
		const uDxDv = gl.getUniformLocation(shaderProgram, "uDxDv");
		const uDyDv = gl.getUniformLocation(shaderProgram, "uDyDv");
		gl.uniform2fv(uDxDh, dxdh.asArray());
		gl.uniform2fv(uDyDh, dydh.asArray());
		gl.uniform2fv(uDxDv, dxdv.asArray());
		gl.uniform2fv(uDyDv, dydv.asArray());

		const centerX = params.center[0];
		const centerY = params.center[1];
		const uCenterX = gl.getUniformLocation(shaderProgram, "uCenterX");
		const uCenterY = gl.getUniformLocation(shaderProgram, "uCenterY");
		gl.uniform2fv(uCenterX, centerX.asArray());
		gl.uniform2fv(uCenterY, centerY.asArray());
		/*
		var numerator = params.numerator;
		var uNumeratorDegree =
				gl.getUniformLocation(shaderProgram, "uNumeratorDegree");
		gl.uniform1i(uNumeratorDegree, numerator.degree);
		var uNumeratorCoeffs =
				gl.getUniformLocation(shaderProgram, "uNumeratorCoeffs");
		var c = numerator.rawCoeffs();
		gl.uniform2fv(uNumeratorCoeffs, c);
		var denominator = params.denominator;
		var uDenominatorDegree =
			gl.getUniformLocation(shaderProgram, "uDenominatorDegree");
		gl.uniform1i(uDenominatorDegree, denominator.degree);
		var uDenominatorCoeffs =
			gl.getUniformLocation(shaderProgram, "uDenominatorCoeffs");
		c = denominator.rawCoeffs();
		gl.uniform2fv(uDenominatorCoeffs, c);*/
	}
}
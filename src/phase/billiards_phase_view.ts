/*import { CallbackPointsInterface } from "core/callback_points_interface";
import { RenderTarget } from "core/canvas";
import { Complex } from "core/complex";
import { IsoScaleTransform } from "core/coordinate_transform";
import { Coords2d } from "core/geometry";
import { AxisRect } from "core/polygon";
import { AllShaders } from "core/shaders/all_shaders";

import { BilliardsPhaseRenderer, BilliardsPhaseRendererParams }
	from "./billiards_phase_render";

//var _paletteShader = AllShaders.fragment.hsvSplit6;
const _paletteShader = AllShaders.fragment.hsvDefault;
//var _paletteShader = AllShaders.fragment.hsvDouble;

export class BilliardsPhaseView {

	renderParams: BilliardsPhaseRendererParams;
	_renderTarget: RenderTarget;
	_renderer: BilliardsPhaseRenderer;

	_modelBounds: AxisRect;
	_coordinateTransform: IsoScaleTransform;
	_needsRedraw: boolean;
	_redrawCounter: number;

	_controlPointsInterface: CallbackPointsInterface | null | undefined;

	_phaseOffset: number;
	_offsetCoords: Coords2d;

	constructor(renderTarget: RenderTarget) {
		this._renderTarget = renderTarget;
		this.renderParams = new BilliardsPhaseRendererParams();
		this._renderer = new BilliardsPhaseRenderer(
			renderTarget.canvas, _paletteShader);

		this._redrawCounter = 1000;
		this._needsRedraw = false;
		this._controlPointsInterface = null;

		// Exposed properties
		this.phaseOffset = 0;
		this.modelBounds = AxisRect.fromXYWH(-1, -1, 2, 2);
		this._renderer.renderFrom(this.modelBounds);

		// The coordinates constantOffset was derived from.
		this._offsetCoords = Coords2d.fromXY(0, 0);

		this._createControlInterfaces();

		requestAnimationFrame(this.generateClockTick());
	}

	// Sets the angle of each axis in its parameter's complex plane.
	// xAngle and yAngle are in radians and should usually vary from
	// 0 - pi/2.
	setAxisAngles(xAngle: number, yAngle: number) {
		this.renderParams.hDelta =
			[Complex.fromPolar(1, xAngle), Complex.zero()];
		this.renderParams.vDelta =
			[Complex.zero(), Complex.fromPolar(1, yAngle)];
		this._needsRedraw = true;
	}

	setCenter(centerX: Complex, centerY: Complex) {
		this.renderParams.center = [centerX, centerY];
		this._needsRedraw = true;
	}

	// A number in the range 0-1. The real function is multiplied by
	// e^{2 * pi * phaseOffset]}``
	get phaseOffset(): number {return this._phaseOffset;}
	set phaseOffset(offset: number) {
		this._phaseOffset = offset;
		this._needsRedraw = true;
	}

	get modelBounds(): AxisRect {
		return this._modelBounds;
	}
	set modelBounds(modelBounds: AxisRect) {
		const transform = IsoScaleTransform.forCenteredAxisRect(
			this._renderTarget.bounds, modelBounds, false, true);
		this._coordinateTransform = transform;
		this._modelBounds = this._renderTarget.bounds.applyTransform(transform);

		// This is a workaround because most shader renderers work by imposing
		// the model coordinate space on the space by setting the texture coords
		// of the corners of the view (and so use an AxisRect as the underlying
		// representation), but this one handles translations by passing the
		// center in as a parameter instead, which is why we renderFrom the
		// origin regardless. This is basically because our existing patterns
		// all assume a 2-dimensional ambient space and we don't have a good API
		// for higher dimensions... so we should fix that soon.
		const size = this._modelBounds.size;
		this._renderer.renderFrom(
			AxisRect.fromCenterAndDimensions(
				Coords2d.origin(), size.width, size.height));

		this._needsRedraw = true;
	}

	generateClockTick() {
		if (this._needsRedraw || this._redrawCounter <= 0) {
			this.redraw();
		} else {
			this._redrawCounter--;
		}
		return () => {
			requestAnimationFrame(this.generateClockTick());
		};
	}

	redraw() {
		this._needsRedraw = false;
		this._redrawCounter = 10000;

		const params = this.renderParams;

		this._renderer.render(params);
	}

	_createControlInterfaces() {

	}
}
*/
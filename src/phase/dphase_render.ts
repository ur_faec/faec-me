

import { Canvas } from "core/canvas";
import { ComplexPoly } from "core/complex_poly";
import { PlaneShaderRenderer, PlaneShaderRendererParams }
	from "core/plane_shader_renderer";
import { AllShaders } from "core/shaders/all_shaders";

export class DPhasePlaneShaderRendererParams extends PlaneShaderRendererParams {

	numerator: ComplexPoly;
	denominator: ComplexPoly;

	constructor() {
		super();
		this.numerator = ComplexPoly.fromReal(1);
		this.denominator = ComplexPoly.fromReal(1);
	}
}

// A Renderer for low-degree complex rational functions.
export class DPhasePlaneShaderRenderer
	extends PlaneShaderRenderer<DPhasePlaneShaderRendererParams>
{
	constructor(canvas: Canvas, paletteFragShader: string) {
		const vertexShader = AllShaders.vertex.identity;
		const fragmentShader =
			AllShaders.fragment.phaseCoeffs
			+ AllShaders.fragment.gridOverlay
			+ paletteFragShader;
		super(canvas, vertexShader, fragmentShader);
	}

	willRender(params: DPhasePlaneShaderRendererParams): void {
		super.willRender(params);

		const gl = this._gl;
		const shaderProgram = this._shaderProgram;
		if (gl == null || shaderProgram == null) {
			return;
		}

		const numerator = params.numerator;
		const uNumeratorDegree =
			gl.getUniformLocation(shaderProgram, "uNumeratorDegree");
		gl.uniform1i(uNumeratorDegree, numerator.degree);
		const uNumeratorCoeffs =
			gl.getUniformLocation(shaderProgram, "uNumeratorCoeffs");
		let c = numerator.rawCoeffs();
		gl.uniform2fv(uNumeratorCoeffs, c);

		const denominator = params.denominator;
		const uDenominatorDegree =
			gl.getUniformLocation(shaderProgram, "uDenominatorDegree");
		gl.uniform1i(uDenominatorDegree, denominator.degree);
		const uDenominatorCoeffs =
			gl.getUniformLocation(shaderProgram, "uDenominatorCoeffs");
		c = denominator.rawCoeffs();
		gl.uniform2fv(uDenominatorCoeffs, c);
	}
}
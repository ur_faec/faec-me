

import { Canvas } from "core/canvas";
import { ComplexPoly } from "core/complex_poly";
import { PlaneShaderRenderer, PlaneShaderRendererParams }
	from "core/plane_shader_renderer";
import { AllShaders } from "core/shaders/all_shaders";

export class ProjectivePhaseRendererParams extends PlaneShaderRendererParams {

	numerator: ComplexPoly;
	denominator: ComplexPoly;

	constructor() {
		super();
		this.numerator = ComplexPoly.fromReal(1);
		this.denominator = ComplexPoly.fromReal(1);
	}
}

// A Renderer for low-degree complex rational functions.
// params: {
//   numerator: (ComplexPoly) The polynomial in the numerator. Defaults to the
//     constant polynomial 1.
//   denominator: (ComplexPoly) The polynomial in the denominator. Defaults to
//     the constant polynomial 1.
// }
export class ProjectivePhaseRenderer
	extends PlaneShaderRenderer<ProjectivePhaseRendererParams>
{

	constructor(canvas: Canvas, paletteFragShader: string) {
		const vertexShader = AllShaders.vertex.identity;
		const fragmentShader =
			AllShaders.fragment.projectivePhase
			+ AllShaders.fragment.gridOverlay
			+ paletteFragShader;
		super(canvas, vertexShader, fragmentShader);
	}

	willRender(params: ProjectivePhaseRendererParams): void {
		super.willRender(params);

		const gl = this._gl;
		const shaderProgram = this._shaderProgram;
		if (gl == null || shaderProgram == null) {
			return;
		}

		const numerator = params.numerator;
		const uNumeratorDegree =
			gl.getUniformLocation(shaderProgram, "uNumeratorDegree");
		gl.uniform1i(uNumeratorDegree, numerator.degree);
		const uNumeratorCoeffs =
			gl.getUniformLocation(shaderProgram, "uNumeratorCoeffs");
		let c = numerator.rawCoeffs();
		gl.uniform2fv(uNumeratorCoeffs, c);

		const denominator = params.denominator;
		const uDenominatorDegree =
			gl.getUniformLocation(shaderProgram, "uDenominatorDegree");
		gl.uniform1i(uDenominatorDegree, denominator.degree);
		const uDenominatorCoeffs =
			gl.getUniformLocation(shaderProgram, "uDenominatorCoeffs");
		c = denominator.rawCoeffs();
		gl.uniform2fv(uDenominatorCoeffs, c);
	}
}
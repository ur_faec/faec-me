

import { ElementsView } from "core/elements_view";
import { Coords2d, OffsetCoords2d } from "core/geometry";
import { AxisRect } from "core/polygon";

import { Line, Renderable } from "core/element_render";

const squareRoot = Math.sqrt(3);
const fourthRoot = Math.sqrt(squareRoot);
// (w, h) are the dimensions of the unit-area equilateral triangle
// with its base aligned with the x axis.
const triWidth = 2 / fourthRoot;
const triHeight = fourthRoot;
const triVerts = [
	Coords2d.origin(),
	Coords2d.fromXY(triWidth, 0),
	Coords2d.fromXY(triWidth / 2, triHeight)];
const triLeft = Coords2d.midpoint(triVerts[0], triVerts[2]);
const triRight = Coords2d.midpoint(triVerts[1], triVerts[2]);

// The horizontal width of the long diagonal, which goes from
// triRight to the base and has length 1.
const diagonalWidth = Math.sqrt(1.0 - triRight.y * triRight.y);
const diagonal = OffsetCoords2d.fromDxDy(diagonalWidth, triRight.y);
const triBase = [
	Coords2d.fromXY(triRight.x - diagonalWidth, 0),
	Coords2d.fromXY((triRight.x - diagonalWidth) * 2, 0),
	Coords2d.fromXY(triRight.x - diagonalWidth + triWidth / 2, 0)];

// The offset of the center base point from the geometric center
// of the edge.
const centerOffset = 2 * triBase[0].x - triWidth / 2;

const innerCoeff = [
	triLeft.asOffsetFrom(triBase[0]).dot(diagonal),
	triBase[2].asOffsetFrom(triBase[0]).dot(diagonal)];

//console.log(
//  `innerCoeffs: ${innerCoeff[0]}, ${innerCoeff[1]}, ` +
//  `${innerCoeff[0] + innerCoeff[1]}`);

const triInner = [
	triBase[0].plus(diagonal.timesReal(innerCoeff[0])),
	triBase[0].plus(diagonal.timesReal(innerCoeff[1]))];
//console.log(`Inner left cut: ${triInner[0].distanceFrom(triLeft)}`);

// The double-covered rect dimensions
const rectHeight = triHeight / 2;
const rectWidth = triWidth / 2;
//let diagonal = OffsetCoords2d.fromDxDy(1.0 / fourthRoot, fourthRoot / 2.0);
//console.log(`Diagonal: ${diagonal.toString()}`);
//console.log(`Diagonal length: ${diagonal.length()}`);
const squaredLength = diagonal.squaredLength();
//let unitDiagonal = diagonal.copy().normalize();
const innerCoeffs = [
	rectHeight * diagonal.dy / squaredLength,
	rectWidth * diagonal.dx / squaredLength];
console.log(`inner comparison: ${innerCoeffs[0]}, ${1 - innerCoeffs[1]}`);

const cap = [triBase[0], triBase[2], triInner[1]];

const capLeft = cap[0].distanceFrom(cap[2]);
const capRight = cap[1].distanceFrom(cap[2]);
//let capBase = tri[0].distanceFrom(tri[1]);
console.log(`Cap left, right: ${capLeft}, ${capRight}`);

export function addLinesForTriangle(
	elements: Array<Renderable>, origin: Coords2d, sign: number
): void {
	const v0 = origin;
	const v1 = Coords2d.fromXY(origin.x + sign * triWidth, origin.y);
	const v2 = Coords2d.fromXY(
		origin.x + sign * triWidth / 2,
		origin.y + sign * triHeight);
	elements.push(new Line(v0, v1));
	elements.push(new Line(v1, v2));
	elements.push(new Line(v2, v0));

	elements.push(new Line(
		origin.plus(triBase[0].asOffsetFromOrigin().timesReal(sign)),
		origin.plus(triRight.asOffsetFromOrigin().timesReal(sign))));

	const inner = [
		triInner[0].asOffsetFromOrigin(),
		triInner[1].asOffsetFromOrigin()];
	elements.push(new Line(
		origin.plus(triLeft.asOffsetFromOrigin().timesReal(sign)),
		origin.plus(inner[0].timesReal(sign))));

	elements.push(new Line(
		origin.plus(triBase[2].asOffsetFromOrigin().timesReal(sign)),
		origin.plus(inner[1].timesReal(sign))));
}

export function addLinesForSquare(
	elements: Array<Renderable>, origin: Coords2d, angle: number
): void {
	// Extract the dimensions for the capstone triangle from its
	// simplest orientation in the base of the equilateral triangle.

	const bottom = capLeft;
	const top = 1 - capLeft;

	const sin = Math.sin(angle);
	const cos = Math.cos(angle);
	const basis = [
		OffsetCoords2d.fromDxDy(cos, sin),
		OffsetCoords2d.fromDxDy(-sin, cos)];

	// All six corners in two adjacent grid squares, ordered
	// widdershins from the base.
	const corners = [
		origin, origin.plus(basis[0]),
		origin.plus(basis[0].timesReal(2)),
		origin.plus(basis[0].timesReal(2)).plus(basis[1]),
		origin.plus(basis[0]).plus(basis[1]),
		origin.plus(basis[1])];
	elements.push(new Line(corners[0], corners[2]));
	elements.push(new Line(corners[2], corners[3]));
	elements.push(new Line(corners[3], corners[5]));
	elements.push(new Line(corners[5], corners[0]));
	elements.push(new Line(corners[1], corners[4]));

	const vTop = [
		corners[5].plus(basis[0].timesReal(top)),
		corners[3].plus(basis[0].timesReal(-bottom))];
	const vBottom = [
		corners[0].plus(basis[0].timesReal(bottom)),
		corners[2].plus(basis[0].timesReal(-top))];
	const vLeft = corners[0].plus(basis[1].timesReal(0.5));
	const vCenter = corners[1].plus(basis[1].timesReal(0.5));
	const vRight = corners[2].plus(basis[1].timesReal(0.5));

	const innerVecs = [
		vLeft.asOffsetFrom(vBottom[0]),
		vRight.asOffsetFrom(vTop[1])];
	const innerCoeff = triBase[0].x / (triWidth / 2);
	const vInner = [
		vBottom[0].plus(innerVecs[0].timesReal(innerCoeff)),
		vTop[1].plus(innerVecs[1].timesReal(innerCoeff))];

	elements.push(new Line(vLeft, vBottom[0]));
	elements.push(new Line(vInner[0], vCenter));
	elements.push(new Line(vInner[0], vTop[0]));

	elements.push(new Line(vTop[1], vRight));
	elements.push(new Line(vInner[1], vCenter));
	elements.push(new Line(vInner[1], vBottom[1]));

	const vOutsideLeft = corners[5].plus(basis[0].timesReal(-capLeft));
	elements.push(new Line(vOutsideLeft, vLeft));

	const vOutsideRight = corners[2].plus(basis[0].timesReal(capLeft));
	elements.push(new Line(vRight, vOutsideRight));

	console.log(`Horizontal shift: ${1 - top}`);
}

export class DudeneyTilingModel extends ElementsView {


	elementsToRender(modelFrame: AxisRect): Array<Renderable> {
		const horizontalShift = true;
		const trueCenter = false;
		const strokeStyle = '#ff33ee';
		const lineWidth = '2';

		const elements: Array<Renderable> = [];
		console.log("elementsToRender");
		let xOffset = 0;
		if (trueCenter) {
			xOffset += centerOffset;
		}
		if (horizontalShift) {
			xOffset += triWidth / 2;
		}
		for (let i = -10; i <= 10; i++) {
			const origin = Coords2d.fromXY(i * triWidth, 1.5);
			addLinesForTriangle(
				elements, Coords2d.fromXY(origin.x, origin.y), 1);
			addLinesForTriangle(elements,
				Coords2d.fromXY(
					origin.x + triWidth / 2,
					origin.y + triHeight), -1);

			addLinesForTriangle(elements,
				Coords2d.fromXY(origin.x + xOffset, origin.y - triHeight), 1);
			addLinesForTriangle(elements,
				Coords2d.fromXY(
					origin.x + triWidth / 2 + xOffset,
					origin.y),
				-1);
		}

		const rowShift = 0.5721554288609622;
		// The angle of the fundamental interval we want to map to the
		// x axis
		const angle = Math.atan2(-1, rowShift);
		// The distance of each cell along the x axis
		const dx = Math.sqrt(1 + rowShift * rowShift);
		for (let i = -10; i <= 10; i++) {
			const origin = Coords2d.fromXY(i * dx, -3);
			addLinesForSquare(elements, origin, -angle);
			/*for (let j = 0; j < 3; j++) {
        addLinesForSquare(elements,
          Coords2d.fromXY(origin.x + rowShift * j, origin.y - j),
          0);//-angle);
      }*/
		}

		for (let i = 0; i < elements.length; i++) {
			const e = elements[i];
			e.style.strokeStyle = strokeStyle;
			e.style.lineWidth = lineWidth;
		}
		return elements;
	}

}
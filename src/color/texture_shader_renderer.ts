

import { Canvas } from "core/canvas";
import { AllShaders } from "core/shaders/all_shaders";

import { ColorOpp } from "./color";

type WebGLData = {
	gl: WebGLRenderingContext;
	
	shaderProgram: WebGLProgram;
	vertexCoordsBuffer: WebGLBuffer;
	textureCoordsBuffer: WebGLBuffer;
	vertexIndexBuffer: WebGLBuffer;

	vertexPositionAttribute: number;
	textureCoordAttribute: number;

	texture: WebGLTexture;
	colorTable: WebGLTexture;

	textureCoordsArray: Float32Array;
}

export class TextureShaderRenderer {

	canvas: Canvas;
	targetColor: ColorOpp;
	inputColor: ColorOpp;

	_glData: WebGLData | null;

	constructor(canvas: Canvas, vertexCode: string | null, fragmentCode: string | null) {
		this.canvas = canvas;
		this.targetColor = new ColorOpp(1, 0, -1);
		this.inputColor = new ColorOpp(0, 1, 0);
		this._glData = WebGLDataForCanvas(
			canvas,
			vertexCode || AllShaders.vertex.identity,
			fragmentCode || AllShaders.fragment.textureBlob);
	}

	willRender(): void {
		const glData = this._glData;
		if (glData == null) {
			return;
		}
		const gl = glData.gl;
		const shaderProgram = glData.shaderProgram;

		gl.bindBuffer(gl.ARRAY_BUFFER, glData.vertexCoordsBuffer);
		gl.vertexAttribPointer(glData.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);
		gl.bindBuffer(gl.ARRAY_BUFFER, glData.textureCoordsBuffer);
		gl.vertexAttribPointer(glData.textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, glData.texture);

		gl.activeTexture(gl.TEXTURE1);
		gl.bindTexture(gl.TEXTURE_2D, glData.colorTable);

		const uTexture = gl.getUniformLocation(shaderProgram, "uTexture");
		gl.uniform1i(uTexture, 0);
		const uColorTable = gl.getUniformLocation(shaderProgram, "uColorTable");
		gl.uniform1i(uColorTable, 1);
		const uTargetColor = gl.getUniformLocation(shaderProgram, "uTargetColor");
		gl.uniform3f(uTargetColor, this.targetColor.value, this.targetColor.redGreen, this.targetColor.blueYellow);
		const uInputColor = gl.getUniformLocation(shaderProgram, "uInputColor");
		gl.uniform3f(uInputColor, this.inputColor.value, this.inputColor.redGreen, this.inputColor.blueYellow);
		/*var uColorMod = gl.getUniformLocation(shaderProgram, "uColorMod");
		gl.uniform2f(uColorMod, this.colorMod0, this.colorMod1);*/
	}

	render(): void {
		const glData = this._glData;
		if (glData == null) {
			return;
		}
		this.willRender();
		const gl = glData.gl;
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, glData.vertexIndexBuffer);
		gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
	}
}

function WebGLDataForCanvas(
	canvas: Canvas, vertexCode: string, fragmentCode: string
): WebGLData | null {
	const gl = canvas.element.getContext("webgl");// || canvas.element.getContext("experimental-webgl");
	if (gl == null) {
		return null;
	}
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // Clear to black, fully opaque
	gl.clearDepth(1.0); // Clear everything
	const texture = CreateTexture(gl, canvas.size.width, canvas.size.height);
	if (texture == null) {
		return null;
	}
	const colorTable = CreateTexture(gl, canvas.size.width, canvas.size.height);
	if (colorTable == null) {
		return null;
	}
	// We're rendering planar data to a flat rect, so the
	// vertices never change, and represent the edges of the canvas --
	// just draw two static triangles between the corners.
	const vertices = [-1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0];
	const vertexIndices = [0, 1, 2, 0, 2, 3];
	const vertexCoordsBuffer = gl.createBuffer();
	if (vertexCoordsBuffer == null) {
		return null;
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, vertexCoordsBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	const vertexIndexBuffer = gl.createBuffer();
	if (vertexIndexBuffer == null) {
		return null;
	}
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);

	const textureCoordsBuffer = gl.createBuffer();
	if (textureCoordsBuffer == null) {
		return null;
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordsBuffer);
	const textureCoordinates = [0, 0, 1, 0, 1, 1, 0, 1];
	const textureCoordsArray = new Float32Array(textureCoordinates);
	if (textureCoordsArray == null) {
		return null;
	}
	gl.bufferData(gl.ARRAY_BUFFER, textureCoordsArray, gl.STATIC_DRAW);
	
	const shaderProgram = CreateShaderProgram(gl, vertexCode, fragmentCode);
	if (shaderProgram == null) {
		return null;
	}
	gl.useProgram(shaderProgram);
	// Shaders using this renderer must contain these uniforms.
	const vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	gl.enableVertexAttribArray(vertexPositionAttribute);
	const textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
	gl.enableVertexAttribArray(textureCoordAttribute);

	return {
		gl, shaderProgram,
		vertexCoordsBuffer, textureCoordsBuffer, vertexIndexBuffer,
		vertexPositionAttribute, textureCoordAttribute,
		texture, colorTable,
		textureCoordsArray,
	};
}

function CreateShaderProgram(
	gl: WebGLRenderingContext, vertexCode: string, fragmentCode: string
): WebGLProgram | null {
	console.log("_createShaderProgram");
	const fragmentShader = CompileShader(gl, fragmentCode, gl.FRAGMENT_SHADER);
	const vertexShader = CompileShader(gl, vertexCode, gl.VERTEX_SHADER);
	if (fragmentShader == null || vertexShader == null) {
		return null;
	}

	const shaderProgram = gl.createProgram();
	if (shaderProgram == null) {
		return null;
	}
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);
	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		return null;
	}

	return shaderProgram;
}

// type should be gl.FRAGMENT_SHADER or gl.VERTEX_SHADER
function CompileShader(
	gl: WebGLRenderingContext, text: string, type: number
): WebGLShader | null {
	const shader = gl.createShader(type);
	if (shader == null) {
		return null;
	}
	gl.shaderSource(shader, text);
	gl.compileShader(shader);
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		// TODO: handle this error.
		console.log("An error occurred compiling the shaders: ", gl.getShaderInfoLog(shader));
		return null;
	}
	return shader;
}

function CreateTexture(
	gl: WebGLRenderingContext, width: number, height: number
): WebGLTexture | null {
	const textureData = new Uint8Array(width * height * 4);
	textureData.fill(0);
	const yCoeff = 3 * 2 * Math.PI / (height - 1);
	const xCoeff = 5 * 2 * Math.PI / (width - 1);
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			const wave = Math.sin(xCoeff * x) + Math.sin(yCoeff * y);
			const intensity = (wave + 2) / 4;
			const offset = y * width + x;
			textureData[offset * 4] = Math.round(255 * intensity);
			//Math.round(x * 255 / (width - 1));
			textureData[offset * 4 + 2] = Math.round(y * 255 / (height - 1));
			textureData[offset * 4 + 3] = 255;
		}
	}

	const texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texture);
	const level = 0;
	const internalFormat = gl.RGBA;
	const border = 0;
	const srcFormat = gl.RGBA;
	const srcType = gl.UNSIGNED_BYTE;
	gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, width, height, border, srcFormat, srcType, textureData);

	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

	return texture;
}



import { Canvas } from "core/canvas";

import { ColorOpp } from "./color";
import { TextureShaderRenderer } from "./texture_shader_renderer";

export class ColorBlobModel {

	_renderer: TextureShaderRenderer;

	_targetColor: ColorOpp;
	_colorMod0: number = 0;
	_colorMod1: number = 0;

	_needsRedraw: boolean;
	_redrawCounter: number;

	constructor(canvas: Canvas, targetColor: ColorOpp) {
		this._renderer = new TextureShaderRenderer(canvas, null, null);
		this._redrawCounter = 1000;
		this._targetColor = targetColor;//new ColorOpp(1, 0, -1);
		this._needsRedraw = true;
		requestAnimationFrame(this.generateClockTick(true));
	}

	generateClockTick(firstTime: boolean): () => void {
		// first time is from the constructor, and shouldn't do any rendering.
		if (this._needsRedraw && !firstTime) {
			this.redraw();
		} else {
			this._redrawCounter--;
		}
		return () => {
			requestAnimationFrame(this.generateClockTick(false));
		};
	}

	get targetColor(): ColorOpp {
		return this._targetColor;
	}

	set targetColor(color: ColorOpp) {
		this._targetColor = color;
	}

	get colorMod0(): number {
		return this._colorMod0;
	}
	set colorMod0(value: number) {
		this._colorMod0 = value;
		this._needsRedraw = true;
	}

	get colorMod1(): number {
		return this._colorMod1;
	}
	set colorMod1(value: number) {
		this._colorMod1 = value;
		this._needsRedraw = true;
	}

	redraw(): void {
		this._needsRedraw = false;
		this._redrawCounter = 1000;
		this._renderer.targetColor = this.targetColor;
		this._renderer.inputColor = new ColorOpp(this.targetColor.value, this.targetColor.redGreen + 0.1 * this.colorMod0, this.targetColor.blueYellow + 0.1 * this.colorMod1);
		this._renderer.render();
	}
}
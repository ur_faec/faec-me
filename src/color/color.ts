

// eslint-disable-next-line max-len
// Conversion formulas from https://www.image-engineering.de/library/technotes/958-how-to-convert-between-srgb-and-ciexyz
// Warning that the formula for delinearizedBrightness on that page seems to
// be buggy.

function linearizeBrightness(v: number) {
	if (v <= 0.04045) {
		return v / 12.92;
	}
	return Math.pow((v + 0.055) / 1.055, 2.4);
}

function delinearizeBrightness(v: number) {
	if (v <= 0.003131308) {
		return v * 12.92;
	}
	return Math.pow(v, 1.0 / 2.4) * 1.055 - 0.055;
}

export class ColorRGB {

	r: number;
	g: number;
	b: number;

	constructor(r: number, g: number, b: number) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	asXYZ(): ColorXYZ {
		const lr = linearizeBrightness(this.r);
		const lg = linearizeBrightness(this.g);
		const lb = linearizeBrightness(this.b);

		return new ColorXYZ(0.4124564 * lr + 0.3575761 * lg + 0.1804375 * lb, 0.2126729 * lr + 0.7151522 * lg + 0.0721750 * lb, 0.0193339 * lr + 0.1191920 * lg + 0.9503041 * lb);
	}

	brightness(): number {
		const lr = linearizeBrightness(this.r);
		const lg = linearizeBrightness(this.g);
		const lb = linearizeBrightness(this.b);
		return delinearizeBrightness(0.2126729 * lr + 0.7151522 * lg + 0.0721750 * lb);
	}
}

export class ColorXYZ {

	x: number;
	y: number;
	z: number;

	constructor(x: number, y: number, z: number) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	asRGB(): ColorRGB {
		const lr = 3.2404542 * this.x - 1.5371385 * this.y - 0.4985314 * this.z;
		const lg = -0.9692660 * this.x + 1.8760108 * this.y + 0.0415560 * this.z;
		const lb = 0.0556434 * this.x - 0.2040259 * this.y + 1.0572252 * this.z;
		return new ColorRGB(delinearizeBrightness(lr), delinearizeBrightness(lg), delinearizeBrightness(lb));
	}

	brightness(): number {
		return this.y;
	}

	scaledBy(scale: number): ColorXYZ {
		return new ColorXYZ(scale * this.x, scale * this.y, scale * this.z);
	}
}


// Representation of a color as opponent channels (value, redGreen, blueYellow)
// derived from sRGB.
// value is the coefficient of the raw hue in the full color:
//   value = red + green + blue
// redGreen and blueYellow together determine the hue and saturation:
//   redGreen = (red - green) / value
//   blueYellow = [blue - (red + green) / 2] / value
// The rgb values can be recovered by inverting these relations:
//   red = (redGreen/2 - blueYellow/3 + 1/3) * value
//   green = (-redGreen/2 - blueYellow/3 + 1/3) * value
//   blue = (2 blueYellow / 3 + 1/3) * value
export class ColorOpp {

	value: number;
	redGreen: number;
	blueYellow: number;

	constructor(value: number, redGreen: number, blueYellow: number) {
		this.value = value;
		this.redGreen = redGreen;
		this.blueYellow = blueYellow;
	}

	static fromRGB(red: number, green: number, blue: number): ColorOpp {
		const value = red + green + blue;
		const redGreen = (red - green) / value;
		const blueYellow = (blue - (red + green) / 2) / value;
		return new ColorOpp(value, redGreen, blueYellow);
	}

	linearGradientTo(c: ColorOpp): (arg0: number) => ColorOpp {
		return (coeff: number) => {
			const value = (1 - coeff) * this.value + coeff * c.value;
			const redGreen = (1 - coeff) * this.redGreen + coeff * c.redGreen;
			const blueYellow = (1 - coeff) * this.blueYellow + coeff * c.blueYellow;
			return new ColorOpp(value, redGreen, blueYellow);
		};
	}
}
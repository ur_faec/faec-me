

import { Complex, TangentComplex, ComplexVector } from "core/complex";

const EPSILON = 0.00000001;

function _reduceDegree(v: ComplexVector, degree: number): number {
	degree = Math.min(degree, v.length - 1);
	while (degree > 0) {
		if (Math.abs(v.array[degree * 2]) >= EPSILON || Math.abs(v.array[degree * 2 + 1]) >= EPSILON) {
			break;
		}
		degree--;
	}
	return degree;
}

export class ComplexPoly {

	degree: number;
	v: ComplexVector;

	_numerator: ComplexPoly | null | undefined;
	_denominator: ComplexPoly | null | undefined;
	_remainder: ComplexPoly | null | undefined;

	// Accepts a list of Complex objects, an existing ComplexVector,
	// or a flattened list of 2*length raw real coefficients.
	constructor(v: ComplexVector, degree: number) {
		this.degree = degree;
		this.v = v;
		this.v.length = degree + 1;
	}

	copy(): ComplexPoly {
		return new ComplexPoly(this.v.copy(), this.degree);
	}

	static zero(): ComplexPoly {
		return new ComplexPoly(ComplexVector.fromFlatList([0, 0]), 0);
	}

	static one(): ComplexPoly {
		return new ComplexPoly(ComplexVector.fromFlatList([1, 0]), 0);
	}

	// coeffs must be non-null and of length >= 1.
	static fromComplexCoeffs(coeffs: Array<Complex>): ComplexPoly {
		const v = ComplexVector.fromComplexList(coeffs);
		return new ComplexPoly(v, _reduceDegree(v, v.length - 1));
	}

	static fromRealCoeffs(coeffs: Array<number>): ComplexPoly {
		const complexCoeffs = coeffs.map(r => Complex.fromReal(r));
		return ComplexPoly.fromComplexCoeffs(complexCoeffs);
	}

	static fromComplex(z: Complex): ComplexPoly {
		const v = ComplexVector.fromComplexList([z]);
		return new ComplexPoly(v, 0);
	}

	static fromReal(r: number): ComplexPoly {
		const v = ComplexVector.fromFlatList([r, 0]);
		return new ComplexPoly(v, 0);
	}

	static unitTermOfDegree(degree: number): ComplexPoly {
		const v = ComplexVector.fromLength(degree + 1);
		v.set(degree, Complex.fromReal(1));
		return new ComplexPoly(v, degree);
	}

	static fromZeroes(zeroes: Array<Complex>): ComplexPoly {
		const total = ComplexPoly.fromReal(1);
		for (const z of zeroes) {
			total.iTimes(ComplexPoly.unitTermOfDegree(1).iMinusComplex(z));
		}
		return total;
	}

	// Returns the complex coeff as a flattened array of 2*degree numbers,
	// primarily for use by webgl renderers.
	rawCoeffs(): number[] {
		const slice = this.v.array.slice(0, (this.degree + 1) * 2);
		return slice.reduce(
			(a: number[], b: number): number[] => {
				a.push(b);
				return a;
			}, []);
	}

	coeffs(): Complex[] {
		const c = [];
		for (let i = 0; i <= this.degree; i++) {
			c.push(this.coefficientOfDegree(i));
		}
		return c;
	}

	coefficientOfDegree(degree: number): Complex {
		return this.v.get(degree);
	}

	leadingCoefficient(): Complex {
		return this.v.get(this.degree);
	}

	constantTerm(): Complex {
		return this.v.get(0);
	}

	iPlus(p: ComplexPoly): ComplexPoly {
		this.v.plus(p.v);
		this.degree = _reduceDegree(this.v, this.v.length - 1);
		return this;
	}

	iPlusComplex(z: Complex): ComplexPoly {
		this.v.set(0, this.v.get(0).plus(z));
		return this;
	}

	iPlusComplexCoeff(z: Complex, i: number): ComplexPoly {
		this.v.extend(i + 1);
		this.v.set(i, this.v.get(i).plus(z));
		this.degree = _reduceDegree(this.v, Math.max(this.degree, i));
		return this;
	}

	iMinus(p: ComplexPoly): ComplexPoly {
		this.v.minus(p.v);
		this.degree = _reduceDegree(this.v, this.v.length - 1);
		return this;
	}

	iMinusComplex(z: Complex): ComplexPoly {
		this.v.set(0, this.v.get(0).minus(z));
		return this;
	}

	iMinusComplexCoeff(z: Complex, i: number): ComplexPoly {
		this.v.extend(i + 1);
		this.v.set(i, this.v.get(i).minus(z));
		this.degree = _reduceDegree(this.v, Math.max(this.degree, i));
		return this;
	}

	remainder(): ComplexPoly {
		if (this._remainder) {
			return this._remainder;
		}
		return ComplexPoly.fromReal(0);
	}

	// This is some kind of sketchy stuff to support dividedBy inline like other
	// ops while still preserving the most recent information about what was
	// divided, so we can get the quotient, remainder, etc...
	numerator(): ComplexPoly {
		if (this._numerator) {
			return this._numerator;
		}
		return this;
	}

	denominator(): ComplexPoly {
		if (this._denominator) {
			return this._denominator;
		}
		return ComplexPoly.fromReal(1);
	}

	iTimes(p: ComplexPoly): ComplexPoly {
		const total = ComplexVector.fromLength(this.degree + p.degree + 1);
		// The input polynomial coeffs, shifted right by the current degree index.
		const cur = p.v.copy();
		for (let i = 0; i <= this.degree; i++) {
			const ci = this.v.get(i);
			total.plus(cur.copy().timesComplex(ci));
			cur.shift();
		}
		this.v = total;
		// Theoretically, degree reduction should only be needed when one of the
		// polys is zero, but let's just make sure regardless.
		this.degree = _reduceDegree(this.v, total.length - 1);
		return this;
	}

	iTimesComplex(z: Complex): ComplexPoly {
		this.v.timesComplex(z);
		return this;
	}

	// k a positive integer
	iTimesZToThePower(k: number): ComplexPoly {
		this.v.shift(k);
		this.degree += k;
		return this;
	}

	iDividedBy(p: ComplexPoly): ComplexPoly {
		const quotient = ComplexPoly.fromReal(0);
		const remainder = this.copy();
		while (remainder.degree > p.degree) {
			const delta = remainder.degree - p.degree;
			const ratio = remainder.leadingCoefficient().dividedBy(p.leadingCoefficient());
			quotient.iPlusComplexCoeff(ratio, delta);
			remainder.iMinus(p.copy().iTimesZToThePower(delta).iTimesComplex(ratio));
		}
		if (remainder.degree == p.degree) {
			const ratio = remainder.leadingCoefficient().dividedBy(p.leadingCoefficient());
			quotient.iPlusComplex(ratio);
			remainder.iMinus(p.copy().iTimesComplex(ratio));
		}
		this._numerator = this.copy();
		this._denominator = p;
		this._remainder = remainder;
		this.v = quotient.v;
		this.degree = quotient.degree;
		return this;
	}

	iDerivative(): ComplexPoly {
		for (let i = 0; i < this.degree; i++) {
			this.v.set(i, this.v.get(i + 1).times(Complex.fromReal(i + 1)));
		}
		// Necessary for degree-0 to work right, a nice precaution for everything
		// else:
		this.v.array[this.degree * 2] = 0;
		this.v.array[this.degree * 2 + 1] = 0;
		if (this.degree > 0) {
			this.degree -= 1;
		}
		return this;
	}

	iIntegral(constantTerm: Complex): ComplexPoly {
		this.v.shift();
		this.degree += 1;
		for (let i = 1; i <= this.degree; i++) {
			this.v.set(i, this.v.get(i).dividedBy(Complex.fromReal(i)));
		}
		if (constantTerm) {
			this.v.set(0, constantTerm);
		}
		return this;
	}

	evaluateAt(z: Complex): Complex {
		let total = Complex.fromReal(0);
		let power = Complex.fromReal(1);
		for (let i = 0; i <= this.degree; i++) {
			total = total.plus(this.v.get(i).times(power));
			power = power.times(z);
		}
		return total;
	}

	derivativeAt(z: Complex): Complex {
		let total = Complex.fromReal(0);
		let power = Complex.fromReal(1);
		for (let i = 1; i <= this.degree; i++) {
			total = total.plus(this.v.get(i).timesScalar(i).times(power));
			power = power.times(z);
		}
		return total;
	}

	evaluateWithTangentAt(z: Complex): TangentComplex {
		return new TangentComplex(this.evaluateAt(z), this.derivativeAt(z));
	}

	// TODO: make required error + randomness rate tunable parameters.
	nearestZero(z: Complex): Complex {
		//return z;
		let iterations = 0;
		const originalZ = z.copy();
		z = z.plus(Complex.fromReal((Math.random() - 0.5) * 0.000001));
		const d = this.copy().iDerivative();
		let v = this.evaluateAt(z);
		let dv = d.evaluateAt(z);
		while (v.abs() > 0.00001) {
			// if deriv was const, would want to move z by -v/dv
			const zdelta = v.times(dv.inverse());
			z.x -= zdelta.x + (Math.random() - 0.5) * 0.000001;
			z.y -= zdelta.y + (Math.random() - 0.5) * 0.000001;
			v = this.evaluateAt(z);
			dv = d.evaluateAt(z);
			iterations++;
			if (iterations == 10000) {
				window.console.log("Gave up finding nearest zero:", this, originalZ, z, v, dv);
				break;
			}
		}
		return z;
	}

	computeZeroes(): Array<Complex> {
		window.console.log("computeZeroes");
		const factor = this.copy();
		const zeroes: Array<Complex> = [];
		for (let i = 0; i < this.degree; i++) {
			const zero = factor.nearestZero(Complex.fromReal(0));
			zeroes.push(zero);
			const term = ComplexPoly.fromReal(1).iTimesZToThePower(1).iMinusComplex(zero);
			factor.iDividedBy(term);
		}
		return zeroes;
	}

	toString(): string {
		const result = [];
		let c;
		for (let i = this.degree; i > 0; i--) {
			c = this.v.get(i);
			if (c.abs() >= EPSILON) {
				result.push(`(${c.toString()}) Z^${i}`);
			}
		}
		c = this.v.get(0);
		if (c.abs() >= EPSILON || this.degree == 0) {
			result.push(`(${c.toString()})`);
		}
		return result.join(" + ");
	}
}

export class ComplexSingularity {

	position: Complex;
	order: number;

	constructor(position: Complex, order: number) {
		this.position = position;
		this.order = order;
	}

	static fromPositionAndOrder(position: Complex, order: number): ComplexSingularity {
		return new ComplexSingularity(position, order);
	}
}

export class Meromorphic {

	numerator: ComplexPoly;
	denominator: ComplexPoly;

	constructor(numerator: ComplexPoly, denominator: ComplexPoly) {
		this.numerator = numerator.copy();
		this.denominator = denominator.copy();
	}

	copy(): Meromorphic {
		return new Meromorphic(this.numerator, this.denominator);
	}

	static fromNumeratorAndDenominator(numerator: ComplexPoly, denominator: ComplexPoly): Meromorphic {
		return new Meromorphic(numerator, denominator);
	}

	static fromNumerator(numerator: ComplexPoly): Meromorphic {
		return new Meromorphic(numerator, ComplexPoly.one());
	}

	static fromSingularities(singularities: Array<ComplexSingularity>): Meromorphic {
		const zeroes = [];
		const poles = [];
		for (const s of singularities) {
			if (s.order >= 0) {
				zeroes.push(s.position);
			} else {
				poles.push(s.position);
			}
		}
		const num = ComplexPoly.fromZeroes(zeroes);
		const den = ComplexPoly.fromZeroes(poles);
		return new Meromorphic(num, den);
	}

	zeroes(): Array<Complex> {
		const zeroes = this.numerator.computeZeroes();
		const result: Array<Complex> = [];
		for (const z of zeroes) {
			result.push(z);
		}
		return result;
	}

	poles(): Array<Complex> {
		const poles = this.denominator.computeZeroes();
		const result: Array<Complex> = [];
		for (const z of poles) {
			result.push(z);
		}
		return result;
	}

	singularities(): Array<ComplexSingularity> {
		const zeroes = this.zeroes().map(position => new ComplexSingularity(position, 1));
		const poles = this.poles().map(position => new ComplexSingularity(position, -1));
		return zeroes.concat(poles);
	}

	derivative(): Meromorphic {
		const dnum = this.numerator.copy().iDerivative();
		const dden = this.denominator.copy().iDerivative();

		const num = dnum.iTimes(this.denominator).iMinus(this.numerator.copy().iTimes(dden));
		const den = this.denominator.copy().iTimes(this.denominator);
		this.numerator = num;
		this.denominator = den;
		return this;
	}
}
import { IsoScaleTransform } from "core/coordinate_transform";
import { Coords2d } from "core/geometry";
import { PlaneRenderer } from "core/plane_renderer";
import { AxisRect, Size, Triangle2d } from "core/polygon";

function imageDataToCanvas(imageData: ImageData): HTMLCanvasElement {
	const canvas = document.createElement('canvas');
	canvas.width = imageData.width;
	canvas.height = imageData.height;
	const context = canvas.getContext("2d");
	if (context) {
		context.putImageData(imageData, 0, 0);
	}
	return canvas;
}

export class Renderable {

	style: {
		[key: string]: string;
	};
	constructor() {
		this.style = {};
	}

	_applyStyles(ctx: CanvasRenderingContext2D): void {
		Object.assign(ctx, this.style);
		/*for (const key in this.style) {
			(ctx as unknown)[key] = this.style[key];
		}*/
	}

	render(ctx: CanvasRenderingContext2D, transform: IsoScaleTransform): void {
		// do nothing
	}
}

export type RenderCallback = (imagedata: ImageData) => void;

export class ImageRendering extends Renderable {

	rect: AxisRect;
	pixelDimensions: Size;
	renderCallback: (imageData: ImageData) => void;

	constructor(rect: AxisRect, pixelDimensions: Size, renderCallback: (imageData: ImageData) => void) {
		super();
		this.rect = rect;
		this.pixelDimensions = pixelDimensions;
		this.renderCallback = renderCallback;
	}

	render(ctx: CanvasRenderingContext2D, modelToCanvas: IsoScaleTransform): void {
		console.log("ImageRendering.render");
		const imageSize = this.pixelDimensions;
		const imageData = ctx.createImageData(imageSize.width, imageSize.height);
		this.renderCallback(imageData);
		const imageCanvas = imageDataToCanvas(imageData);

		const rect = this.rect.applyTransform(modelToCanvas);
		ctx.drawImage(imageCanvas, rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
	}
}

export class Line extends Renderable {

	start: Coords2d;
	end: Coords2d;
	lineDash: Array<number> | null | undefined;

	constructor(start: Coords2d, end: Coords2d) {
		super();
		this.start = start.copy();
		this.end = end.copy();
		this.lineDash = null;
	}

	static fromEndpoints(start: Coords2d, end: Coords2d): Line {
		return new Line(start, end);
	}

	render(ctx: CanvasRenderingContext2D, transform: IsoScaleTransform): void {
		ctx.save();
		this._applyStyles(ctx);
		const start = transform.transformCoords(this.start);
		const end = transform.transformCoords(this.end);
		ctx.beginPath();
		if (this.lineDash) {
			ctx.setLineDash(this.lineDash);
		}
		ctx.moveTo(start.x, start.y);
		ctx.lineTo(end.x, end.y);
		ctx.stroke();
		ctx.restore();
	}
}

export class Polygon extends Renderable {

	coordsList: Array<Coords2d>;

	constructor(coordsList: Array<Coords2d>) {
		super();
		this.coordsList = coordsList.map(c => {return c.copy();});
		this.style.strokeStyle = '#000000';
	}

	static fromTriangle(triangle: Triangle2d): Polygon {
		return new Polygon(triangle.vertexCoords);
	}

	static fromRect(rect: AxisRect): Polygon {
		return new Polygon(rect.vertexCoords());
	}

	render(ctx: CanvasRenderingContext2D, transform: IsoScaleTransform): void {
		const coordsList = this.coordsList.map(c => transform.transformCoords(c));
		ctx.save();
		this._applyStyles(ctx);
		ctx.beginPath();
		ctx.moveTo(coordsList[0].x, coordsList[0].y);
		for (let i = 1; i < coordsList.length; i++) {
			ctx.lineTo(coordsList[i].x, coordsList[i].y);
		}
		ctx.closePath();
		if (this.style.fillStyle) {
			ctx.fill();
		}
		if (this.style.strokeStyle) {
			ctx.stroke();
		}
		ctx.restore();
	}
}

export class Circle extends Renderable {

	center: Coords2d;
	radius: number;

	constructor(center: Coords2d, radius: number) {
		super();
		this.center = center.copy();
		this.radius = radius;
	}

	static fromCenterAndRadius(center: Coords2d, radius: number): Circle {
		return new Circle(center, radius);
	}

	render(ctx: CanvasRenderingContext2D, transform: IsoScaleTransform): void {
		const center = transform.transformCoords(this.center);
		const radius = this.radius * transform.isoScale();
		ctx.save();
		this._applyStyles(ctx);
		ctx.beginPath();
		ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI);
		if (this.style.fillStyle) {
			ctx.fill();
		}
		if (this.style.strokeStyle) {
			ctx.stroke();
		}
		ctx.restore();
	}
}

export class RenderableElementsRendererParams {

	elements: Array<Renderable>;
	constructor() {
		this.elements = [];
	}
}

export class RenderableElementsRenderer extends PlaneRenderer<RenderableElementsRendererParams> {

	_render(ctx: CanvasRenderingContext2D, modelFrame: AxisRect, modelToCanvas: IsoScaleTransform, params: RenderableElementsRendererParams): void {
		for (let i = params.elements.length - 1; i >= 0; i--) {
			params.elements[i].render(ctx, modelToCanvas);
		}
	}
}

export default {
	Renderable,
	Line,
	Polygon,
	Circle,
	ImageRendering,
	RenderableElementsRendererParams,
	RenderableElementsRenderer,
};


import { RenderTarget } from "core/canvas";
import { Coords2d } from "core/geometry";
import { AxisRect } from "core/polygon";

import {
	Polygon, Renderable, RenderableElementsRenderer,
	RenderableElementsRendererParams } from "core/element_render";

export class ElementsView {

	_renderer: RenderableElementsRenderer;
	renderParams: RenderableElementsRendererParams;
	renderCallbacks: Array<() => void>;

	_needsRedraw: boolean;

	constructor(renderTarget: RenderTarget) {
		this._renderer = new RenderableElementsRenderer(renderTarget);
		this.renderParams = new RenderableElementsRendererParams();
		this.renderCallbacks = [];

		// Make sure _needsRedraw is false on the first call to generateClockTick
		// so we don't trigger a render.
		this._needsRedraw = false;
		requestAnimationFrame(this.generateClockTick());
		this._needsRedraw = true;
	}

	onRender(callback: () => void): void {
		this.renderCallbacks.push(callback);
	}

	get modelBounds(): AxisRect {
		return this._renderer.modelBounds;
	}
	set modelBounds(modelBounds: AxisRect) {
		this._renderer.modelBounds = modelBounds;
		this._needsRedraw = true;
	}

	get renderTarget(): RenderTarget {
		return this._renderer.target;
	}

	get renderer(): RenderableElementsRenderer {
		return this._renderer;
	}

	generateClockTick(): () => void {
		if (this._needsRedraw) {
			this.redraw();
		}
		return () => {
			requestAnimationFrame(this.generateClockTick());
		};
	}

	redraw(): void {
		this._needsRedraw = false;

		const frame = this._renderer.modelFrame();
		this.renderParams.elements = this.elementsToRender(frame);
		this._renderer.render(this.renderParams);
		for (const callback of this.renderCallbacks) {
			callback();
		}
	}

	elementsToRender(frameRect: AxisRect): Array<Renderable> {
		const elements: Array<Renderable> = [];
		const verts: Array<Coords2d> = [Coords2d.fromXY(-1, -1), Coords2d.fromXY(1, -1), Coords2d.fromXY(0, 2)];
		const poly = new Polygon(verts);
		poly.style.strokeStyle = '#ff33ee';
		poly.style.lineWidth = '2';
		elements.push(poly);

		return elements;
	}

	setNeedsRedraw(): void {
		this._needsRedraw = true;
	}
}
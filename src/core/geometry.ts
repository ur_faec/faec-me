

import { Complex } from "core/complex";

export class TangentCoords3d {

	base: Coords3d;
	offset: OffsetCoords3d;

	constructor(base: Coords3d, offset: OffsetCoords3d) {
		this.base = base.copy();
		this.offset = offset.copy();
	}

	copy(): TangentCoords3d {
		return new TangentCoords3d(this.base, this.offset);
	}

	static fromBaseAndOffset(base: Coords3d, offset: OffsetCoords3d): TangentCoords3d {
		return new TangentCoords3d(base, offset);
	}

	// Returns coeff such that
	// this.base + coeff * this.offset
	// is the closest point to target along the tangent vector from the
	// base point.
	tangentCoefficientOf(target: Coords3d): number {
		const targetOffset = target.asOffsetFrom(this.base);
		const r = this.offset.length();
		return this.offset.dot(targetOffset) / (r * r);
	}

	// Returns the coords on the line emanating from this rooted tangent
	// that have minimum distance to target.
	nearestCoordsTo(target: Coords3d): Coords3d {
		return this.base.plus(this.offset.timesReal(this.tangentCoefficientOf(target)));
	}

	reflectCoords(coords: Coords3d): Coords3d {
		const nearestCoords = this.nearestCoordsTo(coords);
		const offset = nearestCoords.asOffsetFrom(coords);
		return coords.plus(offset.timesReal(2));
	}

	reflectTCoords(tcoords: OffsetCoords3d): OffsetCoords3d {
		const coords = this.base.plus(tcoords);
		return this.reflectCoords(coords).asOffsetFrom(this.base);
	}

	reversed(): TangentCoords3d {
		return new TangentCoords3d(this.base.plus(this.offset), this.offset.reversed());
	}
}

export class TangentCoords2d {

	base: Coords2d;
	offset: OffsetCoords2d;
	constructor(base: Coords2d, offset: OffsetCoords2d) {
		this.base = base.copy();
		this.offset = offset.copy();
	}

	copy(): TangentCoords2d {
		return new TangentCoords2d(this.base, this.offset);
	}

	static fromBaseAndOffset(base: Coords2d, offset: OffsetCoords2d): TangentCoords2d {
		return new TangentCoords2d(base, offset);
	}

	// Returns coeff such that
	// this.coords + coeff * this.tcoords
	// is the closest point to target along the tangent vector from the
	// base point.
	tangentCoefficientOf(target: Coords2d): number {
		const targetOffset = target.asOffsetFrom(this.base);
		const r = this.offset.length();
		return this.offset.dot(targetOffset) / (r * r);
	}

	// Returns the coords on the line emanating from this rooted tangent
	// that have minimum distance to target.
	nearestCoordsTo(target: Coords2d): Coords2d {
		return this.base.plus(this.offset.timesReal(this.tangentCoefficientOf(target)));
	}

	coefficientOfIntersectionWith(rt: TangentCoords2d): number {
		const a = -rt.offset.dy;
		const b = rt.offset.dx;
		const c = -(a * rt.base.x + b * rt.base.y);
		const c0 = -(a * this.base.x + b * this.base.y);
		const dc = -(a * this.offset.dx + b * this.offset.dy);
		return (c - c0) / dc;
	}

	intersectionWith(rt: TangentCoords2d): Coords2d {
		const c = this.coefficientOfIntersectionWith(rt);
		return this.base.plus(this.offset.timesReal(c));
	}

	reflectCoords(coords: Coords2d): Coords2d {
		const nearestCoords = this.nearestCoordsTo(coords);
		const offset = nearestCoords.asOffsetFrom(coords);
		return coords.plus(offset.timesReal(2));
	}

	reflectTCoords(tcoords: OffsetCoords2d): OffsetCoords2d {
		const coords = this.base.plus(tcoords);
		return this.reflectCoords(coords).asOffsetFrom(this.base);
	}

	reverse(): TangentCoords2d {
		this.base = this.base.plus(this.offset);
		this.offset = this.offset.reversed();
		return this;
	}
}

export class OffsetCoords2d {

	dx: number;
	dy: number;

	// Internal constructor, do not call directly
	constructor(dx: number, dy: number) {
		this.dx = dx;
		this.dy = dy;
	}

	copy(): OffsetCoords2d {
		return new OffsetCoords2d(this.dx, this.dy);
	}

	static zero(): OffsetCoords2d {
		return new OffsetCoords2d(0, 0);
	}

	static fromDxDy(dx: number, dy: number): OffsetCoords2d {
		return new OffsetCoords2d(dx, dy);
	}

	static fromTuple(t: [number, number]): OffsetCoords2d {
		return new OffsetCoords2d(t[0], t[1]);
	}

	static fromComplex(z: Complex): OffsetCoords2d {
		return new OffsetCoords2d(z.x, z.y);
	}

	static fromPolar(magnitude: number, angle: number): OffsetCoords2d {
		return new OffsetCoords2d(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}

	static offsetBetweenCoords(fromCoords: Coords2d, toCoords: Coords2d): OffsetCoords2d {
		return new OffsetCoords2d(toCoords.x - fromCoords.x, toCoords.y - fromCoords.y);
	}

	rootedAtCoords(coords: Coords2d): TangentCoords2d {
		return TangentCoords2d.fromBaseAndOffset(coords, this);
	}

	asComplex(): Complex {
		return Complex.fromCartesian(this.dx, this.dy);
	}

	timesReal(a: number): OffsetCoords2d {
		return new OffsetCoords2d(this.dx * a, this.dy * a);
	}

	times(tc: OffsetCoords2d): OffsetCoords2d {
		const dx = this.dx * tc.dx - this.dy * tc.dy;
		const dy = this.dx * tc.dy + this.dy * tc.dx;
		return new OffsetCoords2d(dx, dy);
	}

	// Conjugate as a complex number.
	conjugate(): OffsetCoords2d {
		return new OffsetCoords2d(this.dx, -this.dy);
	}

	reversed(): OffsetCoords2d {
		return new OffsetCoords2d(-this.dx, -this.dy);
	}

	plus(tcoords: OffsetCoords2d): OffsetCoords2d {
		return new OffsetCoords2d(this.dx + tcoords.dx, this.dy + tcoords.dy);
	}

	dot(v: OffsetCoords2d): number {
		return this.dx * v.dx + this.dy * v.dy;
	}

	// "Cross" of 2-dimensional vectors gives a scalar equal to, among other
	// things, twice the area of the triangle described by the two vectors
	// rooted at a common point (or, the area of the quadrilateral described
	// by traversing the two vectors in either order), with the sign indicating
	// the orientation (positive for counterclockwise).
	cross(): OffsetCoords2d {
		return OffsetCoords2d.fromDxDy(-this.dy, this.dx);
	}

	normalized(): OffsetCoords2d {
		const len = this.length();
		return new OffsetCoords2d(this.dx / len, this.dy / len);
	}

	rotated(angle: number): OffsetCoords2d {
		const cos = Math.cos(angle);
		const sin = Math.sin(angle);
		const dx = this.dx * cos - this.dy * sin;
		const dy = this.dx * sin + this.dy * cos;
		return new OffsetCoords2d(dx, dy);
	}

	squaredLength(): number {
		return this.dx * this.dx + this.dy * this.dy;
	}

	length(): number {
		return Math.sqrt(this.dx * this.dx + this.dy * this.dy);
	}

	toString(): string {
		return `OffsetCoords2d(${this.dx}, ${this.dy})`;
	}
}

export class Coords2d {

	x: number;
	y: number;

	// Internal constructor, do not call directly.
	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	copy(): Coords2d {
		return new Coords2d(this.x, this.y);
	}

	static origin(): Coords2d {
		return Coords2d.fromXY(0, 0);
	}

	static fromXY(x: number, y: number): Coords2d {
		return new Coords2d(x, y);
	}

	static fromTuple(t: [number, number]): Coords2d {
		return new Coords2d(t[0], t[1]);
	}

	static fromComplex(z: Complex): Coords2d {
		return new Coords2d(z.x, z.y);
	}

	static fromPolar(abs: number, angle: number): Coords2d {
		return new Coords2d(abs * Math.cos(angle), abs * Math.sin(angle));
	}

	static midpoint(c1: Coords2d, c2: Coords2d): Coords2d {
		return new Coords2d((c1.x + c2.x) / 2, (c1.y + c2.y) / 2);
	}

	asComplex(): Complex {
		return Complex.fromCartesian(this.x, this.y);
	}

	plus(t: OffsetCoords2d): Coords2d {
		return new Coords2d(this.x + t.dx, this.y + t.dy);
	}

	dot(c: Coords2d): number {
		return this.x * c.x + this.y * c.y;
	}

	asOffsetFrom(c: Coords2d): OffsetCoords2d {
		return OffsetCoords2d.offsetBetweenCoords(c, this);
	}

	asOffsetFromOrigin(): OffsetCoords2d {
		return OffsetCoords2d.fromDxDy(this.x, this.y);
	}

	asTangentRootedAt(c: Coords2d): TangentCoords2d {
		const base = c.copy();
		return TangentCoords2d.fromBaseAndOffset(base, this.asOffsetFrom(base));
	}

	distanceFrom(c: Coords2d): number {
		return this.asOffsetFrom(c).length();
	}

	toString(): string {
		return `Coords2d(${this.x}, ${this.y})`;
	}
}

export class OffsetCoords3d {

	dx: number;
	dy: number;
	dz: number;

	// Internal constructor, do not call directly
	constructor(dx: number, dy: number, dz: number) {
		this.dx = dx;
		this.dy = dy;
		this.dz = dz;
	}

	copy(): OffsetCoords3d {
		return new OffsetCoords3d(this.dx, this.dy, this.dz);
	}

	static zero(): OffsetCoords3d {
		return new OffsetCoords3d(0, 0, 0);
	}

	static fromDxDyDz(dx: number, dy: number, dz: number): OffsetCoords3d {
		return new OffsetCoords3d(dx, dy, dz);
	}

	static fromTuple(t: [number, number, number]): OffsetCoords3d {
		return new OffsetCoords3d(t[0], t[1], t[2]);
	}

	toString(): string {
		return `OffsetCoords3d(${this.dx}, ${this.dy}, ${this.dz})`;
	}

	static offsetBetweenCoords(fromCoords: Coords3d, toCoords: Coords3d): OffsetCoords3d {
		return new OffsetCoords3d(toCoords.x - fromCoords.x, toCoords.y - fromCoords.y, toCoords.z - fromCoords.z);
	}

	rootedAtCoords(coords: Coords3d): TangentCoords3d {
		return TangentCoords3d.fromBaseAndOffset(coords, this);
	}

	timesReal(a: number): OffsetCoords3d {
		return new OffsetCoords3d(this.dx * a, this.dy * a, this.dz * a);
	}

	plus(tcoords: OffsetCoords3d): OffsetCoords3d {
		return new OffsetCoords3d(this.dx + tcoords.dx, this.dy + tcoords.dy, this.dz + tcoords.dz);
	}

	dot(v: OffsetCoords3d): number {
		return this.dx * v.dx + this.dy * v.dy + this.dz * v.dz;
	}

	cross(v: OffsetCoords3d): OffsetCoords3d {
		return OffsetCoords3d.fromDxDyDz(this.dy * v.dz - this.dz * v.dy, this.dz * v.dx - this.dx * v.dz, this.dx * v.dy - this.dy * v.dx);
	}

	length(): number {
		return Math.sqrt(this.squaredLength());
	}

	normalized(): OffsetCoords3d {
		const len = this.length();
		return new OffsetCoords3d(this.dx / len, this.dy / len, this.dz / len);
	}

	reversed(): OffsetCoords3d {
		return new OffsetCoords3d(-this.dx, -this.dy, -this.dz);
	}

	squaredLength(): number {
		return this.dx * this.dx + this.dy * this.dy + this.dz * this.dz;
	}
}

export class Coords3d {

	x: number;
	y: number;
	z: number;

	constructor(x: number, y: number, z: number) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	copy(): Coords3d {
		return new Coords3d(this.x, this.y, this.z);
	}

	static origin(): Coords3d {
		return Coords3d.fromXYZ(0, 0, 0);
	}

	static fromXYZ(x: number, y: number, z: number): Coords3d {
		return new Coords3d(x, y, z);
	}

	static fromTuple(t: [number, number, number]): Coords3d {
		return new Coords3d(t[0], t[1], t[2]);
	}

	plus(t: OffsetCoords3d): Coords3d {
		return new Coords3d(this.x + t.dx, this.y + t.dy, this.z + t.dz);
	}

	asOffsetFrom(c: Coords3d): OffsetCoords3d {
		return OffsetCoords3d.offsetBetweenCoords(c, this);
	}

	asTangentCoordsRootedAt(c: Coords3d): TangentCoords3d {
		return TangentCoords3d.fromBaseAndOffset(c, this.asOffsetFrom(c));
	}

	distanceFrom(c: Coords3d): number {
		return this.asOffsetFrom(c).length();
	}
}
import { JestObjectEquality } from "core/testing";

import { Rational } from "core/algebra/rational";

expect.extend(JestObjectEquality);

test("some arithmetic", () => {
    const a = new Rational(3, 2);
    const b = new Rational(5, 7);
    expect(a.plus(b)).toEqualObject(new Rational(31, 14));

	expect(new Rational(25, 5)).toEqualObject(new Rational(5, 1));
	
	expect(new Rational(-3, 5)).toEqualObject(new Rational(3, -5));

	expect(new Rational(3, 5).lessThan(new Rational(2, 3))).toEqual(true)
});
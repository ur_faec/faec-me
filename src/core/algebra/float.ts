import {OrderedField} from "core/algebra/field"

export class Float implements OrderedField<Float> {
    // We need this entry so generic code can access the underlying
    // (class) type of the field.
    T = Float;

    static zero: Float = new Float(0);
    static one: Float = new Float(1);

    value: number;
    
	constructor(num: number, den: number = 1) {
        this.value = num / den;
    }
    
    toString(): string {
        return this.value.toString();
    }
    
    equals(x: Float): boolean {
        return this.value === x.value;
    }

	plus(x: Float): Float {
		return new Float(this.value + x.value);
	}

	minus(x: Float): Float {
		return new Float(this.value - x.value);
	}

	neg(): Float {
		return new Float(-this.value);
    }
    
    times(x: Float): Float {
        return new Float(this.value * x.value);
    }

    dividedBy(x: Float): Float {
        return new Float(this.value / x.value);
    }

    inverse(): Float {
        return new Float(1 / this.value);
    }

    lessThan(x: Float): boolean {
        return (this.value < x.value);
    }

    lessOrEqual(x: Float): boolean {
        return (this.value <= x.value);
    }

    greaterThan(x: Float): boolean {
        return !this.lessOrEqual(x);
    }

    greaterOrEqual(x: Float): boolean {
        return !this.lessThan(x);
    }
}
/*
function addThings<k extends Field<k>>(a: k, b: k): k {
	return a.plus(b);
}

function teststuff(): Float {
	const a = new Float(5 / 2);
	const b = new Float(8 / 3);
	return addThings(a, b);
}*/

// separate interface for the static parts of a field class, since the
// abstract (typescript) type can't access the members of the constructing
// (javascript) class.
/*export interface FieldType<Element extends Field<Element>> {
	new (num: number, den: number): Element;
	zero: Element;
	one: Element;
}*/

export interface Equality<Element> {
	equals(x: Element): boolean;
}

export interface Ordered<Element> {
	lessThan(x: Element): boolean;
	lessOrEqual(x: Element): boolean;
	greaterThan(x: Element): boolean;
	greaterOrEqual(x: Element): boolean;
}

export interface Field<Element> extends Equality<Element> {
	// "T" points to the static class type for the field, and should
	// always be initialized to (the concrete class constructor
	// corresponding to) Element.
	T: {
		new (num: number, den: number): Element;
		zero: Element;
		one: Element;
	};

	plus(x: Element): Element;
	minus(x: Element): Element;
	neg(): Element;

	times(x: Element): Element;
	dividedBy(x: Element): Element;
	inverse(): Element;
}

export interface OrderedField<Element>
	extends Field<Element>, Ordered<Element>
{
}
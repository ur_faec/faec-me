import {OrderedField} from "core/algebra/field";

// It would be nice to have this class also implement Field -- it
// provides the same interface -- but
// the only way we could get a consistent definition of field operations
// was to parameterize the interface by the concrete type -- thus, a
// truly generic class like Complex, which takes something _other_ than
// itself as a type parameter, can't implement it.
// This isn't the best situation, but it's what's possible in typescript
// for now.
export class Complex<Base extends OrderedField<Base>> {
	x: Base;
	y: Base;

	constructor(x: Base, y: Base) {
		this.x = x;
		this.y = y;
	}

	toString(): string {
		return `(${this.x} + ${this.y} I)`;
	}

	equals(z: Complex<Base>): boolean {
		return this.x.equals(z.x) && this.y.equals(z.y);
	}

	plus(z: Complex<Base>): Complex<Base> {
		return new Complex(this.x.plus(z.x), this.y.plus(z.y));
	}

	minus(z: Complex<Base>): Complex<Base> {
		return new Complex(this.x.minus(z.x), this.y.minus(z.y));
	}

	neg(): Complex<Base> {
		return new Complex(this.x.neg(), this.y.neg());
	}

	squaredNorm(): Base {
		return this.x.times(this.x).plus(this.y.times(this.y));
	}

	times(z: Complex<Base>): Complex<Base> {
		return new Complex(
			this.x.times(z.x).minus(this.y.times(z.y)),
			this.x.times(z.y).plus(this.y.times(z.x)));
	}

	timesScalar(s: Base): Complex<Base> {
		return new Complex(this.x.times(s), this.y.times(s));
	}

	conjugate(): Complex<Base> {
		return new Complex(this.x, this.y.neg());
	}

	inverse(): Complex<Base> {
		const scale = this.squaredNorm().inverse();

		return new Complex(
			this.x.times(scale),
			this.y.neg().times(scale));
	}

	dividedBy(z: Complex<Base>): Complex<Base> {
		return this.times(z.inverse());
	}
}
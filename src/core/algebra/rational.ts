import {OrderedField} from "core/algebra/field"

function gcd(a: bigint, b: bigint): bigint {
	if (b == 0n) {
		return a;
	}
	return gcd(b, a % b);
}

// Reduced is passed into the Rational constructor by internal
// methods to indicate that reduction by common factors has already
// been done.
class Reduced {
	num: bigint;
	den: bigint;

	constructor(num: bigint, den: bigint) {
		this.num = num;
		this.den = den;
	}

	static from(num: bigint, den: bigint): Reduced {
		const factor = gcd(num, den);
		return new Reduced(num / factor, den / factor);
	}
}

export class Rational implements OrderedField<Rational> {
	T = Rational;

	static zero: Rational = new Rational(0);
	static one: Rational = new Rational(1);

	num: bigint;
	den: bigint;

	constructor(arg0: Reduced | bigint | number, arg1: bigint | number = 1n) {
		var reduced: Reduced;
		if (arg0 instanceof Reduced) {
			reduced = arg0;
		} else {
			reduced = Reduced.from(BigInt(arg0), BigInt(arg1));
		}
		// we get cheap equality testing if we make sure the denominator is
		// always positive.
		if (reduced.den < 0n) {
			this.num = -reduced.num;
			this.den = -reduced.den;
		} else {
			this.num = reduced.num;
			this.den = reduced.den;
		}
	}

	static fromString(s: string): Rational | undefined {
		const components = s.split("/");
		if (components.length != 2) {
			return;
		}
		try {
			const num = BigInt(components[0]);
			const den = BigInt(components[1]);
			return new Rational(num, den);
		} catch(err) { }
	}
	
	toString(): string {
		return this.num.toString() + "/" + this.den.toString();
	}

	toJSON(): string {
		return this.toString();
	}

	equals(x: Rational): boolean {
		return (this.num == x.num && this.den == x.den);
	}

	plus(x: Rational): Rational {
		return new Rational(
			this.num * x.den + this.den * x.num,
			this.den * x.den);
	}

	minus(x: Rational): Rational {
		return new Rational(
			this.num * x.den - this.den * x.num,
			this.den * x.den);
	}

	neg(): Rational {
		return new Rational(new Reduced(-this.num, this.den));
	}
	
	times(x: Rational): Rational {
		// Slightly faster to reduce before multiplying
		const left = Reduced.from(this.num, x.den);
		const right = Reduced.from(x.num, this.den);
		const reduced = new Reduced(left.num * right.num, left.den * right.den);
		return new Rational(reduced);
	}

	dividedBy(x: Rational): Rational {
		return this.times(x.inverse());
	}

	inverse(): Rational {
		return new Rational(new Reduced(this.den, this.num));
	}

	lessThan(x: Rational) {
		return (this.num * x.den < x.num * this.den);
	}

	lessOrEqual(x: Rational) {
		return (this.num * x.den <= x.num * this.den);
	}

    greaterThan(x: Rational): boolean {
        return !this.lessOrEqual(x);
    }

    greaterOrEqual(x: Rational): boolean {
        return !this.lessThan(x);
    }
}

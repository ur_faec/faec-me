

import { RigidTransform2d } from "core/coordinate_transform";
import { Coords2d, Coords3d } from "core/geometry";
import { Mod3, Triangle2d, Triangle3d, TrianglePerimeterPoint } from "core/polygon";

// Backing up: An even more special case gluing, in which all 3 verts
// coincide, and the only question is where to glue the three of them along
// the triangle's perimeter.
// The possibilities then are:
//   * the origin (no additional edge glued into that point)
//   * A tuple of:
//       - an edge choice from [0->1, 1->2, 2->3]
//       - a real coefficient in the exclusive range (0, 1) specifying how
//         far along the edge the glue point is
// This is feeling like polynomial roots, again. Like this is a cubic
// equation.
//
// After all, a triangle does have a unique-ish embedding into a circle.
// It is reasonable to parameterize its perimeter by angle with respect
// to the center of that circle. In the equilateral case this would give
// the tetrahedron at exactly points whose phase is an integer multiple of
// pi / 3, otherwise it would be more complicated. We can look at the specific
// cubic polynomial whose roots are the vertices of the triangle.
//
// Not sure why we would do that, but we sure can.
// Fine for now it's just an edge index and an affine parameter over the edge
// in the range [0,1].
export class TriangleTetraGluing {

	perimeterPoint: TrianglePerimeterPoint;

	constructor(perimeterPoint: TrianglePerimeterPoint) {
		this.perimeterPoint = perimeterPoint.copy();
	}

	static fromEdgeIndexAndGluePoint(
		edgeIndex: number, gluePoint: number
	): TriangleTetraGluing {
		const perimeterPoint = new TrianglePerimeterPoint(edgeIndex, gluePoint);
		return new TriangleTetraGluing(perimeterPoint);
	}

	static fromPerimeterPoint(
		perimeterPoint: TrianglePerimeterPoint
	): TriangleTetraGluing {
		return new TriangleTetraGluing(perimeterPoint);
	}

	gluedFacesForTriangle(triangle: Triangle2d): Array<GluedFace> {
		const gluedFace = GluedFace.fromTriangle(triangle);
		gluedFace.creaseAroundEdgesAtCoefficientList([0.5, 0.5, 0.5]);
		//let [centerRight, centerLeft] = faces[0].creaseEdge(1, 0.5);
		//let [centerLL, centerLR] = centerLeft.creaseEdge(1, 0.5);
		//centerRight.creaseEdge(2, 0.5);
		//centerLR.creaseEdge(1, 2/3);
		//centerLL.creaseEdge(1, 0.5);
		//centerRight.children[1].creaseEdge(2, 0.5);
		//centerLR.children[0].creaseEdge(0, 0.75);
		//faces[1].creaseEdge(0, 0.5);
		//faces[2].creaseEdge(0, 0.5);
		const leaves = gluedFace.leafDescendants();
		for (let i = 0; i < leaves.length; i++) {
			const leaf = leaves[i];
			const v = leaf.triangle.vertexCoords.map(c => Coords3d.fromXYZ(c.x, c.y, 0));
			const t = Triangle3d.fromCoords(v[0], v[1], v[2]);
			if (i > 0) {
				t.rotateAroundVertices(0, 1, 0.3);
			}
			leaf.embedding = t;
		}
		EdgeConnection.connect(gluedFace.children[1].edge(2), gluedFace.children[2].edge(1));
		EdgeConnection.connect(gluedFace.children[2].edge(2), gluedFace.children[3].edge(1));
		EdgeConnection.connect(gluedFace.children[3].edge(2), gluedFace.children[1].edge(1));

		return leaves;
	}
}

type GluedFaceEdge = {
	face: GluedFace;
	edgeIndex: number;
};

export class GluedFace {

	triangle: Triangle2d;
	parent: GluedFace | null | undefined;
	children: Array<GluedFace>;

	embedding: Triangle3d | null | undefined;

	// The faces that this face is glued to along each edge, if any.
	connections: Array<EdgeConnection | null>;

	constructor(triangle: Triangle2d, parent: GluedFace | null | undefined) {
		this.triangle = triangle.copy();

		// list of EmbeddedFace giving the gluings of the edges and their angles.
		this.connections = [null, null, null];
		this.children = [];
	}

	static fromTriangle(triangle: Triangle2d): GluedFace {
		return new GluedFace(triangle, null);
	}

	leafDescendants(): Array<GluedFace> {
		const results: Array<GluedFace> = [];
		const process = function (face: GluedFace): void {
			if (face.children.length > 0) {
				for (let i = 0; i < face.children.length; i++) {
					process(face.children[i]);
				}
			} else {
				results.push(face);
			}
		};
		process(this);
		return results;
	}

	// Returns the faces as [left, right], treating the creased
	// edge as the base of the triangle and the creased vertex as the apex.
	creaseAtVertex(vertexIndex: number, edgeCoeff: number): [GluedFace, GluedFace] {
		const [vi, ei1, ei2] = [vertexIndex, (vertexIndex + 1) % 3, (vertexIndex + 2) % 3];

		if (this.children.length > 0) {
			throw "Can't crease a node with children.";
		}
		if (this.connections[ei1]) {
			throw "Creases must end at a boundary, cut the adjacent joint first.";
		}
		const v = this.triangle.vertexCoords;
		const splitPoint = v[ei1].plus(v[ei2].asOffsetFrom(v[ei1]).timesReal(edgeCoeff));

		const leftTri = Triangle2d.fromCoords(v[ei1], splitPoint, v[vi]);
		const rightTri = Triangle2d.fromCoords(splitPoint, v[ei2], v[vi]);

		const left = new GluedFace(leftTri, this);
		const right = new GluedFace(rightTri, this);

		EdgeConnection.connect(left.edge(1), right.edge(2));
		const viCxn = this.connections[vi];
		viCxn?.replace(this.edge(vi), left.edge(2));
		const ei2Cxn = this.connections[ei2];
		ei2Cxn?.replace(this.edge(ei2), right.edge(1));
		if (this.connections[ei2]) {
		}
		const children: [GluedFace, GluedFace] = [left, right];
		this.children = children;
		return children;
	}

	creaseEdge(edgeIndex: number, edgeCoeff: number): [GluedFace, GluedFace] {
		const connection = this.connections[edgeIndex];
		let neighbor: GluedFaceEdge | null = null;
		if (connection) {
			neighbor = connection.traverse(this.edge(edgeIndex));
			connection.sever();

		}
		const [left, right] = this.creaseAtVertex(Mod3(edgeIndex + 2), edgeCoeff);
		if (neighbor) {
			const [neighborLeft, neighborRight] = neighbor.face.creaseAtVertex(Mod3(neighbor.edgeIndex + 2), 1 - edgeCoeff);
			// Swap the left and right because the faces point opposite directions.
			EdgeConnection.connect(left.edge(0), neighborRight.edge(0));
			EdgeConnection.connect(right.edge(0), neighborLeft.edge(0));
		}
		return [left, right];
	}

	// Requires a length-3 list giving coefficients in (0, 1) (exclusive)
	// for each edge's crease position.
	// Returns a length-4 list of the resulting 4 faces, starting with the center
	// and then each perimeter face in the same order as the vertices they
	// contain. The vertices of the inner face are in the same order as the
	// initial order of the edges containing them. The vertices of the outer
	// faces start with the edge that touches the inner face, with the
	// original outer vertex coming last.
	creaseAroundEdgesAtCoefficientList(edgeCoeffs: Array<number>): Array<GluedFace> {
		if (this.children.length > 0) {
			throw "Can't crease a node with children.";
		}
		if (this.connections[0] || this.connections[1] || this.connections[2]) {
			throw "Creases must end at a boundary, cut the adjacent joints first.";
		}
		const v = this.triangle.vertexCoords;
		const creaseCoords: Array<Coords2d> = [];
		for (let i = 0; i < 3; i++) {
			const edge = v[Mod3(i + 1)].asOffsetFrom(v[i]);
			creaseCoords.push(v[i].plus(edge.timesReal(edgeCoeffs[i])));
		}
		const center = new GluedFace(Triangle2d.fromCoords(creaseCoords[0], creaseCoords[1], creaseCoords[2]), this);
		const vertFaces: Array<GluedFace> = [];
		for (let i = 0; i < 3; i++) {
			const face = new GluedFace(Triangle2d.fromCoords(creaseCoords[i], creaseCoords[Mod3(i - 1)], v[i]), this);
			vertFaces.push(face);
			EdgeConnection.connect(center.edge(Mod3(i - 1)), face.edge(0));
		}
		const children = [center, vertFaces[0], vertFaces[1], vertFaces[2]];
		this.children = children;
		return children;
	}

	edge(index: number): GluedFaceEdge {
		return { face: this, edgeIndex: Mod3(index) };
	}

	/*asTriangle () {
		var t = this.triangle;
		var v = this.vertexSources.map(
			(interiorPoint) => t.coordsForInteriorPoint(interiorPoint));
		return Triangle.fromCoordArray(v);
	}*/







	// Creates a list of this face and all faces glued to it. The starting face
	// uses the coordinates of the source triangle, others are rotated as

	/*asTriangleList (depth: number = -1) {
		var visited = Symbol();
		var triangles = [];
		this._asTriangleList(
			visited, AffineTransform3d.identity(), triangles, depth);
		return triangles;
	}
	_asTriangleList (visited, transform, triangles, depth) {
		if (this.visited == visited) {
			return;
		}
		this.visited = visited;
		var tri = transform.applyTransform(this.asTriangle());
		triangles.push(tri);
		if (depth == 0) {
			return;
		}
		for (var i = 0; i < 3; i++) {
			if (!this.connections[i]) {
				continue;
			}
			var edge = this.connections[i].traverse(this.edge(i));
			var child = edge.face;
			var childTransform = AffineTransform3d.rotationAroundRootedAxisByAngle(
				tri.rootedEdge(i), this.connections[i].angle);
			var composedTransform = transform.copy().composeWith(childTransform);
			child._asTriangleList(
				visited, composedTransform, triangles, depth == -1 ? -1 : depth - 1);
		}
	}*/
}

// object wrapping an edge-to-edge gluing between two faces.
class EdgeConnection {

	edges: Array<GluedFaceEdge>;

	constructor(edge1: GluedFaceEdge, edge2: GluedFaceEdge) {
		this.edges = [edge1, edge2];

		this._attachEdge(edge1);
		this._attachEdge(edge2);
	}

	// Connects two objects of type EmbeddedFace.Edge, which is just a wrapper
	// around dictionaries with the keys face : EmbeddedFace and edgeIndex : int
	static connect(edge1: GluedFaceEdge, edge2: GluedFaceEdge) {
		if (edge1.face.connections[edge1.edgeIndex] || edge2.face.connections[edge2.edgeIndex]) {
			throw "Cannot connect edges that are already glued.";
		}
		return new EdgeConnection(edge1, edge2);
	}

	sever() {
		this._severEdge(this.edges[0]);
		this._severEdge(this.edges[1]);
	}

	replace(oldEdge: GluedFaceEdge, newEdge: GluedFaceEdge) {
		const index = this._indexOf(oldEdge);
		this.edges[index] = newEdge;
		this._severEdge(oldEdge);
		this._attachEdge(newEdge);
	}

	traverse(edge: GluedFaceEdge): GluedFaceEdge {
		const index = this._indexOf(edge);
		return this.edges[(index + 1) % 2];
	}

	// Returns a map that transforms coordinates in edge.triangle into the
	// coordinate space of the neighboring face's triangle.
	transitionMap(edge: GluedFaceEdge): RigidTransform2d {
		const index = this._indexOf(edge);
		const source = this.edges[index];
		const dest = this.edges[(index + 1) % 2];
		const sourceEdge = source.face.triangle.rootedEdge(source.edgeIndex);
		const destEdge = dest.face.triangle.rootedEdge(dest.edgeIndex).reverse();
		return RigidTransform2d.fromSourceAndDestLines(sourceEdge, destEdge);
	}

	_indexOf(edge: GluedFaceEdge) {
		for (let i = 0; i < 2; i++) {
			if (edge.face == this.edges[i].face && edge.edgeIndex == this.edges[i].edgeIndex) {
				return i;
			}
		}
		throw "Tried to traverse connection through unrecognized edge";
	}

	_attachEdge(e: GluedFaceEdge) {
		e.face.connections[e.edgeIndex] = this;
	}

	_severEdge(e: GluedFaceEdge) {
		e.face.connections[e.edgeIndex] = null;
	}
}
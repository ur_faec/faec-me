import { RenderTarget } from "core/canvas";
import { ElementsView } from "core/elements_view";
import { Renderable } from "core/element_render";
import { AxisRect } from "core/polygon";

export class ElementsCallbackView extends ElementsView {
	_elementsCallback: () => Array<Renderable>;
	
	constructor(
		renderTarget: RenderTarget,
		modelBounds: AxisRect,
		elementsCallback: () => Array<Renderable>
	) {
		super(renderTarget);
		this._elementsCallback = elementsCallback;
		// ...this should really be a parameter on the parent class constructor
		this.modelBounds = modelBounds;
	}

	elementsToRender(frameRect: AxisRect): Array<Renderable> {
		return this._elementsCallback();
	}
}
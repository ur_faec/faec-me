

import { Coords2d, OffsetCoords2d, Coords3d,
	OffsetCoords3d, TangentCoords2d, TangentCoords3d } from "core/geometry";
import { AxisRect } from "core/polygon";

export interface CoordinateTransform2d {
	transformCoords(coords: Coords2d): Coords2d;
	transformOffset(offset: OffsetCoords2d): OffsetCoords2d;
	inverse(): CoordinateTransform2d;
}

export interface CoordinateTransform3d {
	transformCoords(coords: Coords3d): Coords3d;
	transformOffset(offset: OffsetCoords3d): OffsetCoords3d;
	inverse(): CoordinateTransform3d;
}

export class IdentityTransform3d implements CoordinateTransform3d {
	constructor() { }
	transformCoords(coords: Coords3d): Coords3d {return coords;}
	transformOffset(offset: OffsetCoords3d): OffsetCoords3d {return offset;}
	inverse(): CoordinateTransform3d {return this;}
}

export class IdentityTransform2d implements CoordinateTransform2d {

	constructor() {}
	transformCoords(coords: Coords2d): Coords2d {return coords;}
	transformOffset(offset: OffsetCoords2d): OffsetCoords2d {return offset;}
	inverse(): CoordinateTransform2d {return this;}
}

export class CompositionTransform2d implements CoordinateTransform2d {

	_transforms: Array<CoordinateTransform2d>;
	constructor(transforms: Array<CoordinateTransform2d>) {
		this._transforms = transforms;
	}

	transformCoords(coords: Coords2d): Coords2d {
		let current = coords;
		for (let i = 0; i < this._transforms.length; i++) {
			current = this._transforms[i].transformCoords(current);
		}
		return current;
	}

	transformOffset(offset: OffsetCoords2d): OffsetCoords2d {
		let current = offset;
		for (let i = 0; i < this._transforms.length; i++) {
			current = this._transforms[i].transformOffset(current);
		}
		return current;
	}

	inverse(): CompositionTransform2d {
		const inverseTransforms: Array<CoordinateTransform2d> = [];
		for (let i = this._transforms.length - 1; i >= 0; i--) {
			inverseTransforms.push(this._transforms[i].inverse());
		}
		return new CompositionTransform2d(inverseTransforms);
	}
}

export class CompositionTransform3d implements CoordinateTransform3d {

	_transforms: Array<CoordinateTransform3d>;
	constructor(transforms: Array<CoordinateTransform3d>) {
		this._transforms = transforms;
	}

	transformCoords(coords: Coords3d): Coords3d {
		let current = coords;
		for (let i = 0; i < this._transforms.length; i++) {
			current = this._transforms[i].transformCoords(current);
		}
		return current;
	}

	transformOffset(offset: OffsetCoords3d): OffsetCoords3d {
		let current = offset;
		for (let i = 0; i < this._transforms.length; i++) {
			current = this._transforms[i].transformOffset(current);
		}
		return current;
	}

	inverse(): CompositionTransform3d {
		const inverseTransforms: Array<CoordinateTransform3d> = [];
		for (let i = this._transforms.length - 1; i >= 0; i--) {
			inverseTransforms.push(this._transforms[i].inverse());
		}
		return new CompositionTransform3d(inverseTransforms);
	}
}

export interface Transformable2d {
	applyTransform(transform: CoordinateTransform2d): void;
}

export class AxisTransform2d implements CoordinateTransform2d {

	_offset: OffsetCoords2d;
	_scale: [number, number];

	constructor(offset: OffsetCoords2d, scale: [number, number]) {
		this._offset = offset;
		this._scale = scale;
	}

	transformCoords(coords: Coords2d): Coords2d {
		return Coords2d.fromXY(this._scale[0] * coords.x + this._offset.dx, this._scale[1] * coords.y + this._offset.dy);
	}

	transformOffset(offset: OffsetCoords2d): OffsetCoords2d {
		return OffsetCoords2d.fromDxDy(offset.dx * this._scale[0], offset.dy * this._scale[1]);
	}

	inverse(): AxisTransform2d {
		const inverseOffset = OffsetCoords2d.fromDxDy(-this._offset.dx * this._scale[0], -this._offset.dy * this._scale[1]);
		const inverseScale: [number, number] = [1.0 / this._scale[0], 1.0 / this._scale[1]];
		return new AxisTransform2d(inverseOffset, inverseScale);
	}
}

export class IsoScaleTransform extends AxisTransform2d {

	_isoScale: number;
	_invertY: boolean;

	constructor(offset: OffsetCoords2d, scale: number, invertY: boolean) {
		const xscale = scale;
		const yscale = invertY ? -scale : scale;
		super(offset, [xscale, yscale]);
		this._isoScale = scale;
		this._invertY = invertY;
	}

	isoScale(): number {
		return this._isoScale;
	}

	//static transformInto(fromRect: AxisRect, intoRect: AxisRect, invertY: boolean): IsoScaleTransform {

	//}

	// returns a transform that sends fromRect to the smallest
	// rectangle with the same aspect ratio containing ontoRect
	static transformOnto(fromRect: AxisRect, ontoRect: AxisRect, invertY: boolean): IsoScaleTransform {
		// the ratio of the desired size over the space available for each axis
		const xRatio = ontoRect.size.width / fromRect.size.width;
		const yRatio = ontoRect.size.height / fromRect.size.height;
		const scale = Math.max(xRatio, yRatio);
		const xCoeff = scale;
		const yCoeff = invertY ? -scale : scale;
		const fromCenter = fromRect.center();
		const ontoCenter = ontoRect.center();
		const offset = OffsetCoords2d.fromDxDy(
			ontoCenter.x - xCoeff * fromCenter.x,
			ontoCenter.y - yCoeff * fromCenter.y);
		return new IsoScaleTransform(offset, scale, invertY);
	}

	inverse(): IsoScaleTransform {
		const inverseScale = 1.0 / this._isoScale;
		const inverseOffset = OffsetCoords2d.fromDxDy(-this._offset.dx / this._scale[0], -this._offset.dy / this._scale[1]);
		return new IsoScaleTransform(inverseOffset, inverseScale, this._invertY);
	}
}

export class TranslationTransform3d implements CoordinateTransform3d {

	offset: OffsetCoords3d;

	constructor(offset: OffsetCoords3d) {
		this.offset = offset.copy();
	}

	static fromOffset(offset: OffsetCoords3d): TranslationTransform3d {
		return new TranslationTransform3d(offset);
	}

	transformCoords(coords: Coords3d): Coords3d {
		return coords.plus(this.offset);
	}

	transformOffset(offset: OffsetCoords3d): OffsetCoords3d {
		return offset.copy();
	}

	inverse(): TranslationTransform3d {
		return new TranslationTransform3d(this.offset.timesReal(-1));
	}
}

export class CoordinateProjection {

	transform3d: CoordinateTransform3d;
	transform2d: CoordinateTransform2d;

	constructor(transform3d: CoordinateTransform3d | null | undefined, transform2d: CoordinateTransform2d | null | undefined) {
		this.transform3d = transform3d || new IdentityTransform3d();
		this.transform2d = transform2d || new IdentityTransform2d();
	}

	transformCoords(coords: Coords3d): Coords2d {
		if (this.transform3d) {
			coords = this.transform3d.transformCoords(coords);
		}
		let projected = Coords2d.fromXY(coords.x / coords.z, coords.y / coords.z);
		if (this.transform2d) {
			projected = this.transform2d.transformCoords(projected);
		}
		return projected;
	}

	transformTangentCoords(tc: TangentCoords3d): TangentCoords2d {
		const scale = 1.0 / tc.base.z;
		let base = tc.base;
		let offset = tc.offset;
		if (this.transform3d) {
			const t3d = this.transform3d;
			base = t3d.transformCoords(base);
			offset = t3d.transformOffset(offset);
		}
		let projectedBase = Coords2d.fromXY(scale * base.x, scale * base.y);
		let projectedOffset = OffsetCoords2d.fromDxDy(scale * offset.dx, scale * offset.dy);
		if (this.transform2d) {
			const t2d = this.transform2d;
			projectedBase = t2d.transformCoords(projectedBase);
			projectedOffset = t2d.transformOffset(projectedOffset);
		}
		return TangentCoords2d.fromBaseAndOffset(projectedBase, projectedOffset);
	}
}

export class RigidTransform2d implements CoordinateTransform2d {
	// Angle is clamped to [-Math.PI, Math.PI]
	angle: number;
	offset: OffsetCoords2d;

	constructor(angle: number, offset: OffsetCoords2d) {
		this.angle = angle;
		this.offset = offset.copy();
	}

	static fromSourceAndDestLines(sourceLine: TangentCoords2d, destLine: TangentCoords2d): RigidTransform2d {
		const sourceAngle = Math.atan2(sourceLine.offset.dx, sourceLine.offset.dy);
		const destAngle = Math.atan2(destLine.offset.dx, destLine.offset.dy);
		const angle = (destAngle - sourceAngle) % Math.PI;

		const source = sourceLine.base;
		const sin = Math.sin(angle);
		const cos = Math.cos(angle);
		const rotatedSource = Coords2d.fromXY(source.x * cos - source.y * sin, source.y * cos + source.x * sin);
		const offset = destLine.base.asOffsetFrom(rotatedSource);

		return new RigidTransform2d(angle, offset);
	}

	transformCoords(coords: Coords2d): Coords2d {
		const sin = Math.sin(this.angle);
		const cos = Math.cos(this.angle);
		return Coords2d.fromXY(coords.x * cos - coords.y * sin + this.offset.dx, coords.y * cos + coords.x * sin + this.offset.dy);
	}

	transformOffset(offset: OffsetCoords2d): OffsetCoords2d {
		const sin = Math.sin(this.angle);
		const cos = Math.cos(this.angle);
		return OffsetCoords2d.fromDxDy(offset.dx * cos - offset.dy * sin, offset.dy * cos + offset.dx * sin);
	}

	inverse(): RigidTransform2d {
		const inverseAngle = -this.angle;

		const sin = Math.sin(inverseAngle);
		const cos = Math.cos(inverseAngle);
		const inverseOffset = this.offset.timesReal(-1);
		const rotatedInverseOffset = OffsetCoords2d.fromDxDy(
			inverseOffset.dx * cos - inverseOffset.dy * sin,
			inverseOffset.dy * cos + inverseOffset.dx * sin);
		return new RigidTransform2d(inverseAngle, rotatedInverseOffset);
	}
}
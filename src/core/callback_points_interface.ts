

import { Canvas } from "core/canvas";
import { Coords2d } from "core/geometry";

type ActivePoint = {
	listener: (coords: Coords2d) => void;
	canvasCoords: Coords2d;
};

export type PointsUpdateCallback =
	(updatedCoords: Coords2d) => void;

export type ControlPointsCallback =
	(initialCoords: Coords2d) => PointsUpdateCallback | null;


export class CallbackPointsInterface {

	canvas: Canvas;

	controlPointsCallback: ControlPointsCallback;
	_activePoint: ActivePoint | null;
	_isMouseDown: boolean;
	_isTouchDown: boolean;

	// Attaches to an array of points and exposes them as user-draggable points
	// in an html canvas, restricted to the interior of the given rectangle
	// bounds (in canvas space). If no rectangle is given, defaults to the
	// canvas bounds.
	constructor(canvas: Canvas, controlPointCallback: ControlPointsCallback) {
		this.controlPointsCallback = controlPointCallback;

		// DOM state (constant)
		this.canvas = canvas;

		// Internal UI state
		this._isMouseDown = false;
		this._isTouchDown = false;
		// Contains the state of the point currently being moved from when it
		// was first selected.
		this._activePoint = null;

		this._addEventListeners();
	}

	_internalMouseDown(x: number, y: number): void {
		const canvasCoords = Coords2d.fromXY(x, y);
		const listener = this.controlPointsCallback(canvasCoords);

		if (listener != null) {
			this._isMouseDown = true;
			this._activePoint = {
				listener: listener,
				canvasCoords: canvasCoords,
			};
		}
	}

	_internalMouseMove(x: number, y: number): void {
		if (this._activePoint != null) {
			this._activePoint.listener(Coords2d.fromXY(x, y));
		}
	}

	_addEventListeners(): void {
		this.canvas.element.addEventListener("mousedown",
			(event: MouseEvent) => {
				if (event.button != 0 || this._isTouchDown) {
					return;
				}
				this._internalMouseDown(event.offsetX, event.offsetY);
			});
		this.canvas.element.addEventListener("touchstart",
			(event: TouchEvent) => {
				if (event.changedTouches.length != 1) {
					return;
				}
				this._isTouchDown = true;
				const rect = this.canvas.element.getBoundingClientRect();
				this._internalMouseDown(
					event.touches[0].clientX - rect.left,
					event.touches[0].clientY - rect.top);
				if (this._activePoint != null) {
					event.preventDefault();
				}
			});
		this.canvas.element.addEventListener("mouseup", () => {
			this._isMouseDown = false;
		});
		this.canvas.element.addEventListener("touchend", () => {
			this._isTouchDown = false;
		});

		this.canvas.element.addEventListener("mousemove",
			(event: MouseEvent) => {
				if (!this._isMouseDown) {
					return;
				}
				this._internalMouseMove(event.offsetX, event.offsetY);
			}, false);

		this.canvas.element.addEventListener("touchmove",
			(event: TouchEvent) => {
				if (!this._isTouchDown) {
					return;
				}
				const rect = this.canvas.element.getBoundingClientRect();
				this._internalMouseMove(
					event.touches[0].clientX - rect.left,
					event.touches[0].clientY - rect.top);
			}, false);
	}
}
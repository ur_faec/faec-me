import { Canvas } from "core/canvas";
import { Coords2d } from "core/geometry";
import { AxisRect, Size } from "core/polygon";


import { AllShaders } from "core/shaders/all_shaders";

export class PlaneShaderRendererParams {
	showCartesianGrid: boolean;
	cartesianScale: number;
	showAbsGrid: boolean;
	absScale: number;
	showPhaseGrid: boolean;

	lineWidth: number;

	constructor() {
		this.showCartesianGrid = true;
		this.cartesianScale = 1;
		this.showAbsGrid = true;
		this.absScale = 1;
		this.showPhaseGrid = true;

		this.lineWidth = 1;
	}
}

// Base class for shader-based renderers in the plane.
export class PlaneShaderRenderer<ParamsType extends PlaneShaderRendererParams> {

	canvas: Canvas;
	_modelBounds: AxisRect;

	_shaderProgram: WebGLProgram | null = null;

	_gl: WebGLRenderingContext | null = null;
	_vertexCoordsBuffer: WebGLBuffer | null = null;
	_textureCoordsBuffer: WebGLBuffer | null = null;
	_vertexIndexBuffer: WebGLBuffer | null = null;
	_textureCoordsArray: Float32Array | null = null;

	_vertexPositionAttribute = 0;
	_textureCoordAttribute = 0;

	constructor(canvas: Canvas, vertexCode: string | undefined, fragmentCode: string) {
		this._modelBounds = new AxisRect(Coords2d.fromXY(-1, -1), new Size(2, 2));
		this.canvas = canvas;
		const gl = canvas.element.getContext("experimental-webgl") as WebGLRenderingContext;
		if (gl) {
			gl.clearColor(0.0, 0.0, 0.0, 1.0); // Clear to black, fully opaque
			gl.clearDepth(1.0); // Clear everything
			this._initBuffers(gl);
			this._shaderProgram = this._createShaderProgram(
				gl, vertexCode || AllShaders.vertex.identity, fragmentCode);
			this._gl = gl;
		} else {
			console.log("couldn't get rendering context for canvas");
		}
	}

	renderFrom(modelBounds: AxisRect): void {
		console.log(`PlaneShaderRenderer.renderFrom (${modelBounds.toString()})`);
		this._modelBounds = modelBounds;
		const rect = modelBounds;
		const {
			x: x0,
			y: y0,
		} = rect.origin;
		const {
			x: x1,
			y: y1,
		} = rect.origin.plus(rect.size.asOffsetCoords());
		const textureCoordinates = [x0, y0, x1, y0, x1, y1, x0, y1];
		this._textureCoordsArray = new Float32Array(textureCoordinates);
		this._reloadTextureCoords();
	}

	willRender(params: ParamsType): void {
		if (!this._gl || !this._shaderProgram) {
			return;
		}
		const gl = this._gl;
		const shaderProgram = this._shaderProgram;

		gl.bindBuffer(gl.ARRAY_BUFFER, this._vertexCoordsBuffer);
		gl.vertexAttribPointer(this._vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);
		gl.bindBuffer(gl.ARRAY_BUFFER, this._textureCoordsBuffer);
		gl.vertexAttribPointer(this._textureCoordAttribute, 2, gl.FLOAT, false, 0, 0);

		// Point scale: the ratio of intrinsic coords over screen coords (points).
		const pointScale = Math.abs(this._modelBounds.size.width / this.canvas.size.width);

		const lineWidth = params.lineWidth || 1.0;
		const uLineWidth = gl.getUniformLocation(shaderProgram, "uLineWidth");
		gl.uniform1f(uLineWidth, lineWidth * pointScale);

		// Grid scale
		let cartesianScale = 0;
		let polarScale = 0;
		const showPhase = params.showPhaseGrid;
		if (params.showCartesianGrid && params.cartesianScale > 0) {
			cartesianScale = params.cartesianScale;
		}
		if (params.showAbsGrid && params.absScale > 0) {
			polarScale = params.absScale;
		}
		const uCartesianScale = gl.getUniformLocation(shaderProgram, "uCartesianScale");
		const uPolarScale = gl.getUniformLocation(shaderProgram, "uPolarScale");
		const uShowPhase = gl.getUniformLocation(shaderProgram, "uShowPhase");
		gl.uniform1f(uCartesianScale, cartesianScale);
		gl.uniform1f(uPolarScale, polarScale);
		gl.uniform1i(uShowPhase, showPhase ? 1 : 0);
	}

	// May fail catastrophically if renderTo and renderFrom are not called first.
	render(params: ParamsType): void {
		this.willRender(params);
		if (!this._gl) {
			return;
		}
		const gl = this._gl;
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._vertexIndexBuffer);
		gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
	}

	_createShaderProgram(
		gl: WebGLRenderingContext, vertexCode: string, fragmentCode: string
	): WebGLProgram | null {
		console.log("_createShaderProgram");
		const fragmentShader = this._compileShader(gl, fragmentCode, gl.FRAGMENT_SHADER);
		const vertexShader = this._compileShader(gl, vertexCode, gl.VERTEX_SHADER);
		const shaderProgram = gl.createProgram();

		if (!fragmentShader || !vertexShader || !shaderProgram) {
			return null;
		}
		gl.attachShader(shaderProgram, vertexShader);
		gl.attachShader(shaderProgram, fragmentShader);
		gl.linkProgram(shaderProgram);
		if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
			return null;
		}

		gl.useProgram(shaderProgram);
		// Shaders using this renderer must contain these attributes.
		this._vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
		gl.enableVertexAttribArray(this._vertexPositionAttribute);
		this._textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
		gl.enableVertexAttribArray(this._textureCoordAttribute);

		return shaderProgram;
	}

	// type should be gl.FRAGMENT_SHADER or gl.VERTEX_SHADER
	_compileShader(
		gl: WebGLRenderingContext, text: string, type: number
	): WebGLShader | null {
		const shader = gl.createShader(type);
		if (shader == null) {
			return null;
		}
		gl.shaderSource(shader, text);
		gl.compileShader(shader);
		if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
			// TODO: handle this error.
			console.log("An error occurred compiling the shaders: ", gl.getShaderInfoLog(shader));
			return null;
		}
		return shader;
	}

	_reloadTextureCoords(): void {
		if (!this._gl || !this._textureCoordsArray || !this._textureCoordsBuffer) {
			return;
		}
		this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._textureCoordsBuffer);
		this._gl.bufferData(this._gl.ARRAY_BUFFER, this._textureCoordsArray, this._gl.STATIC_DRAW);
	}

	_initBuffers(gl: WebGLRenderingContext): void {
		console.log("PlaneShaderRenderer._initBuffers");
		// We're rendering planar data to a flat rect, so the
		// vertices never change, and represent the edges of the canvas --
		// just draw two static triangles between the corners.
		const vertices = [-1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0];
		const vertexIndices = [0, 1, 2, 0, 2, 3];
		this._vertexCoordsBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this._vertexCoordsBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
		this._vertexIndexBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._vertexIndexBuffer);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);

		this._textureCoordsBuffer = gl.createBuffer();
		this._reloadTextureCoords();
	}
}
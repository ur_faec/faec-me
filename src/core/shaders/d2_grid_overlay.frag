/* extern */ precision highp float;
/* extern */ float PI;
/* extern */ float EPSILON;
/* extern */ struct D2Complex { vec2 z; vec2 dzdh; vec2 dzdv; };

uniform float uLineWidth;

uniform int uShowPhase;
uniform float uCartesianScale;
uniform float uPolarScale;

// Should be ~0 in a region of diameter ~lineWidth at the origin.
float decayFactor(float x, float dx) {
  float radius = 0.6 * uLineWidth;
  if (abs(x) >= radius) {
		return 1.0;
	}
  // If uLineWidth * dx >= 1, the lines are pretty much solid, so dim
  // the maximum intensity to let the background through.
  float density = min(uLineWidth * dx, 1.0);
  float minimum = density * 0.75;
	return minimum + (1.0 - minimum) * abs(0.5 * x / radius);
}

// Offset of x from the integer grid, ignoring integer multiples of n.
float offsetFromGridExcept(int n, float x) {
	float below = floor(x);
    float above = ceil(x);
    if (abs(mod(below, float(n))) < 0.5) {
        below -= 1.0;
    }
    if (abs(mod(above, float(n))) < 0.5) {
        above += 1.0;
    }
	return min(x-below, above-x);
}

float offsetFromGrid(float x) {
	return min(x-floor(x), ceil(x)-x);
}

float distanceFromNearestInt(float x) {
	return min(x-floor(x), ceil(x)-x);
}

// Distance from the "grid" {..., 1/16, 1/8, 1/4, 1/2, 1, 2, 4, 8, 16, ...}
float offsetFromExpGrid(float x) {
	float below = pow(2.0, max(floor(log2(x+0.001)), 0.0));
	float above = pow(2.0, max(ceil(log2(x+0.001)), 0.0));
	return min(abs(x-below), abs(above-x));
}

float lightGridFn(float x) {
  return offsetFromGrid(x);//ExpGrid(x);
}

float darkGridFn(float x) {
  return offsetFromGrid(x);
}

float _norm(float a, float b) {
  return sqrt(a * a + b * b);
}

float _norm(vec2 v) {
  return sqrt(dot(v, v));
}

// Returns the saturation and value to overlay a polar / cartesian grid
// on a plot.
vec2 D2GridOverlay(D2Complex value) {
  float zAbs = _norm(value.z);
  //float zAbs = sqrt(dot(value.z, value.z));
  vec2 dzAbs = vec2(
    _norm(value.dzdh),
    _norm(value.dzdv));
  //float dzdhAbs = sqrt(dot(value.dzdh, value.dzdh)) + EPSILON;
  //float dzdvAbs = sqrt(dot(value.dzdv, value.dzdv)) + EPSILON;
  float dzAbsNorm = _norm(dzAbs) + EPSILON;

  float phase = atan(value.z.y, value.z.x);
  vec2 dPhase = dzAbs / (zAbs + EPSILON);
  float dPhaseNorm = _norm(dPhase);

  float saturation = 1.0;
  /*if (uShowPhase != 0) {
    float phaseOffset;
    if (uCartesianScale > 0.0) {
      //phaseOffset = offsetFromGridExcept(4, 16.0*phase / (2.0 * PI));//16.0*phase / (2.0 * PI));
      phaseOffset = offsetFromGridExcept(3, 12.0*phase / (2.0 * PI));
    } else {
      //phaseOffset = offsetFromGrid(16.0 * phase / (2.0 * PI));
      phaseOffset = offsetFromGrid(12.0 * phase / (2.0 * PI));
    }
    saturation *=
        decayFactor(phaseOffset / (PI * dPhaseNorm), dPhaseNorm * 6.0);
  }
  if (uPolarScale > 0.0) {
    float absFactor = 1.0;
    if (zAbs > 0.5) {
        float absOffset = lightGridFn(zAbs * uPolarScale) / uPolarScale;
        absFactor = decayFactor(absOffset / dzAbsNorm, dzAbsNorm * uPolarScale);
    }
    saturation *= absFactor;
  }*/
  float intensity = 1.0;
  if (uCartesianScale > 0.0) {
    float imagOffset =
        distanceFromNearestInt(value.z.y * uCartesianScale) / uCartesianScale;
    //    darkGridFn(abs(value.z.y * uCartesianScale) + 1.0) / uCartesianScale;

    float dImag = _norm(value.dzdh[1], value.dzdv[1]);
    float imagFactor =
        decayFactor(imagOffset / dImag, dImag);
    float realOffset =
        darkGridFn(abs(value.z.x * uCartesianScale) + 1.0) / uCartesianScale;
    float dReal = _norm(value.dzdh[0], value.dzdv[0]);
    float realFactor =
        decayFactor(realOffset / dReal, dReal * uCartesianScale);
    intensity = imagFactor;// * realFactor;
  }
  return vec2(saturation, intensity);
}

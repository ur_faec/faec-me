/* extern */ precision highp float;
/* extern */ float PI;
/* extern */ float EPSILON;

uniform float uLineWidth;

uniform int uShowPhase;
uniform float uCartesianScale;
uniform float uPolarScale;

// Should be ~0 in a region of diameter ~lineWidth at the origin.
float decayFactor(float x, float dx) {
  float radius = 0.6 * uLineWidth;
  if (abs(x) >= radius) {
		return 1.0;
	}
  // If uLineWidth * dx >= 1, the lines are pretty much solid, so dim
  // the maximum intensity to let the background through.
  float density = min(uLineWidth * dx, 1.0);
  float minimum = density * 0.75;
	return minimum + (1.0 - minimum) * abs(0.5 * x / radius);
}

// Offset of x from the integer grid, ignoring integer multiples of n.
float offsetFromGridExcept(int n, float x) {
	float below = floor(x);
    float above = ceil(x);
    if (abs(mod(below, float(n))) < 0.5) {
        below -= 1.0;
    }
    if (abs(mod(above, float(n))) < 0.5) {
        above += 1.0;
    }
	return min(x-below, above-x);
}

float offsetFromGrid(float x) {
	return min(x-floor(x), ceil(x)-x);
}

// Distance from the "grid" {..., 1/16, 1/8, 1/4, 1/2, 1, 2, 4, 8, 16, ...}
float offsetFromExpGrid(float x) {
	float below = pow(2.0, max(floor(log2(x+0.001)), 0.0));
	float above = pow(2.0, max(ceil(log2(x+0.001)), 0.0));
	return min(abs(x-below), abs(above-x));
}

float lightGridFn(float x) {
  return offsetFromGrid(x);//ExpGrid(x);
}

float darkGridFn(float x) {
  return offsetFromGrid(x);
}

// Returns the saturation and value to overlay a polar / cartesian grid
// on a plot.
vec2 GridOverlay(vec4 value) {
  float vabs = sqrt(dot(value.xy, value.xy));
  float dvabs = sqrt(dot(value.zw, value.zw)) + EPSILON;
  float phase = atan(value.y, value.x);// / (2.0 * PI) + 0.5;
  float dphase = dvabs / (vabs + EPSILON);

  float saturation = 1.0;
  if (uShowPhase != 0) {
    float phaseOffset;
    if (uCartesianScale > 0.0) {
      //phaseOffset = offsetFromGridExcept(4, 16.0*phase / (2.0 * PI));//16.0*phase / (2.0 * PI));
      phaseOffset = offsetFromGridExcept(3, 12.0*phase / (2.0 * PI));
    } else {
      //phaseOffset = offsetFromGrid(16.0 * phase / (2.0 * PI));
      phaseOffset = offsetFromGrid(12.0 * phase / (2.0 * PI));
    }
    saturation *= decayFactor(phaseOffset / (PI * dphase), dphase * 6.0);
  }
  if (uPolarScale > 0.0) {
    float absFactor = 1.0;
    if (vabs > 0.5) {
        float absOffset = lightGridFn(vabs * uPolarScale) / uPolarScale;
        absFactor = decayFactor(absOffset / dvabs, dvabs * uPolarScale);//1.0);
    }
    saturation *= absFactor;
  }
  float intensity = 1.0;
  if (uCartesianScale > 0.0) {
    float imagOffset =
        darkGridFn(abs(value.y * uCartesianScale) + 1.0) / uCartesianScale;
    float imagFactor = decayFactor(imagOffset / dvabs, dvabs * uCartesianScale);
    float realOffset =
        darkGridFn(abs(value.x * uCartesianScale) + 1.0) / uCartesianScale;
    float realFactor = decayFactor(
      realOffset / dvabs, dvabs * uCartesianScale);
    intensity = imagFactor * realFactor;
  }
  return vec2(saturation, intensity);
}

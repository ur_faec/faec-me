/* extern */ precision highp float;

/* extern */ float EPSILON;
/* extern */ float PI;

/* extern */ struct D2Complex { vec2 z; vec2 dzdh; vec2 dzdv; };

varying highp vec2 vTextureCoord;

uniform highp vec2 uDxDh;
uniform highp vec2 uDyDh;
uniform highp vec2 uDxDv;
uniform highp vec2 uDyDv;

uniform highp vec2 uCenterX;
uniform highp vec2 uCenterY;

D2Complex D2ComplexInit(vec2 z, vec2 dzdx, vec2 dzdy);
D2Complex D2ComplexConstant(vec2 z);
D2Complex D2ComplexNegative(D2Complex v);
D2Complex D2ComplexInverse(D2Complex v);
D2Complex D2ComplexPlus(D2Complex v1, D2Complex v2);
D2Complex D2ComplexMinus(D2Complex v1, D2Complex v2);
D2Complex D2ComplexTimes(D2Complex v1, D2Complex v2);
D2Complex D2ComplexDividedBy(D2Complex numerator, D2Complex denominator);
D2Complex D2ComplexSquared(D2Complex v);
D2Complex D2ComplexConjugate(D2Complex v);

vec2 D2GridOverlay(D2Complex value);

vec3 HSVtoRGB(vec3 hsv);
vec2 GridOverlay(vec4 value);


// for now, hard-code to the explicit billiards path:
// 4 3 -5 -3 -4 4 5 -4
D2Complex billiards_eval(vec2 z) {
  float h = z.x;
  float v = z.y;
  D2Complex x = D2ComplexInit(uCenterX + h * uDxDh + v * uDxDv, uDxDh, uDxDv);
  D2Complex y = D2ComplexInit(uCenterY + h * uDyDh + v * uDyDv, uDyDh, uDyDv);
  D2Complex one = D2ComplexConstant(vec2(1, 0));
  D2Complex im = D2ComplexConstant(vec2(0, 1));
  D2Complex apex = D2ComplexPlus(x, D2ComplexTimes(y, im));
  // could break out xSquared too to save a little more time.
  D2Complex ySquared = D2ComplexSquared(y);
  D2Complex leftSquaredNorm = D2ComplexPlus(D2ComplexSquared(x), ySquared);
  D2Complex rightSquaredNorm =
    D2ComplexPlus(D2ComplexSquared(D2ComplexMinus(x, one)), ySquared);

  D2Complex a0 = D2ComplexDividedBy(D2ComplexSquared(apex), leftSquaredNorm);
  D2Complex a1 = D2ComplexDividedBy(
    D2ComplexSquared(D2ComplexMinus(apex, one)), rightSquaredNorm);

  D2Complex a0_2 = D2ComplexSquared(a0);
  D2Complex a0_4 = D2ComplexSquared(a0_2);
  D2Complex a0_5 = D2ComplexTimes(a0_4, a0);
  D2Complex a1_2 = D2ComplexSquared(a1);
  D2Complex a1_3 = D2ComplexTimes(a1_2, a1);
  D2Complex a1_4 = D2ComplexSquared(a1_2);

  D2Complex offsetSum = D2ComplexConstant(vec2(0, 0));
  D2Complex curVector = one;

  // step 0: 4
  curVector = D2ComplexTimes(D2ComplexNegative(curVector), a0_4);
  offsetSum = D2ComplexPlus(offsetSum, curVector);

  // step 1: 3
  curVector = D2ComplexTimes(D2ComplexNegative(curVector), a1_3);
  offsetSum = D2ComplexPlus(offsetSum, curVector);

  // step 2: -5
  curVector = D2ComplexTimes(
    D2ComplexNegative(curVector), D2ComplexConjugate(a0_5));
  offsetSum = D2ComplexPlus(offsetSum, curVector);

  // step 3: -3
  curVector = D2ComplexTimes(
    D2ComplexNegative(curVector), D2ComplexConjugate(a1_3));
  offsetSum = D2ComplexPlus(offsetSum, curVector);

  // step 4: -4
  curVector = D2ComplexTimes(
    D2ComplexNegative(curVector), D2ComplexConjugate(a0_4));
  offsetSum = D2ComplexPlus(offsetSum, curVector);

  // step 5: 4
  curVector = D2ComplexTimes(D2ComplexNegative(curVector), a1_4);
  offsetSum = D2ComplexPlus(offsetSum, curVector);

  // step 6: 5
  curVector = D2ComplexTimes(D2ComplexNegative(curVector), a0_5);
  offsetSum = D2ComplexPlus(offsetSum, curVector);
/*r
  // step 7: -4
  curVector = D2ComplexTimes(
    D2ComplexNegative(curVector), D2ComplexConjugate(a1_4));
  offsetSum = D2ComplexPlus(offsetSum, curVector);
*/
  return offsetSum;
}
/*
D2Complex billiards_eval(vec2 value) {
  float h = value.x;
  float v = value.y;
  D2Complex x = D2ComplexInit(uCenterX + h * uDxDh + v * uDxDv, uDxDh, uDxDv);
  D2Complex y = D2ComplexInit(uCenterY + h * uDyDh + v * uDyDv, uDyDh, uDyDv);
  D2Complex denom = D2ComplexPlus(D2ComplexSquared(x), D2ComplexSquared(y));

  D2Complex one = D2ComplexConstant(vec2(1, 0));
  D2Complex im = D2ComplexConstant(vec2(0, 1));
  D2Complex z = D2ComplexPlus(x, D2ComplexTimes(y, im));
  return D2ComplexDividedBy(z, denom);
  //D2ComplexTimes(D2ComplexPlus(z, one), D2ComplexMinus(z, one));
}
*/
void main(void) {
  // xy = function, zw = derivative
  D2Complex value = billiards_eval(vTextureCoord);

  float phase = atan(value.z.y, value.z.x);
  vec2 grid = D2GridOverlay(value);

  gl_FragColor = vec4(HSVtoRGB(vec3(phase, grid)), 1.0);//grid)), 1.0);
}

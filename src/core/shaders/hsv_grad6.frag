/* extern */ precision highp float;
/* extern */ float PI;

vec3 hueForPhase(float phase) {
    phase = mod(phase/(2.0*PI), 1.0);
    if (phase < 0.0) {
      phase += 1.0;
    }
    float scale;
    if (phase < 1.0 / 6.0) {
      scale = phase * 3.0 + 0.5;
      return vec3(0.6 * scale, 0.0, 0.6 * scale);
    }
    if (phase < 1.0 / 3.0) {
      scale = (phase - 1.0 / 6.0) * 3.0 + 0.5;
      return vec3(scale, 0.0, 0.0);
    }
    if (phase < 1.0 / 2.0) {
      scale = (phase - 1.0 / 3.0) * 3.0 + 0.5;
      return vec3(0.7 * scale, 0.4 * scale, 0.2 * scale);
    }
    if (phase < 2.0 / 3.0) {
      scale = (phase - 1.0 / 2.0) * 3.0 + 0.5;
      return vec3(0.0, scale, 0.0);
    }
    if (phase < 5.0 / 6.0) {
      scale = (phase - 2.0 / 3.0) * 3.0 + 0.5;
      return vec3(0.0, 0.6 * scale, 0.6 * scale);
    }
    scale = (phase - 5.0 / 6.0) * 3.0 + 0.5;
    return vec3(0.0, 0.0, scale);
}

// Scaling weirdness: "hue" is expected to be an angle measured in radians,
// sv are [0, 1].
vec3 HSVtoRGB(vec3 hsv) {
    vec3 hue = hueForPhase(hsv.x);
    return (hsv.y * hue + (1.0 - hsv.y)) * hsv.z;
}



// This file is a wrapper around all the shaders that exports them as
// strings, so all flow coverage failures are contained to a single file.

// Vertex shaders
import Identity from "./identity.vert";

// Fragment shaders
import BilliardsPhase from "./billiards_phase.frag";
import DComplex from "./dcomplex.frag";
import D2Complex from "./d2complex.frag";
import D2GridOverlay from "./d2_grid_overlay.frag";
import GridOverlay from "./grid_overlay.frag";
import Header from "./header.frag";
import HSVDefault from "./hsv_default.frag";
import HSVDouble from "./hsv_double.frag";
import HSVGrad6 from "./hsv_grad6.frag";
import HSVSplit3 from "./hsv_split3.frag";
import HSVSplit6 from "./hsv_split6.frag";
import HSVSplit12 from "./hsv_split12.frag";
import HSVTrans from "./hsv_trans.frag";
import PhaseCoeffs from "./phase_coeffs.frag";
import Phase from "./phase.frag";
import ProjectivePhase from "./projective_phase.frag";
import Texture from "./texture.frag";
import TextureBlob from "./textured_blob.frag";

const identity: string = Identity;

const billiardsPhase: string = BilliardsPhase;
const dcomplex: string = DComplex;
const d2complex: string = D2Complex;
const d2GridOverlay: string = D2GridOverlay;
const gridOverlay: string = GridOverlay;
const header: string = Header;
const hsvDefault: string = HSVDefault;
const hsvDouble: string = HSVDouble;
const hsvGrad6: string = HSVGrad6;
const hsvSplit3: string = HSVSplit3;
const hsvSplit6: string = HSVSplit6;
const hsvSplit12: string = HSVSplit12;
const hsvTrans: string = HSVTrans;
const phaseCoeffs: string = PhaseCoeffs;
const phase: string = Phase;
const projectivePhase: string = ProjectivePhase;
const texture: string = Texture;
const textureBlob: string = TextureBlob;

export const AllShaders = {
	vertex: { identity },
	fragment: {
		billiardsPhase,
		dcomplex,
		d2complex,
		d2GridOverlay,
		gridOverlay,
		header,
		hsvDefault,
		hsvDouble,
		hsvGrad6,
		hsvSplit3,
		hsvSplit6,
		hsvSplit12,
		hsvTrans,
		phaseCoeffs,
		phase,
		projectivePhase,
		texture,
		textureBlob,
	},
};
precision highp float;

varying highp vec2 vTextureCoord;

const float EPSILON = 0.000001;
const float PI = 3.141592653589793238462643383;

const int MAX_POLY_DEGREE = 8;

// The function being rendered
uniform highp vec2 uZeroes[MAX_POLY_DEGREE];
uniform highp vec2 uPoles[MAX_POLY_DEGREE];
uniform int uZeroCount;
uniform int uPoleCount;

// Externals
vec3 HSVtoRGB(vec3 hsv);
vec2 GridOverlay(vec4 value);

vec2 _complexInverse(vec2 z) {
	float scale = 1.0 / dot(z, z);
	return scale * vec2(z.x, -z.y);
}

vec2 _complexMul(vec2 z1, vec2 z2) {
	return vec2(z1.x * z2.x - z1.y * z2.y,
		z1.x * z2.y + z1.y * z2.x);
}

vec4 derivativeInverse(vec4 z) {
	// (1/z)' = -(1/z^2)(z')
	vec2 value = _complexInverse(z.xy);
	return vec4(
		value,
		-_complexMul(_complexMul(value, value), z.zw)
	);
}

vec4 derivativeMul(vec4 z1, vec4 z2) {
	return vec4(
		_complexMul(z1.xy, z2.xy),
		_complexMul(z1.zw, z2.xy) + _complexMul(z1.xy, z2.zw)
	);
}

vec4 eval(vec2 z) {
	vec4 numerator = vec4(1.0, 0.0, 0.0, 0.0);
	vec4 denominator = vec4(1.0, 0.0, 0.0, 0.0);
	for (int i = 0; i < MAX_POLY_DEGREE; i++) {
		if (i < uZeroCount) {
			numerator = derivativeMul(numerator, vec4(z - uZeroes[i], 1.0, 0.0));
		}
		if (i < uPoleCount) {
			denominator = derivativeMul(denominator, vec4(z - uPoles[i], 1.0, 0.0));
		}
	}
	return derivativeMul(numerator, derivativeInverse(denominator));
}

void main(void) {
	// xy = function, zw = derivative
	highp vec4 value = eval(vTextureCoord);

	float phase = atan(value.y, value.x);
	vec2 grid = GridOverlay(value);

	gl_FragColor = vec4(HSVtoRGB(vec3(phase, grid)), 1.0);
}

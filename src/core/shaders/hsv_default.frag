/* extern */ precision highp float;
/* extern */ float PI;

vec3 hueForPhase(float phase) {
  phase += PI * 1.2;
  vec3 offset = vec3(phase, phase + PI, 2.0*phase);
  vec3 v = sin(offset) + 1.0;
  vec3 hue = vec3(0.4 * v.x, 0.25*v.y, 0.4*v.z);
  hue = hue * hue;
  return hue / sqrt(dot(hue, hue));
}

// Scaling weirdness: "hue" is expected to be an angle measured in radians,
// sv are [0, 1].
vec3 HSVtoRGB(vec3 hsv) {
    vec3 hue = hueForPhase(hsv.x);
    return (hsv.y * hue + (1.0 - hsv.y)) * hsv.z;
}

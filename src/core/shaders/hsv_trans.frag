/* extern */ precision highp float;
/* extern */ float PI;

vec3 hueForPhase(float phase) {
    phase = mod(phase/(2.0*PI) + 0.5, 1.0);
    if (phase < 0.0) {
      phase += 1.0;
    }
    float delta;
    float scale;
    if (phase < 1.0 / 5.0) {
      delta = phase - 1.0 / 10.0;
      scale = 1.0 - 20.0*delta*delta;
      return scale * vec3(0.357, 0.808, 0.980);
    }
    if (phase < 2.0 / 5.0) {
      delta = phase - 3.0 / 10.0;
      scale = 1.0 - 20.0 * delta * delta;
      return scale*vec3(0.961, 0.663, 0.722);
    }
    if (phase < 3.0 / 5.0) {
      delta = phase - 5.0 / 10.0;
      scale = 1.0 - 20.0 * delta * delta;
      return scale*vec3(1.0, 1.0, 1.0);
    }
    if (phase < 4.0 / 5.0) {
      delta = phase - 7.0 / 10.0;
      scale = 1.0 - 20.0 * delta * delta;
      return scale*vec3(0.961, 0.663, 0.722);
    }
    delta = phase - 9.0 / 10.0;
    scale = 1.0 - 20.0*delta*delta;
    return scale * vec3(0.357, 0.808, 0.980);
}

// Scaling weirdness: "hue" is expected to be an angle measured in radians,
// sv are [0, 1].
vec3 HSVtoRGB(vec3 hsv) {
    vec3 hue = hueForPhase(hsv.x);
    return (hsv.y * hue + (1.0 - hsv.y)) * hsv.z;
}

precision highp float;

// Expects constant: MAX_POLY_DEGREE

varying highp vec2 vTextureCoord;

const float EPSILON = 0.000001;
const float PI = 3.141592653589793238462643383;

// Externals
vec3 HSVtoRGB(vec3 hsv);
vec2 GridOverlay(vec4 value);

vec2 complexInverse(vec2 z) {
    float scale = 1.0 / dot(z, z);
    return scale * vec2(z.x, -z.y);
}

vec2 complexMul(vec2 z1, vec2 z2) {
    return vec2(z1.x * z2.x - z1.y * z2.y,
                z1.x * z2.y + z1.y * z2.x);
}

vec4 derivativeInverse(vec4 z) {
  // (1/z)' = -(1/z^2)(z')
  vec2 value = complexInverse(z.xy);
  return vec4(
    value,
    -complexMul(complexMul(value, value), z.zw)
  );
}

vec4 derivativeMul(vec4 z1, vec4 z2) {
  return vec4(
    complexMul(z1.xy, z2.xy),
    complexMul(z1.zw, z2.xy) + complexMul(z1.xy, z2.zw)
  );
}

const int MAX_POLY_DEGREE = 10;

// The function being rendered
uniform highp vec2 uNumeratorCoeffs[MAX_POLY_DEGREE + 1];
uniform highp vec2 uDenominatorCoeffs[MAX_POLY_DEGREE + 1];
uniform int uNumeratorDegree;
uniform int uDenominatorDegree;

vec4 eval(vec2 z) {
  vec4 numerator = vec4(0.0, 0.0, 0.0, 0.0);
  vec4 denominator = vec4(0.0, 0.0, 0.0, 0.0);
  vec4 zCur = vec4(1.0, 0.0, 0.0, 0.0);
  for (int i = 0; i <= MAX_POLY_DEGREE; i++) {
    if (i <= uNumeratorDegree) {
      numerator += derivativeMul(zCur, vec4(uNumeratorCoeffs[i], 0.0, 0.0));
    }
    if (i <= uDenominatorDegree) {
      denominator += derivativeMul(
        zCur, vec4(uDenominatorCoeffs[i], 0.0, 0.0));
    }
    zCur = derivativeMul(zCur, vec4(z, 1.0, 0.0));
  }
  return derivativeMul(numerator, derivativeInverse(denominator));
}

// Maps from the unit circle to a flattening of the Riemann sphere.
vec2 projectFromDisc(vec2 discCoords) {

  return vec2(0.0, 0.0);
}

void main(void) {
  float magnitude = sqrt(
      vTextureCoord.x * vTextureCoord.x + vTextureCoord.y * vTextureCoord.y);
  if (magnitude >= 1.0) {
    gl_FragColor = vec4(0.25, 0.25, 0.25, 1.0);
  } else {
    // longitude angle on the Riemann sphere
    float theta = PI * magnitude;
    float complexMagnitude = sin(theta) / (cos(theta) + 1.0);
    float correctionCoeff = complexMagnitude / magnitude;
    highp vec2 complexPos = correctionCoeff * vTextureCoord;

    // xy = function, zw = derivative
    highp vec4 value = eval(complexPos);

    float phase = atan(value.y, value.x);
    //vec2 grid = GridOverlay(value);

    //gl_FragColor = vec4(HSVtoRGB(vec3(phase, grid)), 1.0);
    gl_FragColor = vec4(HSVtoRGB(vec3(phase, 1.0, 1.0)), 1.0);
  }
}

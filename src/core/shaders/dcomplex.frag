/* extern */ precision highp float;

vec2 complexInverse(vec2 z) {
  float scale = 1.0 / dot(z, z);
  return scale * vec2(z.x, -z.y);
}

vec2 complexTimes(vec2 z1, vec2 z2) {
  return vec2(z1.x * z2.x - z1.y * z2.y,
              z1.x * z2.y + z1.y * z2.x);
}

vec4 dcomplexInverse(vec4 z) {
  // (1/z)' = -(1/z^2)(z')
  vec2 value = complexInverse(z.xy);
  return vec4(
    value,
    -complexTimes(complexTimes(value, value), z.zw)
  );
}

vec4 dcomplexTimes(vec4 z1, vec4 z2) {
  return vec4(
    complexTimes(z1.xy, z2.xy),
    complexTimes(z1.zw, z2.xy) + complexTimes(z1.xy, z2.zw)
  );
}

vec4 dcomplexDivide(vec4 numerator, vec4 denominator) {
  return dcomplexTimes(numerator, dcomplexInverse(denominator));
}

vec4 dcomplexSquare(vec4 z) {
  return dcomplexTimes(z, z);
}

vec4 dcomplexConjugate(vec4 z) {
  return vec4(z.x, -z.y, z.z, -z.w);
}

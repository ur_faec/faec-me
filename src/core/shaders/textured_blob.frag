precision highp float;

varying vec2 vTextureCoord;

// Per-pixel data about how to combine the input hues
// x: interpolation coefficient from uTargetHue (0) to uInputHue (1)
// y: value (luminosity?)
// z: saturation
uniform sampler2D uTexture;

uniform sampler2D uColorLookup;

// These vectors follow the transformation rules described by
// color.js:ColorOpp.
uniform vec3 uTargetColor;
uniform vec3 uInputColor;

// uColorMod.[rg] are the mod values from the main input sliders, scaled
// between -1 and 1.
uniform vec2 uColorMod;

vec3 rgbForHue(vec2 hue) {
  float r = hue.x / 2.0 - (hue.y - 1.0) / 3.0;
  float g = -hue.x / 2.0 - (hue.y - 1.0) / 3.0;
  float b = (2.0 * hue.y + 1.0) / 3.0;
  return vec3(r, g, b);
}

void main(void) {
  vec3 inputColor = rgbForHue(uInputColor.yz);
  vec3 targetColor = rgbForHue(uTargetColor.yz);
  vec3 textureVal = texture2D(uTexture, vTextureCoord).xyz;

  vec3 combinedColor =
    (1.0 - textureVal.x) * inputColor + textureVal.x * targetColor;
  gl_FragColor = vec4(combinedColor, 1.0);
}

/* extern */ precision highp float;

// A complex value with partial derivatives for two parameters (h and v).
struct D2Complex {
  vec2 z; // The scalar value
  vec2 dzdh; // The partial derivative of z with respect to h
  vec2 dzdv; // The partial derivative of z with respect to v
};

D2Complex D2ComplexInit(vec2 z, vec2 dzdh, vec2 dzdv) {
  D2Complex v;
  v.z = z;
  v.dzdh = dzdh;
  v.dzdv = dzdv;
  return v;
}

D2Complex D2ComplexConstant(vec2 z) {
  D2Complex v;
  v.z = z;
  v.dzdh = vec2(0);
  v.dzdv = vec2(0);
  return v;
}

vec2 complexInverse(vec2 z) {
  float scale = 1.0 / dot(z, z);
  return scale * vec2(z.x, -z.y);
}

vec2 complexTimes(vec2 z1, vec2 z2) {
  return vec2(z1.x * z2.x - z1.y * z2.y,
              z1.x * z2.y + z1.y * z2.x);
}

vec2 complexConjugate(vec2 z) {
  return vec2(z.x, -z.y);
}

D2Complex D2ComplexInverse(D2Complex v) {
  // (1/z)' = -(1/z^2)(z')
  vec2 value = complexInverse(v.z);
  vec2 valueSquared = complexTimes(value, value);
  return D2ComplexInit(
    value,
    -complexTimes(valueSquared, v.dzdh),
    -complexTimes(valueSquared, v.dzdv));
}

D2Complex D2ComplexNegative(D2Complex v) {
  return D2ComplexInit(-v.z, -v.dzdh, -v.dzdv);
}

D2Complex D2ComplexPlus(D2Complex v1, D2Complex v2) {
  return D2ComplexInit(v1.z + v2.z, v1.dzdh + v2.dzdh, v1.dzdv + v2.dzdv);
}

D2Complex D2ComplexMinus(D2Complex v1, D2Complex v2) {
  return D2ComplexInit(v1.z - v2.z, v1.dzdh - v2.dzdh, v1.dzdv - v2.dzdv);
}

D2Complex D2ComplexTimes(D2Complex v1, D2Complex v2) {
  return D2ComplexInit(
    complexTimes(v1.z, v2.z),
    complexTimes(v1.dzdh, v2.z) + complexTimes(v1.z, v2.dzdh),
    complexTimes(v1.dzdv, v2.z) + complexTimes(v1.z, v2.dzdv)
  );
}

D2Complex D2ComplexDividedBy(D2Complex numerator, D2Complex denominator) {
  return D2ComplexTimes(numerator, D2ComplexInverse(denominator));
}

D2Complex D2ComplexSquared(D2Complex v) {
  return D2ComplexTimes(v, v);
}

D2Complex D2ComplexConjugate(D2Complex v) {
  return D2ComplexInit(
    complexConjugate(v.z),
    complexConjugate(v.dzdh),
    complexConjugate(v.dzdv));
}

import BigInt from "bn.js";

export class Rational {

	numerator: BigInt;
	denominator: BigInt;

	constructor(numerator: BigInt, denominator: BigInt) {
		if (denominator.isZero()) {
			throw "Division by zero";
		}
		const gcd = numerator.gcd(denominator);
		if (gcd.gtn(1)) {
			// Don't incur the cost of creating new objects unless we actually need
			// to reduce terms.
			this.numerator = numerator.div(gcd);
			this.denominator = denominator.div(gcd);
		} else {
			this.numerator = numerator;
			this.denominator = denominator;
		}
	}

	static fromNumbers(numerator: number, denominator: number): Rational {
		if (!Number.isSafeInteger(numerator) || !Number.isSafeInteger(denominator)) {
			throw "Rational.fromNumbers takes safe integers as input";
		}
		const num = new BigInt(numerator);
		const den = new BigInt(denominator);
		return new Rational(num, den);
	}

	static approximationForNumber(x: number, decimals: number): Rational {
		const xFloor = Math.floor(x);
		if (!Number.isSafeInteger(xFloor)) {
			throw "Rational can't approximate numbers outside the safe integer range";
		}
		if (decimals < 0 || decimals > 15) {
			throw "Rational.approximationForNumber: expected 0 <= decimals <= 15";
		}
		const floatRemainder = x - xFloor;
		const integerPart = new BigInt(xFloor);

		const floatDenominator = Math.pow(10, Math.floor(decimals));
		const denominator = new BigInt(floatDenominator);
		const numerator = new BigInt(Math.floor(floatRemainder * floatDenominator));
		return new Rational(numerator.add(integerPart.mul(denominator)), denominator);
	}

	static fromInt(i: number): Rational {
		return Rational.fromNumbers(i, 1);
	}

	static zero(): Rational {
		return Rational.fromNumbers(0, 1);
	}

	static one(): Rational {
		return Rational.fromNumbers(1, 1);
	}

	toString(): string {
		if (this.denominator.eqn(1)) {
			return String(this.numerator);
		}
		return `${this.numerator}/${this.denominator}`;
	}

	negative(): Rational {
		return new Rational(this.numerator.neg(), this.denominator);
	}

	plus(r: Rational): Rational {
		const num = this.numerator.mul(r.denominator).add(r.numerator.mul(this.denominator));
		const den = this.denominator.mul(r.denominator);
		return new Rational(num, den);
	}

	minus(r: Rational): Rational {
		const num = this.numerator.mul(r.denominator).sub(r.numerator.mul(this.denominator));
		const den = this.denominator.mul(r.denominator);
		return new Rational(num, den);
	}

	times(r: Rational): Rational {
		const num = this.numerator.mul(r.numerator);
		const den = this.denominator.mul(r.denominator);
		return new Rational(num, den);
	}

	inverse(): Rational {
		return new Rational(this.denominator, this.numerator);
	}

	dividedBy(r: Rational): Rational {
		const num = this.numerator.mul(r.denominator);
		const den = this.denominator.mul(r.numerator);
		return new Rational(num, den);
	}

	equals(r: Rational): boolean {
		return this.numerator.eq(r.numerator) && this.denominator.eq(r.denominator);
	}

	lessThan(r: Rational): boolean {
		return this.numerator.mul(r.denominator).lt(r.numerator.mul(this.denominator));
	}

	greaterThan(r: Rational): boolean {
		return r.lessThan(this);
	}

	atMost(r: Rational): boolean {
		return this.numerator.mul(r.denominator).lte(r.numerator.mul(this.denominator));
	}

	atLeast(r: Rational): boolean {
		return r.atMost(this);
	}

	// -1 if this < r, 0 if this == r, 1 if this > r
	compare(r: Rational): number {
		return this.numerator.mul(r.denominator).cmp(r.numerator.mul(this.denominator));
	}

	toNumber(): number {
		return this.numerator.toNumber() / this.denominator.toNumber();
	}
}
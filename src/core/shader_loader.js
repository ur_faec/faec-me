/*module.exports = function (content: string): string {
	//this.cacheable && this.cacheable();
	content = content.replace(/^\s*\/\* extern \*\/.*$/gm, '');
	this.value = content;
	return "module.exports = " + JSON.stringify(content);
};*/

module.exports = function(content) {
	content = content.replace(/^\s*\/\* extern \*\/.*$/gm, '');
	return `export default ${JSON.stringify(content)}`;
};
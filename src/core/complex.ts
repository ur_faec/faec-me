

export class Complex {

	x: number;
	y: number;

	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	copy(): Complex {
		return new Complex(this.x, this.y);
	}

	static zero(): Complex {
		return new Complex(0, 0);
	}

	static one(): Complex {
		return new Complex(1, 0);
	}

	static fromPolar(abs: number, phase: number): Complex {
		return new Complex(abs * Math.cos(phase), abs * Math.sin(phase));
	}

	static fromCartesian(x: number, y: number): Complex {
		return new Complex(x, y);
	}

	static fromXY(x: number, y: number): Complex {
		return new Complex(x, y);
	}

	static fromReal(x: number): Complex {
		return new Complex(x, 0);
	}
	static fromTuple(t: [number, number]): Complex {
		return new Complex(t[0], t[1]);
	}

	phase(): number {
		return Math.atan2(this.y, this.x);
	}

	abs(): number {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	plus(w: Complex): Complex {
		return new Complex(this.x + w.x, this.y + w.y);
	}

	minus(w: Complex): Complex {
		return new Complex(this.x - w.x, this.y - w.y);
	}

	times(w: Complex): Complex {
		return new Complex(this.x * w.x - this.y * w.y, this.x * w.y + this.y * w.x);
	}

	timesScalar(w: number): Complex {
		return new Complex(this.x * w, this.y * w);
	}

	dividedBy(w: Complex): Complex {
		return this.times(w.inverse());
	}

	negate(): Complex {
		return new Complex(-this.x, -this.y);
	}

	invert(): Complex {
		console.log("use Complex.inverse instead of Complex.invert");
		return this.inverse();
	}

	inverse(): Complex {
		const abs = this.abs();
		const scale = 1.0 / (abs * abs);
		return new Complex(this.x * scale, -this.y * scale);
	}

	conjugate(): Complex {
		this.y = -this.y;
		return this;
	}

	asArray(): Array<number> {
		return [this.x, this.y];
	}

	toString(): string {
		return `${this.x} + ${this.y}im`;
	}
}

export class TangentComplex {

	z: Complex;
	dz: Complex;

	constructor(z: Complex, dz: Complex) {
		this.z = z;
		this.dz = dz;
	}

	copy(): TangentComplex {
		return new TangentComplex(this.z, this.dz);
	}

	static zero(): TangentComplex {
		return new TangentComplex(Complex.zero(), Complex.zero());
	}

	static one(): TangentComplex {
		return new TangentComplex(Complex.one(), Complex.zero());
	}

	plus(w: TangentComplex): TangentComplex {
		return new TangentComplex(this.z.plus(w.z), this.dz.plus(w.dz));
	}

	minus(w: TangentComplex): TangentComplex {
		return new TangentComplex(this.z.minus(w.z), this.dz.minus(w.dz));
	}

	times(w: TangentComplex): TangentComplex {
		return new TangentComplex(this.z.times(w.z), this.z.times(w.dz).plus(this.dz.times(w.z)));
	}

	timesScalar(w: number): TangentComplex {
		return new TangentComplex(this.z.timesScalar(w), this.dz.timesScalar(w));
	}

	// d(1/b) = -(1/b^2)db
	// d(a/b) = (da)/b + a d(1/b) = (da)/b - a(db)/b^2
	//   = [(da)b - a(db)]/b^2
	dividedBy(w: TangentComplex): TangentComplex {
		const num = this.dz.times(w.z).minus(this.z.times(w.dz));
		const den = w.z.times(w.z);
		return new TangentComplex(this.z.dividedBy(this.dz), num.dividedBy(den));
	}

	invert(): TangentComplex {
		const num = this.dz.negate();
		const den = this.z.times(this.z);
		return new TangentComplex(this.z.invert(), num.dividedBy(den));
	}

	conjugate(): TangentComplex {
		return new TangentComplex(this.z.conjugate(), this.dz.conjugate());
	}

	toString(): string {
		return `TangentComplex(${this.z.toString()}, ${this.dz.toString()})`;
	}
}

export class ComplexVector {

	length: number;
	array: Float64Array;

	// Internal constructor, do not call directly
	constructor(length: number, array: Float64Array) {
		this.length = length;
		this.array = array;
	}

	static fromLength(length: number): ComplexVector {
		return new ComplexVector(length, new Float64Array(length * 2));
	}

	// Takes the coefficients as a flattened array with tuples of real values
	static fromFlatList(list: Array<number>): ComplexVector {
		return new ComplexVector(list.length / 2, new Float64Array(list));
	}

	static fromComplexList(list: Array<Complex>): ComplexVector {
		const array = new Float64Array(list.length * 2);
		for (let i = 0; i < list.length; i++) {
			array[2 * i] = list[i].x;
			array[2 * i + 1] = list[i].y;
		}
		return new ComplexVector(list.length, array);
	}

	copy(): ComplexVector {
		return new ComplexVector(this.length, new Float64Array(this.array));
	}

	get(i: number): Complex {
		return Complex.fromCartesian(this.array[i * 2], this.array[i * 2 + 1]);
	}

	set(i: number, z: Complex): void {
		this.extend(i + 1);
		this.array[i * 2] = z.x;
		this.array[i * 2 + 1] = z.y;
	}

	// Multiply the vector in place by a complex scalar.
	timesComplex(z: Complex): ComplexVector {
		const length = this.length;
		//var out = new Float64Array(v);
		for (let i = 0; i < length; i++) {
			const x = this.array[i * 2];
			const y = this.array[i * 2 + 1];
			this.array[i * 2] = z.x * x - z.y * y;
			this.array[i * 2 + 1] = z.x * y + z.y * x;
		}
		return this;
	}

	plus(v: ComplexVector): ComplexVector {
		this.extend(v.length);
		for (let i = 0; i < v.length * 2; i++) {
			this.array[i] += v.array[i];
		}
		return this;
	}

	minus(v: ComplexVector): ComplexVector {
		this.extend(v.length);
		for (let i = 0; i < v.length * 2; i++) {
			this.array[i] -= v.array[i];
		}
		return this;
	}

	extend(newLength: number): ComplexVector {
		if (newLength > this.length) {
			// Exceeds current logical length.
			if (!this.array || this.array.length < newLength * 2) {
				// Exceeds current underlying array length.
				const array = new Float64Array(newLength * 2);
				if (this.array) {
					array.set(this.array);
				}
				this.array = array;
			} else {
				// The data length of the array is already large enough, just make
				// sure the new entries don't have leftover data.
				for (let i = this.length; i < newLength; i++) {
					this.array[2 * i] = 0;
					this.array[2 * i + 1] = 0;
				}
			}
			this.length = newLength;
		}
		return this;
	}

	truncate(newLength: number): ComplexVector {
		this.length = newLength;
		return this;
	}

	shift(delta = 1): ComplexVector {
		if (this.array && this.array.length >= (this.length + delta) * 2) {
			this.array.copyWithin(2, 0, this.length * 2);
			this.length += delta;
		} else {
			const array = new Float64Array((this.length + delta) * 2);
			if (this.array) {
				array.set(this.array, 2 * delta);
			}
			this.array = array;
			this.length = this.length + delta;
		}
		return this;
	}
}
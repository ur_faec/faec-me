

import { Coords3d, OffsetCoords3d } from "core/geometry";
import { CoordinateTransform3d } from "core/coordinate_transform";
import { Quaternion } from "core/quaternion";

export class RotationTransform3d implements CoordinateTransform3d {

	q: Quaternion;

	constructor(q: Quaternion) {
		this.q = q.copy();
	}

	static fromQuaternion(q: Quaternion): RotationTransform3d {
		return new RotationTransform3d(q);
	}

	static rotationAroundAxisByAngle(axis: OffsetCoords3d, angle: number): RotationTransform3d {
		axis = axis.normalized();

		const cos = Math.cos(angle / 2);
		const sin = Math.sin(angle / 2);
		const q = Quaternion.fromRIJK(cos, sin * axis.dx, sin * axis.dy, sin * axis.dz);
		return new RotationTransform3d(q);
	}

	transformCoords(coords: Coords3d): Coords3d {
		const c = Quaternion.fromRIJK(0, coords.x, coords.y, coords.z);
		const rotated = this.q.copy().iTimes(c).iDividedBy(this.q);
		return Coords3d.fromXYZ(rotated.i, rotated.j, rotated.k);
	}

	transformOffset(tcoords: OffsetCoords3d): OffsetCoords3d {
		const c = Quaternion.fromRIJK(0, tcoords.dx, tcoords.dy, tcoords.dz);
		const rotated = this.q.copy().iTimes(c).iDividedBy(this.q);
		return OffsetCoords3d.fromDxDyDz(rotated.i, rotated.j, rotated.k);
	}

	inverse(): RotationTransform3d {
		const qi = this.q.copy().invert();
		return RotationTransform3d.fromQuaternion(qi);
	}
}
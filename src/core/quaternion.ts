

import { OffsetCoords3d } from "core/geometry";

export class Quaternion {

	r: number;
	i: number;
	j: number;
	k: number;

	constructor(r: number, i: number, j: number, k: number) {
		this.r = r;
		this.i = i;
		this.j = j;
		this.k = k;
	}

	copy(): Quaternion {
		return new Quaternion(this.r, this.i, this.j, this.k);
	}

	static fromReal(r: number): Quaternion {
		return new Quaternion(r, 0, 0, 0);
	}

	static fromRIJK(r: number, i: number, j: number, k: number): Quaternion {
		return new Quaternion(r, i, j, k);
	}

	static unitFromOffsetCoords3d(tcoords: OffsetCoords3d): Quaternion {
		const length: number = tcoords.length();
		return new Quaternion(0, tcoords.dx / length, tcoords.dy / length, tcoords.dz / length);
	}

	iTimesReal(r: number): Quaternion {
		this.r *= r;
		this.i *= r;
		this.j *= r;
		this.k *= r;
		return this;
	}

	iTimes(q: Quaternion): Quaternion {
		const {
			r,
			i,
			j,
			k,
		} = this;

		this.r = r * q.r - (i * q.i + j * q.j + k * q.k);
		this.i = r * q.i + i * q.r + (j * q.k - k * q.j);
		this.j = r * q.j + j * q.r + (k * q.i - i * q.k);
		this.k = r * q.k + k * q.r + (i * q.j - j * q.i);

		return this;
	}

	invert(): Quaternion {
		const {
			r,
			i,
			j,
			k,
		} = this;
		const coeff = 1.0 / (r * r + i * i + j * j + k * k);
		this.r *= coeff;
		this.i *= -coeff;
		this.j *= -coeff;
		this.k *= -coeff;
		return this;
	}

	inverse(): Quaternion {
		return this.copy().inverse();
	}

	iDividedBy(q: Quaternion): Quaternion {
		return this.iTimes(q.inverse());
	}

	iPlus(q: Quaternion): Quaternion {
		this.r += q.r;
		this.i += q.i;
		this.j += q.j;
		this.k += q.k;
		return this;
	}

	iMinus(q: Quaternion): Quaternion {
		this.r -= q.r;
		this.i -= q.i;
		this.j -= q.j;
		this.k -= q.k;
		return this;
	}

	abs(): number {
		const {
			r,
			i,
			j,
			k,
		} = this;
		return Math.sqrt(r * r + i * i + j * j + k * k);
	}

	normalize(): Quaternion {
		return this.iTimesReal(1.0 / this.abs());
	}
}


import { RenderTarget } from "core/canvas";
import { IsoScaleTransform } from "core/coordinate_transform";
import { Coords2d } from "core/geometry";
import { AxisRect } from "core/polygon";

export class PlaneRenderer<ParamsType> {

	target: RenderTarget;
	modelBounds: AxisRect;
	backgroundColor: string | null | undefined;

	constructor(target: RenderTarget) {
		this.target = target;
		this.modelBounds = AxisRect.fromCenterAndDimensions(Coords2d.origin(), 2, 2);
		this.backgroundColor = "white";
	}

	willRender(params: ParamsType): void {}

	render(params: ParamsType): void {
		this.willRender(params);
		const canvas = this.target.canvas;
		const ctx = canvas.element.getContext('2d');
		if (ctx == null) {
			return;
		}
		ctx.save();
		ctx.scale(canvas.renderScale, canvas.renderScale);
		const canvasBounds = this.target.bounds;
		ctx.beginPath();
		ctx.rect(canvasBounds.origin.x, canvasBounds.origin.y, canvasBounds.size.width, canvasBounds.size.height);
		if (this.backgroundColor != null) {
			ctx.fillStyle = this.backgroundColor;
			ctx.fill();
		}
		ctx.clip();

		const transform = this.modelToCanvasTransform();
		this._render(ctx, this.modelFrame(), transform, params);

		ctx.restore();
	}

	modelToCanvasTransform(): IsoScaleTransform {
		return IsoScaleTransform.transformOnto(this.modelBounds, this.target.bounds, this.target.invertY);
	}

	canvasToModelTransform(): IsoScaleTransform {
		return this.modelToCanvasTransform().inverse();
	}

	modelFrame(): AxisRect {
		const transform = this.canvasToModelTransform();
		return this.target.bounds.applyTransform(transform);
	}

	// This is the function subclasses should override to render a
	// custom view. Called with the rendering context and a transform
	// from model space to rendering context coords.
	_render(
		ctx: CanvasRenderingContext2D,
		modelFrame: AxisRect,
		modelToCanvas: IsoScaleTransform,
		params: ParamsType): void {}
}
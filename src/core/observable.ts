export type Observer<Value> = (oldValue: Value, newValue: Value) => void;

export class Observable<Value> {

	value: Value;
	observers: Array<Observer<Value>>;

	constructor(value: Value) {
		this.value = value;
		this.observers = [];
	}

	addObserver(observer: Observer<Value>): void {
		this.observers.push(observer);
	}

	set(value: Value): void {
		const oldValue = this.value;
		this.value = value;
		for (const observer of this.observers) {
			observer(oldValue, value);
		}
	}
}
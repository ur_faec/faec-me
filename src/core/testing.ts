
// In your testing file, add:
//   import { JestObjectEquality } from "core/testing";
//   ...
//   expect.extend(JestObjectEquality);
// this will allow expect(thing).toEqualObject(otherThing), to test equality
// by calling thing.equals(otherThing).

declare global {
	namespace jest {
		interface Matchers<R> {
				toEqualObject: (expected: any) => CustomMatcherResult;
		}
	}
}

export const JestObjectEquality = {
	toEqualObject(
		received: any,
		expected: any
	): jest.CustomMatcherResult {
		if (received.equals(expected)) {
			return {
				pass: true,
				message: () => `Expected ${received} not to equal ${expected}`,
			};
		}
		return {
			pass: false,
			message: () => `Expected ${received} to equal ${expected}`,
		};
	},
};

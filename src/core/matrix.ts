

import { Complex } from "core/complex";
import { CoordinateTransform3d } from "core/coordinate_transform";
import { Coords3d, OffsetCoords3d, TangentCoords3d } from "core/geometry";
import { Quaternion } from "core/quaternion";
import { RotationTransform3d } from "core/rotation_transform";

type ComplexTriple = [Complex, Complex, Complex];

// 3d vector, treated as row.
export class ComplexLinearFunctional3d {

	v: ComplexTriple;

	constructor(v: ComplexTriple) {
		this.v = [v[0].copy(), v[1].copy(), v[2].copy()];
	}

	copy(): ComplexLinearFunctional3d {
		return new ComplexLinearFunctional3d(this.v);
	}

	static fromRealArray(rv: [number, number, number]): ComplexLinearFunctional3d {
		const cv: ComplexTriple =
			[Complex.fromReal(rv[0]), Complex.fromReal(rv[1]), Complex.fromReal(rv[2])];
		return new ComplexLinearFunctional3d(cv);
	}

	static fromReals(x: number, y: number, z: number): ComplexLinearFunctional3d {
		return ComplexLinearFunctional3d.fromRealArray([x, y, z]);
	}

	static fromComplexArray(cv: ComplexTriple): ComplexLinearFunctional3d {
		return new ComplexLinearFunctional3d(cv);
	}

	static fromOffsetCoords3d(tcoords: OffsetCoords3d): ComplexLinearFunctional3d {
		return ComplexLinearFunctional3d.fromReals(tcoords.dx, tcoords.dy, tcoords.dz);
	}

	static fromCoords3d(coords: Coords3d): ComplexLinearFunctional3d {
		return ComplexLinearFunctional3d.fromReals(coords.x, coords.y, coords.z);
	}

	dual(): ComplexVector3d {
		const v: ComplexTriple =
			[this.v[0].copy().conjugate(), this.v[1].copy().conjugate(), this.v[2].copy().conjugate()];
		return new ComplexVector3d(v);
	}

	transpose(): ComplexVector3d {
		return this.dual();
	}

	timesComplexVector3d(vector3d: ComplexVector3d): Complex {
		const v1 = this.v;
		const v2 = vector3d.v;
		return v1[0].copy().times(v2[0]).plus(v1[1].copy().times(v2[1])).plus(v1[2].copy().times(v2[2]));
	}

	applyComplexMatrix3x3(m: ComplexMatrix3x3): ComplexLinearFunctional3d {
		const v = this.v;
		const mv = m.entries;
		const newv = [v[0].copy().times(mv[0]).plus(v[1].copy().times(mv[3])).plus(v[2].copy().times(mv[6])), v[0].copy().times(mv[1]).plus(v[1].copy().times(mv[4])).plus(v[2].copy().times(mv[7])), v[0].copy().times(mv[2]).plus(v[1].copy().times(mv[5])).plus(v[2].copy().times(mv[8]))];
		this.v[0] = newv[0];
		this.v[1] = newv[1];
		this.v[2] = newv[2];
		return this;
	}

	timesComplexMatrix3x3(m: ComplexMatrix3x3): ComplexLinearFunctional3d {
		return this.applyComplexMatrix3x3(m);
	}

	clipToOffsetCoords3d(): OffsetCoords3d {
		const v = this.v;
		return OffsetCoords3d.fromDxDyDz(v[0].x, v[1].x, v[2].x);
	}
}

// 3d vector, treated as column.
export class ComplexVector3d {

	v: ComplexTriple;

	constructor(v: ComplexTriple) {
		this.v = [v[0].copy(), v[1].copy(), v[2].copy()];
	}

	copy(): ComplexVector3d {
		return new ComplexVector3d(this.v);
	}

	static fromRealArray(rv: [number, number, number]): ComplexVector3d {
		const cv: ComplexTriple =
			[Complex.fromReal(rv[0]), Complex.fromReal(rv[1]), Complex.fromReal(rv[2])];
		return new ComplexVector3d(cv);
	}

	static fromReals(x: number, y: number, z: number): ComplexVector3d {
		return ComplexVector3d.fromRealArray([x, y, z]);
	}

	static fromComplexArray(cv: [Complex, Complex, Complex]): ComplexVector3d {
		return new ComplexVector3d(cv);
	}

	static fromOffsetCoords3d(tcoords: OffsetCoords3d): ComplexVector3d {
		return ComplexVector3d.fromReals(tcoords.dx, tcoords.dy, tcoords.dz);
	}

	static fromCoords3d(coords: Coords3d): ComplexVector3d {
		return ComplexVector3d.fromReals(coords.x, coords.y, coords.z);
	}

	dual(): ComplexLinearFunctional3d {
		const v: ComplexTriple =
			[this.v[0].copy().conjugate(), this.v[0].copy().conjugate(), this.v[0].copy().conjugate()];
		return new ComplexLinearFunctional3d(v);
	}

	transpose(): ComplexLinearFunctional3d {
		return this.dual();
	}

	applyComplexMatrix3x3(m: ComplexMatrix3x3): ComplexVector3d {
		const v = this.v;
		const mv = m.entries;
		const newv = [v[0].copy().times(mv[0]).plus(v[1].copy().times(mv[1])).plus(v[2].copy().times(mv[2])), v[0].copy().times(mv[3]).plus(v[1].copy().times(mv[4])).plus(v[2].copy().times(mv[5])), v[0].copy().times(mv[6]).plus(v[1].copy().times(mv[7])).plus(v[2].copy().times(mv[8]))];
		this.v[0] = newv[0];
		this.v[1] = newv[1];
		this.v[2] = newv[2];
		return this;
	}

	clipToOffsetCoords3d(): OffsetCoords3d {
		const v = this.v;
		return OffsetCoords3d.fromDxDyDz(v[0].x, v[1].x, v[2].x);
	}

	// TODO: timesFunctional should return a 3x3 matrix
}

export class ComplexMatrix3x3 {

	entries: Array<Complex>;

	constructor(entries: Array<Complex>) {
		if (entries.length != 9) {
			throw "ComplexMatrix3x3 requires 9 entries";
		}
		this.entries = entries.map(z => z.copy());
	}

	copy(): ComplexMatrix3x3 {
		return new ComplexMatrix3x3(this.entries);
	}

	static identity(): ComplexMatrix3x3 {
		return ComplexMatrix3x3.fromRealArray([1, 0, 0, 0, 1, 0, 0, 0, 1]);
	}

	// Takes a length-9 array.
	static fromRealArray(realEntries: Array<number>): ComplexMatrix3x3 {
		const complexEntries = realEntries.map(r => Complex.fromReal(r));
		return new ComplexMatrix3x3(complexEntries);
	}

	static fromComplexArray(complexEntries: Array<Complex>): ComplexMatrix3x3 {
		return new ComplexMatrix3x3(complexEntries);
	}

	static fromOffsetCoords3dBasis(basis: [OffsetCoords3d, OffsetCoords3d, OffsetCoords3d]): ComplexMatrix3x3 {
		return ComplexMatrix3x3.fromRealArray([basis[0].dx, basis[0].dy, basis[0].dz, basis[1].dx, basis[1].dy, basis[1].dz, basis[2].dx, basis[2].dy, basis[2].dz]);
	}

	static fromQuaternion(q: Quaternion): ComplexMatrix3x3 {
		// q.r is the cosine of the angle rotation, and q.{i,j,k} are the
		// sine of the angle rotation times the {x,y,z} coordinates of the rotation
		// axis. TODO: Are there nice shortcuts to take here?

		const transform = RotationTransform3d.fromQuaternion(q);
		const standardBasis = [OffsetCoords3d.fromDxDyDz(1, 0, 0), OffsetCoords3d.fromDxDyDz(0, 1, 0), OffsetCoords3d.fromDxDyDz(0, 0, 1)];
		const basis: [OffsetCoords3d, OffsetCoords3d, OffsetCoords3d] =
			[transform.transformOffset(standardBasis[0]), transform.transformOffset(standardBasis[1]), transform.transformOffset(standardBasis[2])];
		return ComplexMatrix3x3.fromOffsetCoords3dBasis(basis);
	}

	plus(m: ComplexMatrix3x3): ComplexMatrix3x3 {
		for (let i = 0; i < 9; i++) {
			this.entries[i] = this.entries[i].plus(m.entries[i]);
		}
		return this;
	}

	timesComplex(z: Complex): ComplexMatrix3x3 {
		for (let i = 0; i < 9; i++) {
			this.entries[i] = this.entries[i].times(z);
		}
		return this;
	}

	times(m: ComplexMatrix3x3): ComplexMatrix3x3 {
		const e1 = this.entries;
		const e2 = m.entries;
		const newEntries = [e1[0].copy().times(e2[0]).plus(e1[1].copy().times(e2[3])).plus(e1[2].copy().times(e2[6])), e1[0].copy().times(e2[1]).plus(e1[1].copy().times(e2[4])).plus(e1[2].copy().times(e2[7])), e1[0].copy().times(e2[2]).plus(e1[1].copy().times(e2[5])).plus(e1[2].copy().times(e2[8])), e1[3].copy().times(e2[0]).plus(e1[4].copy().times(e2[3])).plus(e1[5].copy().times(e2[6])), e1[3].copy().times(e2[1]).plus(e1[4].copy().times(e2[4])).plus(e1[5].copy().times(e2[7])), e1[3].copy().times(e2[2]).plus(e1[4].copy().times(e2[5])).plus(e1[5].copy().times(e2[8])), e1[6].copy().times(e2[0]).plus(e1[7].copy().times(e2[3])).plus(e1[8].copy().times(e2[6])), e1[6].copy().times(e2[1]).plus(e1[7].copy().times(e2[4])).plus(e1[8].copy().times(e2[7])), e1[6].copy().times(e2[2]).plus(e1[7].copy().times(e2[5])).plus(e1[8].copy().times(e2[8]))];
		this.entries[0] = newEntries[0];
		this.entries[1] = newEntries[1];
		this.entries[2] = newEntries[2];
		this.entries[3] = newEntries[3];
		this.entries[4] = newEntries[4];
		this.entries[5] = newEntries[5];
		this.entries[6] = newEntries[6];
		this.entries[7] = newEntries[7];
		this.entries[8] = newEntries[8];
		return this;
	}

	timesComplexVector3d(vector3d: ComplexVector3d): ComplexVector3d {
		return vector3d.copy().applyComplexMatrix3x3(this);
	}

	transpose(): ComplexMatrix3x3 {
		const e = this.entries;
		let tmp: Complex;
		tmp = e[1];e[1] = e[3];e[3] = tmp;
		tmp = e[2];e[2] = e[6];e[6] = tmp;
		tmp = e[5];e[5] = e[7];e[7] = tmp;
		return this;
	}
}

export class AffineTransform3d implements CoordinateTransform3d {

	origin: Coords3d;
	matrix: ComplexMatrix3x3;
	inverseMatrix: ComplexMatrix3x3;

	constructor() {
		this.origin = Coords3d.fromXYZ(0, 0, 0);
		this.matrix = ComplexMatrix3x3.identity();
		this.inverseMatrix = ComplexMatrix3x3.identity();
	}

	copy(): AffineTransform3d {
		const t = new AffineTransform3d();
		t.origin = this.origin.copy();
		t.matrix = this.matrix.copy();
		t.inverseMatrix = this.inverseMatrix.copy();
		return t;
	}

	static identity(): AffineTransform3d {
		return new AffineTransform3d();
	}

	// Similar to RotationTransform.rotationAroundAxisByAngle but takes a
	// RootedOffsetCoords3d to allow rotation around non-origin points.
	static rotationAroundRootedAxisByAngle(rootedAxis: TangentCoords3d, angle: number): AffineTransform3d {
		const axis = rootedAxis.copy();
		//axis.tcoords.normalize();
		const t = new AffineTransform3d();
		t.origin = axis.base;
		const transform = RotationTransform3d.rotationAroundAxisByAngle(axis.offset, angle);
		t.matrix = ComplexMatrix3x3.fromQuaternion(transform.q);
		t.inverseMatrix = ComplexMatrix3x3.fromQuaternion(transform.q.copy().invert());
		return t;
	}

	// Requires an AffineTransform3d, merges the two into a single transform.
	composeWith(t: AffineTransform3d): AffineTransform3d {
		const finalCenter = t.origin.plus(ComplexVector3d.fromCoords3d(this.origin).applyComplexMatrix3x3(t.matrix).clipToOffsetCoords3d());
		const finalMatrix = t.matrix.copy().times(this.matrix);

		this.origin = finalCenter;
		this.matrix = finalMatrix;
		// TODO: inverseMatrix
		return this;
	}

	transformCoords(input: Coords3d): Coords3d {
		const base = input.copy().asOffsetFrom(this.origin);
		const vectorBase = ComplexVector3d.fromOffsetCoords3d(base);
		return this.origin.plus(vectorBase.applyComplexMatrix3x3(this.matrix).clipToOffsetCoords3d());
	}

	transformOffset(input: OffsetCoords3d): OffsetCoords3d {
		const vector = ComplexVector3d.fromOffsetCoords3d(input);
		return vector.applyComplexMatrix3x3(this.matrix).clipToOffsetCoords3d();
	}

	transformTangentCoords(input: TangentCoords3d): TangentCoords3d {
		return TangentCoords3d.fromBaseAndOffset(this.transformCoords(input.base), this.transformOffset(input.offset));
	}

	inverse(): AffineTransform3d {
		const inverse = this.copy();
		const tmp = inverse.matrix;
		inverse.matrix = inverse.inverseMatrix;
		inverse.inverseMatrix = tmp;
		return inverse;
	}
}
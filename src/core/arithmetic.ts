export class Sign {
	static Positive = new Sign();
	static Negative = new Sign();

	private constructor() { }

	static of(value: number): Sign {
		if (value > 0) {
			return Sign.Positive;
		}
		if (value < 0) {
			return Sign.Negative;
		}
		throw `Sign.of(${value}): no sign`;
	}
}


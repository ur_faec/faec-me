

import { Coords2d } from "core/geometry";
import { Size, AxisRect } from "core/polygon";

declare let window: {
	devicePixelRatio: number;
};

export interface CanvasInputListener {
	mouseDown(x: number, y: number): void;
	mouseMove(x: number, y: number): void;
	mouseUp(x: number, y: number): void;
}

export type PositionCallback = (arg0: Coords2d, arg1: boolean) => void;
export type PositionListener = (arg0: Coords2d) => PositionCallback | null;

export class RenderTarget {

	canvas: Canvas;
	bounds: AxisRect;
	invertY: boolean;

	constructor(canvas: Canvas, bounds: AxisRect, invertY = true) {
		this.canvas = canvas;
		this.bounds = bounds;
		this.invertY = invertY;
	}

	static fromCanvasAndBounds(canvas: Canvas, bounds: AxisRect, invertY: boolean): RenderTarget {
		return new RenderTarget(canvas, bounds, invertY);
	}

	static fromCanvas(canvas: Canvas, invertY: boolean): RenderTarget {
		const bounds = AxisRect.fromOriginAndSize(Coords2d.origin(), canvas.size);
		return new RenderTarget(canvas, bounds, invertY);
	}
}

// Base class for shader-based renderers in the plane.
export class Canvas {
	element: HTMLCanvasElement;
	size: Size;
	// ratio of physical pixel density over point size for high def displays.
	renderScale: number;

	inputListener: CanvasInputListener | null;
	positionListeners: Array<PositionListener>;
	positionCallback: PositionCallback | null;

	_isMouseDown: boolean;
	_isTouchDown: boolean;

	constructor(size: Size, container: HTMLElement | null = null) {
		if (window.devicePixelRatio > 1) {
			this.renderScale = window.devicePixelRatio;
		} else {
			this.renderScale = 1;
		}
		this.size = size;
		this.element = this._createCanvasElement();
		if (container) {
			container.style.width = `${this.size.width + 4}px`;
			//container.style.margin = 'auto';
			//container.style.position = 'relative';
			container.append(this.element);
		}

		this.positionListeners = [];
		this.positionCallback = null;
		this.inputListener = null;

		this._isMouseDown = false;
		this._isTouchDown = false;

		this._addInternalListeners();
	}

	asRenderTarget(invertY: boolean): RenderTarget {
		return RenderTarget.fromCanvas(this, invertY);
	}

	renderTargetForBounds(bounds: AxisRect, invertY: boolean): RenderTarget {
		return RenderTarget.fromCanvasAndBounds(this, bounds, invertY);
	}

	_createCanvasElement(): HTMLCanvasElement {
		const width: number = this.size.width;
		const height: number = this.size.height;
		const canvas: HTMLCanvasElement = document.createElement("canvas"); 
		canvas.width = width * this.renderScale;
		canvas.height = height * this.renderScale;
		canvas.style.display = 'inline-block';
		canvas.style.backgroundColor = 'gray';
		canvas.style.width = `${width}px`;
		canvas.style.height = `${height}px`;
		return canvas;
	}

	_internalMouseDown(x: number, y: number): boolean {
		const coords = Coords2d.fromXY(x, y);
		for (const listener of this.positionListeners) {
			const callback = listener(coords);
			if (callback != null) {
				this.positionCallback = callback;
				return true;
			}
		}
		return false;
	}

	_internalMouseMove(x: number, y: number): void {
		const callback = this.positionCallback;
		if (callback != null) {
			callback(Coords2d.fromXY(x, y), false);
		}
	}

	_internalMouseUp(x: number, y: number): void {
		const callback = this.positionCallback;
		if (callback != null) {
			callback(Coords2d.fromXY(x, y), true);
			this.positionCallback = null;
		}
	}

	addPositionListener(listener: PositionListener): void {
		this.positionListeners.push(listener);
	}

	_addInternalListeners(): void {
		const canvasElement = this.element;
		canvasElement.addEventListener("mousedown", (event: MouseEvent) => {
			if (event.button != 0 || this._isTouchDown) {
				return;
			}
			const handled = this._internalMouseDown(event.offsetX, event.offsetY);
			if (handled) {
				this._isMouseDown = true;
				// Should there be a preventDefault here too?
			}
		});
		canvasElement.addEventListener("touchstart", (event: TouchEvent) => {
			if (event.changedTouches.length != 1) {
				return;
			}
			const rect = canvasElement.getBoundingClientRect();
			const handled: boolean = this._internalMouseDown(event.touches[0].clientX - rect.left, event.touches[0].clientY - rect.top);
			if (handled) {
				this._isTouchDown = true;
				event.preventDefault();
			}
		});
		canvasElement.addEventListener("mouseup", (event: MouseEvent) => {
			if (event.button != 0 || !this._isMouseDown) {
				return;
			}
			this._internalMouseUp(event.offsetX, event.offsetY);
		});
		canvasElement.addEventListener("touchend", (event: TouchEvent) => {
			this._isTouchDown = false;
		});

		canvasElement.addEventListener("mousemove", (event: MouseEvent) => {
			if (!this._isMouseDown) {
				return;
			}
			this._internalMouseMove(event.offsetX, event.offsetY);
		}, false);

		canvasElement.addEventListener("touchmove", (event: TouchEvent) => {
			if (!this._isTouchDown) {
				return;
			}
			const rect = canvasElement.getBoundingClientRect();
			this._internalMouseMove(event.touches[0].clientX - rect.left, event.touches[0].clientY - rect.top);
		}, false);
	}
}
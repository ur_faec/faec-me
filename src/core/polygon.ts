

import { CoordinateTransform2d, Transformable2d } from "core/coordinate_transform";
import { Coords2d, OffsetCoords2d, TangentCoords2d, Coords3d, OffsetCoords3d, TangentCoords3d } from "core/geometry";
//import { AffineTransform3d } from 'core/matrix';
import { RotationTransform3d } from "core/rotation_transform";

export class Size {
	// width and height must be >= 0
	width: number;
	height: number;

	constructor(width: number, height: number) {
		if (width < 0 || height < 0) {
			throw `Size(${String(width)}, ${String(height)}) ` + 'expected nonnegative numbers';
		}
		this.width = width;
		this.height = height;
	}

	static fromWH(width: number, height: number): Size {
		return new Size(width, height);
	}

	copy(): Size {
		return new Size(this.width, this.height);
	}

	asOffsetCoords(): OffsetCoords2d {
		return OffsetCoords2d.fromDxDy(this.width, this.height);
	}
}

export class AxisRect {

	origin: Coords2d;
	size: Size;
	constructor(origin: Coords2d, size: Size) {
		this.origin = origin;
		this.size = size;
	}

	static fromOriginAndSize(origin: Coords2d, size: Size): AxisRect {
		return new AxisRect(origin, size);
	}

	static fromXYWH(x: number, y: number, w: number, h: number): AxisRect {
		return new AxisRect(Coords2d.fromXY(x, y), Size.fromWH(w, h));
	}

	static fromOriginAndDiagonal(origin: Coords2d, diagonal: OffsetCoords2d): AxisRect {
		const x = Math.min(origin.x, origin.x + diagonal.dx);
		const y = Math.min(origin.y, origin.y + diagonal.dy);
		const w = Math.abs(diagonal.dx);
		const h = Math.abs(diagonal.dy);
		return AxisRect.fromXYWH(x, y, w, h);
	}

	static fromCenterAndDimensions(center: Coords2d, width: number, height: number): AxisRect {
		const origin = Coords2d.fromXY(center.x - width / 2, center.y - height / 2);
		const size = Size.fromWH(width, height);
		return new AxisRect(origin, size);
	}

	center(): Coords2d {
		return this.origin.plus(this.size.asOffsetCoords().timesReal(0.5));
	}

	containsCoords(coords: Coords2d): boolean {
		const containsX = (this.origin.x <= coords.x && coords.x < this.origin.x + this.size.width);
		const containsY = (this.origin.y <= coords.y && coords.y < this.origin.y + this.size.height);
		return (containsX && containsY);
	}

	vertexCoords(): Array<Coords2d> {
		const {
			x,
			y,
		} = this.origin;
		const {
			width,
			height,
		} = this.size;
		return [Coords2d.fromXY(x, y), Coords2d.fromXY(x + width, y), Coords2d.fromXY(x + width, y + height), Coords2d.fromXY(x, y + height)];
	}

	applyTransform(transform: CoordinateTransform2d): AxisRect {
		const origin = transform.transformCoords(this.origin);
		const diagonal = transform.transformOffset(this.size.asOffsetCoords());
		return AxisRect.fromOriginAndDiagonal(origin, diagonal);
	}

	addMargin(marginSize: number): AxisRect {
		return AxisRect.fromXYWH(this.origin.x - marginSize, this.origin.y - marginSize, this.size.width + 2 * marginSize, this.size.height + 2 * marginSize);
	}

	scaleBy(coefficient: number): AxisRect {
		return AxisRect.fromCenterAndDimensions(this.center(), coefficient * this.size.width, coefficient * this.size.height);
	}
}

// Inclusive of top left, exclusive of bottom right
export class Rect implements Transformable2d {

	origin: Coords2d;
	diagonal: OffsetCoords2d;
	constructor(origin: Coords2d, diagonal: OffsetCoords2d) {
		this.origin = origin;
		this.diagonal = diagonal;
	}

	copy(): Rect {
		return new Rect(this.origin, this.diagonal);
	}

	static fromCenterAndDimensions(center: Coords2d, width: number, height: number): Rect {
		return Rect.fromXYWH(center.x - width / 2, center.y - height / 2, width, height);
	}

	static fromOriginAndDiagonal(origin: Coords2d, diagonal: OffsetCoords2d): Rect {
		return new Rect(origin, diagonal);
	}

	static fromXYWH(x: number, y: number, width: number, height: number): Rect {
		return new Rect(Coords2d.fromXY(x, y), OffsetCoords2d.fromDxDy(width, height));
	}

	static fromDimensions(width: number, height: number): Rect {
		return Rect.fromCenterAndDimensions(Coords2d.origin(), width, height);
	}

	center(): Coords2d {
		return this.origin.plus(this.diagonal.timesReal(0.5));
	}

	vertexCoords(): Array<Coords2d> {
		const o = this.origin;
		const d = this.diagonal;
		return [Coords2d.fromXY(o.x, o.y), Coords2d.fromXY(o.x + d.dx, o.y), Coords2d.fromXY(o.x + d.dx, o.y + d.dy), Coords2d.fromXY(o.x, o.y + d.dy)];
	}

	width(): number {
		return Math.abs(this.diagonal.dx);
	}

	height(): number {
		return Math.abs(this.diagonal.dy);
	}

	applyTransform(transform: CoordinateTransform2d): void {
		this.origin = transform.transformCoords(this.origin);
		this.diagonal = transform.transformOffset(this.diagonal);
	}

	scaleBy(ratio: number): Rect {
		const center = this.center();
		return Rect.fromCenterAndDimensions(center, ratio * this.width(), ratio * this.height());
	}

	addMargin(marginSize: number): Rect {
		const center = this.center();
		return Rect.fromCenterAndDimensions(center, this.width() + 2 * marginSize, this.height() + 2 * marginSize);
	}
}


// Design issues: want to preserve the distinction so far between "vertices"
// or "points" and "coordinates" -- a point only has coordinates with respect
// to a particular basis or embedding or space or whatever. A triangle's corners
// should in principle be points, not coordinates. However, most manipulation
// of it, and most quantifiable questions about it, must be in terms of
// coordinates.

// Similarly at this point we need to start distinguishing more what
// properties are preserved across what spaces. E.g. if we say that points can
// have multiple coordinate representations, we need to ask what we mean by
// the length of an edge, or the angle of a corner.

// So here's one basic conclusion: spaces should be explicitly created, and
// every [T]Coords has an associated space. All operations on [T]Coords
// objects require that both are from the same space. (Though there should
// eventually be convenient sugar to build commuting networks of isomorphisms
// such that we can think of a particular [T]Coords as having a representation
// in many spaces at once.)

// The prototypical example is model space vs canvas space -- it should always
// be explicit which space a particular set of coordinates came from. We can
// extract the scalars and use them without that context if we need to, but
// any [T]Coords object should be with respect to an explicit basis, and
// that basis is immutable (the coords themselves can mutate but not the
// containing space).

// Or -- maybe that is overdoing it? Another way to look at it is that Coords
// are space-independent and are just the unitless coefficients of a basis,
// and by applying them to a defined coordinate space we can get a Point object
// (or something)?

// The details of point / vector arithmetic are a property of the forms that
// cover them. A point of the space could in principle be created in many
// ways, one of which is via coefficients representing the distance to move
// along each basis form. But even vectors that could in principle
// be described computationally can't necessarily be given explicit scalar
// form.

// When we extract a TCoords as the vector between two Coords, what we're really
// doing is extracting (a representation of) a geodesic between the two points.
// It is a locally-shortest path between them that in Euclidean space is also
// globally shortest.

// A geodesic is a special case of a path such that less data is needed to
// go between two points. If we wanted to represent an arbitrary path in
// a space, it could have unbounded levels of complexity. But a geodesic is
// always discrete: it can be entirely represented (albeit with possible
// numerical stability concerns) as a TCoords at its source and a scalar
// distance to travel in that direction. In a non-flat space, of course, there
// may be (infinitely) many such representations between any particular pair of
// points.

export class TrianglePerimeterPoint {

	edgeIndex: number;
	edgeCoeff: number;

	constructor(edgeIndex: number, edgeCoeff: number) {
		if (edgeIndex < 0 || edgeIndex >= 3) {
			throw "Invalid edge index for TrianglePerimeterPoint";
		}
		if (edgeCoeff < 0 || edgeCoeff > 1) {
			throw "TrianglePerimeterPoint requires an edge coefficient in " + " the range [0,1].";
		}
		this.edgeIndex = edgeIndex;
		this.edgeCoeff = edgeCoeff;
	}

	copy(): TrianglePerimeterPoint {
		return new TrianglePerimeterPoint(this.edgeIndex, this.edgeCoeff);
	}

	static fromVertexIndex(vertexIndex: number): TrianglePerimeterPoint {
		return new TrianglePerimeterPoint(vertexIndex, 0);
	}

	static fromEdgeIndexAndCoeff(edgeIndex: number, edgeCoeff: number): TrianglePerimeterPoint {
		return new TrianglePerimeterPoint(edgeIndex, edgeCoeff);
	}
}

export function Mod3(n: number): number {
	return (n % 3 + 3) % 3;
}

// Interior point
export class TriangleInteriorPoint {

	vertexCoefficients: Array<number>; // Length 3
	// Invariants:
	// vertexCoefficients[i] >= 0
	// sum(vertexCoefficients) = 1
	constructor(vertexCoefficients: Array<number>) {
		this.vertexCoefficients = vertexCoefficients.map(c => c);
	}

	copy(): TriangleInteriorPoint {
		return new TriangleInteriorPoint(this.vertexCoefficients);
	}

	static fromVertexIndex(vertexIndex: number): TriangleInteriorPoint {
		const coeffs = [0, 0, 0];
		coeffs[vertexIndex] = 1;
		return new TriangleInteriorPoint(coeffs);
	}

	static fromPerimeterPoint(perimeterPoint: TrianglePerimeterPoint): TriangleInteriorPoint {
		const coeffs = [0, 0, 0];
		coeffs[perimeterPoint.edgeIndex] = 1 - perimeterPoint.edgeCoeff;
		coeffs[(perimeterPoint.edgeIndex + 1) % 3] = perimeterPoint.edgeCoeff;
		return new TriangleInteriorPoint(coeffs);
	}

	// coeff ranges from 0 to 1 for point1 -> point2
	static fromInterpolation(interiorPoint1: TriangleInteriorPoint, interiorPoint2: TriangleInteriorPoint, coeff: number): TriangleInteriorPoint {
		const coeffs = [0, 0, 0];
		const c1 = interiorPoint1.vertexCoefficients;
		const c2 = interiorPoint2.vertexCoefficients;
		for (let i = 0; i < 3; i++) {
			coeffs[i] = (1 - coeff) * c1[i] + coeff * c2[i];
		}
		return new TriangleInteriorPoint(coeffs);
	}
}

export class Triangle3d {

	vertexCoords: Array<Coords3d>;

	constructor(vertexCoords: [Coords3d, Coords3d, Coords3d]) {
		this.vertexCoords = [vertexCoords[0].copy(), vertexCoords[1].copy(), vertexCoords[2].copy()];
	}

	copy(): Triangle3d {
		return Triangle3d.fromCoords(this.vertexCoords[0], this.vertexCoords[1], this.vertexCoords[2]);
	}

	static fromCoords(c1: Coords3d, c2: Coords3d, c3: Coords3d): Triangle3d {
		return new Triangle3d([c1, c2, c3]);
	}

	edge(edgeIndex: number): OffsetCoords3d {
		return this.vertexCoords[Mod3(edgeIndex + 1)].asOffsetFrom(this.vertexCoords[Mod3(edgeIndex)]);
	}

	rootedEdge(edgeIndex: number): TangentCoords3d {
		return this.vertexCoords[Mod3(edgeIndex + 1)].asTangentCoordsRootedAt(this.vertexCoords[Mod3(edgeIndex)]);
	}

	center(): Coords3d {
		const v = this.vertexCoords;
		return v[0].plus(v[1].asOffsetFrom(v[0]).timesReal(1.0 / 3.0)).plus(v[2].asOffsetFrom(v[0]).timesReal(1.0 / 3.0));
	}

	normal(): OffsetCoords3d {
		const v = this.vertexCoords;
		const e1 = v[1].asOffsetFrom(v[0]);
		const e2 = v[2].asOffsetFrom(v[0]);
		return e2.cross(e1).normalized();
	}

	// Rotates the remaining vertex coordinate by the given angle around the axis
	// from v[fromIndex] to v[toIndex].
	rotateAroundVertices(fromIndex: number, toIndex: number, angle: number): void {
		fromIndex = Mod3(fromIndex);
		toIndex = Mod3(toIndex);
		const v = this.vertexCoords;
		const axis = v[toIndex].asTangentCoordsRootedAt(v[fromIndex]);
		const transform = RotationTransform3d.rotationAroundAxisByAngle(axis.offset, angle);
		for (let i = 0; i < 3; i++) {
			if (i != fromIndex && i != toIndex) {
				const fromBaseCoords = v[i].asOffsetFrom(axis.base);
				this.vertexCoords[i] = axis.base.plus(transform.transformOffset(fromBaseCoords));
			}
		}
	}
}

export class Triangle2d implements Transformable2d {

	vertexCoords: Array<Coords2d>;

	constructor(vertexCoords: [Coords2d, Coords2d, Coords2d]) {
		this.vertexCoords = [vertexCoords[0].copy(), vertexCoords[1].copy(), vertexCoords[2].copy()];
	}

	copy(): Triangle2d {
		return Triangle2d.fromCoords(this.vertexCoords[0], this.vertexCoords[1], this.vertexCoords[2]);
	}

	static fromCoords(c1: Coords2d, c2: Coords2d, c3: Coords2d): Triangle2d {
		return new Triangle2d([c1, c2, c3]);
	}

	static fromSideLengths(a: number, b: number, c: number): Triangle2d {
		//throw "Triangle.fromSideLengths unimplemented";

		// Numerically stable Heron's formula:
		const area = Math.sqrt((a + (b + c)) * (c - (a - b)) * (c + (a - b)) * (a + (b - c)));

		// Now treat b as the length of the base, with the first vertex of the
		// triangle being the apex, which moves counterclockwise along a.
		const ascent = 2 * area / b;
		// The right-side root from the left side of the base.
		const rightLeft = b - Math.sqrt(c - ascent * ascent);
		// The left-side root from the right side of the base.
		const leftRight = Math.sqrt(a - ascent * ascent);
		let apexX;
		if (rightLeft < 0) {
			apexX = rightLeft;
		} else {
			apexX = leftRight;
		}
		return Triangle2d.fromCoords(Coords2d.fromXY(apexX, ascent), Coords2d.fromXY(0, 0), Coords2d.fromXY(b, 0));
	}

	coordsForPerimeterPoint(p: TrianglePerimeterPoint): Coords2d {
		const v1 = this.vertexCoords[p.edgeIndex];
		const v2 = this.vertexCoords[Mod3(p.edgeIndex + 1)];
		const e = v2.asOffsetFrom(v1);
		return v1.plus(e.timesReal(p.edgeCoeff));
	}

	coordsForInteriorPoint(p: TriangleInteriorPoint): Coords2d {
		const center = this.center();

		let result = center.copy();
		for (let i = 0; i < 3; i++) {
			const tcoords = this.vertexCoords[i].asOffsetFrom(center);
			result = result.plus(tcoords.timesReal(p.vertexCoefficients[i]));
		}
		return result;
	}

	nearestPerimeterPointTo(coords: Coords2d): TrianglePerimeterPoint {
		let bestPoint = null;
		let bestDist = 0;
		for (let i = 0; i < 3; i++) {
			const v1 = this.vertexCoords[i];
			const v2 = this.vertexCoords[Mod3(i + 1)];
			const e = v2.asTangentRootedAt(v1);
			let coeff = e.tangentCoefficientOf(coords);
			if (coeff < 0) {
				coeff = 0;
			}
			if (coeff > 1.0) {
				coeff = 1.0;
			}
			const p = e.base.plus(e.offset.timesReal(coeff));
			const dist = p.distanceFrom(coords);
			if (!bestPoint || dist < bestDist) {
				bestPoint = new TrianglePerimeterPoint(i, coeff);
				bestDist = dist;
			}
		}
		return bestPoint as TrianglePerimeterPoint;
	}

	nearestVertexTo(coords: Coords2d): number {
		let bestVertex = -1;
		let bestDist = 0;
		for (let i = 0; i < 3; i++) {
			const distance = coords.distanceFrom(this.vertexCoords[i]);
			if (bestVertex < 0 || distance < bestDist) {
				bestVertex = i;
				bestDist = distance;
			}
		}
		return bestVertex;
	}

	center(): Coords2d {
		const v = this.vertexCoords;
		return v[0].plus(v[1].asOffsetFrom(v[0]).timesReal(1.0 / 3.0)).plus(v[2].asOffsetFrom(v[0]).timesReal(1.0 / 3.0));
	}

	area(): number {
		const v = this.vertexCoords;
		const e: TangentCoords2d = v[1].asTangentRootedAt(v[0]);
		const baseCoords = e.nearestCoordsTo(v[2]);
		return v[2].distanceFrom(baseCoords) * e.offset.length() / 2;
	}

	/*normal (): OffsetCoords3d {
		let v = this.vertexCoords;
		var e1 = v[1].asOffsetFrom(v[0]);
		var e2 = v[2].asOffsetFrom(v[0]);
		return e2.cross(e1).normalize();
	}
	// The normal vector rooted at the center of the triangle.
	rootedNormal (): TangentCoords3d {
		return RootedTCoords.fromRootAndTangent(this.center(), this.normal());
	}*/
	edge(edgeIndex: number): OffsetCoords2d {
		return this.vertexCoords[Mod3(edgeIndex + 1)].asOffsetFrom(this.vertexCoords[Mod3(edgeIndex)]);
	}

	rootedEdge(edgeIndex: number): TangentCoords2d {
		return this.vertexCoords[Mod3(edgeIndex + 1)].asTangentRootedAt(this.vertexCoords[Mod3(edgeIndex)]);
	}

	vertex(vertexIndex: number): Coords2d {
		return this.vertexCoords[Mod3(vertexIndex)];
	}

	angleAtVertex(vertexIndex: number): number {
		const a = this.rootedEdge(vertexIndex + 2);
		const b = this.rootedEdge(vertexIndex);
		const dot = a.offset.normalized().timesReal(-1).dot(b.offset.normalized());
		return Math.acos(dot);
	}

	reflectThroughEdge(edgeIndex: number): Triangle2d {
		const edge = this.rootedEdge(edgeIndex);
		const vertexIndex = Mod3(edgeIndex + 2);
		this.vertexCoords[vertexIndex] = edge.reflectCoords(this.vertexCoords[vertexIndex]);
		return this;
	}

	applyTransform(transform: CoordinateTransform2d): void {
		this.vertexCoords[0] = transform.transformCoords(this.vertexCoords[0]);
		this.vertexCoords[1] = transform.transformCoords(this.vertexCoords[1]);
		this.vertexCoords[2] = transform.transformCoords(this.vertexCoords[2]);
	}
}
export class Permutation {

	forwardMap: Array<number>;
	inverseMap: Array<number>;

	_inverse: Permutation | null | undefined;

	constructor(forwardMap: Array<number>, inverseMap: Array<number>) {
		this.forwardMap = forwardMap;
		this.inverseMap = inverseMap;
	}

	static fromForwardMap(forwardMap: Array<number>): Permutation {
		const inverseMap: Array<number> = Array(forwardMap.length).fill(-1);
		for (let i = 0; i < forwardMap.length; i++) {
			// i maps to forwardMap[i],
			// so forwardMap[i] maps from i
			if (inverseMap[forwardMap[i]] != -1) {
				throw `forwardMap is not a permutation: ${String(forwardMap)}`;
			}
			inverseMap[forwardMap[i]] = i;
		}
		return new Permutation(forwardMap, inverseMap);
	}

	static fromInverseMap(inverseMap: Array<number>): Permutation {
		const forwardMap: Array<number> = Array(inverseMap.length).fill(-1);
		for (let i = 0; i < inverseMap.length; i++) {
			// i maps from inverseMap[i],
			// so inverseMap[i] maps to i
			forwardMap[inverseMap[i]] = i;
		}
		return new Permutation(forwardMap, inverseMap);
	}

	static identity(valueCount: number): Permutation {
		const values: Array<number> = [];
		for (let i = 0; i < valueCount; i++) {
			values.push(i);
		}
		return new Permutation(values, values);
	}

	static random(size: number): Permutation {
		const forwardMap: Array<number> = Array(size);
		const used: Array<boolean> = Array(size).fill(false);
		function nthUnused(n: number): number {
			let unusedCount = 0;
			for (let pos = 0; pos < used.length; pos++) {
				if (used[pos]) {
					continue;
				}
				if (unusedCount == n) {
					return pos;
				}
				unusedCount++;
			}
			return -1;
		}
		for (let i = 0; i < size; i++) {
			const randomChoice = Math.floor(Math.random() * (size - i));
			const index = nthUnused(randomChoice);
			used[index] = true;
			forwardMap[i] = index;
		}
		return Permutation.fromForwardMap(forwardMap);
	}

	static randomIrreducible(size: number): Permutation {
		let permutation = Permutation.random(size);
		while (!permutation.isIrreducible()) {
			permutation = Permutation.random(size);
		}
		return permutation;
	}

	get inverse(): Permutation {
		if (this._inverse == null) {
			this._inverse = new Permutation(this.inverseMap, this.forwardMap);
		}
		return this._inverse;
	}

	size(): number {
		return this.forwardMap.length;
	}

	isIrreducible(): boolean {
		const size = this.size();
		let highestIndex = 0;
		for (let i = 0; i < size - 1; i++) {
			highestIndex = Math.max(highestIndex, this.forwardMap[i]);
			if (highestIndex <= i) {
				return false;
			}
		}
		return true;
	}

	applyToArray<Element>(a: Array<Element>): Array<Element> {
		if (a.length != this.size()) {
			throw `Permutation of size ${this.size()} ` + `applied to array of length ${a.length}`;
		}
		const result: Array<Element> = [];
		for (let i = 0; i < this.size(); i++) {
			result.push(a[this.inverseMap[i]]);
		}
		return result;
	}

	// returns the permutation (i) => this(permutation(i))
	compose(permutation: Permutation): Permutation {
		const size = this.size();
		if (permutation.size() != size) {
			throw "Can't compose permutations of different sizes";
		}
		const forwardMap: Array<number> = Array(size);
		for (let i = 0; i < size; i++) {
			forwardMap[i] = this.forwardMap[permutation.forwardMap[i]];
		}
		return Permutation.fromForwardMap(forwardMap);
	}
}
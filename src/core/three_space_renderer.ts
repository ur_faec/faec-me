

import { Canvas } from "core/canvas";
import { Coords2d } from "core/geometry";
import { CoordinateTransform3d, CoordinateProjection, IdentityTransform3d,
	IsoScaleTransform }
	from "core/coordinate_transform";
import { AxisRect, Size } from "core/polygon";

export class ThreeSpaceRenderer<ParamsType> {

	canvas: Canvas;
	modelTransform: CoordinateTransform3d;
	viewportBounds: AxisRect;

	constructor(canvasSize: Size) {
		this.canvas = new Canvas(canvasSize);
		this.modelTransform = new IdentityTransform3d();
		this.viewportBounds = AxisRect.fromCenterAndDimensions(Coords2d.origin(), 1, 1);
	}

	willRender(params: ParamsType): void {}

	render(params: ParamsType): void {
		this.willRender(params);

		const ctx = this.canvas.element.getContext('2d');
		if (ctx == null) {
			return;
		}
		ctx.save();
		ctx.fillStyle = 'white';
		ctx.scale(this.canvas.renderScale, -this.canvas.renderScale);
		ctx.translate(0, -this.canvas.size.height);
		const size = this.canvas.size;
		ctx.beginPath();
		ctx.rect(0, 0, size.width, size.height);
		ctx.fill();
		ctx.clip();

		this._render(ctx, this.modelToCanvasProjection(), params);

		ctx.restore();
	}

	modelToCanvasProjection(): CoordinateProjection {
		const canvasRect = AxisRect.fromOriginAndSize(Coords2d.origin(), this.canvas.size);
		const canvasTransform = IsoScaleTransform.transformOnto(this.viewportBounds, canvasRect, true);
		return new CoordinateProjection(this.modelTransform, canvasTransform);
	}

	// This is the function subclasses should override to render a
	// custom view. Called with the rendering context and a transform
	// from model space to rendering context coords.
	_render(ctx: CanvasRenderingContext2D, projection: CoordinateProjection, params: ParamsType): void {}
}
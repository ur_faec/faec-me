

import { RenderTarget } from "core/canvas";
import { ElementsView } from "core/elements_view";
import { Renderable, Polygon } from "core/element_render";
import { Coords2d } from "core/geometry";
import { Observable } from "core/observable";
import { Permutation } from "core/permutation";
import { AxisRect } from "core/polygon";
import { Rational } from "core/rational";

import { Interval } from "./interval";
import { IntervalExchangeMap } from "./interval_exchange";

const _colors = ["#F9828E", "#D77CA1", "#AA7CA9", "#787BA4", "#477792", "#206F77", "#166458", "#24563A", "#2F4823", "#353813", "#352A0B", "#301C09"];

function _colorForIndex(index: number): string {
	if (index >= 0 && index < _colors.length) {
		return _colors[index];
	}
	return "gray";
}

export class IntervalExchangeModel extends ElementsView {

	intervalExchangeMap: Observable<IntervalExchangeMap>;
	_heightRatio: number;
	_rowHeight: number;

	constructor(renderTarget: RenderTarget, intervalExchangeMap: Observable<IntervalExchangeMap>, heightRatio = 0.065) {
		super(renderTarget);
		this._heightRatio = heightRatio;
		this._rowHeight = 0;
		this.intervalExchangeMap = intervalExchangeMap;
		this._addInputListener();
		this._updateForDomain(intervalExchangeMap.value.input.bounds());
		intervalExchangeMap.addObserver((oldValue: IntervalExchangeMap, newValue: IntervalExchangeMap) => {
			this._updateForDomain(newValue.input.bounds());
		});
	}

	_updateForDomain(interval: Interval): void {
		// Update the row height to scale with the function domain
		const width = interval.length.toNumber();
		this._rowHeight = this._heightRatio * width;

		// Update modelBounds to match the rendered rectangle.
		const renderBounds = AxisRect.fromXYWH(interval.leftBoundary.toNumber(), 0, interval.length.toNumber(), 2 * this._rowHeight);
		console.log("Setting modelBounds for IntervalExchangeModel");
		this.modelBounds = renderBounds.addMargin(0.1 * this._rowHeight);

		this.setNeedsRedraw();
	}

	rectForInterval(interval: Interval, row = 0): AxisRect {
		return AxisRect.fromXYWH(
			interval.leftBoundary.toNumber(), row * this._rowHeight,
			interval.length.toNumber(), this._rowHeight);
	}

	elementsToRender(frameRect: AxisRect): Array<Renderable> {
		const elements: Array<Renderable> = [];
		const map = this.intervalExchangeMap.value;
		for (let i = 0; i < map.input.intervals.length; i++) {
			const rect = this.rectForInterval(map.input.intervals[i]);
			const poly = Polygon.fromRect(rect);
			poly.style.strokeStyle = "black";
			poly.style.fillStyle = _colorForIndex(i);
			elements.push(poly);
		}
		for (let i = 0; i < map.output.intervals.length; i++) {
			const rect = this.rectForInterval(map.output.intervals[i], 1);
			const poly = Polygon.fromRect(rect);
			poly.style.strokeStyle = "black";
			poly.style.fillStyle = _colorForIndex(map.permutation.inverseMap[i]);
			elements.push(poly);
		}
		return elements;
	}

	_addInputListener(): void {
		this.renderTarget.canvas.addPositionListener((canvasCoords: Coords2d) => {
			const modelToCanvas = this.renderer.modelToCanvasTransform();
			const canvasToModel = modelToCanvas.inverse();

			const modelCoords = canvasToModel.transformCoords(canvasCoords);
			if (!this.modelBounds.containsCoords(modelCoords)) {
				return null;
			}

			const intervalExchangeMap = this.intervalExchangeMap.value;

			if (modelCoords.y < this._rowHeight) {
				// Editing input boundaries
				const intervals = intervalExchangeMap.input.intervals;
				let closestBoundary: {index: number;distance: number;} | null | undefined = null;
				for (let index = 0; index + 1 < intervals.length; index++) {
					const interval = intervals[index];
					const boundaryPosition = interval.rightBoundary.toNumber();
					const distance = Math.abs(modelCoords.x - boundaryPosition);
					if (!closestBoundary || distance < closestBoundary.distance) {
						closestBoundary = { index, distance };
					}
				}
				// Positions need to be within 20 pixels of a boundary (in canvas
				// coords) to be considered for dragging.
				const pixelSize = canvasToModel.isoScale();
				const maxDistance = pixelSize * 20;
				if (closestBoundary == null || closestBoundary.distance >= maxDistance) {
					return null;
				}

				// We found the closest point, and it's valid, so set up the listener
				// for any subsequent dragging.
				const targetIndex = closestBoundary.index;
				const minValue = intervals[targetIndex].leftBoundary.toNumber() + pixelSize;
				const maxValue = intervals[targetIndex + 1].rightBoundary.toNumber() - pixelSize;
				return (newCanvasCoords: Coords2d, final: boolean) => {
					const canvasOffset = newCanvasCoords.asOffsetFrom(canvasCoords);
					const modelOffset = canvasToModel.transformOffset(canvasOffset);
					let newPosition = modelCoords.x + modelOffset.dx;
					newPosition = Math.max(newPosition, minValue);
					newPosition = Math.min(newPosition, maxValue);
					const delta = newPosition - modelCoords.x;
					const rationalDelta = Rational.approximationForNumber(delta, 5);
					const newLengths = intervalExchangeMap.intervalLengths.map(x => x);
					newLengths[targetIndex] = newLengths[targetIndex].plus(rationalDelta);
					newLengths[targetIndex + 1] = newLengths[targetIndex + 1].minus(rationalDelta);
					this.intervalExchangeMap.set(IntervalExchangeMap.fromLengthsAndPermutation(newLengths, intervalExchangeMap.permutation));
				};
			} else {
				console.log("Looking for output selection");
				// Editing the output permutation
				const intervals = intervalExchangeMap.output.intervals;
				let selectedIndex: number | null | undefined = null;
				for (let index = 0; index < intervals.length; index++) {
					const interval = intervals[index];
					const leftBoundary = interval.leftBoundary.toNumber();
					const rightBoundary = interval.rightBoundary.toNumber();
					if (leftBoundary <= modelCoords.x && modelCoords.x < rightBoundary) {
						selectedIndex = index;
						break;
					}
				}
				if (selectedIndex != null) {
					// The original index of the moving interval.
					const oldIndex: number = selectedIndex;
					// The most recent index choice (while dragging).
					let currentIndex: number = oldIndex;
					console.log(`Dragging interval ${oldIndex}`);
					return (newCanvasCoords: Coords2d, final: boolean) => {
						const canvasOffset = newCanvasCoords.asOffsetFrom(canvasCoords);
						const modelOffset = canvasToModel.transformOffset(canvasOffset);
						const newPosition = modelCoords.x + modelOffset.dx;
						const delta = newPosition - modelCoords.x;
						const rationalDelta = Rational.approximationForNumber(delta, 5);
						const newLeftBoundary = intervals[oldIndex].leftBoundary.plus(rationalDelta);

						// Find the output interval index closest to newLeftBoundary
						let curPos = intervals[0].leftBoundary;
						let newIndex = 0;
						for (let i = 0; i < intervals.length; i++) {
							if (i == oldIndex) {
								continue;
							}
							const curLength = intervals[i].length;
							const centerPos = curPos.plus(curLength.dividedBy(Rational.fromInt(2)));
							if (centerPos.greaterThan(newLeftBoundary)) {
								break;
							}
							newIndex++;
							curPos = curPos.plus(curLength);
						}

						if (currentIndex != newIndex) {
							console.log(`Moving to index ${newIndex}`);
							currentIndex = newIndex;
							const forwardMap: Array<number> = intervalExchangeMap.permutation.forwardMap;
							const newForwardMap: Array<number> = forwardMap.map((outputIndex: number) => {
								if (outputIndex < Math.min(oldIndex, newIndex) || outputIndex > Math.max(oldIndex, newIndex)) {
									return outputIndex;
								}
								if (outputIndex == oldIndex) {
									return newIndex;
								}
								if (oldIndex < newIndex) {
									return outputIndex - 1;
								}
								return outputIndex + 1;
							});
							const newPermutation = Permutation.fromForwardMap(newForwardMap);
							this.intervalExchangeMap.set(IntervalExchangeMap.fromLengthsAndPermutation(intervalExchangeMap.intervalLengths, newPermutation));
						}
					};
				}
			}
			return null;
		});
	}
}
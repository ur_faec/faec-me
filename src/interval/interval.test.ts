import { Rational } from "core/rational";

import { Interval, DisjointIntervals } from "./interval";

test("Create an Interval", () => {
	const leftBoundary = Rational.one();
	const rightBoundary = Rational.fromNumbers(3, 2);
	const interval = new Interval(leftBoundary, rightBoundary);
	expect(String(interval.leftBoundary)).toEqual("1");
	expect(String(interval.rightBoundary)).toEqual("3/2");
	expect(String(interval.length)).toEqual("1/2");
});

test("Create a DisjointIntervals from interval lengths", () => {
	const di = DisjointIntervals.fromLengths([Rational.fromNumbers(1, 3), Rational.fromNumbers(2, 3)]);
	expect(String(di.intervals[0].leftBoundary)).toEqual("0");
	expect(String(di.intervals[0].rightBoundary)).toEqual("1/3");
	expect(String(di.intervals[1].leftBoundary)).toEqual("1/3");
	expect(String(di.intervals[1].rightBoundary)).toEqual("1");
});

import { IntervalExchangeMap } from "./interval_exchange";

import { Rational } from "core/rational";

import { Interval } from "./interval";

// A helper to construct rational fractions.
function Q(numerator: number, denominator: number | null = null) {
	if (!denominator) {
		return Rational.fromNumbers(numerator, 1);
	}
	return Rational.fromNumbers(numerator, denominator);
}

test("Test 2-cycle", () => {
	const interval = new Interval(Q(0), Q(1));
	const f = IntervalExchangeMap.linearCycleOnInterval(interval, 2);
	expect(f.input.intervals.length).toEqual(2);
	expect(f.output.intervals.length).toEqual(2);
	expect(f.input.intervals[0].leftBoundary.equals(Q(0))).toEqual(true);
	expect(f.input.intervals[1].leftBoundary.equals(Q(1, 2))).toEqual(true);
	expect(f.output.intervals[0].leftBoundary.equals(Q(0))).toEqual(true);
	expect(f.output.intervals[1].leftBoundary.equals(Q(1, 2))).toEqual(true);

	expect(f.applyToPosition(Q(-1))).toBeNull();
	expect(f.applyToPosition(Q(0))?.equals(Q(1, 2))).toEqual(true);
	expect(f.applyToPosition(Q(1, 4))?.equals(Q(3, 4))).toEqual(true);
	expect(f.applyToPosition(Q(1, 2))?.equals(Q(0))).toEqual(true);
	expect(f.applyToPosition(Q(2, 3))?.equals(Q(1, 6))).toEqual(true);
	expect(f.applyToPosition(Q(1))).toBeNull();
});

test("Test 5-cycle", () => {
	const interval = new Interval(Q(0), Q(1));
	const f = IntervalExchangeMap.linearCycleOnInterval(interval, 5);
	expect(f.input.intervals.length).toEqual(2);
	expect(f.output.intervals.length).toEqual(2);
	expect(f.input.intervals[0].leftBoundary.equals(Q(0))).toEqual(true);
	expect(f.input.intervals[1].leftBoundary.equals(Q(4, 5))).toEqual(true);
	expect(f.output.intervals[0].leftBoundary.equals(Q(0))).toEqual(true);
	expect(f.output.intervals[1].leftBoundary.equals(Q(1, 5))).toEqual(true);

	expect(f.applyToPosition(Q(-1, 10))).toBeNull();
	expect(f.applyToPosition(Q(0))?.equals(Q(1, 5))).toEqual(true);
	expect(f.applyToPosition(Q(1, 10))?.equals(Q(3, 10))).toEqual(true);
	expect(f.applyToPosition(Q(3, 7))?.equals(Q(22, 35))).toEqual(true);
	expect(f.applyToPosition(Q(4, 5))?.equals(Q(0))).toEqual(true);
	expect(f.applyToPosition(Q(9, 10))?.equals(Q(1, 10))).toEqual(true);
	expect(f.applyToPosition(Q(1))).toBeNull();
});




import { RenderTarget } from "core/canvas";
import { ElementsView } from "core/elements_view";
import { Polygon, Renderable } from "core/element_render";
import { Coords2d } from "core/geometry";
import { Permutation } from "core/permutation";
import { AxisRect } from "core/polygon";

//let _colors = ["blue", "purple", "darkred"];
const _colors = [
	"#F9828E", "#D77CA1", "#AA7CA9", "#787BA4",
	"#477792", "#206F77", "#166458", "#24563A",
	"#2F4823", "#353813", "#352A0B", "#301C09"];

/*let _colors = [
	"#FFD8F5", "#D0CCE5", "#A6BFCD", "#85AFB1", "#6F9E91", "#628B73",
	"#5B7757", "#556340", "#4E4F2F", "#453C22", "#382A19", "#291B11"];*/
function _colorForIndex(index: number): string {
	if (index >= 0 && index < _colors.length) {
		return _colors[index];
	}
	return "gray";
}

export class RauzyClassModel extends ElementsView {

	inputOrder: Permutation;
	outputOrder: Permutation;

	constructor(renderTarget: RenderTarget, initialPermutation: Permutation) {
		super(renderTarget);
		this.inputOrder = Permutation.identity(initialPermutation.size());
		this.outputOrder = initialPermutation;
		this.renderer.modelBounds = renderTarget.bounds;
		this._addInputListener();
	}

	randomize(): void {
		const size = this.inputOrder.size();
		const shuffle = Permutation.random(size);
		const permutation = Permutation.randomIrreducible(size);
		const inputOrder = shuffle;
		const outputOrder = permutation.compose(shuffle);
		this.inputOrder = inputOrder;
		this.outputOrder = outputOrder;
		this.setNeedsRedraw();
	}

	elementsToRender(frameRect: AxisRect): Array<Renderable> {
		const bounds = this.renderTarget.bounds;
		const intervalCount = this.inputOrder.size();
		const intervalWidth = bounds.size.width / intervalCount;
		const rowHeight = bounds.size.height / 2;
		const rectForInterval = (index: number, row: number): AxisRect => {
			return AxisRect.fromXYWH(
				bounds.origin.x + index * intervalWidth,
				bounds.origin.y + row * rowHeight, intervalWidth, rowHeight);
		};
		const elements: Array<Renderable> = [];
		for (let i = 0; i < this.inputOrder.size(); i++) {
			const inputRect = rectForInterval(i, 0);
			const inputIndex = this.inputOrder.inverseMap[i];
			let poly = Polygon.fromRect(inputRect);
			poly.style.strokeStyle = "black";
			poly.style.fillStyle = _colorForIndex(inputIndex);
			elements.push(poly);

			const outputRect = rectForInterval(i, 1);
			const outputIndex = this.outputOrder.inverseMap[i];
			poly = Polygon.fromRect(outputRect);
			poly.style.strokeStyle = "black";
			poly.style.fillStyle = _colorForIndex(outputIndex);
			elements.push(poly);
		}
		return elements;
	}

	rotateTop(): void {
		const intervalCount = this.inputOrder.size();
		const pivotIndex = this.outputOrder.inverseMap[intervalCount - 1];
		const pivotInputIndex = this.inputOrder.forwardMap[pivotIndex];
		const oldInverseMap = this.inputOrder.inverseMap;
		const newInverseMap: Array<number> = [];
		for (let i = 0; i < intervalCount; i++) {
			if (i <= pivotInputIndex) {
				newInverseMap.push(oldInverseMap[i]);
			} else if (i == pivotInputIndex + 1) {
				newInverseMap.push(oldInverseMap[intervalCount - 1]);
			} else {
				newInverseMap.push(oldInverseMap[i - 1]);
			}
		}
		this.inputOrder = Permutation.fromInverseMap(newInverseMap);
		this.setNeedsRedraw();
	}

	rotateBottom(): void {
		console.log("rotateBottom");
		const intervalCount = this.inputOrder.size();
		const pivotIndex = this.inputOrder.inverseMap[intervalCount - 1];
		const pivotOutputIndex = this.outputOrder.forwardMap[pivotIndex];
		const oldInverseMap = this.outputOrder.inverseMap;
		const newInverseMap: Array<number> = [];
		for (let i = 0; i < intervalCount; i++) {
			if (i <= pivotOutputIndex) {
				newInverseMap.push(oldInverseMap[i]);
			} else if (i == pivotOutputIndex + 1) {
				newInverseMap.push(oldInverseMap[intervalCount - 1]);
			} else {
				newInverseMap.push(oldInverseMap[i - 1]);
			}
		}
		this.outputOrder = Permutation.fromInverseMap(newInverseMap);
		this.setNeedsRedraw();
	}

	lastInputColor(): string {
		const inverseMap = this.inputOrder.inverseMap;
		return _colorForIndex(inverseMap[inverseMap.length - 1]);
	}

	lastOutputColor(): string {
		const inverseMap = this.outputOrder.inverseMap;
		return _colorForIndex(inverseMap[inverseMap.length - 1]);
	}

	_addInputListener(): void {
		this.renderTarget.canvas.addPositionListener(
			(canvasCoords: Coords2d) => {
				const bounds = this.renderTarget.bounds;
				const intervalCount = this.inputOrder.size();
				const intervalWidth = bounds.size.width / intervalCount;
				const rowHeight = bounds.size.height / 2;

				const modelToCanvas = this.renderer.modelToCanvasTransform();
				const canvasToModel = modelToCanvas.inverse();

				const modelCoords = canvasToModel.transformCoords(canvasCoords);
				const modelRect = this.renderer.modelFrame();
				if (!this.modelBounds.containsCoords(modelCoords)) {
					return null;
				}

				const selectedIndex = Math.floor(
					(modelCoords.x - modelRect.origin.x) / intervalWidth);
				const selectedRow = Math.floor(
					(modelCoords.y - modelRect.origin.y) / rowHeight);
				const originalPermutation =
					selectedRow == 0 ? this.inputOrder : this.outputOrder;
				// The most recent index choice (while dragging).
				let currentIndex: number = selectedIndex;
				return (newCanvasCoords: Coords2d, final: boolean): void => {
					const canvasOffset =
						newCanvasCoords.asOffsetFrom(canvasCoords);
					const modelOffset =
						canvasToModel.transformOffset(canvasOffset);
					const indexOffset =
						Math.round(modelOffset.dx / intervalWidth);
					const newIndex = selectedIndex + indexOffset;

					if (currentIndex != newIndex) {
						currentIndex = newIndex;
						const forwardMap: Array<number> =
							originalPermutation.forwardMap;
						const newForwardMap: Array<number> = forwardMap.map(
							(index: number) => {
								if (index < Math.min(selectedIndex, newIndex) ||
									index > Math.max(selectedIndex, newIndex)
								) {
									return index;
								}
								if (index == selectedIndex) {
									return newIndex;
								}
								if (selectedIndex < newIndex) {
									return index - 1;
								}
								return index + 1;
							});
						const newPermutation =
							Permutation.fromForwardMap(newForwardMap);
						if (selectedRow == 0) {
							this.inputOrder = newPermutation;
						} else {
							this.outputOrder = newPermutation;
						}
						this.setNeedsRedraw();
					}
				};
			});
	}
}
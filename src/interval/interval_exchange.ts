

import { Permutation } from "core/permutation";
import { Rational } from "core/rational";

import { Interval, DisjointIntervals } from "./interval";

export class IntervalExchangeMap {

	input: DisjointIntervals;
	output: DisjointIntervals;
	intervalLengths: Array<Rational>;
	permutation: Permutation;

	constructor(input: DisjointIntervals, output: DisjointIntervals, intervalLengths: Array<Rational>, permutation: Permutation) {
		this.input = input;
		this.output = output;
		this.intervalLengths = intervalLengths;
		this.permutation = permutation;
	}

	static fromLengthsAndPermutation(intervalLengths: Array<Rational>, permutation: Permutation): IntervalExchangeMap {
		const input = DisjointIntervals.fromLengths(intervalLengths);
		const outputLengths = permutation.applyToArray(intervalLengths);
		const output = DisjointIntervals.fromLengths(outputLengths);
		return new IntervalExchangeMap(input, output, intervalLengths, permutation);
	}

	static linearCycleOnInterval(interval: Interval, cycleLength: number): IntervalExchangeMap {
		const rationalCycleLength = Rational.fromInt(cycleLength);
		const delta = interval.length.dividedBy(rationalCycleLength);
		const intervalLengths = [interval.length.minus(delta), delta];
		const permutation = Permutation.fromForwardMap([1, 0]);
		return IntervalExchangeMap.fromLengthsAndPermutation(intervalLengths, permutation);
	}

	applyToPosition(pos: Rational): Rational | null | undefined {
		const posIndex: number | null | undefined = this.input.indexForPosition(pos);
		if (posIndex == null) {
			return null;
		}
		const inputIndex: number = posIndex;
		const inputInterval = this.input.intervals[inputIndex];
		const outputIndex = this.permutation.forwardMap[inputIndex];
		const outputInterval = this.output.intervals[outputIndex];
		return pos.plus(outputInterval.leftBoundary).minus(inputInterval.leftBoundary);
	}

	recurse(): IntervalExchangeInduction | null | undefined {
		return this.asInduction().recurse();
	}

	asInduction(): IntervalExchangeInduction {
		const applyCounts: Array<number> = Array(this.intervalLengths.length).fill(1);
		return IntervalExchangeInduction.fromLengthsPermutationAndMetadata(this.intervalLengths, this.permutation, applyCounts, Permutation.identity(this.intervalLengths.length).forwardMap);
	}

	bounds(): Interval {
		return new Interval(this.input.firstInterval.leftBoundary, this.input.lastInterval.rightBoundary);
	}
}

export class IntervalExchangeInduction extends IntervalExchangeMap {

	applyCounts: Array<number>;

	// For each input interval, the input index from which this induction is
	// descended in the original map.
	originalIndices: Array<number>;

	constructor(input: DisjointIntervals, output: DisjointIntervals, intervalLengths: Array<Rational>, permutation: Permutation, applyCounts: Array<number>, originalIndices: Array<number>) {
		super(input, output, intervalLengths, permutation);
		this.applyCounts = applyCounts;
		this.originalIndices = originalIndices;
	}

	// ...this name feels a little silly, but whatever, fight me
	static fromLengthsPermutationAndMetadata(intervalLengths: Array<Rational>, permutation: Permutation, applyCounts: Array<number>, originalIndices: Array<number>): IntervalExchangeInduction {
		const input = DisjointIntervals.fromLengths(intervalLengths);
		const outputLengths = permutation.applyToArray(intervalLengths);
		const output = DisjointIntervals.fromLengths(outputLengths);
		return new IntervalExchangeInduction(input, output, intervalLengths, permutation, applyCounts, originalIndices);
	}

	recurse(): IntervalExchangeInduction | null | undefined {
		const intervalLengths = this.intervalLengths;
		const applyCounts = this.applyCounts;
		const intervalCount = intervalLengths.length;

		const {
			forwardMap,
			inverseMap,
		} = this.permutation;
		const lastIndex = intervalCount - 1;
		// inputIndex: the rightmost input index that maps to lastIndex
		// outputIndex: the output index that lastIndex maps to
		const inputIndex = inverseMap[lastIndex];
		const outputIndex = forwardMap[lastIndex];

		if (intervalLengths[inputIndex].equals(intervalLengths[lastIndex])) {
			return null;
		}
		// We now want to apply the 2-step composition from
		// inputIndex -> lastIndex -> outputIndex
		// but the size of the two intervals differs so restrict to the smaller
		// of the two.
		if (intervalLengths[inputIndex].lessThan(intervalLengths[lastIndex])) {
			// the last input will be trimmed so interval inputIndex can map
			// through it.
			const newLengths = intervalLengths.slice(0, lastIndex);
			newLengths.push(intervalLengths[lastIndex].minus(intervalLengths[inputIndex]));

			const newApplyCounts = applyCounts.slice(0, intervalCount);
			newApplyCounts[inputIndex] = applyCounts[inputIndex] + applyCounts[lastIndex];

			const newForwardMap = forwardMap.map(i => i <= outputIndex ? i : i < lastIndex ? i + 1 : outputIndex + 1);
			const newPermutation = Permutation.fromForwardMap(newForwardMap);

			return IntervalExchangeInduction.fromLengthsPermutationAndMetadata(newLengths, newPermutation, newApplyCounts, this.originalIndices);
		}
		// intervalLengths[inputIndex] > intervalLengths[lastIndex]
		// interval inputIndex will be split and its right component will be
		// mapped twice, to outputIndex.
		const newLengths = intervalLengths.slice(0, lastIndex);
		newLengths.splice(inputIndex, 1, newLengths[inputIndex].minus(intervalLengths[lastIndex]), intervalLengths[lastIndex]);

		const newApplyCounts = applyCounts.slice(0, lastIndex);
		newApplyCounts.splice(inputIndex + 1, 0, applyCounts[inputIndex] + applyCounts[lastIndex]);

		const newForwardMap = forwardMap.slice(0, lastIndex);
		newForwardMap.splice(inputIndex + 1, 0, outputIndex);
		const newPermutation = Permutation.fromForwardMap(newForwardMap);

		const newOriginalIndices: Array<number> = this.originalIndices.map(i => i);
		newOriginalIndices[inputIndex + 1] = this.originalIndices[lastIndex];
		for (let i = inputIndex + 2; i < newOriginalIndices.length; i++) {
			newOriginalIndices[i] = this.originalIndices[i - 1];
		}

		return IntervalExchangeInduction.fromLengthsPermutationAndMetadata(newLengths, newPermutation, newApplyCounts, newOriginalIndices);
	}
}
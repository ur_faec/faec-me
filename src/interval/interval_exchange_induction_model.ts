

import { RenderTarget } from "core/canvas";
import { ElementsView } from "core/elements_view";
import { Renderable, Polygon } from "core/element_render";
import { Observable } from "core/observable";
import { AxisRect } from "core/polygon";

import { Interval } from "./interval";
import { IntervalExchangeMap, IntervalExchangeInduction } from "./interval_exchange";

const _colors = ["#F9828E", "#D77CA1", "#AA7CA9", "#787BA4", "#477792", "#206F77", "#166458", "#24563A", "#2F4823", "#353813", "#352A0B", "#301C09"];

function _colorForIndex(index: number): string {
	if (index >= 0 && index < _colors.length) {
		return _colors[index];
	}
	return "gray";
}

export class IntervalExchangeInductionModel extends ElementsView {

	intervalExchangeMap: Observable<IntervalExchangeMap>;
	_heightRatio: number;
	_rowHeight: number;
	_inductionCount: number;
	_inductionScale: number;

	constructor(renderTarget: RenderTarget, intervalExchangeMap: Observable<IntervalExchangeMap>) {
		super(renderTarget);
		this._heightRatio = 1.0;
		this._rowHeight = 0;
		this._inductionCount = 12;
		this._inductionScale = 0;
		this.intervalExchangeMap = intervalExchangeMap;
		this.renderer.backgroundColor = "lightgray";
		this.setNeedsRedraw();
		intervalExchangeMap.addObserver((oldValue: IntervalExchangeMap, newValue: IntervalExchangeMap) => {
			this.setNeedsRedraw();
		});
	}

	get inductionCount(): number {
		return this._inductionCount;
	}

	set inductionCount(inductionCount: number) {
		this._inductionCount = inductionCount;
		this.setNeedsRedraw();
	}

	get inductionScale(): number {
		return this._inductionScale;
	}

	// Ranges from 0 (scale by actual length) to 1 (scale everything to uniform).
	set inductionScale(inductionScale: number) {
		this._inductionScale = inductionScale;
		this.setNeedsRedraw();
	}

	elementsForMapInBounds(map: IntervalExchangeInduction, renderBounds: AxisRect): Array<Renderable> {
		const elements: Array<Renderable> = [];
		const domain = map.input.bounds();
		const domainLeft = domain.leftBoundary.toNumber();
		const domainLength = domain.length.toNumber();
		const renderLeft = renderBounds.origin.x;
		const renderLength = renderBounds.size.width;
		const rowHeight = renderBounds.size.height / 2;
		const rectForInterval = (interval: Interval, row: number): AxisRect => {
			const leftPos = interval.leftBoundary.toNumber();
			const length = interval.length.toNumber();
			const leftCoeff = (leftPos - domainLeft) / domainLength;
			const lengthCoeff = length / domainLength;
			return AxisRect.fromXYWH(renderLeft + leftCoeff * renderLength, renderBounds.origin.y + row * rowHeight, lengthCoeff * renderLength, rowHeight);
		};

		const inputIntervals = map.input.intervals;
		for (let i = 0; i < inputIntervals.length; i++) {
			const interval = inputIntervals[i];
			const rect = rectForInterval(interval, 0);
			const poly = Polygon.fromRect(rect);
			poly.style.strokeStyle = "black";
			poly.style.fillStyle = _colorForIndex(map.originalIndices[i]);
			elements.push(poly);
		}
		const outputIntervals = map.output.intervals;
		for (let i = 0; i < outputIntervals.length; i++) {
			const interval = outputIntervals[i];
			const rect = rectForInterval(interval, 1);
			const poly = Polygon.fromRect(rect);
			poly.style.strokeStyle = "black";
			const inputIndex = map.permutation.inverseMap[i];
			poly.style.fillStyle = _colorForIndex(map.originalIndices[inputIndex]);
			elements.push(poly);
		}
		return elements;
	}

	elementsToRender(frameRect: AxisRect): Array<Renderable> {
		let elements: Array<Renderable> = [];
		let map: IntervalExchangeInduction = this.intervalExchangeMap.value.asInduction();
		const originalLength = map.input.bounds().length.toNumber();

		const rowHeight = frameRect.size.height / this._inductionCount;
		for (let i = 0; i < this._inductionCount; i++) {
			const length = map.input.bounds().length.toNumber();
			const widthCoeff = length / originalLength;
			const scaledWidthCoeff = (1 - this.inductionScale) * widthCoeff + this.inductionScale;
			const rect = AxisRect.fromXYWH(frameRect.origin.x, frameRect.origin.y + i * rowHeight, scaledWidthCoeff * frameRect.size.width, rowHeight);
			const inductionElements = this.elementsForMapInBounds(map, rect.addMargin(-rowHeight * 0.05));
			elements = elements.concat(inductionElements);
			const induction: IntervalExchangeInduction | null | undefined = map.recurse();
			if (induction == null) {
				break;
			}
			map = induction;
		}
		return elements;
	}
}
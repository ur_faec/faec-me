

import { Rational } from "core/rational";

export class Interval {

	leftBoundary: Rational;
	rightBoundary: Rational;
	length: Rational;

	constructor(leftBoundary: Rational, rightBoundary: Rational) {
		if (leftBoundary.atLeast(rightBoundary)) {
			throw "Intervals must have positive length";
		}
		this.leftBoundary = leftBoundary;
		this.rightBoundary = rightBoundary;
		this.length = rightBoundary.minus(leftBoundary);
	}

	toString(): string {
		return `[${String(this.leftBoundary)}, ${String(this.rightBoundary)})`;
	}

	containsPosition(pos: Rational): boolean {
		return (pos.atLeast(this.leftBoundary) && pos.lessThan(this.rightBoundary));
	}

	center(): Rational {
		return this.leftBoundary.plus(this.rightBoundary).dividedBy(Rational.fromInt(2));
	}
}

export class DisjointIntervals {
	// Invariants: disjoint, in sorted order.
	intervals: Array<Interval>;

	constructor(sortedIntervals: Array<Interval>) {
		this.intervals = sortedIntervals;
	}

	static fromIntervals(intervals: Array<Interval>): DisjointIntervals {
		// Map the input through the identity function, because otherwise sort
		// will reorder the caller's array.
		const sortedIntervals = intervals.map(i => i).sort((a, b) => a.leftBoundary.compare(b.leftBoundary));
		return new DisjointIntervals(sortedIntervals);
	}

	static fromLengths(lengths: Array<Rational>): DisjointIntervals {
		let pos = Rational.zero();
		const intervals: Array<Interval> = [];
		for (const length of lengths) {
			const rightBoundary = pos.plus(length);
			intervals.push(new Interval(pos, rightBoundary));
			pos = rightBoundary;
		}
		return new DisjointIntervals(intervals);
	}

	toString(): string {
		return `{${this.intervals.join(', ')}}`;
	}

	indexForPosition(pos: Rational): number | null | undefined {
		const intervals = this.intervals;
		for (let i = 0; i < intervals.length; i++) {
			if (intervals[i].containsPosition(pos)) {
				return i;
			}
		}
		return null;
	}

	get firstInterval(): Interval {
		return this.intervals[0];
	}

	get lastInterval(): Interval {
		return this.intervals[this.intervals.length - 1];
	}

	bounds(): Interval {
		return new Interval(this.firstInterval.leftBoundary, this.lastInterval.rightBoundary);
	}
}